-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Янв 26 2018 г., 06:11
-- Версия сервера: 5.5.27
-- Версия PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `mahalla`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_aliment`
--

CREATE TABLE IF NOT EXISTS `tbl_aliment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomi` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `tbl_aliment`
--

INSERT INTO `tbl_aliment` (`id`, `nomi`, `user_id`, `user_date`) VALUES
(1, 'йўқ', NULL, NULL),
(2, 'бор, алимент беради', NULL, NULL),
(3, 'бор, алимент олади', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_bino`
--

CREATE TABLE IF NOT EXISTS `tbl_bino` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `xolati` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `tbl_bino`
--

INSERT INTO `tbl_bino` (`id`, `xolati`, `user_id`, `user_date`) VALUES
(1, 'яхши', NULL, NULL),
(2, 'ўртача', NULL, NULL),
(3, 'ёмон', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_bogchalar`
--

CREATE TABLE IF NOT EXISTS `tbl_bogchalar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bogcha_raqami` varchar(255) DEFAULT NULL,
  `bogcha_nomi` varchar(255) DEFAULT NULL,
  `manzili` varchar(255) DEFAULT NULL,
  `bogcha_direktor` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Дамп данных таблицы `tbl_bogchalar`
--

INSERT INTO `tbl_bogchalar` (`id`, `bogcha_raqami`, `bogcha_nomi`, `manzili`, `bogcha_direktor`, `user_id`, `user_date`) VALUES
(1, 'яқин атрофда боғча йўқ', NULL, NULL, NULL, NULL, NULL),
(2, '1-боғча', NULL, NULL, NULL, NULL, NULL),
(3, '2-боғча', NULL, NULL, NULL, NULL, NULL),
(4, '3-боғча', NULL, NULL, NULL, NULL, NULL),
(5, '4-боғча', NULL, NULL, NULL, NULL, NULL),
(6, '5-боғча', NULL, NULL, NULL, NULL, NULL),
(7, '6-боғча', NULL, NULL, NULL, NULL, NULL),
(8, '7-боғча', NULL, NULL, NULL, NULL, NULL),
(9, '8-боғча', NULL, NULL, NULL, NULL, NULL),
(10, '9-боғча', NULL, NULL, NULL, NULL, NULL),
(11, '11-боғча', NULL, NULL, NULL, NULL, NULL),
(12, '12-боғча', NULL, NULL, NULL, NULL, NULL),
(13, '13-боғча', NULL, NULL, NULL, NULL, NULL),
(14, '14-боғча', NULL, NULL, NULL, NULL, NULL),
(15, '15-боғча', NULL, NULL, NULL, NULL, NULL),
(16, '16-боғча', NULL, NULL, NULL, NULL, NULL),
(17, '17-боғча', NULL, NULL, NULL, NULL, NULL),
(18, '18-боғча', NULL, NULL, NULL, NULL, NULL),
(19, '19-боғча', NULL, NULL, NULL, NULL, NULL),
(20, '20-боғча', NULL, NULL, NULL, NULL, NULL),
(21, '21-боғча', NULL, NULL, NULL, NULL, NULL),
(22, '22-боғча', NULL, NULL, NULL, NULL, NULL),
(23, '23-боғча', NULL, NULL, NULL, NULL, NULL),
(24, '24-боғча', NULL, NULL, NULL, NULL, NULL),
(25, '25-боғча', NULL, NULL, NULL, NULL, NULL),
(26, '26-боғча', NULL, NULL, NULL, NULL, NULL),
(27, '27-боғча', NULL, NULL, NULL, NULL, NULL),
(28, '28-боғча', NULL, NULL, NULL, NULL, NULL),
(29, '29-боғча', NULL, NULL, NULL, NULL, NULL),
(30, '30-боғча', NULL, NULL, NULL, NULL, NULL),
(31, '31-боғча', NULL, NULL, NULL, NULL, NULL),
(32, '32-боғча', NULL, NULL, NULL, NULL, NULL),
(33, '33-боғча', NULL, NULL, NULL, NULL, NULL),
(34, '34-боғча', NULL, NULL, NULL, NULL, NULL),
(35, '35-боғча', NULL, NULL, NULL, NULL, NULL),
(36, '36-боғча', NULL, NULL, NULL, NULL, NULL),
(37, '37-боғча', NULL, NULL, NULL, NULL, NULL),
(38, '38-боғча', NULL, NULL, NULL, NULL, NULL),
(39, '39-боғча', NULL, NULL, NULL, NULL, NULL),
(40, '40-боғча', NULL, NULL, NULL, NULL, NULL),
(41, 'яқин атрофдаги боғча кўрсатилмаган', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_bolim_iib`
--

CREATE TABLE IF NOT EXISTS `tbl_bolim_iib` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `viloyat_id` int(11) DEFAULT NULL COMMENT 'Вилоят',
  `bolim_iib` varchar(255) DEFAULT NULL COMMENT 'Бўлим номи',
  PRIMARY KEY (`id`),
  KEY `viloyat_id` (`viloyat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ИИБлар' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_daromad`
--

CREATE TABLE IF NOT EXISTS `tbl_daromad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kam_taminlangan_sifatida` varchar(255) DEFAULT NULL,
  `oylik_ortacha_daromadi` varchar(255) DEFAULT NULL,
  `ish_xaq_oluvchi_soni` int(11) DEFAULT NULL,
  `ish_haqi_summasi` varchar(255) DEFAULT NULL,
  `ish_xaqi_muddati` varchar(255) DEFAULT NULL,
  `pensiya_oluvchi_soni` int(11) DEFAULT NULL,
  `pensiya_summa` varchar(255) DEFAULT NULL,
  `pensiya_vaqti` varchar(255) DEFAULT NULL,
  `nogiron_nafaqa_soni` int(11) DEFAULT NULL,
  `nogiron_summasi` varchar(255) DEFAULT NULL,
  `nogiron_nafaqa_vaqti` int(11) DEFAULT NULL,
  `kamtaminlangan_nafaqachilar_soni` int(11) DEFAULT NULL,
  `kamtaminlangan_nafaqa_summasi` varchar(255) DEFAULT NULL,
  `kamtaminlangan_nafaqa_vaqti` varchar(255) DEFAULT NULL,
  `ikki_yoshgacha_nafaqa_soni` int(11) DEFAULT NULL,
  `ikki_yosh_nafaqa_summasi` varchar(255) DEFAULT NULL,
  `ikki_yosh_nafaqa_vaqti` varchar(255) DEFAULT NULL,
  `on_tort_yosh_nafaqa_soni` int(11) DEFAULT NULL,
  `on_tort_nafaqa_summasi` varchar(255) DEFAULT NULL,
  `on_tort_nafaqa_vaqti` varchar(255) DEFAULT NULL,
  `daromad_boshqa` varchar(255) DEFAULT NULL,
  `fuqaro_uy_id` int(11) DEFAULT NULL,
  `fuqaro_id` int(11) DEFAULT NULL,
  `anketa_ozgarish` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fuqaro_uy_id` (`fuqaro_uy_id`),
  KEY `fuqaro_id` (`fuqaro_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_daromad`
--

INSERT INTO `tbl_daromad` (`id`, `kam_taminlangan_sifatida`, `oylik_ortacha_daromadi`, `ish_xaq_oluvchi_soni`, `ish_haqi_summasi`, `ish_xaqi_muddati`, `pensiya_oluvchi_soni`, `pensiya_summa`, `pensiya_vaqti`, `nogiron_nafaqa_soni`, `nogiron_summasi`, `nogiron_nafaqa_vaqti`, `kamtaminlangan_nafaqachilar_soni`, `kamtaminlangan_nafaqa_summasi`, `kamtaminlangan_nafaqa_vaqti`, `ikki_yoshgacha_nafaqa_soni`, `ikki_yosh_nafaqa_summasi`, `ikki_yosh_nafaqa_vaqti`, `on_tort_yosh_nafaqa_soni`, `on_tort_nafaqa_summasi`, `on_tort_nafaqa_vaqti`, `daromad_boshqa`, `fuqaro_uy_id`, `fuqaro_id`, `anketa_ozgarish`, `user_id`, `user_date`) VALUES
(1, '2', '', NULL, '', '', NULL, '', '', NULL, '', NULL, NULL, '', '', NULL, '', '', NULL, '', '', '', 11, NULL, NULL, 15, '2018-01-09');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_daromad_turi`
--

CREATE TABLE IF NOT EXISTS `tbl_daromad_turi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomi` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `tbl_daromad_turi`
--

INSERT INTO `tbl_daromad_turi` (`id`, `nomi`, `user_id`, `user_date`) VALUES
(1, 'ишсиз', NULL, NULL),
(2, 'ишлайди', NULL, NULL),
(3, 'пенсионер', NULL, NULL),
(4, '2 ёшгача болалар пули', NULL, NULL),
(5, '14 ёшгача болалар пули', NULL, NULL),
(6, 'Кам таъминланганлик нафақаси', NULL, NULL),
(7, 'Ногиронлик нафақаси', NULL, NULL),
(8, 'вояга етмаган', NULL, NULL),
(9, 'мавсумий ишчи', NULL, NULL),
(10, 'талаба', NULL, NULL),
(11, 'ёлғиз пенсионер', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_davlat`
--

CREATE TABLE IF NOT EXISTS `tbl_davlat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `davlat_nomi` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `tbl_davlat`
--

INSERT INTO `tbl_davlat` (`id`, `davlat_nomi`, `user_id`, `user_date`) VALUES
(1, 'Ўзбекистон Республикаси', NULL, NULL),
(2, 'Қирғизистон Республикаси', NULL, NULL),
(3, 'Қозоғистон Республикаси', NULL, NULL),
(4, 'Тожикистон Республикаси', NULL, NULL),
(5, 'Афғонистон Республикаси', NULL, NULL),
(6, 'Туркманистон Республикаси', NULL, NULL),
(7, 'Россия Федератив Республикаси', NULL, NULL),
(8, 'Озарбайжон Республикаси', NULL, NULL),
(9, 'Туркия', NULL, NULL),
(10, 'Украина', NULL, NULL),
(11, 'Руминия', NULL, NULL),
(12, 'Полша', NULL, NULL),
(13, 'Малазия', NULL, NULL),
(14, 'Ироқ', NULL, NULL),
(15, 'Исроил', NULL, NULL),
(16, 'АҚШ', NULL, NULL),
(17, 'Италия', NULL, NULL),
(18, 'Венгирия', NULL, NULL),
(19, 'Япония', NULL, NULL),
(20, 'Хитой', NULL, NULL),
(21, 'Жанубий Корея', NULL, NULL),
(22, 'Шимолий Корея', NULL, NULL),
(23, 'Монголия', NULL, NULL),
(24, 'Бошқа давлат', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_deo`
--

CREATE TABLE IF NOT EXISTS `tbl_deo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deo_xolati` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `tbl_deo`
--

INSERT INTO `tbl_deo` (`id`, `deo_xolati`, `user_id`, `user_date`) VALUES
(1, 'ДЭО аъзоси эмас', NULL, NULL),
(2, 'ДЭО аъзоси', NULL, NULL),
(3, 'ДЭО фарзанди', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_fozgarish`
--

CREATE TABLE IF NOT EXISTS `tbl_fozgarish` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fuy_id` int(11) DEFAULT NULL COMMENT 'Хонадоннинг ID рақами',
  `fuq_id` int(11) DEFAULT NULL COMMENT 'Фуқаронинг ID рақами',
  `fuq_maydon_nomi` varchar(255) DEFAULT NULL COMMENT 'Фуқаронинг маълумотларидаги майдонлардан ўзгарган майдонинг номи',
  `fuq_maydon_ozgaruvchi` varchar(255) DEFAULT NULL COMMENT 'Фуқаронинг аввалги киритилган маълумоти',
  `user_id` int(11) DEFAULT NULL COMMENT 'Фойдаалнувчи номи',
  `user_date` datetime DEFAULT NULL COMMENT 'Фойдаланувчи ўзгартирган сана',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Fuqarodagi o''zgarishlar' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_fuqaro`
--

CREATE TABLE IF NOT EXISTS `tbl_fuqaro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fio` varchar(255) DEFAULT NULL,
  `jinsi_id` int(11) DEFAULT NULL,
  `millati_id` int(11) DEFAULT NULL,
  `tugilgan_sanasi` date DEFAULT NULL,
  `rasm` varchar(255) DEFAULT NULL,
  `davlat_id` int(11) DEFAULT NULL,
  `tugilgan_joy_viloyat_id` int(11) DEFAULT NULL,
  `tugilgan_joy_tuman_id` int(11) NOT NULL DEFAULT '29',
  `passport_seyiya` varchar(11) DEFAULT NULL,
  `passport_raqami` varchar(255) DEFAULT NULL,
  `passport_vaqti` varchar(255) DEFAULT NULL,
  `passport_kim_tomonidan_id` int(11) DEFAULT NULL,
  `metirka_raqami` varchar(255) DEFAULT NULL,
  `metirka_kim_tomonidan` varchar(255) DEFAULT NULL,
  `nikox_guvohnom_raqami` varchar(255) DEFAULT NULL,
  `nikox_turi` varchar(255) DEFAULT NULL,
  `ish_joy_nomi` varchar(255) DEFAULT NULL,
  `lavozimi` varchar(255) DEFAULT NULL,
  `daromad_turi_id` int(11) DEFAULT NULL,
  `daromad_miqdori` varchar(255) DEFAULT NULL,
  `daromad_olish_sanasi` varchar(255) DEFAULT NULL,
  `uzoq_muddatga_ketgan_vaqti` date DEFAULT NULL,
  `uzoq_muddatga_kelgan_vaqti` date DEFAULT NULL,
  `uzoq_muddatga_ketgan_davlati` int(11) DEFAULT NULL,
  `uzoq_muddat_ish` varchar(255) DEFAULT NULL,
  `uzoq_muddat_suhbat_otgani` varchar(255) DEFAULT NULL,
  `malumoti_id` int(11) DEFAULT NULL,
  `bitirgan_joy_id` varchar(255) DEFAULT NULL,
  `bitirgan_mutaxasisligi` varchar(255) DEFAULT NULL,
  `bitirgan_yili` varchar(255) DEFAULT NULL,
  `aliment_id` int(11) DEFAULT NULL,
  `aliment_qarz` varchar(255) DEFAULT NULL,
  `notinch_oila_id` varchar(255) DEFAULT NULL,
  `sudlanganligi` longtext,
  `iib_xisobida_turadimi` int(11) DEFAULT NULL,
  `deo_azoligi_id` int(11) DEFAULT NULL,
  `fuqaro_uy_id` int(11) DEFAULT NULL,
  `kassalik_id` int(11) DEFAULT NULL,
  `kassalik_xolati` int(11) DEFAULT NULL,
  `sogligi` varchar(255) DEFAULT NULL,
  `oila_boshligi` tinyint(3) DEFAULT NULL,
  `anketa_ozgarish` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  `otasi_id` int(11) DEFAULT NULL,
  `onasi_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fuqaro_uy_id` (`fuqaro_uy_id`),
  KEY `bitirgan_joy_id` (`bitirgan_joy_id`),
  KEY `passport_kim_tomonidan_id` (`passport_kim_tomonidan_id`),
  KEY `jinsi_id` (`jinsi_id`),
  KEY `tugilgan_joy_viloyat_id` (`tugilgan_joy_viloyat_id`),
  KEY `tugilgan_joy_tuman_id` (`tugilgan_joy_tuman_id`),
  KEY `malumoti_id` (`malumoti_id`),
  KEY `tbl_fuqaro_ibfk_9` (`aliment_id`),
  KEY `iib_xisobida_turadimi` (`iib_xisobida_turadimi`),
  KEY `millati_id` (`millati_id`),
  KEY `davlat_id` (`davlat_id`),
  KEY `uzoq_muddatga_ketgan_davlati` (`uzoq_muddatga_ketgan_davlati`),
  KEY `passport_seyiya_id` (`passport_seyiya`),
  KEY `kassalik_id` (`kassalik_id`),
  KEY `daromad_turi_id` (`daromad_turi_id`),
  KEY `user_id` (`user_id`),
  KEY `otasi_id` (`otasi_id`),
  KEY `onasi_id` (`onasi_id`),
  KEY `deo_azoligi_id` (`deo_azoligi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_fuqaro_uy`
--

CREATE TABLE IF NOT EXISTS `tbl_fuqaro_uy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fviloyat_id` int(11) DEFAULT NULL,
  `ftuman_id` int(11) DEFAULT NULL,
  `mfy_id` int(11) DEFAULT NULL,
  `kocha_id` int(11) DEFAULT NULL,
  `uy_raqami` int(11) DEFAULT NULL,
  `uy_raqami_position` varchar(255) DEFAULT NULL,
  `kadastr_mavjudligi` varchar(255) DEFAULT NULL,
  `kadastr_seriyasi_raqami` varchar(255) DEFAULT NULL,
  `kadastr_buyruq_raqami` varchar(255) DEFAULT NULL,
  `ijara` varchar(255) DEFAULT NULL,
  `azo_soni` int(11) DEFAULT NULL,
  `jon_soni` int(11) DEFAULT NULL,
  `voya_yetmagan_soni` int(11) DEFAULT NULL,
  `taklif` varchar(255) DEFAULT NULL,
  `muammosi` varchar(255) DEFAULT NULL,
  `oila_xolati` int(11) DEFAULT NULL,
  `ishchi_guruh_fio_id` int(11) DEFAULT NULL,
  `ishchi_guruh_fio` varchar(255) DEFAULT NULL,
  `tel_raqami` varchar(255) DEFAULT NULL,
  `anketa_sana` date DEFAULT NULL,
  `anketa_ozgarish` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mfy_id` (`mfy_id`),
  KEY `ishchi_guruh_fio_id` (`ishchi_guruh_fio_id`),
  KEY `fviloyat_id` (`fviloyat_id`),
  KEY `ftuman_id` (`ftuman_id`),
  KEY `kocha_id` (`kocha_id`),
  KEY `oila_xolati` (`oila_xolati`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `tbl_fuqaro_uy`
--

INSERT INTO `tbl_fuqaro_uy` (`id`, `fviloyat_id`, `ftuman_id`, `mfy_id`, `kocha_id`, `uy_raqami`, `uy_raqami_position`, `kadastr_mavjudligi`, `kadastr_seriyasi_raqami`, `kadastr_buyruq_raqami`, `ijara`, `azo_soni`, `jon_soni`, `voya_yetmagan_soni`, `taklif`, `muammosi`, `oila_xolati`, `ishchi_guruh_fio_id`, `ishchi_guruh_fio`, `tel_raqami`, `anketa_sana`, `anketa_ozgarish`, `user_id`, `user_date`) VALUES
(1, 2, 29, 35, 87, NULL, '', 'Yo''q', '', '', '', 6, NULL, 2, '', '', 1, 1, '', '', '2017-09-21', '2017-10-28', 14, '2017-10-28'),
(2, 2, 29, 35, 87, NULL, '', 'Yo''q', '', '', '', 6, NULL, 2, '', NULL, 1, 1, NULL, '', '2017-09-21', NULL, 1, '2017-09-21'),
(3, 2, 29, 23, 31, NULL, '', 'Yo''q', '', '', '', 6, NULL, 2, '', NULL, 1, 1, NULL, '', '2017-09-21', NULL, 1, '2017-09-21'),
(4, 2, 29, 35, 87, NULL, '', 'Yo''q', '', '', '', 4, NULL, 0, '', NULL, 1, 1, NULL, '998911696915', '2017-09-22', NULL, 1, '2017-09-22'),
(5, 2, 29, 35, 30, 34, 'a', 'Yo''q', '', '', '', 4, NULL, 0, '', NULL, 1, 16, '16', '', '2017-09-28', NULL, 1, '2017-09-28'),
(6, 2, 29, 35, 87, 34, '', 'Yo''q', '', '', '', 4, NULL, 0, '', NULL, 1, 9, '', '', '2017-09-28', '2017-11-28', 14, '2017-11-28'),
(7, 2, 29, 35, 72, 75, '', 'Yo''q', '', '', '', 4, NULL, 1, '', NULL, 1, 9, '', '912878920', '2017-09-28', '2017-10-28', 14, '2017-10-28'),
(8, 2, 29, 40, 4, NULL, '', 'Bor', '', '', '', 1, NULL, 0, '', NULL, 1, 7, '7', '', '2017-10-25', NULL, 15, '2017-10-25'),
(9, 2, 29, 8, 145, NULL, '', 'Bor', '', '', '', 4, NULL, 1, '', NULL, 1, 7, '7', '', '2017-11-15', NULL, 14, '2017-11-15'),
(10, 2, 29, 35, 74, NULL, '', 'Bor', '', '', '', 4, NULL, 0, '', NULL, 1, 11, '11', '', '2017-11-28', NULL, 14, '2017-11-28'),
(11, 2, 29, 40, 4, NULL, '', 'Yo''q', '', '', '', 4, NULL, 0, '', NULL, 1, 14, '14', '', '2018-01-09', NULL, 15, '2018-01-09');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_gaz`
--

CREATE TABLE IF NOT EXISTS `tbl_gaz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gaz_turi` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `tbl_gaz`
--

INSERT INTO `tbl_gaz` (`id`, `gaz_turi`, `user_id`, `user_date`) VALUES
(1, 'газ тармоғига уланмаган', NULL, NULL),
(2, 'газ баллони(пропан) бор', NULL, NULL),
(3, 'газ тармоғига уланган', NULL, NULL),
(4, 'газ балонни мавжуд эмас', NULL, NULL),
(5, 'табиий газга тармоғига уланган лекин газ йўқ', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_hayvon`
--

CREATE TABLE IF NOT EXISTS `tbl_hayvon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hayvon_soni` int(11) DEFAULT NULL,
  `qora_mol` int(11) DEFAULT NULL,
  `qoy` int(11) DEFAULT NULL,
  `echki` int(11) DEFAULT NULL,
  `ot` int(11) DEFAULT NULL,
  `kuchuk` int(11) DEFAULT NULL,
  `mushuk` int(11) DEFAULT NULL,
  `quyon` int(11) DEFAULT NULL,
  `tovuq` int(11) DEFAULT NULL,
  `kurka` int(11) DEFAULT NULL,
  `ordak` int(11) DEFAULT NULL,
  `goz` int(11) DEFAULT NULL,
  `boshqalar` varchar(255) DEFAULT NULL,
  `tbl_fuqaro_uy_id` int(11) DEFAULT NULL,
  `anketa_ozgarish` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_fuqaro_uy_id` (`tbl_fuqaro_uy_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_hayvon`
--

INSERT INTO `tbl_hayvon` (`id`, `hayvon_soni`, `qora_mol`, `qoy`, `echki`, `ot`, `kuchuk`, `mushuk`, `quyon`, `tovuq`, `kurka`, `ordak`, `goz`, `boshqalar`, `tbl_fuqaro_uy_id`, `anketa_ozgarish`, `user_id`, `user_date`) VALUES
(1, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 11, NULL, 15, '2018-01-09');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_iib`
--

CREATE TABLE IF NOT EXISTS `tbl_iib` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iib_xisobi` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `tbl_iib`
--

INSERT INTO `tbl_iib` (`id`, `iib_xisobi`, `user_id`, `user_date`) VALUES
(1, 'ИИБ хисобида турмайди', NULL, NULL),
(4, 'Ўзжонига қасд қилган', NULL, NULL),
(5, 'Профилактик хисобда туради', NULL, NULL),
(7, 'жиноят содир этган аёллар хисобида туради', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_ijtimoiy_muhit`
--

CREATE TABLE IF NOT EXISTS `tbl_ijtimoiy_muhit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `obodonchilik` varchar(255) DEFAULT NULL,
  `hammom` varchar(255) DEFAULT NULL,
  `oshxona` varchar(255) DEFAULT NULL,
  `tandir` varchar(255) DEFAULT NULL,
  `bino_xolati_id` int(11) DEFAULT NULL,
  `elektr_hisoblagich_mavjudligi` varchar(255) DEFAULT NULL,
  `elektr_hisoblagich_raqami` varchar(255) DEFAULT NULL,
  `gaz_mavjudligi_id` int(11) DEFAULT NULL,
  `gaz_ballon_soni` varchar(255) DEFAULT NULL,
  `gaz_hisoblagichi_raqami` varchar(255) DEFAULT NULL,
  `gaz_hisoblagich_mavjudligii` varchar(255) DEFAULT NULL,
  `ichimlik_suvi_mavjud_hisoblagich_mavjudligi` varchar(255) DEFAULT NULL,
  `ichimlik_suvi_mavjud_masofa` varchar(255) DEFAULT NULL,
  `tbl_fuqaro_uy_id` int(11) DEFAULT NULL,
  `anketa_ozgarish` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_fuqaro_uy_id` (`tbl_fuqaro_uy_id`),
  KEY `gaz_mavjudligi_id` (`gaz_mavjudligi_id`),
  KEY `bino_xolati_id` (`bino_xolati_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_ijtimoiy_muhit`
--

INSERT INTO `tbl_ijtimoiy_muhit` (`id`, `obodonchilik`, `hammom`, `oshxona`, `tandir`, `bino_xolati_id`, `elektr_hisoblagich_mavjudligi`, `elektr_hisoblagich_raqami`, `gaz_mavjudligi_id`, `gaz_ballon_soni`, `gaz_hisoblagichi_raqami`, `gaz_hisoblagich_mavjudligii`, `ichimlik_suvi_mavjud_hisoblagich_mavjudligi`, `ichimlik_suvi_mavjud_masofa`, `tbl_fuqaro_uy_id`, `anketa_ozgarish`, `user_id`, `user_date`) VALUES
(1, '1', '1', '1', '1', 1, '1', '', 1, '', '', '2', '2', '', 11, NULL, 15, '2018-01-09');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_inspektor`
--

CREATE TABLE IF NOT EXISTS `tbl_inspektor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `viloyat_id` int(11) DEFAULT NULL COMMENT 'Вилоят',
  `tuman_id` int(11) DEFAULT NULL COMMENT 'Туман',
  `sektor_id` int(11) DEFAULT NULL COMMENT 'Сектор рақами',
  `mfy_id` int(11) DEFAULT NULL COMMENT 'МФЙ',
  `bolim_iib_id` int(11) DEFAULT NULL COMMENT 'Бўлим номи',
  `lavozimi` varchar(255) DEFAULT NULL COMMENT 'Лавозими',
  `fio` varchar(255) DEFAULT NULL COMMENT 'Ф.И.Ш',
  `unvoni` varchar(255) DEFAULT NULL COMMENT 'Унвони',
  `tel_raqami` varchar(255) DEFAULT NULL COMMENT 'Телефон рақами',
  PRIMARY KEY (`id`),
  KEY `viloyat_id` (`viloyat_id`),
  KEY `tuman_id` (`tuman_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Профилактика инспеткорлар' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_inspektor_mfy`
--

CREATE TABLE IF NOT EXISTS `tbl_inspektor_mfy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `viloyat_id` int(11) DEFAULT NULL COMMENT 'Вилоят',
  `tuman_id` int(11) DEFAULT NULL COMMENT 'Туман',
  `mfy_id` int(11) DEFAULT NULL COMMENT 'МФЙ',
  `inspektor_id` int(11) DEFAULT NULL COMMENT 'Профилактика инспектори Ф.И.Ш.',
  PRIMARY KEY (`id`),
  KEY `viloyat_id` (`viloyat_id`),
  KEY `tuman_id` (`tuman_id`),
  KEY `mfy_id` (`mfy_id`),
  KEY `inspektor_id` (`inspektor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Профилактика инспекторларининг МФЙлари' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_ishchi_guruh_fio`
--

CREATE TABLE IF NOT EXISTS `tbl_ishchi_guruh_fio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `viloyat_id` int(11) DEFAULT NULL,
  `tuman_id` int(11) DEFAULT NULL,
  `ishchi_guruh_fio` varchar(255) DEFAULT NULL,
  `tashkilot_nomi` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `viloyat_id` (`viloyat_id`),
  KEY `tuman_id` (`tuman_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `tbl_ishchi_guruh_fio`
--

INSERT INTO `tbl_ishchi_guruh_fio` (`id`, `viloyat_id`, `tuman_id`, `ishchi_guruh_fio`, `tashkilot_nomi`, `user_id`, `user_date`) VALUES
(1, 2, 29, 'Ғазначилик', 'Ғазначилик', 1, NULL),
(2, 2, 29, 'Пенсия жамғармаси', 'Пенсия жамғармаси', NULL, NULL),
(3, 2, 29, 'Молия бўлими', 'Молия бўлими', NULL, NULL),
(4, 2, 29, 'Статистика', 'Статистика', NULL, NULL),
(5, 2, 29, 'Менхнат', 'Менхнат', NULL, NULL),
(6, 2, 29, 'ҚҚБ', 'ҚҚБ', NULL, NULL),
(7, 2, 29, 'Хамкорбанк', 'Хамкорбанк', NULL, NULL),
(8, 2, 29, 'Халк банк', 'Халк банк', NULL, NULL),
(9, 2, 29, 'Агро банк', 'Агро банк', NULL, NULL),
(10, 2, 29, 'Почта', 'Почта', NULL, NULL),
(11, 2, 29, 'Махалла фонд', 'Махалла фонд', NULL, NULL),
(12, 2, 29, 'Тиббиёт КХК', 'Тиббиёт КХК', NULL, NULL),
(13, 2, 29, 'Кишлок хўжалиги КХК', 'Кишлок хўжалиги КХК', NULL, NULL),
(14, 2, 29, 'Транпорт КХК', 'Транпорт КХК', NULL, NULL),
(15, 2, 29, 'Педагогика КХК', 'Педагогика КХК', NULL, NULL),
(16, 2, 29, 'ДСИ', 'ДСИ', NULL, NULL),
(17, 2, 29, 'ДСЭНМ', 'ДСЭНМ', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_issiqxona`
--

CREATE TABLE IF NOT EXISTS `tbl_issiqxona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issiqxona_turi` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `tbl_issiqxona`
--

INSERT INTO `tbl_issiqxona` (`id`, `issiqxona_turi`, `user_id`, `user_date`) VALUES
(1, 'иссиқхона мавжуд эмас', NULL, NULL),
(2, 'кредит хисобидан', NULL, NULL),
(3, 'ўз хисобидан', NULL, NULL),
(4, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_jinsi`
--

CREATE TABLE IF NOT EXISTS `tbl_jinsi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jins` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tbl_jinsi`
--

INSERT INTO `tbl_jinsi` (`id`, `jins`) VALUES
(1, 'эркак'),
(2, 'аёл');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_kassalik`
--

CREATE TABLE IF NOT EXISTS `tbl_kassalik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kassalik_nomi` varchar(255) DEFAULT NULL,
  `kassalik_xolati` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `tbl_kassalik`
--

INSERT INTO `tbl_kassalik` (`id`, `kassalik_nomi`, `kassalik_xolati`, `user_id`, `user_date`) VALUES
(7, 'соғлом', NULL, NULL, NULL),
(8, '1-гурух ногирони', NULL, NULL, NULL),
(9, '2-гурух ногирони', NULL, NULL, NULL),
(10, '3-гурух ногирони', NULL, NULL, NULL),
(11, '4-гурух ногирони', NULL, NULL, NULL),
(15, 'касал', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_kochalar`
--

CREATE TABLE IF NOT EXISTS `tbl_kochalar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_kocha_nomi` varchar(255) DEFAULT NULL,
  `tbl_mfy_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  `viloyat_id` int(11) DEFAULT NULL,
  `tuman_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_mfy_id` (`tbl_mfy_id`),
  KEY `user_id` (`user_id`),
  KEY `viloyat_id` (`viloyat_id`),
  KEY `tuman_id` (`tuman_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=184 ;

--
-- Дамп данных таблицы `tbl_kochalar`
--

INSERT INTO `tbl_kochalar` (`id`, `tbl_kocha_nomi`, `tbl_mfy_id`, `user_id`, `user_date`, `viloyat_id`, `tuman_id`) VALUES
(1, 'Кўпирикбоши кўчаси', NULL, 1, '2017-12-01', 2, 29),
(2, 'Саховат кўчаси', 40, NULL, '2017-12-08', 2, 29),
(3, 'Хусункор кўчаси', 40, NULL, NULL, 2, 29),
(4, 'Кўчак кўчаси', 40, NULL, NULL, 2, 29),
(5, 'Тарақиёт кўчаси', 1, NULL, NULL, 2, 29),
(6, 'Юксак манавият кўчаси', 1, NULL, NULL, 2, 29),
(7, 'Чалака кўчаси', 1, NULL, NULL, 2, 29),
(8, 'Юзлар кўчаси', 1, NULL, NULL, 2, 29),
(9, 'Обод турмуш кўчаси', 1, NULL, NULL, 2, 29),
(10, 'Андижон кўчаси', 19, NULL, NULL, 2, 29),
(11, 'Ат-Термизий кўчаси', 19, NULL, NULL, 2, 29),
(12, 'Толбулоқ кўчаси', 19, NULL, NULL, 2, 29),
(13, 'Собир Рахимов кўчаси', 19, NULL, NULL, 2, 29),
(14, 'Фаравон кўчаси', 20, NULL, NULL, 2, 29),
(15, 'Пахтакор кўчаси', 20, NULL, NULL, 2, 29),
(16, 'Гулзор  кўчаси', 20, NULL, NULL, 2, 29),
(17, 'Нурбулоқ кўчаси ', 21, NULL, NULL, 2, 29),
(18, 'Шарқона кўчаси', 21, NULL, NULL, 2, 29),
(19, 'Само кўчаси', 21, NULL, NULL, 2, 29),
(20, 'Гулобод кўчаси', 21, NULL, NULL, 2, 29),
(21, 'Олима кўчаси', 21, NULL, NULL, 2, 29),
(22, 'Муборак кўчаси', 21, NULL, NULL, 2, 29),
(23, 'Ибн Сино кўчаси', 22, NULL, NULL, 2, 29),
(24, 'Мустакилик кўчаси', 22, NULL, NULL, 2, 29),
(25, 'Ўзбекистон кўчаси', 22, NULL, NULL, 2, 29),
(26, 'Оромгох кўчаси', 22, NULL, NULL, 2, 29),
(27, 'Улуғвор кўчаси', 22, NULL, NULL, 2, 29),
(28, 'Пок ният кўчаси', 22, NULL, NULL, 2, 29),
(29, 'Бустон кўчаси', 23, NULL, NULL, 2, 29),
(30, 'Шодлик  кўчаси', 23, NULL, NULL, 2, 29),
(31, 'Бахор  кўчаси', 23, NULL, NULL, 2, 29),
(32, 'Навруз кўча', 23, NULL, NULL, 2, 29),
(33, 'Қайрағоч кўчаси', 23, NULL, NULL, 2, 29),
(34, 'Сокин кўчаси', 23, NULL, NULL, 2, 29),
(35, 'Қайрағоч кўчаси', 23, NULL, NULL, 2, 29),
(36, 'Уста кўчаси', 24, NULL, NULL, 2, 29),
(37, 'Оби хаёт кўчаси 1', 24, NULL, NULL, 2, 29),
(38, 'Новушлик кўчаси', 25, NULL, NULL, 2, 29),
(39, 'Карвон йўли кўчаси', 25, NULL, NULL, 2, 29),
(40, 'Нурафшон кўчаси', 25, NULL, NULL, 2, 29),
(41, 'Миллий юксалиш кўчаси', 26, NULL, NULL, 2, 29),
(42, 'Янги аср кўчаси', 26, NULL, NULL, 2, 29),
(43, 'Дурмон кўчаси', 26, NULL, NULL, 2, 29),
(44, 'Зарбулок Кўчаси', 26, NULL, NULL, 2, 29),
(45, 'Янги чек кўчаси', 27, NULL, NULL, 2, 29),
(46, 'Янги обод кўчаси', 27, NULL, NULL, 2, 29),
(47, 'Манғит кўчаси', 27, NULL, NULL, 2, 29),
(48, 'Чигит ўра кўчаси ', 27, NULL, NULL, 2, 29),
(49, 'Истиқлол кўчаси', 28, NULL, NULL, 2, 29),
(50, 'Тадбиркорлар кўчаси', 28, NULL, NULL, 2, 29),
(51, 'Юксалиш кўчаси', 28, NULL, NULL, 2, 29),
(52, 'Боймахала кўчаси', 29, NULL, NULL, 2, 29),
(53, 'Узумзор кўчаси', 29, NULL, NULL, 2, 29),
(54, 'Сохибқирон кўчаси', 29, NULL, NULL, 2, 29),
(55, 'Янгиланиш кўчаси', 30, NULL, NULL, 2, 29),
(56, 'Дехқонобод кўчаси', 30, NULL, NULL, 2, 29),
(57, 'Куйтол кўчаси', 31, NULL, NULL, 2, 29),
(58, 'Фидокор кўчаси', 31, NULL, NULL, 2, 29),
(59, 'Гара гура кўчаси', 31, NULL, NULL, 2, 29),
(60, 'Ойдин булоқ  кўчаси', 32, NULL, NULL, 2, 29),
(61, 'Камалак кўчаси ', 32, NULL, NULL, 2, 29),
(62, 'Сўлим кўчаси', 32, NULL, NULL, 2, 29),
(63, 'Гулдиёр кўчаси', 32, NULL, NULL, 2, 29),
(64, 'Файзиобод  кўчаси', 33, NULL, NULL, 2, 29),
(65, 'Дунг  кўчаси', 33, NULL, NULL, 2, 29),
(66, 'Бог кўчаси', 33, NULL, NULL, 2, 29),
(67, 'Раииш кўча', 33, NULL, NULL, 2, 29),
(68, 'Богбон кўчаси', 34, NULL, NULL, 2, 29),
(69, 'Гулзор кўчаси', 34, NULL, NULL, 2, 29),
(70, 'Мундуз кучаси', 34, NULL, NULL, 2, 29),
(71, 'Обод кўчаси', 34, NULL, NULL, 2, 29),
(72, 'Янги авлод кўса', 35, NULL, NULL, 2, 29),
(73, 'Булоқ кўчаси', 35, NULL, NULL, 2, 29),
(74, 'Канди кўчаси', 35, NULL, NULL, 2, 29),
(75, 'Эскилик кўчаси', 35, NULL, NULL, 2, 29),
(76, 'Тараннун  кўчаси', 36, NULL, NULL, 2, 29),
(77, 'Равнак кўчаси', 36, NULL, NULL, 2, 29),
(78, 'Маориф кўчаси', 36, NULL, NULL, 2, 29),
(79, 'Насм кўчаси', 36, NULL, NULL, 2, 29),
(80, 'Ғайтар кўчаси', 38, NULL, NULL, 2, 29),
(81, 'Мактаб кўчаси', 38, NULL, NULL, 2, 29),
(82, 'Боғ кучаси кўчаси', 38, NULL, NULL, 2, 29),
(83, 'Теракзор кўчаси', 38, NULL, NULL, 2, 29),
(84, 'Тарақиёт кўчаси', 1, NULL, NULL, 2, 29),
(85, 'Навоий кўча', 24, NULL, NULL, 2, 29),
(86, 'Оби хаёт 2', 24, NULL, NULL, 2, 29),
(87, 'Баркамол авлод кўчаси', 35, NULL, NULL, 2, 29),
(88, 'Янгилашниш кўча', 30, NULL, NULL, 2, 29),
(89, 'Эгамбердиобод кўча', 9, NULL, NULL, 2, 29),
(90, 'Шоликор кўча', 9, NULL, NULL, 2, 29),
(91, 'Бешчинор кўчаси', 9, NULL, NULL, 2, 29),
(92, 'Газли ер кўчаси', 9, NULL, NULL, 2, 29),
(93, 'Ок бўйра кўчаси', 9, NULL, NULL, 2, 29),
(94, 'Сохибкор кўчаси', 9, NULL, NULL, 2, 29),
(95, 'Боғбон кўча', 34, NULL, NULL, 2, 29),
(96, 'Гулзор кўча', 34, NULL, NULL, 2, 29),
(97, 'Мундуз кўча', 34, NULL, NULL, 2, 29),
(98, 'Обод кўча', 34, NULL, NULL, 2, 29),
(99, 'Қардошлик кўча', 11, NULL, NULL, 2, 29),
(100, 'Бадавлат кўча', 11, NULL, NULL, 2, 29),
(101, 'Забардас кўча', 11, NULL, NULL, 2, 29),
(102, 'Адолат кўча', 11, NULL, NULL, 2, 29),
(103, 'Рохат кўча', 11, NULL, NULL, 2, 29),
(104, 'Фаронов кўча', 20, NULL, NULL, 2, 29),
(105, 'Ёшлар кўча', 20, NULL, NULL, 2, 29),
(106, 'Пахтакор кўча', 20, NULL, NULL, 2, 29),
(107, 'Андижон кўча', 19, NULL, NULL, 2, 29),
(108, 'Гулзор кўча', 20, NULL, NULL, 2, 29),
(109, 'Ат-Термизий кўча', 19, NULL, NULL, 2, 29),
(110, 'Толбулоқ кўча', 19, NULL, NULL, 2, 29),
(111, 'Собир Рахимов кўча', 19, NULL, NULL, 2, 29),
(112, 'Бохор', 5, NULL, NULL, 2, 29),
(113, 'Намунали кўча', 24, NULL, NULL, 2, 29),
(114, 'Нуробод кўча', 27, NULL, NULL, 2, 29),
(115, 'Чоравдор кўчаси', 3, NULL, NULL, 2, 29),
(116, 'Гўзал диёр кўчаси', 3, NULL, NULL, 2, 29),
(117, 'Зарбдор кўчаси', 3, NULL, NULL, 2, 29),
(118, 'Ўзбекистон овози кўчаси', 3, NULL, NULL, 2, 29),
(119, 'Тинчлик кўчаси', 3, NULL, NULL, 2, 29),
(120, 'Камолот кўчаси', 4, NULL, NULL, 2, 29),
(121, 'Истиқлол кўчаси', 4, NULL, NULL, 2, 29),
(122, 'Пиллакор кўчаси', 4, NULL, NULL, 2, 29),
(123, 'Кумуш тола кўчаси', 4, NULL, NULL, 2, 29),
(124, 'Ахиллик кўчаси', 4, NULL, NULL, 2, 29),
(125, 'Канал бўйи кўчаси', 4, NULL, NULL, 2, 29),
(126, 'Бирдамлик кўчаси', 5, NULL, NULL, 2, 29),
(127, 'Намуна кўчаси', 5, NULL, NULL, 2, 29),
(128, 'Бахор кўчаси', 5, NULL, NULL, 2, 29),
(129, 'Тикланиш кўчаси', 5, NULL, NULL, 2, 29),
(130, 'Маданият кўчаси', 5, NULL, NULL, 2, 29),
(131, 'Марифат кўчаси', 5, NULL, NULL, 2, 29),
(132, 'Саноат 1', 6, NULL, NULL, 2, 29),
(133, 'Саноат 2', 6, NULL, NULL, 2, 29),
(134, 'Саноат 3', 6, NULL, NULL, 2, 29),
(135, 'Олмазор кўчаси', 6, NULL, NULL, 2, 29),
(136, 'Ибрат кўчаси', 6, NULL, NULL, 2, 29),
(137, 'Оқшом кўчаси', 6, NULL, NULL, 2, 29),
(138, 'Шифокорлар кўчаси', 6, NULL, NULL, 2, 29),
(139, 'Маорифчилар кўчаси', 6, NULL, NULL, 2, 29),
(140, 'Тегирмонбоши кўчаси', 7, NULL, NULL, 2, 29),
(141, 'Андижон кўча', 7, NULL, NULL, 2, 29),
(142, 'Тошлоқ кўчаси', 7, NULL, NULL, 2, 29),
(143, 'Ўрикзор кўчаси', 7, NULL, NULL, 2, 29),
(144, 'Қамбар ота кўчаси', 8, NULL, NULL, 2, 29),
(145, 'Дўстлиек кўчаси', 8, NULL, NULL, 2, 29),
(146, 'Чаманзор кўчаси', 8, NULL, NULL, 2, 29),
(147, 'Ёғду кўчаси', 8, NULL, NULL, 2, 29),
(148, 'Тикланиш кўчаси', 10, NULL, NULL, 2, 29),
(149, 'Давлатобод кўчаси', 10, NULL, NULL, 2, 29),
(150, 'Сой кўчаси', 10, NULL, NULL, 2, 29),
(151, 'Мустақиллик кўчаси', 10, NULL, NULL, 2, 29),
(152, 'Зиёратчи кўчаси', 10, NULL, NULL, 2, 29),
(153, 'Андижон кўча', 12, NULL, NULL, 2, 29),
(154, 'Ифтихор кўчаси', 12, NULL, NULL, 2, 29),
(155, 'Улус кўчаси', 12, NULL, NULL, 2, 29),
(156, 'Тикка кўчаси', 13, NULL, NULL, 2, 29),
(157, 'Дилкушод кўчаси', 13, NULL, NULL, 2, 29),
(158, 'Янги хаёт кўчаси', 13, NULL, NULL, 2, 29),
(159, 'Иттифоқ кўчаси', 13, NULL, NULL, 2, 29),
(160, 'Каранайчи кўчаси', 14, NULL, NULL, 2, 29),
(161, 'Элчибек кўчаси', 14, NULL, NULL, 2, 29),
(162, 'Уймоут кўчаси', 14, NULL, NULL, 2, 29),
(163, 'Чунтак кўчаси', 15, NULL, NULL, 2, 29),
(164, 'Посёлка кўчаси', 15, NULL, NULL, 2, 29),
(165, 'Матонат кўчаси', 15, NULL, NULL, 2, 29),
(166, 'Гилосзор кўчаси', 15, NULL, NULL, 2, 29),
(167, 'Юқори кўчаси', 16, NULL, NULL, 2, 29),
(168, 'Нишоббўйи кўчаси', 16, NULL, NULL, 2, 29),
(169, 'Зиёратгох кўчаси', 17, NULL, NULL, 2, 29),
(170, 'Чашма кўчаси', 17, NULL, NULL, 2, 29),
(171, 'Беларма кўчаси', 17, NULL, NULL, 2, 29),
(172, 'Юқори Имом кўчаси', 17, NULL, NULL, 2, 29),
(173, 'Олтин мерос кўчаси', 18, NULL, NULL, 2, 29),
(174, 'Қадирият кўчаси', 18, NULL, NULL, 2, 29),
(175, 'Истиқлол кўчаси', 18, NULL, NULL, 2, 29),
(176, 'Тотувлик кўчаси', 18, NULL, NULL, 2, 29),
(177, 'Гулзор кўча', 34, NULL, NULL, 2, 29),
(178, 'Боғбон кўча', 34, NULL, NULL, 2, 29),
(179, 'База кўча', 9, NULL, NULL, 2, 29),
(180, 'Yangi xayot', 12, NULL, NULL, 2, 29),
(181, 'Дилкушод', 12, NULL, NULL, 2, 29),
(182, 'Мунудуз кўча', 34, NULL, NULL, 2, 29),
(183, 'Ўрта ', 12, NULL, NULL, 2, 29);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_kocha_xolati`
--

CREATE TABLE IF NOT EXISTS `tbl_kocha_xolati` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `xonadon_joylashgan_kocha_uzunligi` varchar(255) DEFAULT NULL,
  `xonadon_joylashgan_kocha_eni` varchar(255) DEFAULT NULL,
  `xonadon_joylashgan_kocha_tamirlanganligi` varchar(255) DEFAULT NULL,
  `xonadon_joylashgan_kocha_koliforniya_terak_soni` varchar(255) DEFAULT NULL,
  `xonadon_joylashgan_kocha_yoritilishi` varchar(255) DEFAULT NULL,
  `xonadon_joylashgan_kocha_ariqlarni_xolati` varchar(255) DEFAULT NULL,
  `xonadon_joylashgan_kocha_ariqa_suv_kelishi` varchar(255) DEFAULT NULL,
  `xonadon_joylashgan_kocha_koprik_xolati` varchar(255) DEFAULT NULL,
  `xonadon_joylashgan_kocha_zovur_xolati` varchar(255) DEFAULT NULL,
  `fuqaro_uy_id` int(11) DEFAULT NULL,
  `kocha_qoplamasi` varchar(255) DEFAULT NULL,
  `anketa_ozgarish` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fuqaro_uy_id` (`fuqaro_uy_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_kocha_xolati`
--

INSERT INTO `tbl_kocha_xolati` (`id`, `xonadon_joylashgan_kocha_uzunligi`, `xonadon_joylashgan_kocha_eni`, `xonadon_joylashgan_kocha_tamirlanganligi`, `xonadon_joylashgan_kocha_koliforniya_terak_soni`, `xonadon_joylashgan_kocha_yoritilishi`, `xonadon_joylashgan_kocha_ariqlarni_xolati`, `xonadon_joylashgan_kocha_ariqa_suv_kelishi`, `xonadon_joylashgan_kocha_koprik_xolati`, `xonadon_joylashgan_kocha_zovur_xolati`, `fuqaro_uy_id`, `kocha_qoplamasi`, `anketa_ozgarish`, `user_id`, `user_date`) VALUES
(1, '150м', '20м', 'мухтож эмас', '', '2', '', '2', '2', '2', 11, 'асфалт', NULL, 15, '2018-01-09');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_kollejlar`
--

CREATE TABLE IF NOT EXISTS `tbl_kollejlar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kollej_nomi` varchar(255) DEFAULT NULL,
  `manzili` varchar(255) DEFAULT NULL,
  `kollej_direktori` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_maktab`
--

CREATE TABLE IF NOT EXISTS `tbl_maktab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maktab_raqami` varchar(255) DEFAULT NULL,
  `manzili` varchar(255) DEFAULT NULL,
  `maktab_direktor` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Дамп данных таблицы `tbl_maktab`
--

INSERT INTO `tbl_maktab` (`id`, `maktab_raqami`, `manzili`, `maktab_direktor`, `user_id`, `user_date`) VALUES
(1, '1-мактаб', NULL, NULL, NULL, NULL),
(2, '2-мактаб', NULL, NULL, NULL, NULL),
(3, '3-мактаб', NULL, NULL, NULL, NULL),
(4, '4-мактаб', NULL, NULL, NULL, NULL),
(5, '5-мактаб', NULL, NULL, NULL, NULL),
(6, '6-мактаб', NULL, NULL, NULL, NULL),
(7, '7-мактаб', NULL, NULL, NULL, NULL),
(8, '8-мактаб', NULL, NULL, NULL, NULL),
(9, '9-мактаб', NULL, NULL, NULL, NULL),
(10, '10-мактаб', NULL, NULL, NULL, NULL),
(11, '11-мактаб', NULL, NULL, NULL, NULL),
(12, '12-мактаб', NULL, NULL, NULL, NULL),
(13, '13-мактаб', NULL, NULL, NULL, NULL),
(14, '14-мактаб', NULL, NULL, NULL, NULL),
(15, '15-мактаб', NULL, NULL, NULL, NULL),
(16, '16-мактаб', NULL, NULL, NULL, NULL),
(17, '17-мактаб', NULL, NULL, NULL, NULL),
(18, '18-мактаб', NULL, NULL, NULL, NULL),
(19, '19-мактаб', NULL, NULL, NULL, NULL),
(20, '20-мактаб', NULL, NULL, NULL, NULL),
(21, '21-мактаб', NULL, NULL, NULL, NULL),
(22, '22-мактаб', NULL, NULL, NULL, NULL),
(23, '23-мактаб', NULL, NULL, NULL, NULL),
(24, '24-мактаб', NULL, NULL, NULL, NULL),
(25, '25-мактаб', NULL, NULL, NULL, NULL),
(26, '26-мактаб', NULL, NULL, NULL, NULL),
(27, '27-мактаб', NULL, NULL, NULL, NULL),
(28, '28-мактаб', NULL, NULL, NULL, NULL),
(29, '29-мактаб', NULL, NULL, NULL, NULL),
(30, '30-мактаб', NULL, NULL, NULL, NULL),
(31, '31-мактаб', NULL, NULL, NULL, NULL),
(32, '32-мактаб', NULL, NULL, NULL, NULL),
(33, '33-мактаб', NULL, NULL, NULL, NULL),
(34, '34-мактаб', NULL, NULL, NULL, NULL),
(35, '35-мактаб', NULL, NULL, NULL, NULL),
(36, '36-мактаб', NULL, NULL, NULL, NULL),
(37, '37-мактаб', NULL, NULL, NULL, NULL),
(38, '38-мактаб', NULL, NULL, NULL, NULL),
(39, '39-мактаб', NULL, NULL, NULL, NULL),
(40, '40-мактаб', NULL, NULL, NULL, NULL),
(41, '23-ДИМИ', NULL, NULL, NULL, NULL),
(42, '40-ДИМИ', NULL, NULL, NULL, NULL),
(43, 'яқин атрофдаги мактаб йўқ', NULL, NULL, NULL, NULL),
(44, 'яқин атрофдаги мактаб кўрсатилмаган', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_malumoti`
--

CREATE TABLE IF NOT EXISTS `tbl_malumoti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `malumot_turi` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `tbl_malumoti`
--

INSERT INTO `tbl_malumoti` (`id`, `malumot_turi`, `user_id`, `user_date`) VALUES
(1, 'мактабгача таълим', NULL, NULL),
(2, 'ўрта', NULL, NULL),
(3, 'ўрта махсус', NULL, NULL),
(4, 'олий', NULL, NULL),
(5, 'маълумот киритилмаган', NULL, NULL),
(6, 'она тарбиясида', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_manaviy`
--

CREATE TABLE IF NOT EXISTS `tbl_manaviy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `xonadon_bahosi` varchar(255) DEFAULT NULL,
  `kutubxona` varchar(255) DEFAULT NULL,
  `obuna` varchar(255) DEFAULT NULL,
  `farzandi_davomad` varchar(255) DEFAULT NULL,
  `manaviy_muhit` varchar(255) DEFAULT NULL,
  `jamoat_ishlarida_ishtiroki` varchar(255) DEFAULT NULL,
  `kayfiyat` varchar(255) DEFAULT NULL,
  `muhit_boshqalar` varchar(255) DEFAULT NULL,
  `fuqaro_uy_id` int(11) DEFAULT NULL,
  `anketa_ozgarish` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fuqaro_uy_id` (`fuqaro_uy_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_manaviy`
--

INSERT INTO `tbl_manaviy` (`id`, `xonadon_bahosi`, `kutubxona`, `obuna`, `farzandi_davomad`, `manaviy_muhit`, `jamoat_ishlarida_ishtiroki`, `kayfiyat`, `muhit_boshqalar`, `fuqaro_uy_id`, `anketa_ozgarish`, `user_id`, `user_date`) VALUES
(1, '1', '1', '1', '1', '', '', '', '', 11, NULL, 15, '2018-01-09');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_mfy`
--

CREATE TABLE IF NOT EXISTS `tbl_mfy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mfy_nomi` varchar(255) DEFAULT NULL,
  `tuman_id` int(11) DEFAULT NULL,
  `sektor_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  `viloyat_id` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tuman_id` (`tuman_id`),
  KEY `user_id` (`user_id`),
  KEY `viloyat_id` (`viloyat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Дамп данных таблицы `tbl_mfy`
--

INSERT INTO `tbl_mfy` (`id`, `mfy_nomi`, `tuman_id`, `sektor_id`, `user_id`, `user_date`, `viloyat_id`) VALUES
(1, 'Мустахкам МФЙ', 29, 4, NULL, NULL, 2),
(2, 'Куприкбоши МФЙ', 29, 2, NULL, NULL, 2),
(3, 'Саноатчилар МФЙ', 29, 2, NULL, NULL, 2),
(4, 'Ипакчи МФЙ', 29, 2, NULL, NULL, 2),
(5, 'Бешкавок МФЙ', 29, 2, NULL, NULL, 2),
(6, 'Солпи МФЙ', 29, 2, NULL, NULL, 2),
(7, 'Манак МФЙ', 29, 3, NULL, NULL, 2),
(8, 'Янги Фаргона МФЙ', 29, 3, NULL, NULL, 2),
(9, 'Эгамбердиобод МФЙ', 29, 3, NULL, NULL, 2),
(10, 'Факиркишлок МФЙ', 29, 3, NULL, NULL, 2),
(11, 'Тўраобод МФЙ', 29, 3, NULL, NULL, 2),
(12, 'Дилкушод МФЙ', 29, 3, NULL, NULL, 2),
(13, 'Кувватмурод МФЙ', 29, 3, NULL, NULL, 2),
(14, 'Карнайчи МФЙ', 29, 1, NULL, NULL, 2),
(15, 'Ордай МФЙ', 29, 3, NULL, NULL, 2),
(16, 'Нишоббуйи Мфй', 29, 3, NULL, NULL, 2),
(17, 'Тош ота МФЙ', 29, 1, NULL, NULL, 2),
(18, 'Гулистон МФЙ', 29, 3, NULL, NULL, 2),
(19, 'Андижон МФЙ', 29, 4, NULL, NULL, 2),
(20, 'Бахрин МФЙ', 29, 1, NULL, NULL, 2),
(21, 'Бобур МФЙ', 29, 1, NULL, NULL, 2),
(22, 'Бешкарам МФЙ', 29, 1, NULL, NULL, 2),
(23, 'Навбахор МФЙ', 29, 1, NULL, NULL, 2),
(24, 'Навоий МФЙ', 29, 1, NULL, NULL, 2),
(25, 'Узун кўча МФЙ', 29, 1, NULL, NULL, 2),
(26, 'Жоъме МФЙ', 29, 4, NULL, NULL, 2),
(27, 'Чимбулоқ МФЙ', 29, 2, NULL, NULL, 2),
(28, 'Сўқалоқ МФЙ', 29, 1, NULL, NULL, 2),
(29, 'Қурама МФЙ', 29, 4, NULL, NULL, 2),
(30, 'Етмишмерган МФЙ', 29, 2, NULL, NULL, 2),
(31, 'Гара гури МФЙ', 29, 2, NULL, NULL, 2),
(32, 'Шахрихонсой ШФЙ', 29, 1, NULL, NULL, 2),
(33, 'Сарқамиш МФЙ', 29, 4, NULL, NULL, 2),
(34, 'Хидирша МФЙ', 29, 4, NULL, NULL, 2),
(35, 'Янги хаёт МФЙ', 29, 4, NULL, NULL, 2),
(36, 'Қорабулоқ МФЙ', 29, 2, NULL, NULL, 2),
(38, 'Уч кўча МФЙ', 29, 2, NULL, NULL, 2),
(40, 'Кўприкбоши МФЙ', 29, 2, NULL, NULL, 2),
(41, 'Чироқчи МФЙ', 29, 3, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_mfy_xodimlar`
--

CREATE TABLE IF NOT EXISTS `tbl_mfy_xodimlar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `viloyat_id` int(11) DEFAULT NULL COMMENT 'Вилоят номи',
  `tuman_id` int(11) DEFAULT NULL COMMENT 'Туман номи',
  `mfy_id` int(11) DEFAULT NULL COMMENT 'МФЙ номи',
  `mfy_rais` varchar(255) DEFAULT NULL COMMENT 'МФЙ раисининг Ф.И.Ш.',
  `mfy_rais_tel` int(9) DEFAULT NULL COMMENT 'МФЙ раисининг телефон рақами',
  `mfy_kotiba` varchar(255) DEFAULT NULL COMMENT 'МФЙ котибасининг Ф.И.Ш.',
  `mfy_kotiba_tel` int(9) DEFAULT NULL COMMENT 'МФЙ котибасининг телефон рақами',
  `mfy_posbon` varchar(255) DEFAULT NULL COMMENT 'МФЙ Посбонини Ф.И.Ш.',
  `mfy_posbon_tel` int(9) DEFAULT NULL COMMENT 'МФЙ Посбоннинг телефон рақами',
  `email` varchar(255) DEFAULT NULL COMMENT 'E-mail',
  `user_id` int(11) DEFAULT NULL COMMENT 'Фойдаланувчи:',
  `user_date` date DEFAULT NULL COMMENT 'Бугунги сана:',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `tbl_mfy_xodimlar`
--

INSERT INTO `tbl_mfy_xodimlar` (`id`, `viloyat_id`, `tuman_id`, `mfy_id`, `mfy_rais`, `mfy_rais_tel`, `mfy_kotiba`, `mfy_kotiba_tel`, `mfy_posbon`, `mfy_posbon_tel`, `email`, `user_id`, `user_date`) VALUES
(1, 2, 29, 20, 'aaa', 7415555, 'aaa', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 29, 33, 'aaa', 7415555, 'aaa', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 2, 29, 23, 'aaa', 7415555, 'aaa', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 2, 29, 24, 'aaa', 7415555, 'aaa', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 2, 29, 28, 'aaa', 7415555, 'aaa', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 2, 29, 40, 'koprik', 7415555, 'aaa', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 2, 29, 29, 'aaa', 7415555, 'aaa', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 2, 29, 35, 'yangi', 7415555, 'aaa', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_millati`
--

CREATE TABLE IF NOT EXISTS `tbl_millati` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `millat` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100 ;

--
-- Дамп данных таблицы `tbl_millati`
--

INSERT INTO `tbl_millati` (`id`, `millat`, `user_id`, `user_date`) VALUES
(1, 'Ўзбeк', NULL, NULL),
(2, 'Рус', NULL, NULL),
(3, 'Қорақалпоқ', NULL, NULL),
(4, 'Тожик', NULL, NULL),
(5, 'Қозоқ', NULL, NULL),
(6, 'Туркман', NULL, NULL),
(7, 'Қирғиз', NULL, NULL),
(8, 'Татар', NULL, NULL),
(9, 'Корeйс', NULL, NULL),
(10, 'Уйғур', NULL, NULL),
(11, 'Турк', NULL, NULL),
(12, 'Нeмис', NULL, NULL),
(13, 'Грeк', NULL, NULL),
(14, 'Яҳудий', NULL, NULL),
(15, 'Озарбайжон', NULL, NULL),
(16, 'Арман', NULL, NULL),
(17, 'Грузин', NULL, NULL),
(18, 'Украин', NULL, NULL),
(19, 'Бeларус', NULL, NULL),
(20, 'Молдова', NULL, NULL),
(21, 'Латиш', NULL, NULL),
(22, 'Eстон', NULL, NULL),
(23, 'Литва', NULL, NULL),
(24, 'Афғон', NULL, NULL),
(25, 'Араб', NULL, NULL),
(26, 'Eрон', NULL, NULL),
(27, 'Миср', NULL, NULL),
(28, 'Ҳинду', NULL, NULL),
(29, 'Хитой', NULL, NULL),
(30, 'Монгол', NULL, NULL),
(31, 'Въeтнам', NULL, NULL),
(32, 'Бошқирд', NULL, NULL),
(99, 'Бошқа Миллат', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_muammo`
--

CREATE TABLE IF NOT EXISTS `tbl_muammo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `viloyat_id` int(11) DEFAULT NULL COMMENT 'Вилоят',
  `tuman_id` int(11) DEFAULT NULL COMMENT 'Туман',
  `mfy_id` int(11) DEFAULT NULL COMMENT 'МФЙ',
  `kocha_id` int(11) DEFAULT NULL COMMENT 'Кўча номи',
  `muammo_turlari` int(11) DEFAULT NULL,
  `muammo_mazmuni` varchar(5000) DEFAULT NULL COMMENT 'Муаммо мазмуни',
  `muammo_xolati_id` int(11) DEFAULT NULL COMMENT 'Муаммо холати',
  `xulosa` varchar(5000) DEFAULT NULL COMMENT 'Хулоса',
  `fuqaro_uy_id` int(11) DEFAULT NULL,
  `fuqaro_id` int(11) DEFAULT NULL COMMENT 'Фуқаронинг ФИШ',
  `tashkilot_id` int(11) DEFAULT NULL COMMENT 'Ташкилот номи',
  `qayd_sana` datetime DEFAULT NULL COMMENT 'Муаммо қайд этилган сана',
  `muddat_sana` datetime DEFAULT NULL COMMENT 'Муддати',
  `korilgan_sana` datetime DEFAULT NULL COMMENT 'Муаммо кўрилган сана',
  `xal_etilgan_sana` datetime DEFAULT NULL COMMENT 'Муаммо хал этилган',
  `user_id` int(11) DEFAULT NULL,
  `xal_etuvchi_fio` varchar(30) DEFAULT NULL,
  `etiroz` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tashkilot_id` (`tashkilot_id`),
  KEY `fuqaro_id` (`fuqaro_id`),
  KEY `user_id` (`user_id`),
  KEY `muammo_xolati_id` (`muammo_xolati_id`),
  KEY `viloyat_id` (`viloyat_id`),
  KEY `tuman_id` (`tuman_id`),
  KEY `mfy_id` (`mfy_id`),
  KEY `kocha_id` (`kocha_id`),
  KEY `fuqaro_uy_id` (`fuqaro_uy_id`),
  KEY `etiroz` (`etiroz`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_muammo_turlari`
--

CREATE TABLE IF NOT EXISTS `tbl_muammo_turlari` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `muammo_turlari` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=36 ;

--
-- Дамп данных таблицы `tbl_muammo_turlari`
--

INSERT INTO `tbl_muammo_turlari` (`id`, `muammo_turlari`, `user_id`, `user_date`) VALUES
(1, 'Моддий ёрдам', 1, '2017-09-24'),
(2, 'Даволаниш учун моддий ёрдам', 1, '2017-09-24'),
(3, 'Уйни таъмирлаш учун моддий ёрдам', 1, '2017-09-24'),
(4, 'Ишга жойлаштириш', 1, '2017-09-24'),
(5, 'Хизмат кўрсатиш', 1, '2017-09-24'),
(6, 'Намунали, камолот,  кўп қаватли ёки бошка уй жой билан таъминлаш', 1, NULL),
(7, 'Уй-жой куриш учун ер жаратиш ', 1, NULL),
(8, 'Иссикхона учун ер ажратиш', 1, NULL),
(9, 'Тадбиркорлик фаолияти учун ер майдони ажратиш', 1, NULL),
(10, 'Деҳқон ва фермер хўжалиги юритиш учун ер  ажратиш', 1, NULL),
(11, '“Нол” қийматда \r\nва бошқа масалада  бинолар ажратиш.', 1, NULL),
(12, 'Кредит ажратиш', 1, NULL),
(13, 'Табиий газ ва электр энергия таъминотидан норозили ҳақида', 1, NULL),
(14, 'Соғлиқни сақлаш ва тиббий хизмат масаласи', 1, NULL),
(15, 'Таълим соҳасида', 1, NULL),
(16, 'Моддий-техник таъминотни яхшилаш ва жихозлар ажратиш масаласида', 1, NULL),
(17, 'Моддий ёрдам ва нафақа масаласида', 1, NULL),
(18, 'Ичимлик суви билан таъмин-лаш масаласи', 1, NULL),
(19, 'Савдо дўконларни терминал билан таъминлаш', 1, NULL),
(20, 'Ободонлаштириш масаласи', 1, NULL),
(21, 'Таълим муассасалари таъмирлаш, спорт иншоотларини курилиши масаласи', 1, NULL),
(22, 'Кўрсатилган хизматлар учун ёки бошка ҳақларини ундириш', 1, NULL),
(23, 'Алимент ундириш', 1, NULL),
(24, 'Кўмир ажратиш', 1, NULL),
(25, 'Иш ҳақи ундириш ва мехнат муносабати масаласи', 1, NULL),
(26, 'Ишга жойлаштириш масаласида ', 1, NULL),
(27, 'Паспорт ва фуқароликни расмийлаш-тириш масаласи', 1, NULL),
(28, 'Хаж сафарига бориш масаласида', 1, NULL),
(29, 'Фирибгарлик масаласида', 1, NULL),
(30, 'Транспорт хизмат кўрсатиш', 1, NULL),
(31, 'Йўл қурилиши', 1, NULL),
(32, 'Ногиронлик бўйича протез-ортопедия маҳсулотлари билан таъминлаш', 1, NULL),
(33, 'Суд билан боғлиқ масалалар', 1, NULL),
(34, 'Мурожаатларни куриб чикмаган-лиги ёки куриб чикиш натижасиданнорозили-ги', 1, NULL),
(35, 'Бошқа масалалар', 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_muammo_xolati`
--

CREATE TABLE IF NOT EXISTS `tbl_muammo_xolati` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `xulosa` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `tbl_muammo_xolati`
--

INSERT INTO `tbl_muammo_xolati` (`id`, `xulosa`, `user_id`, `user_date`) VALUES
(1, 'хал этилди', 1, '2017-09-14'),
(2, 'кўриб чиқилди', 1, '2017-09-14'),
(3, 'эътироз билдирилди', 1, '2017-09-14');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_muassasa`
--

CREATE TABLE IF NOT EXISTS `tbl_muassasa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `muassasa_raqami` varchar(255) DEFAULT NULL,
  `muassasa_nomi` varchar(255) DEFAULT NULL,
  `manzili` varchar(255) DEFAULT NULL,
  `muassasa_rahbari` varchar(255) DEFAULT NULL,
  `turi` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `turi` (`turi`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Дамп данных таблицы `tbl_muassasa`
--

INSERT INTO `tbl_muassasa` (`id`, `muassasa_raqami`, `muassasa_nomi`, `manzili`, `muassasa_rahbari`, `turi`, `user_id`, `user_date`) VALUES
(1, '1-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(2, '2-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(3, '3-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(4, '4-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(5, '5-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(6, '6-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(7, '7-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(8, '8-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(9, '9-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(10, '10-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(11, '11-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(12, '12-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(13, '13-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(14, '14-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(15, '15-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(16, '16--боғча', NULL, NULL, NULL, 1, NULL, NULL),
(17, '17-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(18, '18-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(19, '19-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(20, '20--боғча', NULL, NULL, NULL, 1, NULL, NULL),
(21, '21-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(22, '22-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(23, '23-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(24, '24-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(25, '25-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(26, '26-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(27, '27-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(28, '28-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(29, '29-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(30, '30-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(31, '31-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(32, '32-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(33, '33-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(34, '34-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(35, '35-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(36, '36-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(37, '37-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(38, '38-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(39, '39-боғча', NULL, NULL, NULL, 1, NULL, NULL),
(40, '40-боғча', NULL, NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_notinch`
--

CREATE TABLE IF NOT EXISTS `tbl_notinch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oila_xolati` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tbl_notinch`
--

INSERT INTO `tbl_notinch` (`id`, `oila_xolati`, `user_id`, `user_date`) VALUES
(1, 'тинч', NULL, NULL),
(2, 'нотинчч', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_oliygohlar`
--

CREATE TABLE IF NOT EXISTS `tbl_oliygohlar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oligoh_nomi` varchar(255) DEFAULT NULL,
  `manzili` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_oliygohlar`
--

INSERT INTO `tbl_oliygohlar` (`id`, `oligoh_nomi`, `manzili`, `user_id`, `user_date`) VALUES
(1, '1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_passport_seriya`
--

CREATE TABLE IF NOT EXISTS `tbl_passport_seriya` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `harfi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Дамп данных таблицы `tbl_passport_seriya`
--

INSERT INTO `tbl_passport_seriya` (`id`, `harfi`) VALUES
(1, 'AA'),
(2, 'AB'),
(3, 'AC'),
(4, 'AD'),
(5, 'AE'),
(6, 'AF'),
(7, 'AG'),
(8, 'AH'),
(9, 'AJ'),
(10, 'AI'),
(11, 'AK'),
(12, 'AL'),
(13, 'AN'),
(14, 'AP'),
(15, 'AR'),
(16, 'AS'),
(17, 'AT'),
(18, 'AU'),
(19, 'AV'),
(20, 'AX'),
(21, 'AY'),
(22, 'AZ');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_qarz`
--

CREATE TABLE IF NOT EXISTS `tbl_qarz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `umumiy_qarz` varchar(255) DEFAULT NULL,
  `eletr_qarz` varchar(255) DEFAULT NULL,
  `gaz_qarz` varchar(255) DEFAULT NULL,
  `suv_qarz` varchar(255) DEFAULT NULL,
  `soliq_yer_qarz` varchar(255) DEFAULT NULL,
  `soliq_mulk_qarz` varchar(255) DEFAULT NULL,
  `schr_qarz` varchar(255) DEFAULT NULL,
  `chiqindi_qarz` varchar(255) DEFAULT NULL,
  `aliment_qarz` varchar(255) DEFAULT NULL,
  `qarz_boshqalar` varchar(255) DEFAULT NULL,
  `tbl_fuqaro_uy_id` int(11) DEFAULT NULL,
  `tbl_fuqaro_id` int(11) DEFAULT NULL,
  `anketa_ozgarish` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_fuqaro_uy_id` (`tbl_fuqaro_uy_id`),
  KEY `tbl_fuqaro_id` (`tbl_fuqaro_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_qarz`
--

INSERT INTO `tbl_qarz` (`id`, `umumiy_qarz`, `eletr_qarz`, `gaz_qarz`, `suv_qarz`, `soliq_yer_qarz`, `soliq_mulk_qarz`, `schr_qarz`, `chiqindi_qarz`, `aliment_qarz`, `qarz_boshqalar`, `tbl_fuqaro_uy_id`, `tbl_fuqaro_id`, `anketa_ozgarish`, `user_id`, `user_date`) VALUES
(1, '0', '', '', '', '', '', '', '', '', '', 11, NULL, NULL, 15, '2018-01-09');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_rahbarlar`
--

CREATE TABLE IF NOT EXISTS `tbl_rahbarlar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `r_fio` varchar(255) DEFAULT NULL,
  `ish_joy_nomi` varchar(255) DEFAULT NULL,
  `lavozimi` varchar(255) DEFAULT NULL,
  `viloyat_id` int(11) DEFAULT NULL,
  `tuman_id` int(11) DEFAULT NULL,
  `sektor` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tuman_id` (`tuman_id`),
  KEY `viloyat_id` (`viloyat_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `tbl_rahbarlar`
--

INSERT INTO `tbl_rahbarlar` (`id`, `r_fio`, `ish_joy_nomi`, `lavozimi`, `viloyat_id`, `tuman_id`, `sektor`, `user_id`, `user_date`) VALUES
(1, 'Қ.Қирғизбоев', 'Хўжаобод туман хокимияти', 'туман хокими', 2, 29, 1, NULL, NULL),
(2, 'Д.Турахонов', 'Хўжаобод туман прокуратураси', 'туман прокурори', 2, 29, 2, NULL, NULL),
(3, 'С.Мамадалиев', 'Хўжаобод туман ИИБ', 'тумани ИИБ бошлиғи', 2, 29, 3, NULL, NULL),
(4, 'А.Усмонов', 'Хўжаобод туман ДСИ', 'туман ДСИ бошлиғи', 2, 29, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_sektor`
--

CREATE TABLE IF NOT EXISTS `tbl_sektor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sektor_raqami` int(11) DEFAULT NULL,
  `sektor_viloyat_id` int(11) DEFAULT NULL,
  `sektor_tuman_id` int(11) DEFAULT NULL,
  `sektor_mfy_id` int(11) DEFAULT NULL,
  `sektor_rahbari` int(11) DEFAULT NULL,
  `sektor_ish_joyi` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sektor_mfy_id` (`sektor_mfy_id`),
  KEY `sektor_viloyat_id` (`sektor_viloyat_id`),
  KEY `sektor_tuman_id` (`sektor_tuman_id`),
  KEY `sektor_rahbari` (`sektor_rahbari`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Дамп данных таблицы `tbl_sektor`
--

INSERT INTO `tbl_sektor` (`id`, `sektor_raqami`, `sektor_viloyat_id`, `sektor_tuman_id`, `sektor_mfy_id`, `sektor_rahbari`, `sektor_ish_joyi`, `user_id`, `user_date`) VALUES
(1, 1, 2, 29, 20, 1, NULL, NULL, NULL),
(2, 1, 2, 29, 21, 1, NULL, NULL, NULL),
(3, 1, 2, 29, 23, 1, NULL, NULL, NULL),
(4, 1, 2, 29, 25, 1, NULL, NULL, NULL),
(5, 1, 2, 29, 24, 1, NULL, NULL, NULL),
(6, 1, 2, 29, 22, 1, NULL, NULL, NULL),
(7, 1, 2, 29, 32, 1, NULL, NULL, NULL),
(8, 1, 2, 29, 28, 1, NULL, NULL, NULL),
(9, 1, 2, 29, 14, 1, NULL, NULL, NULL),
(10, 1, 2, 29, 17, 1, NULL, NULL, NULL),
(11, 2, 2, 29, 5, 2, NULL, NULL, NULL),
(12, 2, 2, 29, 30, 2, NULL, NULL, NULL),
(13, 2, 2, 29, 31, 2, NULL, NULL, NULL),
(14, 2, 2, 29, 36, 2, NULL, NULL, NULL),
(15, 2, 2, 29, 38, 2, NULL, NULL, NULL),
(16, 2, 2, 29, 2, 2, NULL, NULL, NULL),
(17, 2, 2, 29, 4, 2, NULL, NULL, NULL),
(18, 2, 2, 29, 3, 2, NULL, NULL, NULL),
(19, 2, 2, 29, 3, 2, NULL, NULL, NULL),
(20, 2, 2, 29, 27, 2, NULL, NULL, NULL),
(21, 3, 2, 29, 13, 3, NULL, NULL, NULL),
(22, 3, 2, 29, 12, 3, NULL, NULL, NULL),
(23, 3, 2, 29, 15, 3, NULL, NULL, NULL),
(24, 3, 2, 29, 41, 3, NULL, NULL, NULL),
(25, 3, 2, 29, 7, 3, NULL, NULL, NULL),
(26, 3, 2, 29, 16, 3, NULL, NULL, NULL),
(27, 3, 2, 29, 18, 3, NULL, NULL, NULL),
(28, 3, 2, 29, 10, 3, NULL, NULL, NULL),
(29, 3, 2, 29, 11, 3, NULL, NULL, NULL),
(30, 3, 2, 29, 8, 3, NULL, NULL, NULL),
(31, 3, 2, 29, 8, 3, NULL, NULL, NULL),
(32, 4, 2, 29, 35, 4, NULL, NULL, NULL),
(33, 4, 2, 29, 34, 4, NULL, NULL, NULL),
(34, 4, 2, 29, 19, 4, NULL, NULL, NULL),
(35, 4, 2, 29, 26, 4, NULL, NULL, NULL),
(36, 4, 2, 29, 1, 4, NULL, NULL, NULL),
(37, 4, 2, 29, 29, 4, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_sektorlar`
--

CREATE TABLE IF NOT EXISTS `tbl_sektorlar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sektor_raqami` int(11) DEFAULT NULL COMMENT 'Сектор рақами',
  `viloyat_id` int(11) DEFAULT NULL COMMENT 'Вилоят',
  `tuman_id` int(11) DEFAULT NULL COMMENT 'Туман',
  PRIMARY KEY (`id`),
  KEY `viloyat_id` (`viloyat_id`),
  KEY `tuman_id` (`tuman_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Секторлар' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_shahar`
--

CREATE TABLE IF NOT EXISTS `tbl_shahar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shahar_nomi` varchar(255) DEFAULT NULL,
  `viloyat_id` int(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `viloyat_id` (`viloyat_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=380 ;

--
-- Дамп данных таблицы `tbl_shahar`
--

INSERT INTO `tbl_shahar` (`id`, `shahar_nomi`, `viloyat_id`, `user_id`, `user_date`) VALUES
(1, 'shahar_nomi', NULL, NULL, NULL),
(2, 'Тахиатош шаҳри', 1, NULL, NULL),
(3, 'Халқобод шаҳри', 1, NULL, NULL),
(4, 'Оқманғит шаҳри', 1, NULL, NULL),
(5, 'Бeруний шаҳри', 1, NULL, NULL),
(6, 'Хўжайли шаҳри', 1, NULL, NULL),
(7, 'Чимбой шаҳри', 1, NULL, NULL),
(8, 'Манғит шаҳри', 1, NULL, NULL),
(9, 'Мўйноқ шаҳри', 1, NULL, NULL),
(10, 'Тўрткўл шаҳри', 1, NULL, NULL),
(11, 'Шуманай шаҳри', 1, NULL, NULL),
(12, 'Қўнғирот шаҳри', 1, NULL, NULL),
(13, 'Eлликтeпа шаҳри', 1, NULL, NULL),
(14, 'Бўстон шаҳри', 1, NULL, NULL),
(15, 'Қозонкeтган шаҳри', 1, NULL, NULL),
(16, 'Кeгeйли шаҳри', 1, NULL, NULL),
(17, 'Лeнинобод шаҳри', 1, NULL, NULL),
(18, 'Қораузақ шаҳри', 1, NULL, NULL),
(19, 'Тахтакўпир шаҳри', 1, NULL, NULL),
(30, 'Андижон шаҳри', 2, NULL, NULL),
(32, 'Асака шаҳри', 2, NULL, NULL),
(33, 'Шаҳрихон шаҳри', 2, NULL, NULL),
(35, 'Хонобод шаҳри', 2, NULL, NULL),
(36, 'Қорасув шаҳри', 2, NULL, NULL),
(60, 'Наманган шаҳри', 3, NULL, NULL),
(61, 'Тошбулоқ шаҳри', 3, NULL, NULL),
(62, 'Косонсой шаҳри', 3, NULL, NULL),
(63, 'Ҳаққулобод шаҳри', 3, NULL, NULL),
(64, 'Учқўрғон шаҳри', 3, NULL, NULL),
(65, 'Чортоқ шаҳри', 3, NULL, NULL),
(66, 'Чуст шаҳри', 3, NULL, NULL),
(67, 'Поп шаҳри', 3, NULL, NULL),
(68, 'Тўрақўрғон шаҳри', 3, NULL, NULL),
(69, 'Янгиқўрғон шаҳри', 3, NULL, NULL),
(70, 'Жомашуй шаҳри', 3, NULL, NULL),
(71, 'Уйчи шаҳри', 3, NULL, NULL),
(90, 'Фарғона шаҳри', 4, NULL, NULL),
(91, 'Қувасой шаҳри', 4, NULL, NULL),
(92, 'Водил шаҳри', 4, NULL, NULL),
(93, 'Қўқон шаҳри', 4, NULL, NULL),
(94, 'Данғара шаҳри', 4, NULL, NULL),
(95, 'Марғилон шаҳри', 4, NULL, NULL),
(96, 'Лангар шаҳри', 4, NULL, NULL),
(97, 'Олтиариқ шаҳри', 4, NULL, NULL),
(98, 'Ҳамза шаҳри', 4, NULL, NULL),
(99, 'Қува шаҳри', 4, NULL, NULL),
(100, 'Бeшариқ шаҳри', 4, NULL, NULL),
(101, 'Риштон шаҳри', 4, NULL, NULL),
(102, 'Яйпан шаҳри', 4, NULL, NULL),
(103, 'Боғдод шаҳри', 4, NULL, NULL),
(104, 'Ёзёвон шаҳри', 4, NULL, NULL),
(105, 'Равон шаҳри', 4, NULL, NULL),
(106, 'Тошлоқ шаҳри', 4, NULL, NULL),
(107, 'Янгиқўрғон шаҳри', 4, NULL, NULL),
(108, 'Учкўприк шаҳри', 4, NULL, NULL),
(109, 'Навбаҳор шаҳри', 4, NULL, NULL),
(110, 'Киргули шаҳри', 4, NULL, NULL),
(120, 'Бухоро шаҳри', 5, NULL, NULL),
(121, 'Ғалаосиё шаҳри', 5, NULL, NULL),
(122, 'Қоровулбозор шаҳри', 5, NULL, NULL),
(123, 'Когон шаҳри', 5, NULL, NULL),
(124, 'Ғиждувон шаҳри', 5, NULL, NULL),
(125, 'Ромитан шаҳри', 5, NULL, NULL),
(126, 'Газли шаҳри', 5, NULL, NULL),
(127, 'Вобкeнт шаҳри', 5, NULL, NULL),
(128, 'Жондор шаҳри', 5, NULL, NULL),
(129, 'Олот шаҳри', 5, NULL, NULL),
(130, 'Шофиркон шаҳри', 5, NULL, NULL),
(131, 'Қоракўл шаҳри', 5, NULL, NULL),
(132, 'Пeшку шаҳри', 5, NULL, NULL),
(150, 'Урганч шаҳри', 6, NULL, NULL),
(151, 'Қоровул шаҳри', 6, NULL, NULL),
(152, 'Хива шаҳри', 6, NULL, NULL),
(153, 'Дружба шаҳри', 6, NULL, NULL),
(154, 'Хазорасп шаҳри', 6, NULL, NULL),
(155, 'Гурлан шаҳри', 6, NULL, NULL),
(156, 'Хонқа шаҳри', 6, NULL, NULL),
(157, 'Шовот шаҳри', 6, NULL, NULL),
(158, 'Янгибозор шаҳри', 6, NULL, NULL),
(159, 'Қўшкўпир шаҳри', 6, NULL, NULL),
(160, 'Боғот шаҳри', 6, NULL, NULL),
(161, 'Янгиариқ шаҳри', 6, NULL, NULL),
(162, 'Питняк шаҳри', 6, NULL, NULL),
(180, 'Тeрмиз шаҳри', 7, NULL, NULL),
(181, 'Учқизил шаҳри', 7, NULL, NULL),
(182, 'Дeнов шаҳри', 7, NULL, NULL),
(183, 'Сариосиё шаҳри', 7, NULL, NULL),
(184, 'Шорғун шаҳри', 7, NULL, NULL),
(185, 'Ангор шаҳри', 7, NULL, NULL),
(186, 'Бойсун шаҳри', 7, NULL, NULL),
(187, 'Жарқўрғон шаҳри', 7, NULL, NULL),
(188, 'Узун шаҳри', 7, NULL, NULL),
(189, 'Шeробод шаҳри', 7, NULL, NULL),
(190, 'Шўрчи шаҳри', 7, NULL, NULL),
(191, 'Қумқўрғон шаҳри', 7, NULL, NULL),
(192, 'Комсомолобод шаҳри', 7, NULL, NULL),
(193, 'Сариқ шаҳри', 7, NULL, NULL),
(194, 'Бандихон шаҳри', 7, NULL, NULL),
(195, 'Қарлуқ шаҳри', 7, NULL, NULL),
(210, 'Қарши шаҳри', 8, NULL, NULL),
(211, 'Бeшкeнт шаҳри', 8, NULL, NULL),
(212, 'Китоб шаҳри', 8, NULL, NULL),
(213, 'Косон шаҳри', 8, NULL, NULL),
(214, 'Муборак шаҳри', 8, NULL, NULL),
(215, 'Шаҳрисабз шаҳри', 8, NULL, NULL),
(216, 'Янги Нишон шаҳри', 8, NULL, NULL),
(217, 'Толлимаржон шаҳри', 8, NULL, NULL),
(218, 'Чироқчи шаҳри', 8, NULL, NULL),
(219, 'Яккабоғ шаҳри', 8, NULL, NULL),
(220, 'Қамаши шаҳри', 8, NULL, NULL),
(221, 'Ғузор шаҳри', 8, NULL, NULL),
(222, 'Муғлон шаҳри', 8, NULL, NULL),
(223, 'Помуқ шаҳри', 8, NULL, NULL),
(224, 'Корашина шаҳри', 8, NULL, NULL),
(225, 'Янги миришкор шаҳри', 8, NULL, NULL),
(240, 'Жиззах шаҳри', 9, NULL, NULL),
(250, 'Учтeпа шаҳри', 9, NULL, NULL),
(251, 'Ғаллаорол шаҳри', 9, NULL, NULL),
(252, 'Маржонбулоқ шаҳри', 9, NULL, NULL),
(253, 'Улянов шаҳри', 9, NULL, NULL),
(254, 'Зомин шаҳри', 9, NULL, NULL),
(255, 'Дўстлик шаҳри', 9, NULL, NULL),
(256, 'Гагарин шаҳри', 9, NULL, NULL),
(257, 'Пахтакор шаҳри', 9, NULL, NULL),
(258, 'Усмат шаҳри', 9, NULL, NULL),
(259, 'Зарбдор шаҳри', 9, NULL, NULL),
(260, 'Янги Қишлоқ шаҳри', 9, NULL, NULL),
(261, 'Арнасой шаҳри', 9, NULL, NULL),
(270, 'Навоий шаҳри', 10, NULL, NULL),
(271, 'Кармана шаҳри', 10, NULL, NULL),
(272, 'Зарафшон шаҳри', 10, NULL, NULL),
(273, 'Томдибулоқ шаҳри', 10, NULL, NULL),
(274, 'Учқудуқ шаҳри', 10, NULL, NULL),
(275, 'Нурота шаҳри', 10, NULL, NULL),
(276, 'Қизилтeпа шаҳри', 10, NULL, NULL),
(277, 'Янгиработ шаҳри', 10, NULL, NULL),
(278, 'Конимeх шаҳри', 10, NULL, NULL),
(279, 'Бeшработ шаҳри', 10, NULL, NULL),
(300, 'Гулобод шаҳри', 11, NULL, NULL),
(301, 'Самарқанд шаҳри', 11, NULL, NULL),
(302, 'Пайшанба шаҳри', 11, NULL, NULL),
(303, 'Каттақўрғон шаҳри', 11, NULL, NULL),
(304, 'Оқтош шаҳри', 11, NULL, NULL),
(305, 'Ургут шаҳри', 11, NULL, NULL),
(306, 'Булунғур шаҳри', 11, NULL, NULL),
(307, 'Жомбой шаҳри', 11, NULL, NULL),
(308, 'Иштихон шаҳри', 11, NULL, NULL),
(309, 'Нуробод шаҳри', 11, NULL, NULL),
(310, 'Жума шаҳри', 11, NULL, NULL),
(311, 'Чeлак шаҳри', 11, NULL, NULL),
(312, 'Лоиш шаҳри', 11, NULL, NULL),
(313, 'Зиёвутдин шаҳри', 11, NULL, NULL),
(314, 'Гўзалкeнт шаҳри', 11, NULL, NULL),
(315, 'Пояриқ шаҳри', 11, NULL, NULL),
(316, 'Тойлоқ шаҳри', 11, NULL, NULL),
(317, 'Қўшработ шаҳри', 11, NULL, NULL),
(330, 'Сирдарё шаҳри', 12, NULL, NULL),
(331, 'Янгиeр шаҳри', 12, NULL, NULL),
(332, 'Ширин шаҳри', 12, NULL, NULL),
(333, 'Фарҳод шаҳри', 12, NULL, NULL),
(334, 'Гулистон шаҳри', 12, NULL, NULL),
(335, 'Крeстянский шаҳри', 12, NULL, NULL),
(336, 'Бахт шаҳри', 12, NULL, NULL),
(337, 'Сайхун шаҳри', 12, NULL, NULL),
(338, 'Пахтаобод шаҳри', 12, NULL, NULL),
(339, 'Дмитровский шаҳри', 12, NULL, NULL),
(340, 'Қаҳрамон шаҳри', 12, NULL, NULL),
(341, 'Наврўз шаҳри', 12, NULL, NULL),
(342, 'Сардоба шаҳри', 12, NULL, NULL),
(360, 'Чирчиқ шаҳри', 13, NULL, NULL),
(361, 'Қибрай шаҳри', 13, NULL, NULL),
(362, 'Оҳангарон шаҳри', 13, NULL, NULL),
(363, 'Ангрeн шаҳри', 13, NULL, NULL),
(364, 'Янгиобод шаҳри', 13, NULL, NULL),
(365, 'Бeкобод шаҳри', 13, NULL, NULL),
(366, 'Зафар шаҳри', 13, NULL, NULL),
(367, 'Олмалиқ шаҳри', 13, NULL, NULL),
(368, 'Пскeнт шаҳри', 13, NULL, NULL),
(369, 'Янгиюл шаҳри', 13, NULL, NULL),
(370, 'Ғазалкeнт шаҳри', 13, NULL, NULL),
(371, 'Бўка шаҳри', 13, NULL, NULL),
(372, 'Оққўрғон шаҳри', 13, NULL, NULL),
(373, 'Паркeнт шаҳри', 13, NULL, NULL),
(374, 'Кeлeс шаҳри', 13, NULL, NULL),
(375, 'Чиноз шаҳри', 13, NULL, NULL),
(376, 'Тўйтeпа шаҳри', 13, NULL, NULL),
(377, 'Солдатский шаҳри', 13, NULL, NULL),
(378, 'Eшонгузар шаҳри', 13, NULL, NULL),
(379, 'Янгибозор шаҳри', 13, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_shaoriti`
--

CREATE TABLE IF NOT EXISTS `tbl_shaoriti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vosita_soni` int(11) DEFAULT NULL,
  `televizor` int(11) DEFAULT NULL,
  `kir_yuvish_vositasi` int(11) DEFAULT NULL,
  `sovutgich` int(11) DEFAULT NULL,
  `DVD` int(11) DEFAULT NULL,
  `Konditsioner` int(11) DEFAULT NULL,
  `kompyuter` int(11) DEFAULT NULL,
  `internet` varchar(255) DEFAULT NULL,
  `dazmol` int(11) DEFAULT NULL,
  `vosita_boshqalar` varchar(255) DEFAULT NULL,
  `tbl_fuqaro_uy_id` int(11) DEFAULT NULL,
  `anketa_ozgarish` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_fuqaro_uy_id` (`tbl_fuqaro_uy_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_shaoriti`
--

INSERT INTO `tbl_shaoriti` (`id`, `vosita_soni`, `televizor`, `kir_yuvish_vositasi`, `sovutgich`, `DVD`, `Konditsioner`, `kompyuter`, `internet`, `dazmol`, `vosita_boshqalar`, `tbl_fuqaro_uy_id`, `anketa_ozgarish`, `user_id`, `user_date`) VALUES
(1, 0, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', 11, NULL, 15, '2018-01-09');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_tashkilot`
--

CREATE TABLE IF NOT EXISTS `tbl_tashkilot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tviloyat_id` int(11) DEFAULT NULL,
  `ttuman_id` int(11) DEFAULT NULL,
  `tmfy_id` int(11) DEFAULT NULL,
  `tkocha_id` int(11) DEFAULT NULL,
  `tnomi` varchar(255) DEFAULT NULL,
  `trahbari` varchar(255) DEFAULT NULL,
  `ttelefon_raqami` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tviloyat_id` (`tviloyat_id`),
  KEY `ttuman_id` (`ttuman_id`),
  KEY `tmfy_id` (`tmfy_id`),
  KEY `tkocha_id` (`tkocha_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `tbl_tashkilot`
--

INSERT INTO `tbl_tashkilot` (`id`, `tviloyat_id`, `ttuman_id`, `tmfy_id`, `tkocha_id`, `tnomi`, `trahbari`, `ttelefon_raqami`, `email`, `user_id`, `user_date`) VALUES
(1, 2, 29, NULL, NULL, 'Туман прокурори ёрдамчиси', 'Келишув асосида', 74741444, '', 1, '2017-09-14'),
(2, 2, 29, NULL, NULL, 'Туман ИИБ раҳбари', 'С.Мамадалиев', NULL, '', 1, '2017-09-14'),
(3, 2, 29, NULL, NULL, 'Туман хокими', 'Қ.Қирғизбоев ', NULL, '', 1, '2017-09-14'),
(4, 2, 29, NULL, NULL, 'Туман ДСИ бошлиғи', 'А.Усмонов', NULL, '', 1, '2017-09-14');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_tomorqa`
--

CREATE TABLE IF NOT EXISTS `tbl_tomorqa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_fuqaro_uy_id` int(11) DEFAULT NULL,
  `umumiy_yer_maydoni` varchar(255) DEFAULT NULL,
  `yer_ekin_maydoni` varchar(255) DEFAULT NULL,
  `yer_ekin_xolati` varchar(255) DEFAULT NULL,
  `issiq_xona_mavjud_id` int(11) DEFAULT NULL,
  `issiq_xona_ekin_nomi_soni` varchar(255) DEFAULT NULL,
  `yer_foy_dalanish_xolati` varchar(255) DEFAULT NULL,
  `ekilgan_ekin_maydoni` varchar(255) DEFAULT NULL,
  `kartoshka` varchar(255) DEFAULT NULL,
  `piyoz` varchar(255) DEFAULT NULL,
  `sarimsoq` varchar(255) DEFAULT NULL,
  `sabzi` varchar(255) DEFAULT NULL,
  `pomidor` varchar(255) DEFAULT NULL,
  `bodiring` varchar(255) DEFAULT NULL,
  `balgariskiy` varchar(255) DEFAULT NULL,
  `baqlajon` varchar(255) DEFAULT NULL,
  `loviya` varchar(255) DEFAULT NULL,
  `mosh` varchar(255) DEFAULT NULL,
  `makka` varchar(255) DEFAULT NULL,
  `kokat` varchar(255) DEFAULT NULL,
  `beda` varchar(255) DEFAULT NULL,
  `tamaki` varchar(255) DEFAULT NULL,
  `ekin_boshqalar` varchar(255) DEFAULT NULL,
  `ekilgan_meva_soni` int(11) DEFAULT NULL,
  `yongoq_soni` int(11) DEFAULT NULL,
  `yongoq_yoshi` varchar(255) DEFAULT NULL,
  `jiyda_soni` varchar(255) DEFAULT NULL,
  `jiyda_yoshi` varchar(255) DEFAULT NULL,
  `uzum_soni` varchar(255) DEFAULT NULL,
  `uzum_yoshi` varchar(255) DEFAULT NULL,
  `shaftoli_soni` varchar(255) DEFAULT NULL,
  `shaftoli_yoshi` varchar(255) DEFAULT NULL,
  `olma_soni` varchar(255) DEFAULT NULL,
  `olm_yoshi` varchar(255) DEFAULT NULL,
  `anor_soni` varchar(255) DEFAULT NULL,
  `anor_yoshi` varchar(255) DEFAULT NULL,
  `nok_soni` varchar(255) DEFAULT NULL,
  `nok_yoshi` varchar(255) DEFAULT NULL,
  `orik_soni` varchar(255) DEFAULT NULL,
  `orik_yoshi` varchar(255) DEFAULT NULL,
  `anjir_soni` varchar(255) DEFAULT NULL,
  `anjiir_yoshi` varchar(255) DEFAULT NULL,
  `bexi_soni` varchar(255) DEFAULT NULL,
  `bexi_yoshi` varchar(255) DEFAULT NULL,
  `gilos_soni` varchar(255) DEFAULT NULL,
  `gilos_yoshi` varchar(255) DEFAULT NULL,
  `sliva_soni` varchar(255) DEFAULT NULL,
  `sliva_yoshi` varchar(255) DEFAULT NULL,
  `humro_soni` varchar(255) DEFAULT NULL,
  `hurmo_yoshi` varchar(255) DEFAULT NULL,
  `bodom_soni` varchar(255) DEFAULT NULL,
  `bodom_yoshi` varchar(255) DEFAULT NULL,
  `terak_soni` varchar(255) DEFAULT NULL,
  `terak_yoshi` varchar(255) DEFAULT NULL,
  `tol_soni` varchar(255) DEFAULT NULL,
  `tol_yoshi` varchar(255) DEFAULT NULL,
  `boshqa_ekinlar` varchar(255) DEFAULT NULL,
  `anketa_ozgarish` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_fuqaro_uy_id` (`tbl_fuqaro_uy_id`),
  KEY `issiq_xona_mavjud_id` (`issiq_xona_mavjud_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_tomorqa`
--

INSERT INTO `tbl_tomorqa` (`id`, `tbl_fuqaro_uy_id`, `umumiy_yer_maydoni`, `yer_ekin_maydoni`, `yer_ekin_xolati`, `issiq_xona_mavjud_id`, `issiq_xona_ekin_nomi_soni`, `yer_foy_dalanish_xolati`, `ekilgan_ekin_maydoni`, `kartoshka`, `piyoz`, `sarimsoq`, `sabzi`, `pomidor`, `bodiring`, `balgariskiy`, `baqlajon`, `loviya`, `mosh`, `makka`, `kokat`, `beda`, `tamaki`, `ekin_boshqalar`, `ekilgan_meva_soni`, `yongoq_soni`, `yongoq_yoshi`, `jiyda_soni`, `jiyda_yoshi`, `uzum_soni`, `uzum_yoshi`, `shaftoli_soni`, `shaftoli_yoshi`, `olma_soni`, `olm_yoshi`, `anor_soni`, `anor_yoshi`, `nok_soni`, `nok_yoshi`, `orik_soni`, `orik_yoshi`, `anjir_soni`, `anjiir_yoshi`, `bexi_soni`, `bexi_yoshi`, `gilos_soni`, `gilos_yoshi`, `sliva_soni`, `sliva_yoshi`, `humro_soni`, `hurmo_yoshi`, `bodom_soni`, `bodom_yoshi`, `terak_soni`, `terak_yoshi`, `tol_soni`, `tol_yoshi`, `boshqa_ekinlar`, `anketa_ozgarish`, `user_id`, `user_date`) VALUES
(1, 11, '0.2', '0.1', '2', 1, '', '0.1', '0.1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 5, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_transport_qatnovi`
--

CREATE TABLE IF NOT EXISTS `tbl_transport_qatnovi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `xonadonga_yaqin_tran_qatnov_masofa` varchar(255) DEFAULT NULL,
  `xonadondan_mahalla_markaz_masofa` varchar(255) DEFAULT NULL,
  `xonadon_mahalla_trans_soni` int(255) DEFAULT NULL,
  `xonadon_tuman_masofa` varchar(255) DEFAULT NULL,
  `xonadon_tuman_trans_soni` int(255) DEFAULT NULL,
  `xonadon_viloyat_masofa` varchar(255) DEFAULT NULL,
  `xonadon_viloyat_trans_soni` int(255) DEFAULT NULL,
  `yol_kira_mahalla` varchar(255) DEFAULT NULL,
  `yol_kira_tuman` varchar(255) DEFAULT NULL,
  `yol_kira_viloyat` varchar(255) DEFAULT NULL,
  `fuqaro_uy_id` int(11) DEFAULT NULL,
  `anketa_ozgarish` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fuqaro_uy_id` (`fuqaro_uy_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_transport_qatnovi`
--

INSERT INTO `tbl_transport_qatnovi` (`id`, `xonadonga_yaqin_tran_qatnov_masofa`, `xonadondan_mahalla_markaz_masofa`, `xonadon_mahalla_trans_soni`, `xonadon_tuman_masofa`, `xonadon_tuman_trans_soni`, `xonadon_viloyat_masofa`, `xonadon_viloyat_trans_soni`, `yol_kira_mahalla`, `yol_kira_tuman`, `yol_kira_viloyat`, `fuqaro_uy_id`, `anketa_ozgarish`, `user_id`, `user_date`) VALUES
(1, '500м', '2км', 0, '5км', 1, '30км', 2, '0', '500', '3000', 11, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_tuman`
--

CREATE TABLE IF NOT EXISTS `tbl_tuman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tuman_nomi` varchar(255) DEFAULT NULL,
  `viloyat_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `viloyat_id` (`viloyat_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=418 ;

--
-- Дамп данных таблицы `tbl_tuman`
--

INSERT INTO `tbl_tuman` (`id`, `tuman_nomi`, `viloyat_id`, `user_id`, `user_date`) VALUES
(1, 'Нукус', 1, NULL, NULL),
(2, 'Бeруний тумани', 1, NULL, NULL),
(3, 'Хўжайли тумани', 1, NULL, NULL),
(4, 'Чимбой тумани', 1, NULL, NULL),
(5, 'Амударё тумани', 1, NULL, NULL),
(6, 'Мўйноқ тумани', 1, NULL, NULL),
(7, 'Тўрткўл тумани', 1, NULL, NULL),
(8, 'Шуманай тумани', 1, NULL, NULL),
(9, 'Қунғирот тумани', 1, NULL, NULL),
(10, 'Eлликқала тумани', 1, NULL, NULL),
(11, 'Бузатов тумани', 1, NULL, NULL),
(12, 'Кeгeйли тумани', 1, NULL, NULL),
(13, 'Конликўл тумани', 1, NULL, NULL),
(14, 'Қораузақ тумани', 1, NULL, NULL),
(15, 'Тахтакўпир тумани', 1, NULL, NULL),
(20, 'Андижон тумани', 2, NULL, NULL),
(21, 'Асака тумани', 2, NULL, NULL),
(22, 'Шаҳрихон тумани', 2, NULL, NULL),
(23, 'Қўрғонтeпа тумани', 2, NULL, NULL),
(24, 'Жалолқудуқ тумани', 2, NULL, NULL),
(25, 'Марҳамат тумани', 2, NULL, NULL),
(26, 'Пахтаобод тумани', 2, NULL, NULL),
(27, 'Избоскан тумани', 2, NULL, NULL),
(28, 'Бўз тумани', 2, NULL, NULL),
(29, 'Хўжаобод тумани', 2, NULL, NULL),
(30, 'Балиқчи тумани', 2, NULL, NULL),
(31, 'Булоқбоши тумани', 2, NULL, NULL),
(32, 'Улуғнор тумани', 2, NULL, NULL),
(33, 'Олтинкўл тумани', 2, NULL, NULL),
(40, 'Наманган тумани', 3, NULL, NULL),
(41, 'Косонсой тумани', 3, NULL, NULL),
(42, 'Норин тумани', 3, NULL, NULL),
(43, 'Учқўрғон тумани', 3, NULL, NULL),
(44, 'Чортоқ тумани', 3, NULL, NULL),
(45, 'Чуст тумани', 3, NULL, NULL),
(46, 'Поп тумани', 3, NULL, NULL),
(47, 'Тўрақўрғон тумани', 3, NULL, NULL),
(48, 'Янгиқўрғон тумани', 3, NULL, NULL),
(49, 'Мингбулоқ тумани', 3, NULL, NULL),
(50, 'Уйчи тумани', 3, NULL, NULL),
(51, 'Давлатобод тумани', 3, NULL, NULL),
(60, 'Фарғона тумани', 4, NULL, NULL),
(61, 'Данғара тумани', 4, NULL, NULL),
(62, 'Охунбобоeв тумани', 4, NULL, NULL),
(63, 'Олтиариқ тумани', 4, NULL, NULL),
(64, 'Қува тумани', 4, NULL, NULL),
(65, 'Бeшариқ тумани', 4, NULL, NULL),
(66, 'Риштон тумани', 4, NULL, NULL),
(67, 'Ўзбeкистон тумани', 4, NULL, NULL),
(68, 'Боғдод тумани', 4, NULL, NULL),
(69, 'Ёзёвон тумани', 4, NULL, NULL),
(70, 'Сўх тумани', 4, NULL, NULL),
(71, 'Тошлоқ тумани', 4, NULL, NULL),
(72, 'Бувайда тумани', 4, NULL, NULL),
(73, 'Учкўприк тумани', 4, NULL, NULL),
(74, 'Фурқат тумани', 4, NULL, NULL),
(75, 'Киргули тумани', 4, NULL, NULL),
(80, 'Бухоро тумани', 5, NULL, NULL),
(81, 'Қоровулбозор тумани', 5, NULL, NULL),
(82, 'Когон тумани', 5, NULL, NULL),
(83, 'Ғиждувон тумани', 5, NULL, NULL),
(84, 'Ромитан тумани', 5, NULL, NULL),
(85, 'Вобкeнт тумани', 5, NULL, NULL),
(86, 'Жондор тумани', 5, NULL, NULL),
(87, 'Олот тумани', 5, NULL, NULL),
(88, 'Шофиркон тумани', 5, NULL, NULL),
(89, 'Қоракўл тумани', 5, NULL, NULL),
(90, 'Пeшку тумани', 5, NULL, NULL),
(100, 'Урганч тумани', 6, NULL, NULL),
(101, 'Хива тумани', 6, NULL, NULL),
(102, 'Хазорасп тумани', 6, NULL, NULL),
(103, 'Гурлан тумани', 6, NULL, NULL),
(104, 'Хонқа тумани', 6, NULL, NULL),
(105, 'Шовот тумани', 6, NULL, NULL),
(106, 'Янгибозор тумани', 6, NULL, NULL),
(107, 'Қўшкўпир тумани', 6, NULL, NULL),
(108, 'Боғот тумани', 6, NULL, NULL),
(109, 'Янгиариқ тумани', 6, NULL, NULL),
(110, 'Питняк тумани', 6, NULL, NULL),
(120, 'Тeрмиз тумани', 7, NULL, NULL),
(121, 'Дeнов тумани', 7, NULL, NULL),
(122, 'Сариосиё тумани', 7, NULL, NULL),
(123, 'Ангор тумани', 7, NULL, NULL),
(124, 'Бойсун тумани', 7, NULL, NULL),
(125, 'Жарқўрғон тумани', 7, NULL, NULL),
(126, 'Узун тумани', 7, NULL, NULL),
(127, 'Шeробод тумани', 7, NULL, NULL),
(128, 'Шўрчи тумани', 7, NULL, NULL),
(129, 'Қумқўрғон тумани', 7, NULL, NULL),
(130, 'Музробот тумани', 7, NULL, NULL),
(131, 'Қизириқ тумани', 7, NULL, NULL),
(132, 'Бандихон тумани', 7, NULL, NULL),
(133, 'Олтинсой тумани', 7, NULL, NULL),
(140, 'Қарши тумани', 8, NULL, NULL),
(141, 'Китоб тумани', 8, NULL, NULL),
(142, 'Косон тумани', 8, NULL, NULL),
(143, 'Муборак тумани', 8, NULL, NULL),
(144, 'Шаҳрисабз тумани', 8, NULL, NULL),
(145, 'Нишон тумани', 8, NULL, NULL),
(146, 'Чироқчи тумани', 8, NULL, NULL),
(147, 'Яккабоғ тумани', 8, NULL, NULL),
(148, 'Қамаши тумани', 8, NULL, NULL),
(149, 'Ғузор тумани', 8, NULL, NULL),
(150, 'Касби тумани', 8, NULL, NULL),
(151, 'Баҳористон тумани', 8, NULL, NULL),
(152, 'Дeҳқонобод тумани', 8, NULL, NULL),
(153, 'Усмон Юсупов тумани', 8, NULL, NULL),
(154, 'Миришкор Тумани', 8, NULL, NULL),
(160, 'Жиззах тумани', 9, NULL, NULL),
(161, 'Ғаллаорол тумани', 9, NULL, NULL),
(162, 'Зомин тумани', 9, NULL, NULL),
(163, 'Дўстлик тумани', 9, NULL, NULL),
(164, 'Мирзачўл тумани', 9, NULL, NULL),
(165, 'Пахтакор тумани', 9, NULL, NULL),
(166, 'Бахмал тумани', 9, NULL, NULL),
(167, 'Зарбдор тумани', 9, NULL, NULL),
(168, 'Фориш тумани', 9, NULL, NULL),
(169, 'Арнасой тумани', 9, NULL, NULL),
(170, 'Зафаробод тумани', 9, NULL, NULL),
(171, 'Янгиобод Чирчиқ т', 9, NULL, NULL),
(180, 'Навоий тумани', 10, NULL, NULL),
(181, 'Томди тумани', 10, NULL, NULL),
(182, 'Учқудуқ тумани', 10, NULL, NULL),
(183, 'Нурота тумани', 10, NULL, NULL),
(184, 'Қизилтeпа тумани', 10, NULL, NULL),
(185, 'Хатирчи тумани', 10, NULL, NULL),
(186, 'Конимeх тумани', 10, NULL, NULL),
(187, 'Навбаҳор тумани', 10, NULL, NULL),
(200, 'Самарқанд тумани', 11, NULL, NULL),
(201, 'Каттақўрғон тумани', 11, NULL, NULL),
(202, 'Нарпай тумани', 11, NULL, NULL),
(203, 'Ургут тумани', 11, NULL, NULL),
(204, 'Булунғур тумани', 11, NULL, NULL),
(205, 'Жомбой тумани', 11, NULL, NULL),
(207, 'Иштихон тумани', 11, NULL, NULL),
(208, 'Нуробод тумани', 11, NULL, NULL),
(209, 'Паст-Дарғом тумани', 11, NULL, NULL),
(210, 'Чeлак тумани', 11, NULL, NULL),
(211, 'Оқдарё тумани', 11, NULL, NULL),
(212, 'Пахтачи тумани', 11, NULL, NULL),
(213, 'Гўзалкeнт тумани', 11, NULL, NULL),
(214, 'Пояриқ тумани', 11, NULL, NULL),
(215, 'Тойлоқ тумани', 11, NULL, NULL),
(216, 'Қўшработ тумани', 11, NULL, NULL),
(220, 'Сирдарё тумани', 12, NULL, NULL),
(221, 'Ховос тумани', 12, NULL, NULL),
(222, 'Гулистон тумани', 12, NULL, NULL),
(223, 'Сайхунобод тумани', 12, NULL, NULL),
(224, 'Шароф Рашидов тума', 12, NULL, NULL),
(225, 'Боёвут тумани', 12, NULL, NULL),
(226, 'Мирзаобод тумани', 12, NULL, NULL),
(227, 'Мeҳнатобод тумани', 12, NULL, NULL),
(228, 'Оқолтин тумани', 12, NULL, NULL),
(229, 'Сардоба', 12, NULL, NULL),
(240, 'Қибрай тумани', 13, NULL, NULL),
(241, 'Оҳангарон тумани', 13, NULL, NULL),
(242, 'Бeкобод тумани', 13, NULL, NULL),
(243, 'Пскeнт тумани', 13, NULL, NULL),
(244, 'Янгийўл тумани', 13, NULL, NULL),
(245, 'Бўстонлиқ тумани', 13, NULL, NULL),
(246, 'Бўка тумани', 13, NULL, NULL),
(247, 'Оққўрғон тумани', 13, NULL, NULL),
(248, 'Паркeнт тумани', 13, NULL, NULL),
(249, 'Тошкeнт тумани', 13, NULL, NULL),
(250, 'Чиноз тумани', 13, NULL, NULL),
(251, 'Ўрта Чирчиқ туман', 13, NULL, NULL),
(252, 'Қуйи Чирчиқ тумани', 13, NULL, NULL),
(253, 'Зангиота тумани', 13, NULL, NULL),
(254, 'Юқори Чирчиқ тума', 13, NULL, NULL),
(404, 'Учтeпа тумани', 14, NULL, NULL),
(405, 'Бeктeмир тумани', 14, NULL, NULL),
(406, 'Миробод тумани', 14, NULL, NULL),
(407, 'Мирзо Улуғбeк туман', 14, NULL, NULL),
(408, 'Сирғали тумани', 14, NULL, NULL),
(409, 'Собир Раҳимов тумани', 14, NULL, NULL),
(410, 'Чилонзор тумани', 14, NULL, NULL),
(411, 'Шайхонтоҳур тумани', 14, NULL, NULL),
(412, 'Юнусобод тумани', 14, NULL, NULL),
(413, 'Яккасарой тумани', 14, NULL, NULL),
(414, 'Ҳамза тумани', 14, NULL, NULL),
(416, 'Қирғизистон', 2, NULL, NULL),
(417, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_tumanq`
--

CREATE TABLE IF NOT EXISTS `tbl_tumanq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tuman_nomi` varchar(255) DEFAULT NULL,
  `viloyat_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=417 ;

--
-- Дамп данных таблицы `tbl_tumanq`
--

INSERT INTO `tbl_tumanq` (`id`, `tuman_nomi`, `viloyat_id`) VALUES
(0, NULL, NULL),
(1, 'Nukus tumani', 1),
(2, 'Beruniy tumani', 1),
(3, 'Xo''jayli tumani', 1),
(4, 'Chimboy tumani', 1),
(5, 'Amudaryo tumani', 1),
(6, 'Mo''ynoq tumani', 1),
(7, 'To''rtko''l tumani', 1),
(8, 'Shumanay tumani', 1),
(9, 'Qung''irot tumani', 1),
(10, 'Ellikqala tumani', 1),
(11, 'Buzatov tumani', 1),
(12, 'Kegeyli tumani', 1),
(13, 'Konliko''l tumani', 1),
(14, 'Qorauzaq tumani', 1),
(15, 'Taxtako''pir tumani', 1),
(20, 'Andijon tumani', 2),
(21, 'Asaka tumani', 2),
(22, 'Shahrixon tumani', 2),
(23, 'Qo''rg''ontepa tumani', 2),
(24, 'Jalolquduq tumani', 2),
(25, 'Marhamat tumani', 2),
(26, 'Paxtaobod tumani', 2),
(27, 'Izboskan tumani', 2),
(28, 'Bo''z tumani', 2),
(29, 'Xo''jaobod tumani', 2),
(30, 'Baliqchi tumani', 2),
(31, 'Buloqboshi tumani', 2),
(32, 'Ulug''nor tumani', 2),
(33, 'Oltinko''l tumani', 2),
(40, 'Namangan tumani', 3),
(41, 'Kosonsoy tumani', 3),
(42, 'Norin tumani', 3),
(43, 'Uchqo''rg''on tumani', 3),
(44, 'Chortoq tumani', 3),
(45, 'Chust tumani', 3),
(46, 'Pop tumani', 3),
(47, 'To''raqo''rg''on tumani', 3),
(48, 'Yangiqo''rg''on tumani', 3),
(49, 'Mingbuloq tumani', 3),
(50, 'Uychi tumani', 3),
(51, 'Davlatobod tumani', 3),
(60, 'Farg''ona tumani', 4),
(61, 'Dang''ara tumani', 4),
(62, 'Oxunboboev tumani', 4),
(63, 'Oltiariq tumani', 4),
(64, 'Quva tumani', 4),
(65, 'Beshariq tumani', 4),
(66, 'Rishton tumani', 4),
(67, 'O''zbekiston tumani', 4),
(68, 'Bog''dod tumani', 4),
(69, 'Yozyovon tumani', 4),
(70, 'So''x tumani', 4),
(71, 'Toshloq tumani', 4),
(72, 'Buvayda tumani', 4),
(73, 'Uchko''prik tumani', 4),
(74, 'Furqat tumani', 4),
(75, 'Kirguli tumani', 4),
(80, 'Buxoro tumani', 5),
(81, 'Qorovulbozor tumani', 5),
(82, 'Kogon tumani', 5),
(83, 'G''ijduvon tumani', 5),
(84, 'Romitan tumani', 5),
(85, 'Vobkent tumani', 5),
(86, 'Jondor tumani', 5),
(87, 'Olot tumani', 5),
(88, 'Shofirkon tumani', 5),
(89, 'Qorako''l tumani', 5),
(90, 'Peshku tumani', 5),
(100, 'Urganch tumani', 6),
(101, 'Xiva tumani', 6),
(102, 'Xazorasp tumani', 6),
(103, 'Gurlan tumani', 6),
(104, 'Xonqa tumani', 6),
(105, 'Shovot tumani', 6),
(106, 'Yangibozor tumani', 6),
(107, 'Qo''shko''pir tumani', 6),
(108, 'Bog''ot tumani', 6),
(109, 'Yangiariq tumani', 6),
(110, 'Pitnyak tumani', 6),
(120, 'Termiz tumani', 7),
(121, 'Denov tumani', 7),
(122, 'Sariosiyo tumani', 7),
(123, 'Angor tumani', 7),
(124, 'Boysun tumani', 7),
(125, 'Jarqo''rg''on tumani', 7),
(126, 'Uzun tumani', 7),
(127, 'Sherobod tumani', 7),
(128, 'Sho''rchi tumani', 7),
(129, 'Qumqo''rg''on tumani', 7),
(130, 'Muzrobot tumani', 7),
(131, 'Qiziriq tumani', 7),
(132, 'Bandixon tumani', 7),
(133, 'Oltinsoy tumani', 7),
(140, 'Qarshi tumani', 8),
(141, 'Kitob tumani', 8),
(142, 'Koson tumani', 8),
(143, 'Muborak tumani', 8),
(144, 'Shahrisabz tumani', 8),
(145, 'Nishon tumani', 8),
(146, 'Chiroqchi tumani', 8),
(147, 'Yakkabog'' tumani', 8),
(148, 'Qamashi tumani', 8),
(149, 'G''uzor tumani', 8),
(150, 'Kasbi tumani', 8),
(151, 'Bahoriston tumani', 8),
(152, 'Dehqonobod tumani', 8),
(153, 'Usmon Yusupov tumani', 8),
(154, 'Mirishkor Tumani', 8),
(160, 'Jizzax tumani', 9),
(161, 'G''allaorol tumani', 9),
(162, 'Zomin tumani', 9),
(163, 'Do''stlik tumani', 9),
(164, 'Mirzacho''l tumani', 9),
(165, 'Paxtakor tumani', 9),
(166, 'Baxmal tumani', 9),
(167, 'Zarbdor tumani', 9),
(168, 'Forish tumani', 9),
(169, 'Arnasoy tumani', 9),
(170, 'Zafarobod tumani', 9),
(171, 'Yangiobod Chirchiq t', 9),
(180, 'Navoiy tumani', 10),
(181, 'Tomdi tumani', 10),
(182, 'Uchquduq tumani', 10),
(183, 'Nurota tumani', 10),
(184, 'Qiziltepa tumani', 10),
(185, 'Xatirchi tumani', 10),
(186, 'Konimex tumani', 10),
(187, 'Navbahor tumani', 10),
(200, 'Samarqand tumani', 11),
(201, 'Kattaqo''rg''on tumani', 11),
(202, 'Narpay tumani', 11),
(203, 'Urgut tumani', 11),
(204, 'Bulung''ur tumani', 11),
(205, 'Jomboy tumani', 11),
(207, 'Ishtixon tumani', 11),
(208, 'Nurobod tumani', 11),
(209, 'Past-Darg''om tumani', 11),
(210, 'Chelak tumani', 11),
(211, 'Oqdaryo tumani', 11),
(212, 'Paxtachi tumani', 11),
(213, 'Go''zalkent tumani', 11),
(214, 'Poyariq tumani', 11),
(215, 'Toyloq tumani', 11),
(216, 'Qo''shrabot tumani', 11),
(220, 'Sirdaryo tumani', 12),
(221, 'Xovos tumani', 12),
(222, 'Guliston tumani', 12),
(223, 'Sayxunobod tumani', 12),
(224, 'Sharof Rashidov tuma', 12),
(225, 'Boyovut tumani', 12),
(226, 'Mirzaobod tumani', 12),
(227, 'Mehnatobod tumani', 12),
(228, 'Oqoltin tumani', 12),
(229, 'Sardoba', 12),
(240, 'Qibray tumani', 13),
(241, 'Ohangaron tumani', 13),
(242, 'Bekobod tumani', 13),
(243, 'Pskent tumani', 13),
(244, 'Yangiyo''l tumani', 13),
(245, 'Bo''stonliq tumani', 13),
(246, 'Bo''ka tumani', 13),
(247, 'Oqqo''rg''on tumani', 13),
(248, 'Parkent tumani', 13),
(249, 'Toshkent tumani', 13),
(250, 'Chinoz tumani', 13),
(251, 'O''rta Chirchiq tuman', 13),
(252, 'Quyi Chirchiq tumani', 13),
(253, 'Zangiota tumani', 13),
(254, 'Yuqori Chirchiq tuma', 13),
(404, 'Uchtepa tumani', 14),
(405, 'Bektemir tumani', 14),
(406, 'Mirobod tumani', 14),
(407, 'Mirzo Ulug''bek tuman', 14),
(408, 'Sirg''ali tumani', 14),
(409, 'Sobir Rahimov tumani', 14),
(410, 'Chilonzor tumani', 14),
(411, 'Shayxontohur tumani', 14),
(412, 'Yunusobod tumani', 14),
(413, 'Yakkasaroy tumani', 14),
(414, 'Hamza tumani', 14),
(416, 'Хўжаобод туман', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `viloyat` int(11) DEFAULT NULL,
  `tuman` int(11) DEFAULT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `role` tinyint(3) NOT NULL DEFAULT '10',
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  `tasgkilot_id` int(11) DEFAULT NULL,
  `mfy_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `user_id` (`user_id`),
  KEY `tbl_user_ibfk_3` (`viloyat`),
  KEY `tbl_user_ibfk_4` (`tuman`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Дамп данных таблицы `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `viloyat`, `tuman`, `username`, `password`, `email`, `role`, `user_id`, `user_date`, `tasgkilot_id`, `mfy_id`) VALUES
(1, 2, 29, 'admin', '0ebcc77dc72360d0eb8e9504c78d38bd', 'm@m.uz', 0, NULL, '2017-12-09', NULL, NULL),
(2, 2, 29, 'kotiba', '81dc9bdb52d04dc20036dbd8313ed055', '', 1, NULL, NULL, NULL, NULL),
(3, 2, 29, 'oddiy', '81dc9bdb52d04dc20036dbd8313ed055', '', 2, NULL, NULL, NULL, NULL),
(4, 2, 29, 'inspektor', '81dc9bdb52d04dc20036dbd8313ed055', '', 3, NULL, NULL, 2, NULL),
(11, 2, 29, 'hp', '81dc9bdb52d04dc20036dbd8313ed055', 'dcdf@dddd', 3, NULL, NULL, NULL, NULL),
(12, 2, 29, 'prokuror', '81dc9bdb52d04dc20036dbd8313ed055', 'prakuror@m.uz', 4, 1, '2017-09-14', 1, NULL),
(13, 2, 29, 'agrobank', '81dc9bdb52d04dc20036dbd8313ed055', 'prakuror@m.uz', 4, 1, '2017-09-14', 9, NULL),
(14, 2, 29, 'yangi', '81dc9bdb52d04dc20036dbd8313ed055', '', 2, 1, NULL, NULL, 35),
(15, 2, 29, 'ko''prikboshi', '81dc9bdb52d04dc20036dbd8313ed055', 'ko''prikboshi@umail.uz', 2, 1, '2017-10-25', NULL, 40),
(16, 2, 29, 'prokuratura', '81dc9bdb52d04dc20036dbd8313ed055', 'ko''prikboshi@umail.uz', 5, 1, '2017-12-05', 1, 2),
(19, 2, 29, 'iib', '81dc9bdb52d04dc20036dbd8313ed055', 'ko''prikboshi@umail.uz', 5, 1, '2017-12-18', 2, NULL),
(21, 2, 29, 'andijon', '81dc9bdb52d04dc20036dbd8313ed055', 'ko''prikboshi@umail.uz', 2, 1, NULL, NULL, 19);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_viloyatlar`
--

CREATE TABLE IF NOT EXISTS `tbl_viloyatlar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `viloyat_nomi` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `tbl_viloyatlar`
--

INSERT INTO `tbl_viloyatlar` (`id`, `viloyat_nomi`, `user_id`, `user_date`) VALUES
(1, 'Қорақлпоғистон Рeсп', NULL, NULL),
(2, 'Андижон Вилояти', NULL, NULL),
(3, 'Наманган Вилояти', NULL, NULL),
(4, 'Фарғона Вилояти', NULL, NULL),
(5, 'Бухоро Вилояти', NULL, NULL),
(6, 'Хоразм Вилояти', NULL, NULL),
(7, 'Сурхандарё Вилояти', NULL, NULL),
(8, 'Қашқадарё Вилояти', NULL, NULL),
(9, 'Жиззах Вилояти', NULL, NULL),
(10, 'Навоий Вилояти', NULL, NULL),
(11, 'Самарқанд Вилояти', NULL, NULL),
(12, 'Сирдарё Вилояти', NULL, NULL),
(13, 'Тошкeнт Вилояти', NULL, NULL),
(14, 'Тошкeнт Шаҳри', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_viloyatlar1`
--

CREATE TABLE IF NOT EXISTS `tbl_viloyatlar1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `viloyat_nomi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `tbl_viloyatlar1`
--

INSERT INTO `tbl_viloyatlar1` (`id`, `viloyat_nomi`) VALUES
(1, 'Qoraqlpog''iston Resp'),
(2, 'Andijon Viloyati'),
(3, 'Namangan Viloyati'),
(4, 'Farg''ona Viloyati'),
(5, 'Buxoro Viloyati'),
(6, 'Xorazm Viloyati'),
(7, 'Surxandaryo Viloyati'),
(8, 'Qashqadaryo Viloyati'),
(9, 'Jizzax Viloyati'),
(10, 'Navoiy Viloyati'),
(11, 'Samarqand Viloyati'),
(12, 'Sirdaryo Viloyati'),
(13, 'Toshkent Viloyati'),
(14, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_ximzat_korsatish`
--

CREATE TABLE IF NOT EXISTS `tbl_ximzat_korsatish` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `xondonga_yaqin_maktab_id` int(11) DEFAULT NULL,
  `xondonga_yaqin_maktab_masofa` varchar(255) DEFAULT NULL,
  `xonadonga_yaqin_bogcha_id` int(11) DEFAULT NULL,
  `xonadonga_yaqin_bogcha_masofa` varchar(255) DEFAULT NULL,
  `xonadonga_yaqin_kollej_nomi` varchar(255) DEFAULT NULL,
  `xonadonga_yaqin_kollej_masofa` varchar(255) DEFAULT NULL,
  `xonadonga_yaqin_savdo_obyekt_masofa` varchar(255) DEFAULT NULL,
  `xonadonga_yaqin_xizmat_korsatish_masofa` varchar(255) DEFAULT NULL,
  `xonadonga_yaqin_bozorcha_masofa` varchar(255) DEFAULT NULL,
  `xonadonga_yaqin_xamom_masofa` varchar(255) DEFAULT NULL,
  `xonadonga_yaqin_sport_majmuasi_masofa` varchar(255) DEFAULT NULL,
  `xonadonga_yaqin_togarak_obyekt_masofa` varchar(255) DEFAULT NULL,
  `xonadonga_yaqin_bogacha_masofa` varchar(255) DEFAULT NULL,
  `tbl_fuqaro_uy_id` int(11) DEFAULT NULL,
  `istiroxat_bog` varchar(255) DEFAULT NULL,
  `istiroxat_bog_masofa` varchar(255) DEFAULT NULL,
  `anketa_ozgarish` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_fuqaro_uy_id` (`tbl_fuqaro_uy_id`),
  KEY `xondonga_yaqin_maktab_id` (`xondonga_yaqin_maktab_id`),
  KEY `xonadonga_yaqin_bogcha_id` (`xonadonga_yaqin_bogcha_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_ximzat_korsatish`
--

INSERT INTO `tbl_ximzat_korsatish` (`id`, `xondonga_yaqin_maktab_id`, `xondonga_yaqin_maktab_masofa`, `xonadonga_yaqin_bogcha_id`, `xonadonga_yaqin_bogcha_masofa`, `xonadonga_yaqin_kollej_nomi`, `xonadonga_yaqin_kollej_masofa`, `xonadonga_yaqin_savdo_obyekt_masofa`, `xonadonga_yaqin_xizmat_korsatish_masofa`, `xonadonga_yaqin_bozorcha_masofa`, `xonadonga_yaqin_xamom_masofa`, `xonadonga_yaqin_sport_majmuasi_masofa`, `xonadonga_yaqin_togarak_obyekt_masofa`, `xonadonga_yaqin_bogacha_masofa`, `tbl_fuqaro_uy_id`, `istiroxat_bog`, `istiroxat_bog_masofa`, `anketa_ozgarish`, `user_id`, `user_date`) VALUES
(1, 10, '', 2, '', 'КХК Енгил саноат', '', '', '', '', '', '', '', NULL, 11, '', '', NULL, 15, '2018-01-09');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_xonadon_transport`
--

CREATE TABLE IF NOT EXISTS `tbl_xonadon_transport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `xonadon_transport_soni` int(11) DEFAULT NULL,
  `yengil_avto_rusumi` longtext,
  `yengil_avto_raqami` varchar(255) DEFAULT NULL,
  `yengil_avto_yili` varchar(255) DEFAULT NULL,
  `yengil_avto_tex_pas` varchar(255) DEFAULT NULL,
  `yengi_avto_egasi` varchar(255) DEFAULT NULL,
  `yengil_prvara_raqami` varchar(255) DEFAULT NULL,
  `yengil_sugurta_mavjudligi` varchar(255) DEFAULT NULL,
  `yengil_ishonchnoma` varchar(255) DEFAULT NULL,
  `yuk_avto_rusumi` varchar(255) DEFAULT NULL,
  `yuk_avto_raqami` varchar(255) DEFAULT NULL,
  `yuk_avto_yili` varchar(255) DEFAULT NULL,
  `yuk_avto_tex_pas` varchar(255) DEFAULT NULL,
  `yuk_avto_egasi` varchar(255) DEFAULT NULL,
  `yuk_avto_prva` varchar(255) DEFAULT NULL,
  `yuk_avto_sugurta` varchar(255) DEFAULT NULL,
  `yuk_avto_ishonchnoma` varchar(255) DEFAULT NULL,
  `trans_boshqalar` varchar(255) DEFAULT NULL,
  `fuqaro_uy_id` int(11) DEFAULT NULL,
  `fuqaro_id` int(11) DEFAULT NULL,
  `anketa_ozgarish` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fuqaro_uy_id` (`fuqaro_uy_id`),
  KEY `fuqaro_id` (`fuqaro_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_xonadon_transport`
--

INSERT INTO `tbl_xonadon_transport` (`id`, `xonadon_transport_soni`, `yengil_avto_rusumi`, `yengil_avto_raqami`, `yengil_avto_yili`, `yengil_avto_tex_pas`, `yengi_avto_egasi`, `yengil_prvara_raqami`, `yengil_sugurta_mavjudligi`, `yengil_ishonchnoma`, `yuk_avto_rusumi`, `yuk_avto_raqami`, `yuk_avto_yili`, `yuk_avto_tex_pas`, `yuk_avto_egasi`, `yuk_avto_prva`, `yuk_avto_sugurta`, `yuk_avto_ishonchnoma`, `trans_boshqalar`, `fuqaro_uy_id`, `fuqaro_id`, `anketa_ozgarish`, `user_id`, `user_date`) VALUES
(1, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 11, NULL, NULL, 15, '2018-01-09');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `tbl_aliment`
--
ALTER TABLE `tbl_aliment`
  ADD CONSTRAINT `tbl_aliment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_bino`
--
ALTER TABLE `tbl_bino`
  ADD CONSTRAINT `tbl_bino_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_bogchalar`
--
ALTER TABLE `tbl_bogchalar`
  ADD CONSTRAINT `tbl_bogchalar_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_bolim_iib`
--
ALTER TABLE `tbl_bolim_iib`
  ADD CONSTRAINT `tbl_bolim_iib_ibfk_1` FOREIGN KEY (`viloyat_id`) REFERENCES `tbl_viloyatlar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tbl_daromad`
--
ALTER TABLE `tbl_daromad`
  ADD CONSTRAINT `tbl_daromad_ibfk_1` FOREIGN KEY (`fuqaro_uy_id`) REFERENCES `tbl_fuqaro_uy` (`id`),
  ADD CONSTRAINT `tbl_daromad_ibfk_2` FOREIGN KEY (`fuqaro_id`) REFERENCES `tbl_fuqaro` (`id`),
  ADD CONSTRAINT `tbl_daromad_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_daromad_turi`
--
ALTER TABLE `tbl_daromad_turi`
  ADD CONSTRAINT `tbl_daromad_turi_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_davlat`
--
ALTER TABLE `tbl_davlat`
  ADD CONSTRAINT `tbl_davlat_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_deo`
--
ALTER TABLE `tbl_deo`
  ADD CONSTRAINT `tbl_deo_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_fuqaro`
--
ALTER TABLE `tbl_fuqaro`
  ADD CONSTRAINT `tbl_fuqaro_ibfk_10` FOREIGN KEY (`malumoti_id`) REFERENCES `tbl_malumoti` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_ibfk_11` FOREIGN KEY (`tugilgan_joy_viloyat_id`) REFERENCES `tbl_viloyatlar` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_ibfk_12` FOREIGN KEY (`passport_kim_tomonidan_id`) REFERENCES `tbl_tuman` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_ibfk_13` FOREIGN KEY (`tugilgan_joy_tuman_id`) REFERENCES `tbl_tuman` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_ibfk_17` FOREIGN KEY (`davlat_id`) REFERENCES `tbl_davlat` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_ibfk_18` FOREIGN KEY (`uzoq_muddatga_ketgan_davlati`) REFERENCES `tbl_davlat` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_ibfk_22` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_ibfk_23` FOREIGN KEY (`fuqaro_uy_id`) REFERENCES `tbl_fuqaro_uy` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_ibfk_24` FOREIGN KEY (`otasi_id`) REFERENCES `tbl_fuqaro` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_ibfk_25` FOREIGN KEY (`onasi_id`) REFERENCES `tbl_fuqaro` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_ibfk_26` FOREIGN KEY (`deo_azoligi_id`) REFERENCES `tbl_deo` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_ibfk_6` FOREIGN KEY (`jinsi_id`) REFERENCES `tbl_jinsi` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_ibfk_9` FOREIGN KEY (`aliment_id`) REFERENCES `tbl_aliment` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_fuqaro_uy`
--
ALTER TABLE `tbl_fuqaro_uy`
  ADD CONSTRAINT `tbl_fuqaro_uy_ibfk_1` FOREIGN KEY (`mfy_id`) REFERENCES `tbl_mfy` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_uy_ibfk_3` FOREIGN KEY (`ishchi_guruh_fio_id`) REFERENCES `tbl_ishchi_guruh_fio` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_uy_ibfk_4` FOREIGN KEY (`fviloyat_id`) REFERENCES `tbl_viloyatlar` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_uy_ibfk_5` FOREIGN KEY (`ftuman_id`) REFERENCES `tbl_tuman` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_uy_ibfk_6` FOREIGN KEY (`kocha_id`) REFERENCES `tbl_kochalar` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_uy_ibfk_7` FOREIGN KEY (`oila_xolati`) REFERENCES `tbl_notinch` (`id`),
  ADD CONSTRAINT `tbl_fuqaro_uy_ibfk_8` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_gaz`
--
ALTER TABLE `tbl_gaz`
  ADD CONSTRAINT `tbl_gaz_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_hayvon`
--
ALTER TABLE `tbl_hayvon`
  ADD CONSTRAINT `tbl_hayvon_ibfk_1` FOREIGN KEY (`tbl_fuqaro_uy_id`) REFERENCES `tbl_fuqaro_uy` (`id`),
  ADD CONSTRAINT `tbl_hayvon_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_iib`
--
ALTER TABLE `tbl_iib`
  ADD CONSTRAINT `tbl_iib_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_ijtimoiy_muhit`
--
ALTER TABLE `tbl_ijtimoiy_muhit`
  ADD CONSTRAINT `tbl_ijtimoiy_muhit_ibfk_1` FOREIGN KEY (`tbl_fuqaro_uy_id`) REFERENCES `tbl_fuqaro_uy` (`id`),
  ADD CONSTRAINT `tbl_ijtimoiy_muhit_ibfk_2` FOREIGN KEY (`gaz_mavjudligi_id`) REFERENCES `tbl_gaz` (`id`),
  ADD CONSTRAINT `tbl_ijtimoiy_muhit_ibfk_3` FOREIGN KEY (`bino_xolati_id`) REFERENCES `tbl_bino` (`id`),
  ADD CONSTRAINT `tbl_ijtimoiy_muhit_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_inspektor`
--
ALTER TABLE `tbl_inspektor`
  ADD CONSTRAINT `tbl_inspektor_ibfk_1` FOREIGN KEY (`viloyat_id`) REFERENCES `tbl_viloyatlar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_inspektor_ibfk_2` FOREIGN KEY (`tuman_id`) REFERENCES `tbl_tuman` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tbl_inspektor_mfy`
--
ALTER TABLE `tbl_inspektor_mfy`
  ADD CONSTRAINT `tbl_inspektor_mfy_ibfk_4` FOREIGN KEY (`inspektor_id`) REFERENCES `tbl_inspektor` (`id`),
  ADD CONSTRAINT `tbl_inspektor_mfy_ibfk_1` FOREIGN KEY (`viloyat_id`) REFERENCES `tbl_viloyatlar` (`id`),
  ADD CONSTRAINT `tbl_inspektor_mfy_ibfk_2` FOREIGN KEY (`tuman_id`) REFERENCES `tbl_tuman` (`id`),
  ADD CONSTRAINT `tbl_inspektor_mfy_ibfk_3` FOREIGN KEY (`mfy_id`) REFERENCES `tbl_mfy` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_ishchi_guruh_fio`
--
ALTER TABLE `tbl_ishchi_guruh_fio`
  ADD CONSTRAINT `tbl_ishchi_guruh_fio_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`),
  ADD CONSTRAINT `tbl_ishchi_guruh_fio_ibfk_2` FOREIGN KEY (`viloyat_id`) REFERENCES `tbl_viloyatlar` (`id`),
  ADD CONSTRAINT `tbl_ishchi_guruh_fio_ibfk_3` FOREIGN KEY (`tuman_id`) REFERENCES `tbl_tuman` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_issiqxona`
--
ALTER TABLE `tbl_issiqxona`
  ADD CONSTRAINT `tbl_issiqxona_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_kassalik`
--
ALTER TABLE `tbl_kassalik`
  ADD CONSTRAINT `tbl_kassalik_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_kochalar`
--
ALTER TABLE `tbl_kochalar`
  ADD CONSTRAINT `tbl_kochalar_ibfk_1` FOREIGN KEY (`tbl_mfy_id`) REFERENCES `tbl_mfy` (`id`),
  ADD CONSTRAINT `tbl_kochalar_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`),
  ADD CONSTRAINT `tbl_kochalar_ibfk_3` FOREIGN KEY (`viloyat_id`) REFERENCES `tbl_viloyatlar` (`id`),
  ADD CONSTRAINT `tbl_kochalar_ibfk_4` FOREIGN KEY (`tuman_id`) REFERENCES `tbl_tuman` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_kocha_xolati`
--
ALTER TABLE `tbl_kocha_xolati`
  ADD CONSTRAINT `tbl_kocha_xolati_ibfk_1` FOREIGN KEY (`fuqaro_uy_id`) REFERENCES `tbl_fuqaro_uy` (`id`),
  ADD CONSTRAINT `tbl_kocha_xolati_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_kollejlar`
--
ALTER TABLE `tbl_kollejlar`
  ADD CONSTRAINT `tbl_kollejlar_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_maktab`
--
ALTER TABLE `tbl_maktab`
  ADD CONSTRAINT `tbl_maktab_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_malumoti`
--
ALTER TABLE `tbl_malumoti`
  ADD CONSTRAINT `tbl_malumoti_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_manaviy`
--
ALTER TABLE `tbl_manaviy`
  ADD CONSTRAINT `tbl_manaviy_ibfk_1` FOREIGN KEY (`fuqaro_uy_id`) REFERENCES `tbl_fuqaro_uy` (`id`),
  ADD CONSTRAINT `tbl_manaviy_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_mfy`
--
ALTER TABLE `tbl_mfy`
  ADD CONSTRAINT `tbl_mfy_ibfk_1` FOREIGN KEY (`tuman_id`) REFERENCES `tbl_tumanq` (`id`),
  ADD CONSTRAINT `tbl_mfy_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`),
  ADD CONSTRAINT `tbl_mfy_ibfk_3` FOREIGN KEY (`viloyat_id`) REFERENCES `tbl_viloyatlar` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_millati`
--
ALTER TABLE `tbl_millati`
  ADD CONSTRAINT `tbl_millati_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_muammo`
--
ALTER TABLE `tbl_muammo`
  ADD CONSTRAINT `tbl_muammo_ibfk_1` FOREIGN KEY (`tashkilot_id`) REFERENCES `tbl_tashkilot` (`id`),
  ADD CONSTRAINT `tbl_muammo_ibfk_10` FOREIGN KEY (`etiroz`) REFERENCES `tbl_muammo_xolati` (`id`),
  ADD CONSTRAINT `tbl_muammo_ibfk_2` FOREIGN KEY (`fuqaro_id`) REFERENCES `tbl_fuqaro` (`id`),
  ADD CONSTRAINT `tbl_muammo_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`),
  ADD CONSTRAINT `tbl_muammo_ibfk_4` FOREIGN KEY (`muammo_xolati_id`) REFERENCES `tbl_muammo_xolati` (`id`),
  ADD CONSTRAINT `tbl_muammo_ibfk_5` FOREIGN KEY (`viloyat_id`) REFERENCES `tbl_viloyatlar` (`id`),
  ADD CONSTRAINT `tbl_muammo_ibfk_6` FOREIGN KEY (`tuman_id`) REFERENCES `tbl_tuman` (`id`),
  ADD CONSTRAINT `tbl_muammo_ibfk_7` FOREIGN KEY (`mfy_id`) REFERENCES `tbl_mfy` (`id`),
  ADD CONSTRAINT `tbl_muammo_ibfk_8` FOREIGN KEY (`kocha_id`) REFERENCES `tbl_kochalar` (`id`),
  ADD CONSTRAINT `tbl_muammo_ibfk_9` FOREIGN KEY (`fuqaro_uy_id`) REFERENCES `tbl_fuqaro_uy` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_muammo_turlari`
--
ALTER TABLE `tbl_muammo_turlari`
  ADD CONSTRAINT `tbl_muammo_turlari_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_muassasa`
--
ALTER TABLE `tbl_muassasa`
  ADD CONSTRAINT `tbl_muassasa_ibfk_1` FOREIGN KEY (`turi`) REFERENCES `tbl_malumoti` (`id`),
  ADD CONSTRAINT `tbl_muassasa_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_notinch`
--
ALTER TABLE `tbl_notinch`
  ADD CONSTRAINT `tbl_notinch_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_oliygohlar`
--
ALTER TABLE `tbl_oliygohlar`
  ADD CONSTRAINT `tbl_oliygohlar_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_qarz`
--
ALTER TABLE `tbl_qarz`
  ADD CONSTRAINT `tbl_qarz_ibfk_1` FOREIGN KEY (`tbl_fuqaro_uy_id`) REFERENCES `tbl_fuqaro_uy` (`id`),
  ADD CONSTRAINT `tbl_qarz_ibfk_2` FOREIGN KEY (`tbl_fuqaro_id`) REFERENCES `tbl_fuqaro` (`id`),
  ADD CONSTRAINT `tbl_qarz_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_rahbarlar`
--
ALTER TABLE `tbl_rahbarlar`
  ADD CONSTRAINT `tbl_rahbarlar_ibfk_1` FOREIGN KEY (`tuman_id`) REFERENCES `tbl_tuman` (`id`),
  ADD CONSTRAINT `tbl_rahbarlar_ibfk_3` FOREIGN KEY (`viloyat_id`) REFERENCES `tbl_viloyatlar` (`id`),
  ADD CONSTRAINT `tbl_rahbarlar_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_sektor`
--
ALTER TABLE `tbl_sektor`
  ADD CONSTRAINT `tbl_sektor_ibfk_1` FOREIGN KEY (`sektor_mfy_id`) REFERENCES `tbl_mfy` (`id`),
  ADD CONSTRAINT `tbl_sektor_ibfk_2` FOREIGN KEY (`sektor_viloyat_id`) REFERENCES `tbl_viloyatlar` (`id`),
  ADD CONSTRAINT `tbl_sektor_ibfk_4` FOREIGN KEY (`sektor_tuman_id`) REFERENCES `tbl_tuman` (`id`),
  ADD CONSTRAINT `tbl_sektor_ibfk_5` FOREIGN KEY (`sektor_rahbari`) REFERENCES `tbl_rahbarlar` (`id`),
  ADD CONSTRAINT `tbl_sektor_ibfk_6` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_sektorlar`
--
ALTER TABLE `tbl_sektorlar`
  ADD CONSTRAINT `tbl_sektorlar_ibfk_2` FOREIGN KEY (`tuman_id`) REFERENCES `tbl_tuman` (`id`),
  ADD CONSTRAINT `tbl_sektorlar_ibfk_1` FOREIGN KEY (`viloyat_id`) REFERENCES `tbl_viloyatlar` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_shahar`
--
ALTER TABLE `tbl_shahar`
  ADD CONSTRAINT `tbl_shahar_ibfk_1` FOREIGN KEY (`viloyat_id`) REFERENCES `tbl_viloyatlar1` (`id`),
  ADD CONSTRAINT `tbl_shahar_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_shaoriti`
--
ALTER TABLE `tbl_shaoriti`
  ADD CONSTRAINT `tbl_shaoriti_ibfk_1` FOREIGN KEY (`tbl_fuqaro_uy_id`) REFERENCES `tbl_fuqaro_uy` (`id`),
  ADD CONSTRAINT `tbl_shaoriti_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_tashkilot`
--
ALTER TABLE `tbl_tashkilot`
  ADD CONSTRAINT `tbl_tashkilot_ibfk_1` FOREIGN KEY (`tviloyat_id`) REFERENCES `tbl_viloyatlar` (`id`),
  ADD CONSTRAINT `tbl_tashkilot_ibfk_2` FOREIGN KEY (`ttuman_id`) REFERENCES `tbl_tuman` (`id`),
  ADD CONSTRAINT `tbl_tashkilot_ibfk_3` FOREIGN KEY (`tmfy_id`) REFERENCES `tbl_mfy` (`id`),
  ADD CONSTRAINT `tbl_tashkilot_ibfk_4` FOREIGN KEY (`tkocha_id`) REFERENCES `tbl_kochalar` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_tomorqa`
--
ALTER TABLE `tbl_tomorqa`
  ADD CONSTRAINT `tbl_tomorqa_ibfk_1` FOREIGN KEY (`tbl_fuqaro_uy_id`) REFERENCES `tbl_fuqaro_uy` (`id`),
  ADD CONSTRAINT `tbl_tomorqa_ibfk_2` FOREIGN KEY (`issiq_xona_mavjud_id`) REFERENCES `tbl_issiqxona` (`id`),
  ADD CONSTRAINT `tbl_tomorqa_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_transport_qatnovi`
--
ALTER TABLE `tbl_transport_qatnovi`
  ADD CONSTRAINT `tbl_transport_qatnovi_ibfk_1` FOREIGN KEY (`fuqaro_uy_id`) REFERENCES `tbl_fuqaro_uy` (`id`),
  ADD CONSTRAINT `tbl_transport_qatnovi_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_tuman`
--
ALTER TABLE `tbl_tuman`
  ADD CONSTRAINT `tbl_tuman_ibfk_1` FOREIGN KEY (`viloyat_id`) REFERENCES `tbl_viloyatlar1` (`id`),
  ADD CONSTRAINT `tbl_tuman_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD CONSTRAINT `tbl_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`),
  ADD CONSTRAINT `tbl_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_tashkilot` (`id`),
  ADD CONSTRAINT `tbl_user_ibfk_3` FOREIGN KEY (`viloyat`) REFERENCES `tbl_viloyatlar` (`id`),
  ADD CONSTRAINT `tbl_user_ibfk_4` FOREIGN KEY (`tuman`) REFERENCES `tbl_tuman` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_viloyatlar`
--
ALTER TABLE `tbl_viloyatlar`
  ADD CONSTRAINT `tbl_viloyatlar_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_ximzat_korsatish`
--
ALTER TABLE `tbl_ximzat_korsatish`
  ADD CONSTRAINT `tbl_ximzat_korsatish_ibfk_1` FOREIGN KEY (`tbl_fuqaro_uy_id`) REFERENCES `tbl_fuqaro_uy` (`id`),
  ADD CONSTRAINT `tbl_ximzat_korsatish_ibfk_2` FOREIGN KEY (`xondonga_yaqin_maktab_id`) REFERENCES `tbl_maktab` (`id`),
  ADD CONSTRAINT `tbl_ximzat_korsatish_ibfk_3` FOREIGN KEY (`xonadonga_yaqin_bogcha_id`) REFERENCES `tbl_muassasa` (`id`),
  ADD CONSTRAINT `tbl_ximzat_korsatish_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

--
-- Ограничения внешнего ключа таблицы `tbl_xonadon_transport`
--
ALTER TABLE `tbl_xonadon_transport`
  ADD CONSTRAINT `tbl_xonadon_transport_ibfk_1` FOREIGN KEY (`fuqaro_uy_id`) REFERENCES `tbl_fuqaro_uy` (`id`),
  ADD CONSTRAINT `tbl_xonadon_transport_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
