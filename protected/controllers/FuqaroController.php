<?php

class FuqaroController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('index','view','loadtuman','view'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('update'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Inspektor.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create','update','delete','admin'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Admins.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Users.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create','update','admin'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Kotiba.php"),
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
    public function actionLoadtuman()
    {
        ECascadeDropDown::checkValidRequest();
        $data = Tuman::model()->findAll('viloyat_id=:viloyat_nomi',
            array(':viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
        ECascadeDropDown::renderListData($data,'id','tuman_nomi','',Yii::t("strings",'Туманни танланг'));

    }

    public function actionView($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
    public function actionCreate($fid=0,$id=0)
    {
        if($fid==0)
        {
            //$model = new Fuqaro_Uy();
            $this->redirect(array('/fuqaro_Uy/create'));
        }
        $this->layoutTitle=Yii::t('strings','Operations');
        $model=new Fuqaro;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

		if(isset($_POST['FuqaroNew'])) $_POST['Fuqaro']=$_POST['FuqaroNew'];
		if(isset($_POST['FuqaroPassport'])) $_POST['Fuqaro']=$_POST['FuqaroPassport'];
		if(isset($_POST['FuqaroMetrika'])) $_POST['Fuqaro']=$_POST['FuqaroMetrika'];
		if(isset($_POST['FuqaroDaromad'])) $_POST['Fuqaro']=$_POST['FuqaroDaromad'];

		if(isset($_POST['Fuqaro']))
		{
			if(trim($_POST['Fuqaro']["uzoq_muddatga_ketgan_davlati"])!==""
				or trim($_POST['Fuqaro']["uzoq_muddatga_ketgan_vaqti"])!==""
				or trim($_POST['Fuqaro']["uzoq_muddat_ish"])!==""

			)
				$model= new FuqaroNew();
			elseif(trim($_POST['Fuqaro']["passport_seyiya"])!==""
				or trim($_POST['Fuqaro']["passport_raqami"])!==""
				or trim($_POST['Fuqaro']["passport_vaqti"])!==""
				or trim($_POST['Fuqaro']["passport_kim_tomonidan_id"])!==""
			)
				$model= new FuqaroPassport();
			elseif((trim($_POST['Fuqaro']["passport_seyiya"])==""
				and trim($_POST['Fuqaro']["passport_raqami"])==""
					and trim($_POST['Fuqaro']["passport_vaqti"])==""
						and trim($_POST['Fuqaro']["passport_kim_tomonidan_id"])=="")
				and ( trim($_POST['Fuqaro']["metirka_raqami"])==""
				or trim($_POST['Fuqaro']["metirka_kim_tomonidan"])==""
				)
			)
				$model= new FuqaroMetrika();
			elseif($_POST['Fuqaro']["daromad_turi_id"]==2)
				$model= new FuqaroDaromad();
            # $rnd = rand(0,9999);
            $model->attributes=$_POST['Fuqaro'];
          #  $uploadedFile=CUploadedFile::getInstance($model,'rasm');
           # $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
           # $model->rasm = $fileName;

            //$model->password=md5($model->password);
            if($model->save())
             #   $uploadedFile->saveAs(Yii::app()->basePath.'/../images/'.$fileName);
                $this->redirect(array('create','id'=>$model->id,'fid'=>$fid));
        }
        $this->render('create',array(
            'model'=>$model,
            'fid'=>$fid,
        ));
    }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=$this->loadModel($id);
		$fid=$model->fuqaro_uy_id;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['FuqaroNew'])) $_POST['Fuqaro']=$_POST['FuqaroNew'];
		if(isset($_POST['FuqaroPassport'])) $_POST['Fuqaro']=$_POST['FuqaroPassport'];
		if(isset($_POST['FuqaroMetrika'])) $_POST['Fuqaro']=$_POST['FuqaroMetrika'];
		if(isset($_POST['FuqaroDaromad'])) $_POST['Fuqaro']=$_POST['FuqaroDaromad'];

		if(isset($_POST['Fuqaro']))
		{
			if(trim($_POST['Fuqaro']["uzoq_muddatga_ketgan_davlati"])!==""
				or trim($_POST['Fuqaro']["uzoq_muddatga_ketgan_vaqti"])!==""
				or trim($_POST['Fuqaro']["uzoq_muddat_ish"])!==""

			)
				$model= FuqaroNew::model('FuqaroNew')->findByPk($id);//
			elseif(trim($_POST['Fuqaro']["passport_seyiya"])!==""
				or trim($_POST['Fuqaro']["passport_raqami"])!==""
				or trim($_POST['Fuqaro']["passport_vaqti"])!==""
				or trim($_POST['Fuqaro']["passport_kim_tomonidan_id"])!==""
			)
				$model= FuqaroPassport::model("FuqaroPassport")->findByPk($id);//();
			elseif((trim($_POST['Fuqaro']["passport_seyiya"])==""
				and trim($_POST['Fuqaro']["passport_raqami"])==""
					and trim($_POST['Fuqaro']["passport_vaqti"])==""
						and trim($_POST['Fuqaro']["passport_kim_tomonidan_id"])=="")
				and ( trim($_POST['Fuqaro']["metirka_raqami"])==""
					or trim($_POST['Fuqaro']["metirka_kim_tomonidan"])==""
				)
			)
				$model= FuqaroMetrika::model("FuqaroMetrika")->findByPk($id);//();
			elseif($_POST['Fuqaro']["daromad_turi_id"]==2)
				$model= FuqaroDaromad::model("FuqaroDaromad")->findByPk($id);//();
       #     $_POST['Fuqaro']['rasm'] = $model->rasm;
			$model->attributes=$_POST['Fuqaro'];
         /*   $uploadedFile=CUploadedFile::getInstance($model,'rasm');*/
			if($model->save())
        /*       if(!empty($uploadedFile))  // check if uploaded file is set or not
                {
                    $uploadedFile->saveAs(Yii::app()->basePath.'/../images/'.$model->image);
                }
		*/
            $this->redirect(array('view','id'=>$model->id,'fid'=>$fid));
		}

		$this->render('update',array(
			'model'=>$model,
			'fid'=>$fid,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
       $criteria = new CDbCriteria;
       $criteria->order = 'id asc';
       $this->layoutTitle=Yii::t('strings','Operations');
		$dataProvider=new CActiveDataProvider('Fuqaro',
            array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>30,
                )
        ));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=new Fuqaro('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Fuqaro']))
			$model->attributes=$_GET['Fuqaro'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Fuqaro the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Fuqaro::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Fuqaro $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='fuqaro-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
