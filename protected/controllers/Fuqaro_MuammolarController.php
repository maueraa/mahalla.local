<?php

class Fuqaro_MuammolarController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','create_kiritish','create_answer','yechim','xal','update_moddiy_yordam'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=new Fuqaro_Muammolar;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Fuqaro_Muammolar']))
		{
			$model->attributes=$_POST['Fuqaro_Muammolar'];
			//$model->password=md5($model->password);
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
    public function actionCreate_Kiritish()
    {
        $this->layoutTitle=Yii::t('strings','Operations');
        $model=new Fuqaro_Muammolar;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Fuqaro_Muammolar']))
        {
            $model->attributes=$_POST['Fuqaro_Muammolar'];
            //$model->password=md5($model->password);
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('create_kiritish',array(
            'model'=>$model,
        ));
    }
    public function actionCreate_Answer()
    {
        $this->layoutTitle=Yii::t('strings','Operations');
        $model=new Fuqaro_Muammolar;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Fuqaro_Muammolar']))
        {
            $model->attributes=$_POST['Fuqaro_Muammolar'];
            //$model->password=md5($model->password);
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('create_answer',array(
            'model'=>$model,
        ));
    }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Fuqaro_Muammolar']))
		{
			$model->attributes=$_POST['Fuqaro_Muammolar'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
    public function actionUpdate_moddiy_yordam($id)
    {
        $this->layoutTitle=Yii::t('strings','Operations');
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Fuqaro_Muammolar']))
        {
            $model->attributes=$_POST['Fuqaro_Muammolar'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update_moddiy_yordam',array(
            'model'=>$model,
        ));
    }
    public function actionYechim($id)
    {
        $this->layoutTitle=Yii::t('strings','Operations');
        $model=$this->loadModel($id);
        if($model->korilgan_sana===null)
        {
            $model->korilgan_sana=date("Y-m-d H:i:s");
            $model->save();
        }

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Fuqaro_Muammolar']))
        {
            $model->attributes=$_POST['Fuqaro_Muammolar'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update_yechim',array(
            'model'=>$model,
        ));
    }
    public function actionXal($id)
    {
        $this->layoutTitle=Yii::t('strings','Operations');
        $model=$this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Fuqaro_Muammolar']))
        {
            $model->attributes=$_POST['Fuqaro_Muammolar'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update_xal_etish',array(
            'model'=>$model,
        ));
    }
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($mfy_id=null,$mfy_idy=null,$mfy_idx=null)
	{
        if($mfy_id!==null)
        {
            #$mfy_id=isset(Yii::app()->user->mfy_id)?Yii::app()->user->mfy_id:(Yii::app()->user->name=="admin"?$mfy_id:0);
            $criteria = new CDbCriteria;
            $criteria->condition = "mfy_id = $mfy_id";
            $this->layoutTitle=Yii::t('strings','Operations');
    		$dataProvider=new CActiveDataProvider('Fuqaro_Muammolar',array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>30,
            )));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
        }
        elseif($mfy_idy!==null)
            {
                #$mfy_id=isset(Yii::app()->user->mfy_id)?Yii::app()->user->mfy_id:(Yii::app()->user->name=="admin"?$mfy_id:0);
                $criteria = new CDbCriteria;
                $criteria->condition = "mfy_id = $mfy_idy and muammo_xolati_id = 1";
                $this->layoutTitle=Yii::t('strings','Operations');
                $dataProvider=new CActiveDataProvider('Fuqaro_Muammolar',array(
                    'criteria'=>$criteria,
                    'pagination'=>array(
                        'pageSize'=>30,
                    )));
                $this->render('index',array(
                    'dataProvider'=>$dataProvider,
                ));
            }
        elseif($mfy_idx!==null)
        {
            #$mfy_id=isset(Yii::app()->user->mfy_id)?Yii::app()->user->mfy_id:(Yii::app()->user->name=="admin"?$mfy_id:0);
            $criteria = new CDbCriteria;
            $criteria->condition = "mfy_id = $mfy_idx and muammo_xolati_id is null";
            $this->layoutTitle=Yii::t('strings','Operations');
            $dataProvider=new CActiveDataProvider('Fuqaro_Muammolar',array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>30,
                )));
            $this->render('index',array(
                'dataProvider'=>$dataProvider,
            ));
        }

	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=new Fuqaro_Muammolar('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Fuqaro_Muammolar']))
			$model->attributes=$_GET['Fuqaro_Muammolar'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Fuqaro_Muammolar the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Fuqaro_Muammolar::model()->findByPk($id);
		if($model===null)

            throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Fuqaro_Muammolar $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='fuqaro--muammolar-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
