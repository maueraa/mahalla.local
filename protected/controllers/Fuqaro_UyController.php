<?php

class Fuqaro_UyController extends Controller
{



	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','loadtuman','loadmfy','loadkocha','loadsektor','loadtashkilot','loadfuqaro_uy','loadfuqaro','loadigf','loadigffio','loadigfs','loadigffios'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('view'),
				'users'=>array('@'),
			),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('update'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Inspektor.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create','update','delete','admin'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Admins.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','view'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Sector.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Users.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create','update','admin'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Kotiba.php"),
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionLoadigfs()
	{
		ECascadeDropDown::checkValidRequest();
		$data = Tashkilot::model()->findAll('ttuman_id=:viloyat_nomi',
			array('viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
		ECascadeDropDown::renderListData($data,'id','tnomi','',Yii::t("strings",'Ташкилотни танланг'));

	}
	public function actionLoadigffios()
	{
		ECascadeDropDown::checkValidRequest();
		$data = Tashkilot::model()->findAll('id=:viloyat_nomi',
			array('viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
		ECascadeDropDown::renderListData($data,'id','trahbari','',null);

	}
    public function actionLoadigf()
    {
        ECascadeDropDown::checkValidRequest();
        $data = Ishchi_Guruh_Fio::model()->findAll('tuman_id=:viloyat_nomi',
            array('viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
        ECascadeDropDown::renderListData($data,'id','tashkilot_nomi','',Yii::t("strings",'Ташкилотни танланг'));

    }
    public function actionLoadigffio()
    {
        ECascadeDropDown::checkValidRequest();
        $data = Ishchi_Guruh_Fio::model()->findAll('id=:viloyat_nomi',
            array('viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
        ECascadeDropDown::renderListData($data,'id','ishchi_guruh_fio','',null);

    }
    public function actionLoadtuman()
    {
        ECascadeDropDown::checkValidRequest();
        $data = Tuman::model()->findAll('viloyat_id=:viloyat_nomi',
            array('viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
        ECascadeDropDown::renderListData($data,'id','tuman_nomi','',Yii::t("strings",'Туманни танланг'));

    }
    public function actionLoadmfy()
    {
        ECascadeDropDown::checkValidRequest();
        $data = Mfy::model()->findAll('tuman_id=:viloyat_nomi',
            array(':viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
        ECascadeDropDown::renderListData($data,'id','mfy_nomi','',Yii::t("strings",'МФЙни танланг'));

    }
    public function actionLoadsektor()
    {
        ECascadeDropDown::checkValidRequest();
        $data = Rahbarlar::model()->findAll('tuman_id=:viloyat_nomi',
            array(':viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
        ECascadeDropDown::renderListData($data,'id','sektor','',Yii::t("strings",'Секторни танланг'));

    }
    public function actionLoadkocha()
    {
        ECascadeDropDown::checkValidRequest();
        $data = Kochalar::model()->findAll('tbl_mfy_id=:viloyat_nomi',
            array(':viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
        ECascadeDropDown::renderListData($data,'id','tbl_kocha_nomi','',Yii::t("strings",'Кўчани танланг'));

    }
    public function actionLoadtashkilot()
    {
        ECascadeDropDown::checkValidRequest();
        $data = Tashkilot::model()->findAll('ttuman_id=:viloyat_nomi',
            array(':viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
        ECascadeDropDown::renderListData($data,'id','tnomi','',Yii::t("strings",'Ташкилотни танланг'));

    }
    public function actionLoadfuqaro_uy()
    {
        ECascadeDropDown::checkValidRequest();
        $data = Fuqaro_Uy::model()->findAll('kocha_id=:viloyat_nomi',
            array(':viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
        ECascadeDropDown::renderListData2($data,'id','uy_raqami','',Yii::t("strings",'Фуқарони уйини танланг'));

    }
    public function actionLoadfuqaro()
    {
        ECascadeDropDown::checkValidRequest();
        $data = Fuqaro::model()->findAll('fuqaro_uy_id=:viloyat_nomi',
            array(':viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
        ECascadeDropDown::renderListData($data,'id','fio','',Yii::t("strings",'Фуқарони танланг'));

    }
    public function actionView($id)
	{
		$this->layoutTitle=Yii::t('strings','Меню');
		$this->render('view',array(
			'model'=>$this->loadModel($id),

		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
    public function actionCreate()
    {


        $this->layoutTitle=Yii::t('strings','Меню');
        $model=new Fuqaro_Uy;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Fuqaro_Uy']))
        {
            $model->attributes=$_POST['Fuqaro_Uy'];
            //$model->password=md5($model->password);
            if($model->save())
                $this->redirect(array('/manaviy/create','fid'=>$model->id));
        } 

        $this->render('create',array(
            'model'=>$model,
		
           
        ));
    }

    /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
    	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=$this->loadModel($id);


		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Fuqaro_Uy']))
		{
			$model->attributes=$_POST['Fuqaro_Uy'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($mfy_id=0)
	{
        if($mfy_id!==0)
        {
        $mfy_id=isset(Yii::app()->user->mfy_id)?Yii::app()->user->mfy_id:(Yii::app()->user->name=="admin"?$mfy_id:0);
        $criteria = new CDbCriteria;
		$criteria->condition = "mfy_id = '$mfy_id'";
		#$criteria->order = 'uy_raqami asc';
		$this->layoutTitle=Yii::t('strings','Operations');
		$dataProvider=new CActiveDataProvider('Fuqaro_Uy',array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>30,
			)

		));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
        }
        else
        {
        $this->layoutTitle=Yii::t('strings','Operations');
        $dataProvider=new CActiveDataProvider('Fuqaro_Uy',array(
           'pagination'=>array(
                'pageSize'=>30,
            )
        ));
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
        }
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=new Fuqaro_Uy('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Fuqaro_Uy']))
			$model->attributes=$_GET['Fuqaro_Uy'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Fuqaro_Uy the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Fuqaro_Uy::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Fuqaro_Uy $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='fuqaro--uy-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
