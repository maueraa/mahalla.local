<?php

class Fuqaro_kasallarController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
#	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','sektor','mfy','kocha','view','loadsektor','loadmfy','loadkocha'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Inspektor.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','sektor','mfy','kocha','view','loadsektor','loadmfy','loadkocha'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Admins.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','sektor','mfy','kocha','view','loadsektor','loadmfy','loadkocha'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Sector.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','mfy','kocha','view','loadsektor','loadmfy','loadkocha'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Kotiba.php"),
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
    public function actionLoadmfy()
    {
        ECascadeDropDown::checkValidRequest();
        $data = Mfy::model()->findAll('sektor_id=:viloyat_nomi',
            array(':viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
        ECascadeDropDown::renderListData($data,'id','mfy_nomi','',Yii::t("strings",'МФЙни танланг'));

    }
    public function actionLoadsektor()
    {
        ECascadeDropDown::checkValidRequest();
        $data = Rahbarlar::model()->findAll('tuman_id=:viloyat_nomi',
            array(':viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
        ECascadeDropDown::renderListData($data,'id','sektor','',Yii::t("strings",'Секторни танланг'));

    }
    public function actionLoadkocha()
    {
        ECascadeDropDown::checkValidRequest();
        $data = Kochalar::model()->findAll('tbl_mfy_id=:viloyat_nomi',
            array(':viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
        ECascadeDropDown::renderListData($data,'id','tbl_kocha_nomi','',Yii::t("strings",'Кўчани танланг'));

    }

	public function actionView($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=new Fuqaro;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Fuqaro']))
		{
			$model->attributes=$_POST['Fuqaro'];
			//$model->password=md5($model->password);
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Fuqaro']))
		{
			$model->attributes=$_POST['Fuqaro'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($tuman)
	{
		$tuman1 = Tuman::model()->findByPk($tuman);
		$criteria = new CDbCriteria;
        $criteria->condition = "tugilgan_joy_tuman_id = '$tuman' and kassalik_xolati in (2,3)";
		$this->layoutTitle=Yii::t('strings','Operations');
		$dataProvider=new CActiveDataProvider('Fuqaro',
            array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>30,
                )
            ));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'tuman'=>$tuman1->tuman_nomi,
		));
	}
    public function actionMfy($mfy)
    {
		$mfy1 = Mfy::model()->findByPk($mfy);
		$criteria = new CDbCriteria;
        $criteria->condition = "fuqaro_uy_id in
        (
            select id from tbl_fuqaro_uy where mfy_id =$mfy
        )   and kassalik_xolati in (2,3)";
        $this->layoutTitle=Yii::t('strings','Operations');
        $dataProvider=new CActiveDataProvider('Fuqaro',
            array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>30,
                )
            ));
        $this->render('mfy',array(
            'dataProvider'=>$dataProvider,
			'mfy'=>$mfy1->mfy_nomi,
        ));
    }
    public function actionSektor($sektor)
    {

		$criteria = new CDbCriteria;

        $criteria->condition = "fuqaro_uy_id in
        (
            select id from tbl_fuqaro_uy where mfy_id in
        (
            select id from tbl_mfy where sektor_id= '$sektor'
        )
        )   and kassalik_xolati in (2,3)";
        $this->layoutTitle=Yii::t('strings','Operations');
        $dataProvider=new CActiveDataProvider('Fuqaro',
            array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>30,
                )
            ));
        $this->render('sektor',array(
            'dataProvider'=>$dataProvider,
			'sektor'=>$sektor,
        ));
    }

	/**
	 * Manages all models.
	 */
    public function actionKocha($kocha)
    {
		$kocha1=Kochalar::model()->findByPk($kocha);
        $criteria = new CDbCriteria;
        $criteria->condition = "fuqaro_uy_id in
        (
            select id from tbl_fuqaro_uy where kocha_id =$kocha
        )   and kassalik_xolati in (2,3)";
        $this->layoutTitle=Yii::t('strings','Operations');
        $dataProvider=new CActiveDataProvider('Fuqaro',
            array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>30,
                )
            ));
        $this->render('kocha',array(
            'dataProvider'=>$dataProvider,
			'kocha'=>$kocha1->tbl_kocha_nomi,
        ));
    }


	public function actionAdmin()
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=new Fuqaro('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Fuqaro']))
			$model->attributes=$_GET['Fuqaro'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Fuqaro the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Fuqaro::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Fuqaro $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='fuqaro-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
