<?php

class FuqarolarController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create','update','delete','admin','view','index'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Admins.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create','view','index'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Users.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('view','index'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Sector.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create','update','view','index'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Kotiba.php"),
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=new Fuqarolar;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Fuqarolar']))
		{
			$model->attributes=$_POST['Fuqarolar'];
			//$model->password=md5($model->password);
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=$this->loadModel($id);
		$fid=$model->fuqaro_uy_id;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['FuqarolarNew'])) $_POST['Fuqarolar']=$_POST['FuqarolarNew'];
		if(isset($_POST['FuqarolarPassport'])) $_POST['Fuqarolar']=$_POST['FuqarolarPassport'];
		if(isset($_POST['FuqarolarMetrika'])) $_POST['Fuqarolar']=$_POST['FuqaroMetrika'];
		if(isset($_POST['FuqarolarDaromad'])) $_POST['Fuqarolar']=$_POST['FuqarolarDaromad'];

		if(isset($_POST['Fuqarolar']))
		{
			if(trim($_POST['Fuqarolar']["uzoq_muddatga_ketgan_davlati"])!==""
				or trim($_POST['Fuqarolar']["uzoq_muddatga_ketgan_vaqti"])!==""
				or trim($_POST['Fuqarolar']["uzoq_muddat_ish"])!==""

			)
				$model= FuqaroNew::model('FuqaroNew')->findByPk($id);//
			elseif(trim($_POST['Fuqarolar']["passport_seyiya"])!==""
				or trim($_POST['Fuqarolar']["passport_raqami"])!==""
				or trim($_POST['Fuqarolar']["passport_vaqti"])!==""
				or trim($_POST['Fuqarolar']["passport_kim_tomonidan_id"])!==""
			)
				$model= FuqarolarPassport::model("FuqarolarPassport")->findByPk($id);//();
			elseif((trim($_POST['Fuqarolar']["passport_seyiya"])==""
				and trim($_POST['Fuqarolar']["passport_raqami"])==""
					and trim($_POST['Fuqarolar']["passport_vaqti"])==""
						and trim($_POST['Fuqarolar']["passport_kim_tomonidan_id"])=="")
				and ( trim($_POST['Fuqarolar']["metirka_raqami"])==""
					or trim($_POST['Fuqarolar']["metirka_kim_tomonidan"])==""
				)
			)
				$model= FuqaroMetrika::model("FuqaroMetrika")->findByPk($id);//();
			elseif($_POST['Fuqarolar']["daromad_turi_id"]==2)
				$model= FuqarolarDaromad::model("FuqarolarDaromad")->findByPk($id);//();
       #     $_POST['Fuqarolar']['rasm'] = $model->rasm;
			$model->attributes=$_POST['Fuqarolar'];
         /*   $uploadedFile=CUploadedFile::getInstance($model,'rasm');*/
			if($model->save())
        /*       if(!empty($uploadedFile))  // check if uploaded file is set or not
                {
                    $uploadedFile->saveAs(Yii::app()->basePath.'/../images/'.$model->image);
                }
		*/
            $this->redirect(array('view','id'=>$model->id,'fid'=>$fid));
		}

		$this->render('update',array(
			'model'=>$model,
			'fid'=>$fid,
		));
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($fuq)
	{
        $criteria = new CDbCriteria;
        $criteria->condition = "fuqaro_uy_id = '$fuq'";
		$this->layoutTitle=Yii::t('strings','Operations');
		$dataProvider=new CActiveDataProvider('Fuqarolar',
            array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>30,
                )

            ));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
            'fuqs'=>$fuq,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=new Fuqarolar('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Fuqarolar']))
			$model->attributes=$_GET['Fuqarolar'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Fuqarolar the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Fuqarolar::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Fuqarolar $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='fuqarolar-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
