<?php

class Ishchi_Guruh_FioController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create','update','delete','admin','view','index','loadtuman'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Admins.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create','update','view','index','loadtuman'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Kotiba.php"),
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionLoadtuman()
	{
		ECascadeDropDown::checkValidRequest();
		$data = Tuman::model()->findAll('viloyat_id=:viloyat_nomi',
			array(':viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
		ECascadeDropDown::renderListData($data,'id','tuman_nomi','',Yii::t("strings",'Туманни танланг'));

	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=new Ishchi_Guruh_Fio;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Ishchi_Guruh_Fio']))
		{
			$model->attributes=$_POST['Ishchi_Guruh_Fio'];
			//$model->password=md5($model->password);
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Ishchi_Guruh_Fio']))
		{
			$model->attributes=$_POST['Ishchi_Guruh_Fio'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($guruh)
	{
        $criteria = new CDbCriteria;
        $criteria->condition = "tuman_id = '$guruh'";
        $this->layoutTitle=Yii::t('strings','Operations');
		$dataProvider=new CActiveDataProvider('Ishchi_Guruh_Fio',array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>30,
            )));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=new Ishchi_Guruh_Fio('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Ishchi_Guruh_Fio']))
			$model->attributes=$_GET['Ishchi_Guruh_Fio'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Ishchi_Guruh_Fio the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Ishchi_Guruh_Fio::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Ishchi_Guruh_Fio $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ishchi--guruh--fio-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
