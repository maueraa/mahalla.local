<?php

class JamlamaController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create','update','delete','admin','view','index'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Admins.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create','view','index'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Users.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('view','index'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Sector.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create','update','view','index','lala','lala2','lala3'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Kotiba.php"),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex($id)
	{
        $dar=$this->loadModel($id,Daromad::model());
        $fuqaro_uy=$this->loadModel($id,fuqaro_Uy::model(),"id");
        $manav=$this->loadModel($id,Manaviy::model());
        $qatnov=$this->loadModel($id,Transport_Qatnovi::model());
        $transport=$this->loadModel($id,Xonadon_Transport::model());
        $f=$this->loadModel($id,Fuqaro::model());
        $qarz=$this->loadModel($id,Qarz::model(),"tbl_fuqaro_uy_id");
		//$qarz=Qarz::model()->find();
		$tomorq=$this->loadModel($id,Tomorqa::model(),"tbl_fuqaro_uy_id");
		$sharoit=$this->loadModel($id,Shaoriti::model(),"tbl_fuqaro_uy_id");
		$hayvonlar=$this->loadModel($id,Hayvon::model(),"tbl_fuqaro_uy_id");
        $im=$this->loadModel($id,Ijtimoiy_Muhit::model(),"tbl_fuqaro_uy_id");
		#$tuman=$this->loadModel($id,Tuman::model(),"tbl_fuqaro_uy_id");
		$kocha_xolati=$this->loadModel($id,Kocha_Xolati::model(),"fuqaro_uy_id");
		$xizmat=$this->loadModel($id,Ximzat_Korsatish::model(),"tbl_fuqaro_uy_id");
		$this->render('index',
        array(
            'dar'=>$dar,
            'im'=>$im,
            'fuqaro_uy'=>$fuqaro_uy,
            'fuqaro'=>$f,
            'manaviy'=>$manav,
            'transport_qatnovi'=>$qatnov,
            'xonadon_transport'=>$transport,
			'qarzs'=>$qarz,
			'tomorqas'=>$tomorq,
			'shaoritis'=>$sharoit,
			'hayvons'=>$hayvonlar,
		//	'tumans'=>$tuman,
			'kocha'=>$kocha_xolati,
            'xiz'=>$xizmat,
        )
        );

	}
    public function loadModel($id, $modelname, $field="fuqaro_uy_id")
    {
        $model=$modelname->find("$field=$id");
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
    public function actionLala()
    {

		$this->render('lala');
    }

    public function actionLala2()
    {

        $this->render('lala2');
    }
    public function actionLala3()
    {

        $this->render('lala3');
    }

    public function actionView($id)
    {
        $dar=$this->loadModel($id,Daromad::model());
        $fuqaro_uy=$this->loadModel($id,fuqaro_Uy::model(),"id");
        $manav=$this->loadModel($id,Manaviy::model());
        $qatnov=$this->loadModel($id,Transport_Qatnovi::model());
        $transport=$this->loadModel($id,Xonadon_Transport::model());
        $f=$this->loadModel($id,Fuqaro::model());
        $qarz=$this->loadModel($id,Qarz::model(),"tbl_fuqaro_uy_id");
        //$qarz=Qarz::model()->find();
        $tomorq=$this->loadModel($id,Tomorqa::model(),"tbl_fuqaro_uy_id");
        $sharoit=$this->loadModel($id,Shaoriti::model(),"tbl_fuqaro_uy_id");
        $hayvonlar=$this->loadModel($id,Hayvon::model(),"tbl_fuqaro_uy_id");
        $im=$this->loadModel($id,Ijtimoiy_Muhit::model(),"tbl_fuqaro_uy_id");
        #$tuman=$this->loadModel($id,Tuman::model(),"tbl_fuqaro_uy_id");
        $kocha_xolati=$this->loadModel($id,Kocha_Xolati::model(),"fuqaro_uy_id");
        $this->layout='//layouts/layout3';
        # mPDF

        $mPDF1 = Yii::app()->ePdf->mpdf();

        # You can easily override default constructor's params
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');

        # render (full page)
        #$mPDF1->WriteHTML($this->render('view', array('model'=>$this->loadModel($id)), true));
        $mPDF1->WriteHTML($this->render('index', array(
            'dar'=>$dar,
            'im'=>$im,
            'fuqaro_uy'=>$fuqaro_uy,
            'fuqaro'=>$f,
            'manaviy'=>$manav,
            'transport_qatnovi'=>$qatnov,
            'xonadon_transport'=>$transport,
			'qarzs'=>$qarz,
			'tomorqas'=>$tomorq,
			'shaoritis'=>$sharoit,
			'hayvons'=>$hayvonlar,
		//	'tumans'=>$tuman,
			'kocha'=>$kocha_xolati,

        ), true));
        # Load a stylesheet
        #$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/main.css');
        #$mPDF1->WriteHTML($stylesheet, 1);

        # renderPartial (only 'view' of current controller)
        #  $mPDF1->WriteHTML($this->renderPartial('view', array('model'=>$this->loadModel($id)), true));

        # Renders image
        # $mPDF1->WriteHTML(CHtml::image(Yii::getPathOfAlias('webroot.css') . '/bg.gif' ));

        # Outputs ready PDF
        $file_name= $f->fio.'.pdf';

        #$file_name= $id.'.pdf';
        $mPDF1->Output($file_name,EYiiPdf::OUTPUT_TO_DOWNLOAD );
        #$mPDF1->Output();

        /* ////////////////////////////////////////////////////////////////////////////////////

                # HTML2PDF has very similar syntax
                $html2pdf = Yii::app()->ePdf->HTML2PDF();
                $html2pdf->WriteHTML($this->renderPartial('index', array(), true));
                $html2pdf->Output();

                ////////////////////////////////////////////////////////////////////////////////////

                # Example from HTML2PDF wiki: Send PDF by email
                $content_PDF = $html2pdf->Output('', EYiiPdf::OUTPUT_TO_STRING);
                require_once(dirname(__FILE__).'/pjmail/pjmail.class.php');
                $mail = new PJmail();
                $mail->setAllFrom('webmaster@my_site.net', "My personal site");
                $mail->addrecipient('mail_user@my_site.net');
                $mail->addsubject("Example sending PDF");
                $mail->text = "This is an example of sending a PDF file";
                $mail->addbinattachement("my_document.pdf", $content_PDF);
                $res = $mail->sendmail();


*/
    }

    // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}