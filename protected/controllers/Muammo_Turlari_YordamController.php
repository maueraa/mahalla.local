<?php

class Muammo_Turlari_YordamController extends Controller
{
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','create_kiritish','create_answer','yordam','view'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','view'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex($tuman)
	{
        $tuman1= Tuman::model()->findByPk($tuman);
        $criteria = new CDbCriteria;
		$criteria->condition = "muammo_turlari in (1,2,3)";
		$this->layoutTitle=Yii::t('strings','Operations');
		$dataProvider=new CActiveDataProvider('Fuqaro_Muammolar',array('criteria'=>$criteria,));
		$this->render('index',array('dataProvider'=>$dataProvider,'tuman'=>$tuman1));
	}

	public function actionYordam()
	{
		$this->render('yordam');
	}
	public function actionView($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$this->render('view',array(
			'model'=>Fuqaro_Muammolar::model()->findByPk($id),
		));
	}


	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}