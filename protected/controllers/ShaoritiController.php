<?php

class ShaoritiController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create','update','delete','admin','view','index'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Admins.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create','view','index'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Users.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('view','index'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Sector.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('create','update','view','index'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Kotiba.php"),
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($fid=0,$toview=0)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=new Shaoriti;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Shaoriti']))
		{
			$model->attributes=$_POST['Shaoriti'];
			//$model->password=md5($model->password);
			if($model->save())
                if($toview)
            $this->redirect(array('shaoriti/view','id'=>$model->tbl_fuqaro_uy_id));
            else
            $this->redirect(array('/Ximzat_Korsatish/create','fid'=>$fid));
		}

		$this->render('create',array(
			'model'=>$model,
            'fid'=>$fid,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id=0,$fid=0)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		if ($fid!==0)
			$model=Shaoriti::model()->find("tbl_fuqaro_uy_id like '$fid'");
		else
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Shaoriti']))
		{
			$model->attributes=$_POST['Shaoriti'];
			if($model->save())

				$this->redirect(array('view','id'=>$model->tbl_fuqaro_uy_id));
		}
        if(empty($model))
        {
            $this->redirect(array('/shaoriti/create','fid'=>$fid,'toview'=>'1'));
        }

        $this->render('update',array(
			'model'=>$model,
			'fid'=>$fid,

		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$dataProvider=new CActiveDataProvider('Shaoriti');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=new Shaoriti('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Shaoriti']))
			$model->attributes=$_GET['Shaoriti'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Shaoriti the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		#$model=Shaoriti::model()->findByPk($id);
		$model=Shaoriti::model()->find("tbl_fuqaro_uy_id like '$id'");
		if($model===null)
            $this->redirect(array('/shaoriti/create','fid'=>$id,'toview'=>'1'));
#			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Shaoriti $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='shaoriti-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
