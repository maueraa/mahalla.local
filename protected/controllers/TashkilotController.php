<?php

class TashkilotController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','loadtuman','loadmfy','loadkocha','loadsektor'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionLoadtuman()
    {
        ECascadeDropDown::checkValidRequest();
        $data = Tuman::model()->findAll('viloyat_id=:viloyat_nomi',
            array(':viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
        ECascadeDropDown::renderListData($data,'id','tuman_nomi','',Yii::t("strings",'Туманни танланг'));

    }
    public function actionLoadmfy()
    {
        ECascadeDropDown::checkValidRequest();
        $data = Mfy::model()->findAll('tuman_id=:viloyat_nomi',
            array(':viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
        ECascadeDropDown::renderListData($data,'id','mfy_nomi','',Yii::t("strings",'МФЙни танланг'));

    }
    public function actionLoadsektor()
    {
        ECascadeDropDown::checkValidRequest();
        $data = Rahbarlar::model()->findAll('tuman_id=:viloyat_nomi',
            array(':viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
        ECascadeDropDown::renderListData($data,'id','sektor','',Yii::t("strings",'Секторни танланг'));

    }
    public function actionLoadkocha()
    {
        ECascadeDropDown::checkValidRequest();
        $data = Kochalar::model()->findAll('tbl_mfy_id=:viloyat_nomi',
            array(':viloyat_nomi'=>ECascadeDropDown::submittedKeyValue()));
        ECascadeDropDown::renderListData($data,'id','tbl_kocha_nomi','',Yii::t("strings",'Кўчани танланг'));

    }

    /**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=new Tashkilot;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Tashkilot']))
		{
			$model->attributes=$_POST['Tashkilot'];
			//$model->password=md5($model->password);
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Tashkilot']))
		{
			$model->attributes=$_POST['Tashkilot'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$dataProvider=new CActiveDataProvider('Tashkilot');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->layoutTitle=Yii::t('strings','Operations');
		$model=new Tashkilot('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Tashkilot']))
			$model->attributes=$_GET['Tashkilot'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Tashkilot the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Tashkilot::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Tashkilot $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tashkilot-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
