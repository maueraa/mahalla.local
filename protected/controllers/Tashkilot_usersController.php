<?php

class Tashkilot_usersController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update','create_kiritish','create_answer','yechim'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }



	public function actionEtiroz()
	{
		$this->render('etiroz');
	}

    public function actionIndex($tashkilot_id)
    {
        $tashkilot_id=isset(Yii::app()->user->tashkilot_id)?Yii::app()->user->tashkilot_id:(Yii::app()->user->name=="admin"?$tashkilot_id:0);
        $criteria = new CDbCriteria;
        $criteria->condition = "tashkilot_id = '$tashkilot_id'";
        $this->layoutTitle=Yii::t('strings','Operations');
        $dataProvider=new CActiveDataProvider('Fuqaro_Muammolar',
        array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>30,
            )

        )
        );
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

	public function actionXal_etilgani()
	{
		$this->render('xal_etilgani');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}