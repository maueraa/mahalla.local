<?php

class Uyushmagan_yoshlarController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','sektor','mfy','kocha','view'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Inspektor.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','sektor','mfy','kocha','view'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Admins.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','sektor','mfy','kocha','view'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Sector.php"),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('index','mfy','kocha','view'),
                'users'=>include_once(dirname(__FILE__)."/../userroles/bino/Kotiba.php"),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex($tuman)
    {
        $tuman1 = Tuman::model()->findByPk($tuman);
        $criteria = new CDbCriteria;
      #  $criteria->condition = "tugilgan_joy_tuman_id = '$tuman' and tugilgan_sanasi>'".(date("Y")-18)."-".date("m")."-".date("d")."'and (passport_raqami='' or passport_raqami is null)  ";
     $criteria->condition = "tugilgan_joy_tuman_id = '$tuman' and tugilgan_sanasi<'".(date("Y")-16)."-".date("m")."-".date("d")."' and tugilgan_sanasi>'".(date("Y")-31)."-".date("m")."-".date("d")."'and daromad_turi_id in (1,9)";
        $this->layoutTitle=Yii::t('strings','Operations');
        $dataProvider=new CActiveDataProvider('Fuqaro',
            array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>30,
                )
            ));
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
            'tuman'=>$tuman1->tuman_nomi,

        ));
    }
    public function actionMfy($mfy)
    {
		$tuman1 = Mfy::model()->findByPk($mfy);
        $criteria = new CDbCriteria;
        $criteria->condition = "
        fuqaro_uy_id in
        (
            select id from tbl_fuqaro_uy where mfy_id =$mfy
		)   and tugilgan_sanasi<'".(date("Y")-16)."-".date("m")."-".date("d")."' and tugilgan_sanasi>'".(date("Y")-31)."-".date("m")."-".date("d")."'and daromad_turi_id in (1,9)";
        $dataProvider=new CActiveDataProvider('Fuqaro',
            array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>30,
                )
            ));
        $this->render('mfy',array(
            'dataProvider'=>$dataProvider,
			'mfy'=>$tuman1->mfy_nomi,
        ));
    }
    public function actionSektor($sektor)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = "
        fuqaro_uy_id in
        (
            select id from tbl_fuqaro_uy where mfy_id in
        (
            select id from tbl_mfy where sektor_id= '$sektor'
        )
        )
         and tugilgan_sanasi<'".(date("Y")-16)."-".date("m")."-".date("d")."' and tugilgan_sanasi>'".(date("Y")-31)."-".date("m")."-".date("d")."'and daromad_turi_id in (1,9)";
        $dataProvider=new CActiveDataProvider('Fuqaro',
            array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>30,
                )
            ));
        $this->render('sektor',array(
            'dataProvider'=>$dataProvider,
			'sektor'=>$sektor,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionKocha($kocha)
    {
		$tuman1 = Kochalar::model()->findByPk($kocha);
		$criteria = new CDbCriteria;
        $criteria->condition = "
            fuqaro_uy_id in
        (
            select id from tbl_fuqaro_uy where kocha_id =$kocha
        )
         and tugilgan_sanasi<'".(date("Y")-16)."-".date("m")."-".date("d")."' and tugilgan_sanasi>'".(date("Y")-31)."-".date("m")."-".date("d")."'and daromad_turi_id in (1,9)";
        $dataProvider=new CActiveDataProvider('Fuqaro',
            array(
                'criteria'=>$criteria,
                'pagination'=>array(
                    'pageSize'=>30,
                )
            ));
        $this->render('kocha',array(
            'dataProvider'=>$dataProvider,
			'kocha'=>$tuman1->tbl_kocha_nomi,
        ));
    }
    public function actionView($id)
    {
        $this->layoutTitle=Yii::t('strings','Operations');
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }
    public function loadModel($id)
    {
        $model=Fuqarolar::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}