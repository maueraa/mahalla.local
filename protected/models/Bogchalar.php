<?php

/**
 * This is the model class for table "{{bogchalar}}".
 *
 * The followings are the available columns in table '{{bogchalar}}':
 * @property integer $id
 * @property string $bogcha_raqami
 * @property string $bogcha_nomi
 * @property string $manzili
 * @property string $bogcha_direktor
 *
 * The followings are the available model relations:
 * @property Fuqaro[] $fuqaros
 */
class Bogchalar extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{bogchalar}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('bogcha_raqami, bogcha_nomi, manzili, bogcha_direktor', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, bogcha_raqami, bogcha_nomi, manzili, bogcha_direktor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fuqaros' => array(self::HAS_MANY, 'Fuqaro', 'bitirgan_joy_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'bogcha_raqami' => Yii::t('strings','Bogcha Raqami'),
			'bogcha_nomi' => Yii::t('strings','Bogcha Nomi'),
			'manzili' => Yii::t('strings','Manzili'),
			'bogcha_direktor' => Yii::t('strings','Bogcha Direktor'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('bogcha_raqami',$this->bogcha_raqami,true);
		$criteria->compare('bogcha_nomi',$this->bogcha_nomi,true);
		$criteria->compare('manzili',$this->manzili,true);
		$criteria->compare('bogcha_direktor',$this->bogcha_direktor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bogchalar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
