<?php

/**
 * This is the model class for table "{{daromad}}".
 *
 * The followings are the available columns in table '{{daromad}}':
 * @property integer $id
 * @property string $kam_taminlangan_sifatida
 * @property string $oylik_ortacha_daromadi
 * @property integer $ish_xaq_oluvchi_soni
 * @property string $ish_haqi_summasi
 * @property string $ish_xaqi_muddati
 * @property integer $pensiya_oluvchi_soni
 * @property string $pensiya_summa
 * @property string $pensiya_vaqti
 * @property integer $nogiron_nafaqa_soni
 * @property string $nogiron_summasi
 * @property integer $nogiron_nafaqa_vaqti
 * @property integer $kamtaminlangan_nafaqachilar_soni
 * @property string $kamtaminlangan_nafaqa_summasi
 * @property string $kamtaminlangan_nafaqa_vaqti
 * @property integer $ikki_yoshgacha_nafaqa_soni
 * @property string $ikki_yosh_nafaqa_summasi
 * @property string $ikki_yosh_nafaqa_vaqti
 * @property integer $on_tort_yosh_nafaqa_soni
 * @property string $on_tort_nafaqa_summasi
 * @property string $on_tort_nafaqa_vaqti
 * @property string $daromad_boshqa
 * @property integer $fuqaro_uy_id
 * @property integer $fuqaro_id
 *
 * The followings are the available model relations:
 * @property FuqaroUy $fuqaroUy
 * @property Fuqaro $fuqaro
 */
class Daromad extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{daromad}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kam_taminlangan_sifatida','required'),
			array('ish_xaq_oluvchi_soni, pensiya_oluvchi_soni, nogiron_nafaqa_soni, nogiron_nafaqa_vaqti, kamtaminlangan_nafaqachilar_soni, ikki_yoshgacha_nafaqa_soni, on_tort_yosh_nafaqa_soni, fuqaro_uy_id, fuqaro_id, user_id', 'numerical', 'integerOnly'=>true),
			array('kam_taminlangan_sifatida, oylik_ortacha_daromadi, ish_haqi_summasi, ish_xaqi_muddati, pensiya_summa, pensiya_vaqti, nogiron_summasi, kamtaminlangan_nafaqa_summasi, kamtaminlangan_nafaqa_vaqti, ikki_yosh_nafaqa_summasi, ikki_yosh_nafaqa_vaqti, on_tort_nafaqa_summasi, on_tort_nafaqa_vaqti, daromad_boshqa', 'length', 'max'=>255),
            array('anketa_ozgarish, user_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, kam_taminlangan_sifatida, oylik_ortacha_daromadi, ish_xaq_oluvchi_soni, ish_haqi_summasi, ish_xaqi_muddati, pensiya_oluvchi_soni, pensiya_summa, pensiya_vaqti, nogiron_nafaqa_soni, nogiron_summasi, nogiron_nafaqa_vaqti, kamtaminlangan_nafaqachilar_soni, kamtaminlangan_nafaqa_summasi, kamtaminlangan_nafaqa_vaqti, ikki_yoshgacha_nafaqa_soni, ikki_yosh_nafaqa_summasi, ikki_yosh_nafaqa_vaqti, on_tort_yosh_nafaqa_soni, on_tort_nafaqa_summasi, on_tort_nafaqa_vaqti, daromad_boshqa, fuqaro_uy_id, fuqaro_id, anketa_ozgarish, user_id, user_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fuqaroUy' => array(self::BELONGS_TO, 'FuqaroUy', 'fuqaro_uy_id'),
			'fuqaro' => array(self::BELONGS_TO, 'Fuqaro', 'fuqaro_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'kam_taminlangan_sifatida' => Yii::t('strings','Кам таъмин-ланган оилалар сифатида рўйхатда туриши'),
			'oylik_ortacha_daromadi' => Yii::t('strings','Хонадонни ўртача ойлик даромади (пeнсия,иш ҳақи томорқа ва бошқа даромад-лар)'),
			'ish_xaq_oluvchi_soni' => Yii::t('strings','Иш хақи олувчи сони'),
			'ish_haqi_summasi' => Yii::t('strings','Иш хақи суммаси'),
			'ish_xaqi_muddati' => Yii::t('strings','Иш хақи оладиган вақти'),
			'pensiya_oluvchi_soni' => Yii::t('strings','Пенсия олувчи сони'),
			'pensiya_summa' => Yii::t('strings','Пенсия суммаси'),
			'pensiya_vaqti' => Yii::t('strings','Пенсия оладиган вақти'),
			'nogiron_nafaqa_soni' => Yii::t('strings','Ногирон нафақа сони'),
			'nogiron_summasi' => Yii::t('strings','Ногирон суммаси'),
			'nogiron_nafaqa_vaqti' => Yii::t('strings','Ногирон нафақа оладиган вақти'),
			'kamtaminlangan_nafaqachilar_soni' => Yii::t('strings','Камтаъминлаган нафақачилар сони'),
			'kamtaminlangan_nafaqa_summasi' => Yii::t('strings','Камтаъминланланган нафақа суммаси'),
			'kamtaminlangan_nafaqa_vaqti' => Yii::t('strings','Камтаъминланган нафақа оладиган вақти'),
			'ikki_yoshgacha_nafaqa_soni' => Yii::t('strings','2-ёшгача болалар пуллари олувчи сони'),
			'ikki_yosh_nafaqa_summasi' => Yii::t('strings','2-ёшгача болалар пуллари суммаси'),
			'ikki_yosh_nafaqa_vaqti' => Yii::t('strings','2-ёшгача болалар пулларини оладиган вакти'),
			'on_tort_yosh_nafaqa_soni' => Yii::t('strings','14-ёшгача болалар пуллари олувчи сони'),
			'on_tort_nafaqa_summasi' => Yii::t('strings','14-ёшгача болалар пуллари суммаси'),
			'on_tort_nafaqa_vaqti' => Yii::t('strings','14-ёшгача болалар пуллари оладиган вакти'),
			'daromad_boshqa' => Yii::t('strings','Бошқа даромадлари'),
			'fuqaro_uy_id' => Yii::t('strings','Fuqaro Uy'),
			'fuqaro_id' => Yii::t('strings','Fuqaro'),
            'anketa_ozgarish' => Yii::t('strings','Анкетани ўзгариш санаси'),
            'user_id' => Yii::t('strings','Фойдаланувчи'),
            'user_date' => Yii::t('strings','Сана'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('kam_taminlangan_sifatida',$this->kam_taminlangan_sifatida,true);
		$criteria->compare('oylik_ortacha_daromadi',$this->oylik_ortacha_daromadi,true);
		$criteria->compare('ish_xaq_oluvchi_soni',$this->ish_xaq_oluvchi_soni);
		$criteria->compare('ish_haqi_summasi',$this->ish_haqi_summasi,true);
		$criteria->compare('ish_xaqi_muddati',$this->ish_xaqi_muddati,true);
		$criteria->compare('pensiya_oluvchi_soni',$this->pensiya_oluvchi_soni);
		$criteria->compare('pensiya_summa',$this->pensiya_summa,true);
		$criteria->compare('pensiya_vaqti',$this->pensiya_vaqti,true);
		$criteria->compare('nogiron_nafaqa_soni',$this->nogiron_nafaqa_soni);
		$criteria->compare('nogiron_summasi',$this->nogiron_summasi,true);
		$criteria->compare('nogiron_nafaqa_vaqti',$this->nogiron_nafaqa_vaqti);
		$criteria->compare('kamtaminlangan_nafaqachilar_soni',$this->kamtaminlangan_nafaqachilar_soni);
		$criteria->compare('kamtaminlangan_nafaqa_summasi',$this->kamtaminlangan_nafaqa_summasi,true);
		$criteria->compare('kamtaminlangan_nafaqa_vaqti',$this->kamtaminlangan_nafaqa_vaqti,true);
		$criteria->compare('ikki_yoshgacha_nafaqa_soni',$this->ikki_yoshgacha_nafaqa_soni);
		$criteria->compare('ikki_yosh_nafaqa_summasi',$this->ikki_yosh_nafaqa_summasi,true);
		$criteria->compare('ikki_yosh_nafaqa_vaqti',$this->ikki_yosh_nafaqa_vaqti,true);
		$criteria->compare('on_tort_yosh_nafaqa_soni',$this->on_tort_yosh_nafaqa_soni);
		$criteria->compare('on_tort_nafaqa_summasi',$this->on_tort_nafaqa_summasi,true);
		$criteria->compare('on_tort_nafaqa_vaqti',$this->on_tort_nafaqa_vaqti,true);
		$criteria->compare('daromad_boshqa',$this->daromad_boshqa,true);
		$criteria->compare('fuqaro_uy_id',$this->fuqaro_uy_id);
		$criteria->compare('fuqaro_id',$this->fuqaro_id);
        $criteria->compare('anketa_ozgarish',$this->anketa_ozgarish,true);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('user_date',$this->user_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Daromad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
