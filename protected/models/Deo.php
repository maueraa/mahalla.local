<?php

/**
 * This is the model class for table "{{deo}}".
 *
 * The followings are the available columns in table '{{deo}}':
 * @property integer $id
 * @property string $deo_xolati
 * @property integer $user_id
 * @property string $user_date
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Fuqaro[] $fuqaros
 */
class Deo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{deo}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('deo_xolati', 'length', 'max'=>255),
			array('user_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, deo_xolati, user_id, user_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'fuqaros' => array(self::HAS_MANY, 'Fuqaro', 'deo_azoligi_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'deo_xolati' => Yii::t('strings','Deo Xolati'),
			'user_id' => Yii::t('strings','User'),
			'user_date' => Yii::t('strings','User Date'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('deo_xolati',$this->deo_xolati,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_date',$this->user_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Deo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
