<?php

/**
 * This is the model class for table "{{fozgarish}}".
 *
 * The followings are the available columns in table '{{fozgarish}}':
 * @property integer $id
 * @property integer $fuy_id
 * @property integer $fuq_id
 * @property string $fuq_maydon_nomi
 * @property string $fuq_maydon_ozgaruvchi
 * @property integer $user_id
 * @property string $user_date
 */
class Fozgarish extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{fozgarish}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('fuy_id, fuq_id, user_id', 'numerical', 'integerOnly'=>true),
			array('fuq_maydon_nomi, fuq_maydon_ozgaruvchi', 'length', 'max'=>255),
			array('user_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fuy_id, fuq_id, fuq_maydon_nomi, fuq_maydon_ozgaruvchi, user_id, user_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'fuy_id' => Yii::t('strings','Хонадоннинг ID рақами'),
			'fuq_id' => Yii::t('strings','Фуқаронинг ID рақами'),
			'fuq_maydon_nomi' => Yii::t('strings','Фуқаронинг маълумотларидаги майдонлардан ўзгарган майдонинг номи'),
			'fuq_maydon_ozgaruvchi' => Yii::t('strings','Фуқаронинг аввалги киритилган маълумоти'),
			'user_id' => Yii::t('strings','Фойдаалнувчи номи'),
			'user_date' => Yii::t('strings','Фойдаланувчи ўзгартирган сана'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fuy_id',$this->fuy_id);
		$criteria->compare('fuq_id',$this->fuq_id);
		$criteria->compare('fuq_maydon_nomi',$this->fuq_maydon_nomi,true);
		$criteria->compare('fuq_maydon_ozgaruvchi',$this->fuq_maydon_ozgaruvchi,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_date',$this->user_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Fozgarish the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
