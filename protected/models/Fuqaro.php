<?php

/**
 * This is the model class for table "{{fuqaro}}".
 *
 * The followings are the available columns in table '{{fuqaro}}':
 * @property integer $id
 * @property string $fio
 * @property integer $jinsi_id
 * @property string $tugilgan_sanasi
 * @property integer $tugilgan_joy_viloyat_id
 * @property integer $tugilgan_joy_tuman_id
 * @property string $passport_raqami
 * @property string $passport_vaqti
 * @property integer $passport_kim_tomonidan_id
 * @property string $metirka_raqami
 * @property string $metirka_kim_tomonidan
 * @property string $ish_joy_nomi
 * @property string $lavozimi
 * @property integer $daromad_turi_id
 * @property string $daromad_miqdori
 * @property string $daromad_olish_sanasi
 * @property string $uzoq_muddatga_ketgan_vaqti
 * @property string $uzoq_muddatga_kelgan_vaqti
 * @property string $uzoq_muddatga_ketgan_davlati
 * @property string $uzoq_muddat_suhbat_otgani
 * @property integer $malumoti_id
 * @property integer $bitirgan_joy_id
 * @property string $bitirgan_mutaxasisligi
 * @property string $bitirgan_yili
 * @property integer $aliment_id
 * @property string $aliment_qarz
 * @property string $notinch_oila
 * @property string $sudlanganligi
 * @property string $iib_xisobida_turadimi
 * @property integer $fuqaro_uy_id
 * @property string $sogligi
 *
 * The followings are the available model relations:
 * @property Daromad[] $daromads
 * @property Malumoti $malumoti
 * @property Tumanq $passportKimTomonidan
 * @property Jinsi $jinsi
 * @property Viloyatlar1 $tugilganJoyViloyat
 * @property Tumanq $tugilganJoyTuman
 * @property Aliment $aliment
 * @property Qarz[] $qarzs
 */
class Fuqaro extends CActiveRecord
{
    protected function beforeSave()
    {
        if(parent::beforeSave()){
            #oila azolari soni
			$this->passport_seyiya= strtoupper(Yii::CyrLat(($this->passport_seyiya)));
	    if(trim($this->uzoq_muddatga_ketgan_vaqti)=="") $this->uzoq_muddatga_ketgan_vaqti=null;
	    if(trim($this->uzoq_muddatga_kelgan_vaqti)=="") $this->uzoq_muddatga_kelgan_vaqti=null;
	    if(trim($this->tugilgan_sanasi)=="") $this->tugilgan_sanasi=null;
/*            $mod = Fuqaro::model()->findAll("fuqaro_uy_id='$this->fuqaro_uy_id'");
            #$mod = Fuqaro::model()->findAll("fio like '%комилов%'");
            //$mod = Fuqaro::model()->findAll("tsana between '".(date("Y")-18)."-01-01' and '".(date("Y"))."-12-31'");
            #$mod = Fuqaro::model()->findAll("tugilgan_sanasi between '1951-12-01' and '1951-12-01'");
            $soni = count($mod);
            $model = Fuqaro_Uy::model()->findByPk($this->fuqaro_uy_id);
            $model->azo_soni=$soni+1;

            #voyaga yetmagan soni

            $mod2 = Fuqaro::model()->findAll("tugilgan_sanasi between '".(date("Y")-18)."-01-01' and '".(date("Y"))."-12-31'");
            $soni2 = count($mod2);
            $model-> voya_yetmagan_soni=$soni;

           #daromdi

            $ishsiz = Yii::app()->db->createCommand()->
                select('sum(daromad_miqdori) as dsum ')
                ->from("{{Fuqaro}}")
                ->where('daromad_turi_id=1 and fuqaro_uy_id=$this->fuqaro_uy_id')->queryAll();
                $mod3=Daromad::model()->find("fuqaro_uy_id='".$this->fuqaro_uy_id."'");
            if (isset($ishsiz[0]))  $mod3-> daromad_boshqa =$ishsiz[0]["dsum"];


         $ishlaydi = Yii::app()->db->createCommand()->
                select('sum(daromad_miqdori) as dsum, count(*) as cnt ')
                ->from("{{Fuqaro}}")
                ->where('daromad_turi_id=2')->queryAll();
            if (isset($ishlaydi[0])){
           $mod3-> ish_xaq_oluvchi_soni =$ishlaydi[0]["cnt"];
            $mod3-> ish_haqi_summasi =$ishlaydi[0]["dsum"];}

            $pensioner = Yii::app()->db->createCommand()->
                select('sum(daromad_miqdori) as dsum,count(*) as cnt ')
                ->from("{{Fuqaro}}")
                ->where('daromad_turi_id=3')->queryAll();
            if (isset($pensioner[0]))
            {
            $mod3-> pensiya_oluvchi_soni=$pensioner[0]["cnt"];
            $mod3-> pensiya_summa =$pensioner[0]["dsum"];
            }

            $ikki_yosh = Yii::app()->db->createCommand()->
                select('sum(daromad_miqdori) as dsum,count(*) as cnt ')
                ->from("{{Fuqaro}}")
                ->where('daromad_turi_id=4')->queryAll();
            if (isset($ikki_yosh[0])){
            $mod3->  ikki_yoshgacha_nafaqa_soni =$ikki_yosh[0]["cnt"];
            $mod3->  ikki_yosh_nafaqa_summasi =$ikki_yosh[0]["dsum"];
            }
            $on_tort = Yii::app()->db->createCommand()->
                select('sum(daromad_miqdori) as dsum,count(*) as cnt ')
                ->from("{{Fuqaro}}")
                ->where('daromad_turi_id=5')->queryAll();
            if (isset($on_tort[0])){
            $mod3->  on_tort_yosh_nafaqa_soni =$on_tort[0]["cnt"];
            $mod3->  on_tort_nafaqa_summasi =$on_tort[0]["dsum"];
            }

            $kamtaminlangan = Yii::app()->db->createCommand()->
                select('sum(daromad_miqdori) as dsum,count(*) as cnt ')
                ->from("{{Fuqaro}}")
                ->where('daromad_turi_id=6')->queryAll();
            if (isset($kamtaminlangan[0])){
            $mod3->  kamtaminlangan_nafaqachilar_soni =$kamtaminlangan[0]["cnt"];
            $mod3->  kamtaminlangan_nafaqa_summasi =$kamtaminlangan[0]["dsum"];
            }
            $nogiron= Yii::app()->db->createCommand()->
                select('sum(daromad_miqdori) as dsum,count(*) as cnt ')
                ->from("{{Fuqaro}}")
                ->where('daromad_turi_id=7')->queryAll();
            if (isset($nogiron[0])){
            $mod3->  nogiron_nafaqa_soni =$nogiron[0]["cnt"];
            $mod3->  nogiron_summasi =$nogiron[0]["dsum"];
            }

/**/    if($this->uzoq_muddatga_kelgan_vaqti!==null)

            {
				$this->uzoq_muddatga_kelgan_vaqti=str_replace("-","/",$this->uzoq_muddatga_kelgan_vaqti);
                $this->uzoq_muddatga_kelgan_vaqti=date("Y-m-d", strtotime($this->uzoq_muddatga_kelgan_vaqti));
            }
           if($this->uzoq_muddatga_ketgan_vaqti!==null)
           {
			   $this->uzoq_muddatga_ketgan_vaqti=str_replace("-","/",$this->uzoq_muddatga_ketgan_vaqti);
			   $this->uzoq_muddatga_ketgan_vaqti=date("Y-m-d", strtotime($this->uzoq_muddatga_ketgan_vaqti));
           }
           if($this->tugilgan_sanasi!==null)
           {
			   $this->tugilgan_sanasi=str_replace("-","/",$this->tugilgan_sanasi);
			   $this->tugilgan_sanasi=date("Y-m-d", strtotime($this->tugilgan_sanasi));
           }

         //   $model->save();
           // $mod3->save();
            return true;
        }
        else return false;


    }
    protected function afterFind()
    {
        if ($this->uzoq_muddatga_kelgan_vaqti!==null and $this->uzoq_muddatga_kelgan_vaqti!=='1970-01-01') $this->uzoq_muddatga_kelgan_vaqti=date("d.m.Y", strtotime($this->uzoq_muddatga_kelgan_vaqti));
        if ($this->uzoq_muddatga_ketgan_vaqti!==null and $this->uzoq_muddatga_ketgan_vaqti!=='1970-01-01') $this->uzoq_muddatga_ketgan_vaqti=date("d.m.Y", strtotime($this->uzoq_muddatga_ketgan_vaqti));
        if ($this->tugilgan_sanasi!==null) $this->tugilgan_sanasi=date("d.m.Y", strtotime($this->tugilgan_sanasi));
        parent::afterFind();
    }

    public function getjinsi()
    {
        $jinsi = Jinsi::model()->findAll();
        $jinsi = CHtml::listData($jinsi,'id','jins');
        return $jinsi;
    }

    public function getjoyvil()
    {
        $tjoyvil = Viloyatlar::model()->findAll();
        $tjoyvil = CHtml::listData($tjoyvil,'id','viloyat_nomi');
        foreach ($tjoyvil as $key=>$val){
            $tjoyvil[$key]=Yii::t("strings",CHtml::encode($val));
        }
        return $tjoyvil;
    }

    public function getjoytuman()
    {
        $tjoytuman = Tuman::model()->findAll();
        $tjoytuman = CHtml::listData($tjoytuman,'id','tuman_nomi','viloyat_id');
        return $tjoytuman;
    }

    public function getjoytumanpas()
    {
        $tjoytumanpas = Tuman::model()->findAll();
        $tjoytumanpas = CHtml::listData($tjoytumanpas,'id','tuman_nomi','viloyat_id');
        return $tjoytumanpas;
    }

    public  function getdaromad_turi()
    {
        $daromad_turi = Daromad_Turi::model()->findAll();
        $daromad_turi = CHtml::listData($daromad_turi,'id','nomi');
        return $daromad_turi;
    }

    public function getmalumoti()
    {
        $malumoti = Malumoti::model()->findAll();
        $malumoti = CHtml::listData($malumoti,'id','malumot_turi');
        return $malumoti;
    }
    public function getalimenti()
    {
        $aliment2 = Aliment::model()->findAll();
        $aliment2 = CHtml::listData($aliment2,'id','nomi');
        return $aliment2;
    }
    public function getnotinch()
    {
        $notinch = Notinch::model()->findAll();
        $notinch = CHtml::listData($notinch,'id','oila_xolati');
        return $notinch;
    }
    public function getiib()
    {
        $iib = Iib::model()->findAll();
        $iib = CHtml::listData($iib,'id','iib_xisobi');
        return $iib;
    }
    public function getkasallik()
    {
        $iib = Kassalik::model()->findAll();
        $iib = CHtml::listData($iib,'id','kassalik_nomi');
        return $iib;
    }
    public function getmillati()
    {
        $millati = Millati::model()->findAll();
        $millati = CHtml::listData($millati,'id','millat');
        return $millati;
    }
    public function getdavlat()
    {
        $davlat = Davlat::model()->findAll();
        $davlat = CHtml::listData($davlat,'id','davlat_nomi');
        return $davlat;
    }
    public function getota($fid)
    {
        $fa = Fuqaro::model()->findAll("jinsi_id=1 and fuqaro_uy_id = $fid");
        $fa = CHtml::listData($fa,'id','fio');
        return $fa;
    }
    public function getona($fid)
    {
        $mo = Fuqaro::model()->findAll("jinsi_id=2 and fuqaro_uy_id = $fid");
        $mo = CHtml::listData($mo,'id','fio');
        return $mo;
    }
	public function getdeoazo()
	{
		$deoazo = Deo::model()->findAll();
		$deoazo= CHtml::listData($deoazo,'id','deo_xolati');
		return $deoazo;
	}
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{fuqaro}}';
	}


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jinsi_id, tugilgan_joy_tuman_id, tugilgan_joy_viloyat_id, daromad_turi_id, fio, tugilgan_sanasi, davlat_id,nikox_turi,iib_xisobida_turadimi, deo_azoligi_id,malumoti_id,aliment_id,notinch_oila_id, kassalik_id, kassalik_xolati, oila_boshligi ','required'),
			array('jinsi_id, millati_id, davlat_id, tugilgan_joy_viloyat_id, tugilgan_joy_tuman_id, passport_kim_tomonidan_id, daromad_turi_id, uzoq_muddatga_ketgan_davlati, malumoti_id, aliment_id, iib_xisobida_turadimi, deo_azoligi_id, fuqaro_uy_id, kassalik_id, kassalik_xolati, oila_boshligi, user_id, otasi_id, onasi_id', 'numerical', 'integerOnly'=>true),
			array('fio, rasm, passport_vaqti, metirka_raqami, metirka_kim_tomonidan, nikox_guvohnom_raqami, nikox_turi, ish_joy_nomi, lavozimi, daromad_miqdori, daromad_olish_sanasi, uzoq_muddat_ish, uzoq_muddat_suhbat_otgani, bitirgan_joy_id, bitirgan_mutaxasisligi, bitirgan_yili, aliment_qarz, notinch_oila_id, sogligi', 'length', 'max'=>255),
            array('passport_seyiya', 'length', 'max'=>2),
			array('passport_raqami', 'length', 'max'=>7),
            array('tugilgan_sanasi, uzoq_muddatga_ketgan_vaqti, uzoq_muddatga_kelgan_vaqti, sudlanganligi, anketa_ozgarish, user_date', 'safe'),
            array('rasm', 'file', 'types'=>'jpg, gif, png','allowEmpty'=>true, 'on'=>'update'),
            array('date', 'safe'),
            array( 'rasm', 'length', 'max'=>255, 'on'=>'insert,update'),

            // The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fio, jinsi_id, millati_id, tugilgan_sanasi, rasm, davlat_id, tugilgan_joy_viloyat_id, tugilgan_joy_tuman_id, passport_seyiya, passport_raqami, passport_vaqti, passport_kim_tomonidan_id, metirka_raqami, metirka_kim_tomonidan, nikox_guvohnom_raqami, nikox_turi, ish_joy_nomi, lavozimi, daromad_turi_id, daromad_miqdori, daromad_olish_sanasi, uzoq_muddatga_ketgan_vaqti, uzoq_muddatga_kelgan_vaqti, uzoq_muddatga_ketgan_davlati, uzoq_muddat_ish, uzoq_muddat_suhbat_otgani, malumoti_id, bitirgan_joy_id, bitirgan_mutaxasisligi, bitirgan_yili, aliment_id, aliment_qarz, notinch_oila_id, sudlanganligi, iib_xisobida_turadimi, deo_azoligi_id, fuqaro_uy_id, kassalik_id, kassalik_xolati, sogligi, oila_boshligi, anketa_ozgarish, user_id, user_date, otasi_id, onasi_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daromads' => array(self::HAS_MANY, 'Daromad', 'fuqaro_id'),
			'malumoti' => array(self::BELONGS_TO, 'Malumoti', 'malumoti_id'),
            'tugilganJoyViloyat' => array(self::BELONGS_TO, 'Viloyatlar', 'tugilgan_joy_viloyat_id'),
            'daromad_turi' => array(self::BELONGS_TO, 'Daromad_Turi', 'daromad_turi_id'),
            'passportKimTomonidan' => array(self::BELONGS_TO, 'Tuman', 'passport_kim_tomonidan_id'),
            'jinsi' => array(self::BELONGS_TO, 'Jinsi', 'jinsi_id'),
            'kassalik' => array(self::BELONGS_TO, 'Kassalik', 'kassalik_id'),
            'iibXisobidaTuradimi' => array(self::BELONGS_TO, 'Iib', 'iib_xisobida_turadimi'),
            'millati' => array(self::BELONGS_TO, 'Millati', 'millati_id'),
            'davlat' => array(self::BELONGS_TO, 'Davlat', 'davlat_id'),
            'uzoqMuddatgaKetganDavlati' => array(self::BELONGS_TO, 'Davlat', 'uzoq_muddatga_ketgan_davlati'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'otasi' => array(self::BELONGS_TO, 'Fuqaro', 'otasi_id'),
            'fuqaros' => array(self::HAS_MANY, 'Fuqaro', 'otasi_id'),
            'onasi' => array(self::BELONGS_TO, 'Fuqaro', 'onasi_id'),
            'fuqaros1' => array(self::HAS_MANY, 'Fuqaro', 'onasi_id'),
            'deoAzoligi' => array(self::BELONGS_TO, 'Deo', 'deo_azoligi_id'),
            'jinsi' => array(self::BELONGS_TO, 'Jinsi', 'jinsi_id'),
			'tugilganJoyTuman' => array(self::BELONGS_TO, 'Tuman', 'tugilgan_joy_tuman_id'),
			'aliment' => array(self::BELONGS_TO, 'Aliment', 'aliment_id'),
			'qarzs' => array(self::HAS_MANY, 'Qarz', 'tbl_fuqaro_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'fio' => Yii::t('strings','Фуқароннинг ФИШ'),
			'jinsi_id' => Yii::t('strings','Жинси'),
            'millati_id' => Yii::t('strings','Миллати'),
			'tugilgan_sanasi' => Yii::t('strings','Туғилган санаси мисол:2017-09-16'),
            'davlat_id' => Yii::t('strings','Давлатни танланг'),
            'tugilgan_joy_viloyat_id' => Yii::t('strings','Вилоятни танланг'),
            'tugilgan_joy_tuman_id' => Yii::t('strings','Туммани танланг'),
            'rasm' => Yii::t('strings','Rasm'),
            'passport_raqami' => Yii::t('strings','Паспорт рақами'),
            'passport_seyiya' => Yii::t('strings','Паспорт серияси'),
            'passport_vaqti' => Yii::t('strings','Паспорт олган вақти'),
            'passport_kim_tomonidan_id' => Yii::t('strings','Пасспорти ким томонидан берилган'),
            'metirka_raqami' => Yii::t('strings','Метирка рақами'),
            'metirka_kim_tomonidan' => Yii::t('strings','Метирка ким томонидан берилган'),
            'nikox_turi' => Yii::t('strings','Никох тури'),
            'nikox_guvohnom_raqami' => Yii::t('strings','Никох гувохнома рақами'),
            'ish_joy_nomi' => Yii::t('strings','Иш жойи номи'),
            'lavozimi' => Yii::t('strings','Лавозими'),
            'daromad_turi_id' => Yii::t('strings','Даромад тури'),
            'daromad_miqdori' => Yii::t('strings','Даромад миқдори'),
            'daromad_olish_sanasi' => Yii::t('strings','Даромад олиш санаси'),
            'uzoq_muddatga_ketgan_vaqti' => Yii::t('strings','Узоқ муддатга кетган вактди'),
            'uzoq_muddatga_kelgan_vaqti' => Yii::t('strings','Узоқ муддатдан келган вакти'),
            'uzoq_muddatga_ketgan_davlati' => Yii::t('strings','Узоқ муддатга кетган ёки бориб келган давлат номи'),
			'uzoq_muddat_ish' => Yii::t('strings','Узоқ муддатга борган жойида нима иш билан шуғулланган'),
            'uzoq_muddat_suhbat_otgani' => Yii::t('strings','Узоқ муддатдан кeлиб сухбат ва тиббий кўрикдан ўтганлиги'),
            'malumoti_id' => Yii::t('strings','Маълумоти'),
            'bitirgan_joy_id' => Yii::t('strings','Битирган жойи ёки ўқиш жойи'),
            'bitirgan_mutaxasisligi' => Yii::t('strings','Мутахассислиги'),
            'bitirgan_yili' => Yii::t('strings','Битирган йили'),
            'aliment_id' => Yii::t('strings','Алимент (Майдонга 0 рақами киритманг!!!)'),
            'aliment_qarz' => Yii::t('strings','Алиментдан қарздорлиги'),
            'notinch_oila_id' => Yii::t('strings','Нотинч оила'),
            'sudlanganligi' => Yii::t('strings','Судланганлиги (қачон ва қайси модда)'),
            'iib_xisobida_turadimi' => Yii::t('strings','ИИБ ҳисобида туриши'),
            'deo_azoligi_id' => Yii::t('strings','ДЭО аъзолар рўйхатида'),
            'fuqaro_uy_id' => Yii::t('strings','Fuqaro Uy'),
            'kassalik_id' => Yii::t('strings','Касаллик ёки ногиронлиги'),
            'kassalik_xolati' => Yii::t('strings','Касаллик даражаси'),
            'sogligi' => Yii::t('strings','Диагноз'),
            'oila_boshligi' => Yii::t('strings','Оила бошлиғи (агар оила бошлиғи бўлса 1 ни қўйинг, бўлмаса тўлдирманг)'),
            'anketa_ozgarish' => Yii::t('strings','Анкета ўзгарган санаси'),
            'user_id' =>Yii::t('strings','Фойдаланувчи'),
			'user_date' => Yii::t('strings','Сана'),
            'onasi_id' => Yii::t('strings','Онаси'),
            'otasi_id' => Yii::t('strings','Отаси'),


		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fio',$this->fio,true);
		$criteria->compare('jinsi_id',$this->jinsi_id);
        $criteria->compare('millati_id',$this->millati_id);
		$criteria->compare('tugilgan_sanasi',$this->tugilgan_sanasi,true);
        $criteria->compare('rasm',$this->rasm,true);
        $criteria->compare('davlat_id',$this->davlat_id);
        $criteria->compare('tugilgan_joy_viloyat_id',$this->tugilgan_joy_viloyat_id);
        $criteria->compare('tugilgan_joy_tuman_id',$this->tugilgan_joy_tuman_id);
        $criteria->compare('passport_seyiya',$this->passport_seyiya,true);
		$criteria->compare('passport_raqami',$this->passport_raqami,true);
		$criteria->compare('passport_vaqti',$this->passport_vaqti,true);
		$criteria->compare('passport_kim_tomonidan_id',$this->passport_kim_tomonidan_id);
		$criteria->compare('metirka_raqami',$this->metirka_raqami,true);
		$criteria->compare('metirka_kim_tomonidan',$this->metirka_kim_tomonidan,true);
        $criteria->compare('nikox_turi',$this->nikox_turi,true);
        $criteria->compare('nikox_guvohnom_raqami',$this->nikox_guvohnom_raqami,true);
        $criteria->compare('ish_joy_nomi',$this->ish_joy_nomi,true);
        $criteria->compare('lavozimi',$this->lavozimi,true);
        $criteria->compare('daromad_turi_id',$this->daromad_turi_id);
        $criteria->compare('daromad_miqdori',$this->daromad_miqdori,true);
        $criteria->compare('daromad_olish_sanasi',$this->daromad_olish_sanasi,true);
        $criteria->compare('uzoq_muddatga_ketgan_vaqti',$this->uzoq_muddatga_ketgan_vaqti,true);
        $criteria->compare('uzoq_muddatga_kelgan_vaqti',$this->uzoq_muddatga_kelgan_vaqti,true);
        $criteria->compare('uzoq_muddatga_ketgan_davlati',$this->uzoq_muddatga_ketgan_davlati,true);
        $criteria->compare('uzoq_muddat_suhbat_otgani',$this->uzoq_muddat_suhbat_otgani,true);
        $criteria->compare('malumoti_id',$this->malumoti_id);
        $criteria->compare('bitirgan_joy_id',$this->bitirgan_joy_id,true);
        $criteria->compare('bitirgan_mutaxasisligi',$this->bitirgan_mutaxasisligi,true);
        $criteria->compare('bitirgan_yili',$this->bitirgan_yili,true);
        $criteria->compare('aliment_id',$this->aliment_id);
        $criteria->compare('aliment_qarz',$this->aliment_qarz,true);
        $criteria->compare('notinch_oila_id',$this->notinch_oila_id,true);
        $criteria->compare('sudlanganligi',$this->sudlanganligi,true);
        $criteria->compare('iib_xisobida_turadimi',$this->iib_xisobida_turadimi);
        $criteria->compare('deo_azoligi_id',$this->deo_azoligi_id);
        $criteria->compare('fuqaro_uy_id',$this->fuqaro_uy_id);
        $criteria->compare('kassalik_id',$this->kassalik_id);
        $criteria->compare('kassalik_xolati',$this->kassalik_xolati);
        $criteria->compare('sogligi',$this->sogligi,true);
        $criteria->compare('oila_boshligi',$this->oila_boshligi);
        $criteria->compare('anketa_ozgarish',$this->anketa_ozgarish,true);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('user_date',$this->user_date,true);
        $criteria->compare('otasi_id',$this->otasi_id);
        $criteria->compare('onasi_id',$this->onasi_id);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Fuqaro the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
