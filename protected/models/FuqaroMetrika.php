<?php
/**
 * Created by JetBrains PhpStorm.
 * User: MUSLIMBEK
 * Date: 15.09.17
 * Time: 19:47
 * To change this template use File | Settings | File Templates.
 */
class FuqaroMetrika extends Fuqaro
{
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(

            array('metirka_raqami, metirka_kim_tomonidan','required'),

            array('jinsi_id, tugilgan_joy_tuman_id, tugilgan_joy_viloyat_id, daromad_turi_id, fio, tugilgan_sanasi, davlat_id,nikox_turi,iib_xisobida_turadimi, deo_azoligi_id,malumoti_id,aliment_id,notinch_oila_id, kassalik_id, kassalik_xolati, oila_boshligi ','required'),
            array('jinsi_id, millati_id, davlat_id, tugilgan_joy_viloyat_id, tugilgan_joy_tuman_id, passport_kim_tomonidan_id, daromad_turi_id, uzoq_muddatga_ketgan_davlati, malumoti_id, aliment_id, iib_xisobida_turadimi, deo_azoligi_id, fuqaro_uy_id, kassalik_id, kassalik_xolati, oila_boshligi, user_id, otasi_id, onasi_id', 'numerical', 'integerOnly'=>true),
            array('fio, rasm, passport_vaqti, metirka_raqami, metirka_kim_tomonidan, nikox_guvohnom_raqami, nikox_turi, ish_joy_nomi, lavozimi, daromad_miqdori, daromad_olish_sanasi, uzoq_muddat_ish, uzoq_muddat_suhbat_otgani, bitirgan_joy_id, bitirgan_mutaxasisligi, bitirgan_yili, aliment_qarz, notinch_oila_id, sogligi', 'length', 'max'=>255),
            array('passport_seyiya', 'length', 'max'=>2),
			array('passport_raqami', 'length', 'max'=>7),
            array('tugilgan_sanasi, uzoq_muddatga_ketgan_vaqti, uzoq_muddatga_kelgan_vaqti, sudlanganligi, anketa_ozgarish, user_date', 'safe'),
            array('rasm', 'file', 'types'=>'jpg, gif, png','allowEmpty'=>true, 'on'=>'update'),
            array('date', 'safe'),
            array( 'rasm', 'length', 'max'=>255, 'on'=>'insert,update'),

            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, fio, jinsi_id, millati_id, tugilgan_sanasi, rasm, davlat_id, tugilgan_joy_viloyat_id, tugilgan_joy_tuman_id, passport_seyiya, passport_raqami, passport_vaqti, passport_kim_tomonidan_id, metirka_raqami, metirka_kim_tomonidan, nikox_guvohnom_raqami, nikox_turi, ish_joy_nomi, lavozimi, daromad_turi_id, daromad_miqdori, daromad_olish_sanasi, uzoq_muddatga_ketgan_vaqti, uzoq_muddatga_kelgan_vaqti, uzoq_muddatga_ketgan_davlati, uzoq_muddat_ish, uzoq_muddat_suhbat_otgani, malumoti_id, bitirgan_joy_id, bitirgan_mutaxasisligi, bitirgan_yili, aliment_id, aliment_qarz, notinch_oila_id, sudlanganligi, iib_xisobida_turadimi, deo_azoligi_id, fuqaro_uy_id, kassalik_id, kassalik_xolati, sogligi, oila_boshligi, anketa_ozgarish, user_id, user_date, otasi_id, onasi_id', 'safe', 'on'=>'search'),
        );
    }

}
