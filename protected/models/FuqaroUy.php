<?php

/**
 * This is the model class for table "{{fuqaro_uy}}".
 *
 * The followings are the available columns in table '{{fuqaro_uy}}':
 * @property integer $id
 * @property integer $fviloyat_id
 * @property integer $ftuman_id
 * @property integer $mfy_id
 * @property integer $kocha_id
 * @property string $uy_raqami
 * @property string $kadastr_mavjudligi
 * @property string $kadastr_seriyasi_raqami
 * @property string $kadastr_buyruq_raqami
 * @property string $ijara
 * @property integer $azo_soni
 * @property integer $jon_soni
 * @property integer $voya_yetmagan_soni
 * @property string $taklif
 * @property string $muammosi
 * @property integer $ishchi_guruh_fio_id
 * @property string $tel_raqami
 * @property string $anketa_sana
 */
class FuqaroUy extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{fuqaro_uy}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('fviloyat_id, ftuman_id, mfy_id, kocha_id, azo_soni, jon_soni, voya_yetmagan_soni, ishchi_guruh_fio_id', 'numerical', 'integerOnly'=>true),
			array('uy_raqami, kadastr_mavjudligi, kadastr_seriyasi_raqami, kadastr_buyruq_raqami, ijara, taklif, muammosi, tel_raqami', 'length', 'max'=>255),
			array('anketa_sana', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fviloyat_id, ftuman_id, mfy_id, kocha_id, uy_raqami, kadastr_mavjudligi, kadastr_seriyasi_raqami, kadastr_buyruq_raqami, ijara, azo_soni, jon_soni, voya_yetmagan_soni, taklif, muammosi, ishchi_guruh_fio_id, tel_raqami, anketa_sana', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'fviloyat_id' => Yii::t('strings','Fviloyat'),
			'ftuman_id' => Yii::t('strings','Ftuman'),
			'mfy_id' => Yii::t('strings','Mfy'),
			'kocha_id' => Yii::t('strings','Kocha'),
			'uy_raqami' => Yii::t('strings','Uy Raqami'),
			'kadastr_mavjudligi' => Yii::t('strings','Kadastr Mavjudligi'),
			'kadastr_seriyasi_raqami' => Yii::t('strings','Kadastr Seriyasi Raqami'),
			'kadastr_buyruq_raqami' => Yii::t('strings','Kadastr Buyruq Raqami'),
			'ijara' => Yii::t('strings','Ijara'),
			'azo_soni' => Yii::t('strings','Azo Soni'),
			'jon_soni' => Yii::t('strings','Jon Soni'),
			'voya_yetmagan_soni' => Yii::t('strings','Voya Yetmagan Soni'),
			'taklif' => Yii::t('strings','Taklif'),
			'muammosi' => Yii::t('strings','Muammosi'),
			'ishchi_guruh_fio_id' => Yii::t('strings','Ishchi Guruh Fio'),
			'tel_raqami' => Yii::t('strings','Tel Raqami'),
			'anketa_sana' => Yii::t('strings','Anketa Sana'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fviloyat_id',$this->fviloyat_id);
		$criteria->compare('ftuman_id',$this->ftuman_id);
		$criteria->compare('mfy_id',$this->mfy_id);
		$criteria->compare('kocha_id',$this->kocha_id);
		$criteria->compare('uy_raqami',$this->uy_raqami,true);
		$criteria->compare('kadastr_mavjudligi',$this->kadastr_mavjudligi,true);
		$criteria->compare('kadastr_seriyasi_raqami',$this->kadastr_seriyasi_raqami,true);
		$criteria->compare('kadastr_buyruq_raqami',$this->kadastr_buyruq_raqami,true);
		$criteria->compare('ijara',$this->ijara,true);
		$criteria->compare('azo_soni',$this->azo_soni);
		$criteria->compare('jon_soni',$this->jon_soni);
		$criteria->compare('voya_yetmagan_soni',$this->voya_yetmagan_soni);
		$criteria->compare('taklif',$this->taklif,true);
		$criteria->compare('muammosi',$this->muammosi,true);
		$criteria->compare('ishchi_guruh_fio_id',$this->ishchi_guruh_fio_id);
		$criteria->compare('tel_raqami',$this->tel_raqami,true);
		$criteria->compare('anketa_sana',$this->anketa_sana,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FuqaroUy the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
