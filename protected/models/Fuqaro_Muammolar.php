<?php

/**
 * This is the model class for table "{{muammo}}".
 *
 * The followings are the available columns in table '{{muammo}}':
 * @property integer $id
 * @property integer $viloyat_id
 * @property integer $tuman_id
 * @property integer $mfy_id
 * @property integer $kocha_id
 * @property string $muammo_mazmuni
 * @property integer $muammo_xolati_id
 * @property string $xulosa
 * @property integer $fuqaro_uy_id
 * @property integer $fuqaro_id
 * @property integer $tashkilot_id
 * @property string $qayd_sana
 * @property string $muddat_sana
 * @property string $korilgan_sana
 * @property string $xal_etilgan_sana
 * @property integer $user_id
 *
 * The followings are the available model relations:
 * @property Tashkilot $tashkilot
 * @property Fuqaro $fuqaro
 * @property User $user
 * @property MuammoXolati $muammoXolati
 * @property Viloyatlar $viloyat
 * @property Tuman $tuman
 * @property Mfy $mfy
 * @property Kochalar $kocha
 * @property FuqaroUy $fuqaroUy
 */
class Fuqaro_Muammolar extends CActiveRecord
{
    protected function beforeSave()
    {
        if(parent::beforeSave())
        {
            if(trim($this->muddat_sana)=="") $this->muddat_sana=null;
            if(trim($this->muddat_sana)=="") $this->muddat_sana=null;
            if($this->muddat_sana!==null)
            $this->muddat_sana=date('Y-m-d',strtotime($this->muddat_sana));
            return true;
        } else
        {
            return false;
        }
    }


    protected function afterFind() {
        $date = ($this->muddat_sana==null) ? null : date('d.m.Y', strtotime($this->muddat_sana));
        $this->muddat_sana= $date;
        parent::afterFind();
    }

    public function getxolat()
    {
        $xolat = Muammo_Xolati::model()->findAll();
        $xolat = CHtml::listData($xolat,'id','xulosa');
        return $xolat;
    }
    public function getfviloyat()
    {
        $fviloyat = Viloyatlar::model()->findAll();
        $fviloyat = CHtml::listData($fviloyat,'id','viloyat_nomi');
        foreach ($fviloyat as $key=>$val){
            $fviloyat[$key]=Yii::t("strings",CHtml::encode($val));

        }
        return $fviloyat;
    }
    public function gettashkilot()
    {
       $tashkilot = Tashkilot::model()->findAll();
        $tashkilot =CHtml::listData($tashkilot,'id','tnomi');
        return $tashkilot;
    }
    public function getmuammoturlari()
    {
       $muammo_turlari = Muammo_Turlari::model()->findAll();
        $muammo_turlari =CHtml::listData($muammo_turlari,'id','muammo_turlari');
        return $muammo_turlari;
    }

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{muammo}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('viloyat_id, tuman_id, mfy_id, kocha_id, muammo_turlari, fuqaro_uy_id, fuqaro_id, muammo_mazmuni','required'),
			array('viloyat_id, tuman_id, mfy_id, kocha_id, muammo_turlari, muammo_xolati_id, fuqaro_uy_id, fuqaro_id, tashkilot_id, user_id, etiroz', 'numerical', 'integerOnly'=>true),
			array('muammo_mazmuni, xulosa', 'length', 'max'=>5000),
			array('xal_etuvchi_fio', 'length', 'max'=>30),
			array('qayd_sana, muddat_sana, korilgan_sana, xal_etilgan_sana', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, viloyat_id, tuman_id, mfy_id, kocha_id, muammo_turlari, muammo_mazmuni, muammo_xolati_id, xulosa, fuqaro_uy_id, fuqaro_id, tashkilot_id, qayd_sana, muddat_sana, korilgan_sana, xal_etilgan_sana, user_id, xal_etuvchi_fio, etiroz', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'muammoTurlari' => array(self::BELONGS_TO,'MuammoTurlari', 'muammo_turlari'),
			'tashkilot' => array(self::BELONGS_TO, 'Tashkilot', 'tashkilot_id'),
			'fuqaro' => array(self::BELONGS_TO, 'Fuqaro', 'fuqaro_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'muammoXolati' => array(self::BELONGS_TO, 'Muammo_Xolati', 'muammo_xolati_id'),
			'viloyat' => array(self::BELONGS_TO, 'Viloyatlar', 'viloyat_id'),
			'tuman' => array(self::BELONGS_TO, 'Tuman', 'tuman_id'),
			'mfy' => array(self::BELONGS_TO, 'Mfy', 'mfy_id'),
			'kocha' => array(self::BELONGS_TO, 'Kochalar', 'kocha_id'),
			'fuqaroUy' => array(self::BELONGS_TO, 'FuqaroUy', 'fuqaro_uy_id'),
            'etiroz0' => array(self::BELONGS_TO, 'MuammoXolati', 'etiroz'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'viloyat_id' => Yii::t('strings','Вилоят'),
			'tuman_id' => Yii::t('strings','Туман'),
			'mfy_id' => Yii::t('strings','МФЙ'),
			'kocha_id' => Yii::t('strings','Кўча номи'),
			'muammo_turlari' => Yii::t('strings','Муаммо турини кўрсатинг'),
			'muammo_mazmuni' => Yii::t('strings','Муаммо мазмуни'),
			'muammo_xolati_id' => Yii::t('strings','Муаммо холати'),
			'xulosa' => Yii::t('strings','Хулоса'),
			'fuqaro_uy_id' => Yii::t('strings','Фуқаронинг уй рақами'),
			'fuqaro_id' => Yii::t('strings','Фуқаронинг ФИШ'),
			'tashkilot_id' => Yii::t('strings','Ташкилот номи'),
			'qayd_sana' => Yii::t('strings','Муаммо қайд этилган сана'),
			'muddat_sana' => Yii::t('strings','Муддати'),
			'korilgan_sana' => Yii::t('strings','Муаммо кўрилган сана'),
			'xal_etilgan_sana' => Yii::t('strings','Муаммо хал этилган сана'),
			'user_id' => Yii::t('strings','Фолдаланувчи номи'),
            'xal_etuvchi_fio' => Yii::t('strings','Хал этувчи шахс ФИО'),
            'etiroz' => Yii::t('strings','Эътироз'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('viloyat_id',$this->viloyat_id);
		$criteria->compare('tuman_id',$this->tuman_id);
		$criteria->compare('mfy_id',$this->mfy_id);
		$criteria->compare('kocha_id',$this->kocha_id);
		$criteria->compare('muammo_turlari',$this->muammo_turlari);
		$criteria->compare('muammo_mazmuni',$this->muammo_mazmuni,true);
		$criteria->compare('muammo_xolati_id',$this->muammo_xolati_id);
		$criteria->compare('xulosa',$this->xulosa,true);
		$criteria->compare('fuqaro_uy_id',$this->fuqaro_uy_id);
		$criteria->compare('fuqaro_id',$this->fuqaro_id);
		$criteria->compare('tashkilot_id',$this->tashkilot_id);
		$criteria->compare('qayd_sana',$this->qayd_sana,true);
		$criteria->compare('muddat_sana',$this->muddat_sana,true);
		$criteria->compare('korilgan_sana',$this->korilgan_sana,true);
		$criteria->compare('xal_etilgan_sana',$this->xal_etilgan_sana,true);
		$criteria->compare('user_id',$this->user_id);
        $criteria->compare('xal_etuvchi_fio',$this->xal_etuvchi_fio,true);
        $criteria->compare('etiroz',$this->etiroz);

        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Fuqaro_Muammolar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
