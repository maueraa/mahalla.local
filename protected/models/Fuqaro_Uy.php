<?php

/**
 * This is the model class for table "{{fuqaro_uy}}".
 *
 * The followings are the available columns in table '{{fuqaro_uy}}':
 * @property integer $id
 * @property integer $fviloyat_id
 * @property integer $ftuman_id
 * @property integer $mfy_id
 * @property integer $kocha_id
 * @property integer $uy_raqami
 * @property string $kadastr_mavjudligi
 * @property string $kadastr_seriyasi_raqami
 * @property string $kadastr_buyruq_raqami
 * @property string $ijara
 * @property integer $azo_soni
 * @property integer $jon_soni
 * @property integer $voya_yetmagan_soni
 * @property string $taklif
 * @property string $muammosi
 * @property integer $ishchi_guruh_fio_id
 * @property string $tel_raqami
 * @property string $anketa_sana
 *
 * The followings are the available model relations:
 * @property Daromad[] $daromads
 * @property Mfy $mfy
 * @property IshchiGuruhFio $ishchiGuruhFio
 * @property Viloyatlar1 $fviloyat
 * @property Tumanq $ftuman
 * @property Hayvon[] $hayvons
 * @property IjtimoiyMuhit[] $ijtimoiyMuhits
 * @property KochaXolati[] $kochaXolatis
 * @property Manaviy[] $manaviys
 * @property Qarz[] $qarzs
 * @property Shaoriti[] $shaoritis
 * @property Tomorqa[] $tomorqas
 * @property TransportQatnovi[] $transportQatnovis
 * @property XimzatKorsatish[] $ximzatKorsatishes
 * @property XonadonTransport[] $xonadonTransports
 */
class Fuqaro_Uy extends CActiveRecord
{
    public function getfviloyat()
    {
        $fviloyat = Viloyatlar::model()->findAll();
        $fviloyat = CHtml::listData($fviloyat,'id','viloyat_nomi');
        foreach ($fviloyat as $key=>$val){
            $fviloyat[$key]=Yii::t("strings",CHtml::encode($val));

        }
        return $fviloyat;
    }
    public function getftuman()
    {
        $ftuman = Tuman::model()->findAll();
        $ftuman= CHtml::listData($ftuman,'id','tuman_nomi','viloyat_id');
        return $ftuman;
    }
    public function getfmfy()
    {
        $fmfy = Mfy::model()->findAll();
        $fmfy= CHtml::listData($fmfy,'id','tuman_nomi','viloyat_id');
        return $fmfy;
    }

    public function getfkocha()
    {
        $fkocha = Kochalar::model()->findAll();
        $fkocha = CHtml::listData($fkocha,'id','tuman_nomi','viloyat_id');
        return $fkocha;
    }
public function getfishchi_guruh()
    {
        $fishchi_guruh = Ishchi_Guruh_Fio::model()->findAll();
        $fishchi_guruh = CHtml::listData($fishchi_guruh,'id','ishchi_guruh_fio');
        return $fishchi_guruh;
    }
    public function getnotinch2()
    {
        $notinch2= Notinch::model()->findAll();
        $notinch2= CHtml::listData($notinch2,'id','oila_xolati');
        return $notinch2;
    }
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{fuqaro_uy}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fviloyat_id, ftuman_id, mfy_id, kocha_id, azo_soni,ishchi_guruh_fio_id,voya_yetmagan_soni,oila_xolati,kadastr_mavjudligi,','required'),
			array('fviloyat_id, ftuman_id, mfy_id, kocha_id, uy_raqami, azo_soni, jon_soni, voya_yetmagan_soni, oila_xolati, ishchi_guruh_fio_id, user_id', 'numerical', 'integerOnly'=>true),
			array('uy_raqami_position, kadastr_mavjudligi, kadastr_seriyasi_raqami, kadastr_buyruq_raqami, ijara, taklif, muammosi, ishchi_guruh_fio, tel_raqami', 'length', 'max'=>255),
			array('anketa_sana, anketa_ozgarish, user_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fviloyat_id, ftuman_id, mfy_id, kocha_id, uy_raqami, uy_raqami_position, kadastr_mavjudligi, kadastr_seriyasi_raqami, kadastr_buyruq_raqami, ijara, azo_soni, jon_soni, voya_yetmagan_soni, taklif, muammosi, oila_xolati, ishchi_guruh_fio_id, ishchi_guruh_fio, tel_raqami, anketa_sana, anketa_ozgarish, user_id, user_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daromads' => array(self::HAS_MANY, 'Daromad', 'fuqaro_uy_id'),
			'mfy' => array(self::BELONGS_TO, 'Mfy', 'mfy_id'),
			'ishchiGuruhFio' => array(self::BELONGS_TO, 'Ishchi_Guruh_Fio', 'ishchi_guruh_fio_id'),
			'fviloyat' => array(self::BELONGS_TO, 'Viloyatlar', 'fviloyat_id'),
			'ftuman' => array(self::BELONGS_TO, 'Tuman', 'ftuman_id'),
			'kochalar' => array(self::BELONGS_TO, 'Kochalar', 'kocha_id'),
            'oilaXolati' => array(self::BELONGS_TO, 'Notinch', 'oila_xolati'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'hayvons' => array(self::HAS_MANY, 'Hayvon', 'tbl_fuqaro_uy_id'),
			'ijtimoiyMuhits' => array(self::HAS_MANY, 'IjtimoiyMuhit', 'tbl_fuqaro_uy_id'),
			'kochaXolatis' => array(self::HAS_MANY, 'KochaXolati', 'fuqaro_uy_id'),
			'manaviys' => array(self::HAS_MANY, 'Manaviy', 'fuqaro_uy_id'),
			'qarzs' => array(self::HAS_MANY, 'Qarz', 'tbl_fuqaro_uy_id'),
			'shaoritis' => array(self::HAS_MANY, 'Shaoriti', 'tbl_fuqaro_uy_id'),
			'tomorqas' => array(self::HAS_MANY, 'Tomorqa', 'tbl_fuqaro_uy_id'),
			'transportQatnovis' => array(self::HAS_MANY, 'TransportQatnovi', 'fuqaro_uy_id'),
			'ximzatKorsatishes' => array(self::HAS_MANY, 'XimzatKorsatish', 'tbl_fuqaro_uy_id'),
			'xonadonTransports' => array(self::HAS_MANY, 'XonadonTransport', 'fuqaro_uy_id'),
			'fuqaro' => array(self::HAS_MANY, 'Fuqaro', 'fuqaro_uy_id'),
            'muammos' => array(self::HAS_MANY, 'Muammo', 'fuqaro_uy_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'fviloyat_id' => Yii::t('strings','Вилоят'),
			'ftuman_id' => Yii::t('strings','Туман'),
			'mfy_id' => Yii::t('strings','МФЙ номи'),
			'kocha_id' => Yii::t('strings','Кўча номи'),
			'uy_raqami' => Yii::t('strings','Уй рақами'),
            'uy_raqami_position' => Yii::t('strings','Уй рақамининг индекси'),
            'kadastr_mavjudligi' => Yii::t('strings','Кадастр мавжудлиги'),
            'kadastr_seriyasi_raqami' => Yii::t('strings','Кадастр серия рақами'),
            'kadastr_buyruq_raqami' => Yii::t('strings','Кадастр буйруқ рақами'),
            'ijara' => Yii::t('strings','Ижара'),
            'azo_soni' => Yii::t('strings','Аъзо сони'),
            'jon_soni' => Yii::t('strings','Jon Soni'),
            'voya_yetmagan_soni' => Yii::t('strings','Вояга етмаганлар сони'),
            'taklif' => Yii::t('strings','Таклиф'),
            'muammosi' => Yii::t('strings','Муаммоси'),
            'oila_xolati' => Yii::t('strings','Оилавий холати(тинч ёки жанжалкаш)'),
            'ishchi_guruh_fio_id' => Yii::t('strings','Ишчи гурухдаги ташкилот номи'),
			'ishchi_guruh_fio' => Yii::t('strings','Ишчи гурухдаги ташкилот рахбари ФИШ'),
			'tel_raqami' => Yii::t('strings','Телефон рақами +998'),
            'anketa_sana' => Yii::t('strings','Анкета санаси'),
            'anketa_ozgarish' => Yii::t('strings','Анкета ўзгарган санаси'),
            'user_id' => Yii::t('strings','Фойдаланувчи'),
			'user_date' =>Yii::t('strings','Сана'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fviloyat_id',$this->fviloyat_id);
		$criteria->compare('ftuman_id',$this->ftuman_id);
		$criteria->compare('mfy_id',$this->mfy_id);
		$criteria->compare('kocha_id',$this->kocha_id);
        $criteria->compare('uy_raqami',$this->uy_raqami);
        $criteria->compare('uy_raqami_position',$this->uy_raqami_position,true);
        $criteria->compare('kadastr_mavjudligi',$this->kadastr_mavjudligi,true);
        $criteria->compare('kadastr_seriyasi_raqami',$this->kadastr_seriyasi_raqami,true);
        $criteria->compare('kadastr_buyruq_raqami',$this->kadastr_buyruq_raqami,true);
        $criteria->compare('ijara',$this->ijara,true);
        $criteria->compare('azo_soni',$this->azo_soni);
        $criteria->compare('jon_soni',$this->jon_soni);
        $criteria->compare('voya_yetmagan_soni',$this->voya_yetmagan_soni);
        $criteria->compare('taklif',$this->taklif,true);
        $criteria->compare('muammosi',$this->muammosi,true);
        $criteria->compare('oila_xolati',$this->oila_xolati);
        $criteria->compare('ishchi_guruh_fio_id',$this->ishchi_guruh_fio_id);
		$criteria->compare('ishchi_guruh_fio',$this->ishchi_guruh_fio,true);
        $criteria->compare('tel_raqami',$this->tel_raqami,true);
        $criteria->compare('anketa_sana',$this->anketa_sana,true);
        $criteria->compare('anketa_ozgarish',$this->anketa_ozgarish,true);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('user_date',$this->user_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Fuqaro_Uy the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
