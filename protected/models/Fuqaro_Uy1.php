<?php

/**
 * This is the model class for table "{{fuqaro_uy}}".
 *
 * The followings are the available columns in table '{{fuqaro_uy}}':
 * @property integer $id
 * @property integer $f_id
 * @property integer $fviloyat_id
 * @property integer $ftuman_id
 * @property integer $mfy_id
 * @property integer $kocha_id
 * @property integer $uy_raqami
 * @property string $kadastr_mavjudligi
 * @property string $kadastr_seriyasi_raqami
 * @property string $kadastr_buyruq_raqami
 * @property string $ijara
 * @property integer $azo_soni
 * @property integer $jon_soni
 * @property integer $voya_yetmagan_soni
 * @property string $taklif
 * @property string $muammosi
 * @property integer $ishchi_guruh_fio_id
 * @property string $tel_raqami
 * @property string $anketa_sana
 *
 * The followings are the available model relations:
 * @property Daromad[] $daromads
 * @property Mfy $mfy
 * @property IshchiGuruhFio $ishchiGuruhFio
 * @property Viloyatlar $fviloyat
 * @property Tuman $ftuman
 * @property Fuqaro $f
 * @property Hayvon[] $hayvons
 * @property IjtimoiyMuhit[] $ijtimoiyMuhits
 * @property KochaXolati[] $kochaXolatis
 * @property Manaviy[] $manaviys
 * @property Qarz[] $qarzs
 * @property Shaoriti[] $shaoritis
 * @property Tomorqa[] $tomorqas
 * @property TransportQatnovi[] $transportQatnovis
 * @property XimzatKorsatish[] $ximzatKorsatishes
 * @property XonadonTransport[] $xonadonTransports
 */
class Fuqaro_Uy extends CActiveRecord
{
    public function getfviloyat()
    {
        $fuyviloyati = Viloyatlar::model()->findAll();
        $fuyviloyati = CHtml::listData($fuyviloyati,'id','viloyat_nomi');
        return $fuyviloyati;
    }
    public function getftuman()
    {
        $ftuman = Tuman::model()->findAll();
        $ftuman = CHtml::listData($ftuman,'id','tuman_nomi');
        return $ftuman;
    }
    public function getfmfy()
    {
        $fmfy = Mfy::model()->findAll();
        $fmfy = CHtml::listData($fmfy,'id','mfy_nomi');
        return $fmfy;
    }

    public function getfkocha()
    {
        $fkocha = Kochalar::model()->findAll();
        $fkocha = CHtml::listData($fkocha,'id','tbl_kocha_nomi');
        return $fkocha;
    }
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{fuqaro_uy}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('f_id, fviloyat_id, ftuman_id, mfy_id, kocha_id, uy_raqami, azo_soni, jon_soni, voya_yetmagan_soni, ishchi_guruh_fio_id', 'numerical', 'integerOnly'=>true),
			array('kadastr_mavjudligi, kadastr_seriyasi_raqami, kadastr_buyruq_raqami, ijara, taklif, muammosi, tel_raqami', 'length', 'max'=>255),
			array('anketa_sana', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, f_id, fviloyat_id, ftuman_id, mfy_id, kocha_id, uy_raqami, kadastr_mavjudligi, kadastr_seriyasi_raqami, kadastr_buyruq_raqami, ijara, azo_soni, jon_soni, voya_yetmagan_soni, taklif, muammosi, ishchi_guruh_fio_id, tel_raqami, anketa_sana', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daromads' => array(self::HAS_MANY, 'Daromad', 'fuqaro_uy_id'),
			'mfy' => array(self::BELONGS_TO, 'Mfy', 'mfy_id'),
			'ishchiGuruhFio' => array(self::BELONGS_TO, 'IshchiGuruhFio', 'ishchi_guruh_fio_id'),
			'fviloyat' => array(self::BELONGS_TO, 'Viloyatlar', 'fviloyat_id'),
			'ftuman' => array(self::BELONGS_TO, 'Tuman', 'ftuman_id'),
			'f' => array(self::BELONGS_TO, 'Fuqaro', 'f_id'),
			'hayvons' => array(self::HAS_MANY, 'Hayvon', 'tbl_fuqaro_uy_id'),
			'ijtimoiyMuhits' => array(self::HAS_MANY, 'IjtimoiyMuhit', 'tbl_fuqaro_uy_id'),
			'kochaXolatis' => array(self::HAS_MANY, 'KochaXolati', 'fuqaro_uy_id'),
			'manaviys' => array(self::HAS_MANY, 'Manaviy', 'fuqaro_uy_id'),
			'qarzs' => array(self::HAS_MANY, 'Qarz', 'tbl_fuqaro_uy_id'),
			'shaoritis' => array(self::HAS_MANY, 'Shaoriti', 'tbl_fuqaro_uy_id'),
			'tomorqas' => array(self::HAS_MANY, 'Tomorqa', 'tbl_fuqaro_uy_id'),
			'transportQatnovis' => array(self::HAS_MANY, 'TransportQatnovi', 'fuqaro_uy_id'),
			'ximzatKorsatishes' => array(self::HAS_MANY, 'XimzatKorsatish', 'tbl_fuqaro_uy_id'),
			'xonadonTransports' => array(self::HAS_MANY, 'XonadonTransport', 'fuqaro_uy_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'f_id' => Yii::t('strings','F'),
			'fviloyat_id' => Yii::t('strings','Fviloyat'),
			'ftuman_id' => Yii::t('strings','Ftuman'),
			'mfy_id' => Yii::t('strings','Mfy'),
			'kocha_id' => Yii::t('strings','Kocha'),
			'uy_raqami' => Yii::t('strings','Uy Raqami'),
			'kadastr_mavjudligi' => Yii::t('strings','Kadastr Mavjudligi'),
			'kadastr_seriyasi_raqami' => Yii::t('strings','Kadastr Seriyasi Raqami'),
			'kadastr_buyruq_raqami' => Yii::t('strings','Kadastr Buyruq Raqami'),
			'ijara' => Yii::t('strings','Ijara'),
			'azo_soni' => Yii::t('strings','Azo Soni'),
			'jon_soni' => Yii::t('strings','Jon Soni'),
			'voya_yetmagan_soni' => Yii::t('strings','Voya Yetmagan Soni'),
			'taklif' => Yii::t('strings','Taklif'),
			'muammosi' => Yii::t('strings','Muammosi'),
			'ishchi_guruh_fio_id' => Yii::t('strings','Ishchi Guruh Fio'),
			'tel_raqami' => Yii::t('strings','Tel Raqami'),
			'anketa_sana' => Yii::t('strings','Anketa Sana'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('f_id',$this->f_id);
		$criteria->compare('fviloyat_id',$this->fviloyat_id);
		$criteria->compare('ftuman_id',$this->ftuman_id);
		$criteria->compare('mfy_id',$this->mfy_id);
		$criteria->compare('kocha_id',$this->kocha_id);
		$criteria->compare('uy_raqami',$this->uy_raqami);
		$criteria->compare('kadastr_mavjudligi',$this->kadastr_mavjudligi,true);
		$criteria->compare('kadastr_seriyasi_raqami',$this->kadastr_seriyasi_raqami,true);
		$criteria->compare('kadastr_buyruq_raqami',$this->kadastr_buyruq_raqami,true);
		$criteria->compare('ijara',$this->ijara,true);
		$criteria->compare('azo_soni',$this->azo_soni);
		$criteria->compare('jon_soni',$this->jon_soni);
		$criteria->compare('voya_yetmagan_soni',$this->voya_yetmagan_soni);
		$criteria->compare('taklif',$this->taklif,true);
		$criteria->compare('muammosi',$this->muammosi,true);
		$criteria->compare('ishchi_guruh_fio_id',$this->ishchi_guruh_fio_id);
		$criteria->compare('tel_raqami',$this->tel_raqami,true);
		$criteria->compare('anketa_sana',$this->anketa_sana,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Fuqaro_Uy the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
