<?php

/**
 * This is the model class for table "{{fuqaro}}".
 *
 * The followings are the available columns in table '{{fuqaro}}':
 * @property integer $id
 * @property string $fio
 * @property integer $jinsi_id
 * @property string $tugilgan_sanasi
 * @property integer $tugilgan_joy_viloyat_id
 * @property integer $tugilgan_joy_tuman_id
 * @property string $passport_raqami
 * @property string $passport_vaqti
 * @property integer $passport_kim_tomonidan_id
 * @property string $metirka_raqami
 * @property string $metirka_kim_tomonidan
 * @property string $ish_joy_nomi
 * @property string $lavozimi
 * @property integer $daromad_turi_id
 * @property string $daromad_miqdori
 * @property string $daromad_olish_sanasi
 * @property string $uzoq_muddatga_ketgan_vaqti
 * @property string $uzoq_muddatga_kelgan_vaqti
 * @property string $uzoq_muddatga_ketgan_davlati
 * @property string $uzoq_muddat_suhbat_otgani
 * @property integer $malumoti_id
 * @property string $bitirgan_joy_id
 * @property string $bitirgan_mutaxasisligi
 * @property string $bitirgan_yili
 * @property integer $aliment_id
 * @property string $aliment_qarz
 * @property string $notinch_oila
 * @property string $sudlanganligi
 * @property string $iib_xisobida_turadimi
 * @property integer $fuqaro_uy_id
 * @property integer $kassalik_id
 * @property string $kassalik_xolati
 * @property string $sogligi
 * @property integer $oila_boshligi
 * @property string $nikox_turi
 * @property string $nikox_guvohnom_raqami
 * @property string $rasm
 *
 * The followings are the available model relations:
 * @property Daromad[] $daromads
 * @property Malumoti $malumoti
 * @property Viloyatlar $tugilganJoyViloyat
 * @property Tuman $passportKimTomonidan
 * @property Tuman $tugilganJoyTuman
 * @property Jinsi $jinsi
 * @property Aliment $aliment
 * @property Qarz[] $qarzs
 */
class Fuqaro_kasalla extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{fuqaro}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('jinsi_id, tugilgan_joy_viloyat_id, tugilgan_joy_tuman_id, passport_kim_tomonidan_id, daromad_turi_id, malumoti_id, aliment_id, fuqaro_uy_id, kassalik_id, oila_boshligi', 'numerical', 'integerOnly'=>true),
			array('fio, passport_raqami, passport_vaqti, metirka_raqami, metirka_kim_tomonidan, ish_joy_nomi, lavozimi, daromad_miqdori, daromad_olish_sanasi, uzoq_muddatga_ketgan_davlati, uzoq_muddat_suhbat_otgani, bitirgan_joy_id, bitirgan_mutaxasisligi, bitirgan_yili, aliment_qarz, notinch_oila, iib_xisobida_turadimi, kassalik_xolati, sogligi, nikox_turi, nikox_guvohnom_raqami, rasm', 'length', 'max'=>255),
			array('tugilgan_sanasi, uzoq_muddatga_ketgan_vaqti, uzoq_muddatga_kelgan_vaqti, sudlanganligi', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fio, jinsi_id, tugilgan_sanasi, tugilgan_joy_viloyat_id, tugilgan_joy_tuman_id, passport_raqami, passport_vaqti, passport_kim_tomonidan_id, metirka_raqami, metirka_kim_tomonidan, ish_joy_nomi, lavozimi, daromad_turi_id, daromad_miqdori, daromad_olish_sanasi, uzoq_muddatga_ketgan_vaqti, uzoq_muddatga_kelgan_vaqti, uzoq_muddatga_ketgan_davlati, uzoq_muddat_suhbat_otgani, malumoti_id, bitirgan_joy_id, bitirgan_mutaxasisligi, bitirgan_yili, aliment_id, aliment_qarz, notinch_oila, sudlanganligi, iib_xisobida_turadimi, fuqaro_uy_id, kassalik_id, kassalik_xolati, sogligi, oila_boshligi, nikox_turi, nikox_guvohnom_raqami, rasm', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'daromads' => array(self::HAS_MANY, 'Daromad', 'fuqaro_id'),
			'malumoti' => array(self::BELONGS_TO, 'Malumoti', 'malumoti_id'),
			'tugilganJoyViloyat' => array(self::BELONGS_TO, 'Viloyatlar', 'tugilgan_joy_viloyat_id'),
			'passportKimTomonidan' => array(self::BELONGS_TO, 'Tuman', 'passport_kim_tomonidan_id'),
			'tugilganJoyTuman' => array(self::BELONGS_TO, 'Tuman', 'tugilgan_joy_tuman_id'),
            'daromadTuri' => array(self::BELONGS_TO, 'DaromadTuri', 'daromad_turi_id'),
			'jinsi' => array(self::BELONGS_TO, 'Jinsi', 'jinsi_id'),
			'aliment' => array(self::BELONGS_TO, 'Aliment', 'aliment_id'),
			'qarzs' => array(self::HAS_MANY, 'Qarz', 'tbl_fuqaro_id'),
            'kassalik' => array(self::BELONGS_TO, 'Kassalik', 'kassalik_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'fio' => Yii::t('strings','Fio'),
			'jinsi_id' => Yii::t('strings','Jinsi'),
			'tugilgan_sanasi' => Yii::t('strings','Tugilgan Sanasi'),
			'tugilgan_joy_viloyat_id' => Yii::t('strings','Tugilgan Joy Viloyat'),
			'tugilgan_joy_tuman_id' => Yii::t('strings','Tugilgan Joy Tuman'),
			'passport_raqami' => Yii::t('strings','Passport Raqami'),
			'passport_vaqti' => Yii::t('strings','Passport Vaqti'),
			'passport_kim_tomonidan_id' => Yii::t('strings','Passport Kim Tomonidan'),
			'metirka_raqami' => Yii::t('strings','Metirka Raqami'),
			'metirka_kim_tomonidan' => Yii::t('strings','Metirka Kim Tomonidan'),
			'ish_joy_nomi' => Yii::t('strings','Ish Joy Nomi'),
			'lavozimi' => Yii::t('strings','Lavozimi'),
			'daromad_turi_id' => Yii::t('strings','Daromad Turi'),
			'daromad_miqdori' => Yii::t('strings','Daromad Miqdori'),
			'daromad_olish_sanasi' => Yii::t('strings','Daromad Olish Sanasi'),
			'uzoq_muddatga_ketgan_vaqti' => Yii::t('strings','Uzoq Muddatga Ketgan Vaqti'),
			'uzoq_muddatga_kelgan_vaqti' => Yii::t('strings','Uzoq Muddatga Kelgan Vaqti'),
			'uzoq_muddatga_ketgan_davlati' => Yii::t('strings','Uzoq Muddatga Ketgan Davlati'),
			'uzoq_muddat_suhbat_otgani' => Yii::t('strings','Uzoq Muddat Suhbat Otgani'),
			'malumoti_id' => Yii::t('strings','Malumoti'),
			'bitirgan_joy_id' => Yii::t('strings','Bitirgan Joy'),
			'bitirgan_mutaxasisligi' => Yii::t('strings','Bitirgan Mutaxasisligi'),
			'bitirgan_yili' => Yii::t('strings','Bitirgan Yili'),
			'aliment_id' => Yii::t('strings','Aliment'),
			'aliment_qarz' => Yii::t('strings','Aliment Qarz'),
			'notinch_oila' => Yii::t('strings','Notinch Oila'),
			'sudlanganligi' => Yii::t('strings','Sudlanganligi'),
			'iib_xisobida_turadimi' => Yii::t('strings','Iib Xisobida Turadimi'),
			'fuqaro_uy_id' => Yii::t('strings','Fuqaro Uy'),
			'kassalik_id' => Yii::t('strings','Kassalik'),
			'kassalik_xolati' => Yii::t('strings','Kassalik Xolati'),
			'sogligi' => Yii::t('strings','Sogligi'),
			'oila_boshligi' => Yii::t('strings','Oila Boshligi'),
			'nikox_turi' => Yii::t('strings','Nikox Turi'),
			'nikox_guvohnom_raqami' => Yii::t('strings','Nikox Guvohnom Raqami'),
			'rasm' => Yii::t('strings','Rasm'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fio',$this->fio,true);
		$criteria->compare('jinsi_id',$this->jinsi_id);
		$criteria->compare('tugilgan_sanasi',$this->tugilgan_sanasi,true);
		$criteria->compare('tugilgan_joy_viloyat_id',$this->tugilgan_joy_viloyat_id);
		$criteria->compare('tugilgan_joy_tuman_id',$this->tugilgan_joy_tuman_id);
		$criteria->compare('passport_raqami',$this->passport_raqami,true);
		$criteria->compare('passport_vaqti',$this->passport_vaqti,true);
		$criteria->compare('passport_kim_tomonidan_id',$this->passport_kim_tomonidan_id);
		$criteria->compare('metirka_raqami',$this->metirka_raqami,true);
		$criteria->compare('metirka_kim_tomonidan',$this->metirka_kim_tomonidan,true);
		$criteria->compare('ish_joy_nomi',$this->ish_joy_nomi,true);
		$criteria->compare('lavozimi',$this->lavozimi,true);
		$criteria->compare('daromad_turi_id',$this->daromad_turi_id);
		$criteria->compare('daromad_miqdori',$this->daromad_miqdori,true);
		$criteria->compare('daromad_olish_sanasi',$this->daromad_olish_sanasi,true);
		$criteria->compare('uzoq_muddatga_ketgan_vaqti',$this->uzoq_muddatga_ketgan_vaqti,true);
		$criteria->compare('uzoq_muddatga_kelgan_vaqti',$this->uzoq_muddatga_kelgan_vaqti,true);
		$criteria->compare('uzoq_muddatga_ketgan_davlati',$this->uzoq_muddatga_ketgan_davlati,true);
		$criteria->compare('uzoq_muddat_suhbat_otgani',$this->uzoq_muddat_suhbat_otgani,true);
		$criteria->compare('malumoti_id',$this->malumoti_id);
		$criteria->compare('bitirgan_joy_id',$this->bitirgan_joy_id,true);
		$criteria->compare('bitirgan_mutaxasisligi',$this->bitirgan_mutaxasisligi,true);
		$criteria->compare('bitirgan_yili',$this->bitirgan_yili,true);
		$criteria->compare('aliment_id',$this->aliment_id);
		$criteria->compare('aliment_qarz',$this->aliment_qarz,true);
		$criteria->compare('notinch_oila',$this->notinch_oila,true);
		$criteria->compare('sudlanganligi',$this->sudlanganligi,true);
		$criteria->compare('iib_xisobida_turadimi',$this->iib_xisobida_turadimi,true);
		$criteria->compare('fuqaro_uy_id',$this->fuqaro_uy_id);
		$criteria->compare('kassalik_id',$this->kassalik_id);
		$criteria->compare('kassalik_xolati',$this->kassalik_xolati,true);
		$criteria->compare('sogligi',$this->sogligi,true);
		$criteria->compare('oila_boshligi',$this->oila_boshligi);
		$criteria->compare('nikox_turi',$this->nikox_turi,true);
		$criteria->compare('nikox_guvohnom_raqami',$this->nikox_guvohnom_raqami,true);
		$criteria->compare('rasm',$this->rasm,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Fuqaro_kasalla the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
