<?php

/**
 * This is the model class for table "{{hayvon}}".
 *
 * The followings are the available columns in table '{{hayvon}}':
 * @property integer $id
 * @property integer $hayvon_soni
 * @property integer $qora_mol
 * @property integer $qoy
 * @property integer $echki
 * @property integer $ot
 * @property integer $kuchuk
 * @property integer $mushuk
 * @property integer $quyon
 * @property integer $tovuq
 * @property integer $kurka
 * @property integer $ordak
 * @property integer $goz
 * @property string $boshqalar
 * @property integer $tbl_fuqaro_uy_id
 *
 * The followings are the available model relations:
 * @property FuqaroUy $tblFuqaroUy
 */
class Hayvon extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{hayvon}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('hayvon_soni','required'),
			array('hayvon_soni, qora_mol, qoy, echki, ot, kuchuk, mushuk, quyon, tovuq, kurka, ordak, goz, tbl_fuqaro_uy_id,user_id', 'numerical', 'integerOnly'=>true),
			array('boshqalar', 'length', 'max'=>255),
            array('anketa_ozgarish, user_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, hayvon_soni, qora_mol, qoy, echki, ot, kuchuk, mushuk, quyon, tovuq, kurka, ordak, goz, boshqalar, tbl_fuqaro_uy_id, anketa_ozgarish, user_id, user_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblFuqaroUy' => array(self::BELONGS_TO, 'FuqaroUy', 'tbl_fuqaro_uy_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'hayvon_soni' => Yii::t('strings','Хайвон сони'),
			'qora_mol' => Yii::t('strings','Қора мол'),
			'qoy' => Yii::t('strings','Қўй'),
			'echki' => Yii::t('strings','Эчки'),
			'ot' => Yii::t('strings','От'),
			'kuchuk' => Yii::t('strings','Кучук'),
			'mushuk' => Yii::t('strings','Мушук'),
			'quyon' => Yii::t('strings','Қуён'),
			'tovuq' => Yii::t('strings','Товуқ'),
			'kurka' => Yii::t('strings','Курка'),
			'ordak' => Yii::t('strings','Ўрдак'),
			'goz' => Yii::t('strings','Ғоз'),
			'boshqalar' => Yii::t('strings','Бошқа хайвон турлари номи ва сони ёзилсин (чизикча билан ажратиб ёзинг)'),
			'tbl_fuqaro_uy_id' => Yii::t('strings','Tbl Fuqaro Uy'),
            'anketa_ozgarish' => Yii::t('strings','Анкетани ўзгариш санаси'),
			'user_id' => Yii::t('strings','фойдаланувчи'),
			'user_date' => Yii::t('strings','Сана'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('hayvon_soni',$this->hayvon_soni);
		$criteria->compare('qora_mol',$this->qora_mol);
		$criteria->compare('qoy',$this->qoy);
		$criteria->compare('echki',$this->echki);
		$criteria->compare('ot',$this->ot);
		$criteria->compare('kuchuk',$this->kuchuk);
		$criteria->compare('mushuk',$this->mushuk);
		$criteria->compare('quyon',$this->quyon);
		$criteria->compare('tovuq',$this->tovuq);
		$criteria->compare('kurka',$this->kurka);
		$criteria->compare('ordak',$this->ordak);
		$criteria->compare('goz',$this->goz);
		$criteria->compare('boshqalar',$this->boshqalar,true);
		$criteria->compare('tbl_fuqaro_uy_id',$this->tbl_fuqaro_uy_id);
        $criteria->compare('anketa_ozgarish',$this->anketa_ozgarish,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_date',$this->user_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Hayvon the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
