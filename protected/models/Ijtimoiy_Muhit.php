<?php

/**
 * This is the model class for table "{{ijtimoiy_muhit}}".
 *
 * The followings are the available columns in table '{{ijtimoiy_muhit}}':
 * @property integer $id
 * @property string $obodonchilik
 * @property string $hammom
 * @property string $oshxona
 * @property string $tandir
 * @property integer $bino_xolati_id
 * @property string $elektr_hisoblagich_mavjudligi
 * @property string $elektr_hisoblagich_raqami
 * @property integer $gaz_mavjudligi_id
 * @property string $gaz_ballon_soni
 * @property string $gaz_hisoblagichi_raqami
 * @property string $gaz_hisoblagich_mavjudligii
 * @property string $ichimlik_suvi_mavjud_hisoblagich_mavjudligi
 * @property string $ichimlik_suvi_mavjud_masofa
 * @property integer $tbl_fuqaro_uy_id
 *
 * The followings are the available model relations:
 * @property FuqaroUy $tblFuqaroUy
 * @property Gaz $gazMavjudligi
 * @property Bino $binoXolati
 */
class Ijtimoiy_Muhit extends CActiveRecord
{
    public function getgaz_turi()
    {
        $gaz = Gaz::model()->findAll();
        $gaz = CHtml::listData($gaz,'id','gaz_turi');
        return $gaz;
    }
    public function getbino_xolati()
    {
        $bino = Bino::model()->findAll();
        $bino = CHtml::listData($bino,'id','xolati');
        return $bino;
    }

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{ijtimoiy_muhit}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gaz_mavjudligi_id,gaz_hisoblagich_mavjudligii,ichimlik_suvi_mavjud_hisoblagich_mavjudligi','required'),
			array('bino_xolati_id, gaz_mavjudligi_id, tbl_fuqaro_uy_id, user_id', 'numerical', 'integerOnly'=>true),
			array('obodonchilik, hammom, oshxona, tandir, elektr_hisoblagich_mavjudligi, elektr_hisoblagich_raqami, gaz_ballon_soni, gaz_hisoblagichi_raqami, gaz_hisoblagich_mavjudligii, ichimlik_suvi_mavjud_hisoblagich_mavjudligi, ichimlik_suvi_mavjud_masofa', 'length', 'max'=>255),
            array('anketa_ozgarish, user_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, obodonchilik, hammom, oshxona, tandir, bino_xolati_id, elektr_hisoblagich_mavjudligi, elektr_hisoblagich_raqami, gaz_mavjudligi_id, gaz_ballon_soni, gaz_hisoblagichi_raqami, gaz_hisoblagich_mavjudligii, ichimlik_suvi_mavjud_hisoblagich_mavjudligi, ichimlik_suvi_mavjud_masofa, tbl_fuqaro_uy_id, anketa_ozgarish, user_id, user_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblFuqaroUy' => array(self::BELONGS_TO, 'FuqaroUy', 'tbl_fuqaro_uy_id'),
			'gazMavjudligi' => array(self::BELONGS_TO, 'Gaz', 'gaz_mavjudligi_id'),
			'binoXolati' => array(self::BELONGS_TO, 'Bino', 'bino_xolati_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'obodonchilik' => Yii::t('strings','Ободончилик ҳолати'),
			'hammom' => Yii::t('strings','Хаммом'),
			'oshxona' => Yii::t('strings','Ошхона'),
			'tandir' => Yii::t('strings','Тандир'),
			'bino_xolati_id' => Yii::t('strings','Бино холати'),
			'elektr_hisoblagich_mavjudligi' => Yii::t('strings','Элeктр  ҳисоблагич мавжудлиги'),
			'elektr_hisoblagich_raqami' => Yii::t('strings','Элeктр  ҳисоблагич ёки китобча рақами'),
			'gaz_mavjudligi_id' => Yii::t('strings','Табиий газ мавжудлиги'),
			'gaz_ballon_soni' => Yii::t('strings','Пропан газ баллонлар сони'),
			'gaz_hisoblagichi_raqami' => Yii::t('strings','Табиий газ ҳисоблагич рақами'),
			'gaz_hisoblagich_mavjudligii' => Yii::t('strings','Табиий газ ҳисоблагич мавжудлиги'),
			'ichimlik_suvi_mavjud_hisoblagich_mavjudligi' => Yii::t('strings','Ичимлик суви тортилгани ва ҳисоблагич (бўлмаса қанча масофадан ташийди)'),
			'ichimlik_suvi_mavjud_masofa' => Yii::t('strings','Ичимлик суви бўлмаса қанча масофадан ташийди'),
			'tbl_fuqaro_uy_id' => Yii::t('strings','Tbl Fuqaro Uy'),
			'user_id' => Yii::t('strings','Фойдаланувчи'),
			'user_date' => Yii::t('strings','Сана'),

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('obodonchilik',$this->obodonchilik,true);
		$criteria->compare('hammom',$this->hammom,true);
		$criteria->compare('oshxona',$this->oshxona,true);
		$criteria->compare('tandir',$this->tandir,true);
		$criteria->compare('bino_xolati_id',$this->bino_xolati_id);
		$criteria->compare('elektr_hisoblagich_mavjudligi',$this->elektr_hisoblagich_mavjudligi,true);
		$criteria->compare('elektr_hisoblagich_raqami',$this->elektr_hisoblagich_raqami,true);
		$criteria->compare('gaz_mavjudligi_id',$this->gaz_mavjudligi_id);
		$criteria->compare('gaz_ballon_soni',$this->gaz_ballon_soni,true);
		$criteria->compare('gaz_hisoblagichi_raqami',$this->gaz_hisoblagichi_raqami,true);
		$criteria->compare('gaz_hisoblagich_mavjudligii',$this->gaz_hisoblagich_mavjudligii,true);
		$criteria->compare('ichimlik_suvi_mavjud_hisoblagich_mavjudligi',$this->ichimlik_suvi_mavjud_hisoblagich_mavjudligi,true);
		$criteria->compare('ichimlik_suvi_mavjud_masofa',$this->ichimlik_suvi_mavjud_masofa,true);
		$criteria->compare('tbl_fuqaro_uy_id',$this->tbl_fuqaro_uy_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_date',$this->user_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ijtimoiy_Muhit the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
