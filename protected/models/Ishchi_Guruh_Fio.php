<?php

/**
 * This is the model class for table "{{ishchi_guruh_fio}}".
 *
 * The followings are the available columns in table '{{ishchi_guruh_fio}}':
 * @property integer $id
 * @property string $ishchi_guruh_fio
 * @property string $tashkilot_nomi
 *
 * The followings are the available model relations:
 * @property FuqaroUy[] $fuqaroUys
 */
class Ishchi_Guruh_Fio extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function getvil()
	{
		$vil = Viloyatlar::model()->findAll();
		$vil = CHtml::listData($vil,'id','viloyat_nomi');
		foreach ($vil as $key=>$val){
			$vil[$key]=Yii::t("strings",CHtml::encode($val));
		}
		return $vil;
	}

	public function tableName()
	{
		return '{{ishchi_guruh_fio}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('viloyat_id, tuman_id, user_id', 'numerical', 'integerOnly'=>true),
			array('ishchi_guruh_fio, tashkilot_nomi', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, viloyat_id, tuman_id, ishchi_guruh_fio, tashkilot_nomi, user_id, user_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fuqaroUys' => array(self::HAS_MANY, 'FuqaroUy', 'ishchi_guruh_fio_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'viloyat' => array(self::BELONGS_TO, 'Viloyatlar', 'viloyat_id'),
			'tuman' => array(self::BELONGS_TO, 'Tuman', 'tuman_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'viloyat_id' => Yii::t('strings','Вилоят'),
			'tuman_id' => Yii::t('strings','Туман'),
			'ishchi_guruh_fio' => Yii::t('strings','Ишчи гурух рахбари ФИШ'),
			'tashkilot_nomi' => Yii::t('strings','Ташкилот номи'),
			'user_id' => Yii::t('strings','Фойдаланувчи'),
			'user_date' => Yii::t('strings','Сана'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('viloyat_id',$this->viloyat_id);
		$criteria->compare('tuman_id',$this->tuman_id);
		$criteria->compare('ishchi_guruh_fio',$this->ishchi_guruh_fio,true);
		$criteria->compare('tashkilot_nomi',$this->tashkilot_nomi,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_date',$this->user_date,true);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ishchi_Guruh_Fio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
