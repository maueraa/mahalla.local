<?php

/**
 * This is the model class for table "{{kocha_xolati}}".
 *
 * The followings are the available columns in table '{{kocha_xolati}}':
 * @property integer $id
 * @property string $xonadon_joylashgan_kocha_uzunligi
 * @property string $xonadon_joylashgan_kocha_eni
 * @property string $xonadon_joylashgan_kocha_tamirlanganligi
 * @property string $xonadon_joylashgan_kocha_koliforniya_terak_soni
 * @property string $xonadon_joylashgan_kocha_yoritilishi
 * @property string $xonadon_joylashgan_kocha_ariqlarni_xolati
 * @property string $xonadon_joylashgan_kocha_ariqa_suv_kelishi
 * @property string $xonadon_joylashgan_kocha_koprik_xolati
 * @property string $xonadon_joylashgan_kocha_zovur_xolati
 * @property integer $fuqaro_uy_id
 *
 * The followings are the available model relations:
 * @property FuqaroUy $fuqaroUy
 */
class Kocha_Xolati extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kocha_xolati}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('xonadon_joylashgan_kocha_zovur_xolati,xonadon_joylashgan_kocha_koprik_xolati,xonadon_joylashgan_kocha_ariqa_suv_kelishi,xonadon_joylashgan_kocha_uzunligi,xonadon_joylashgan_kocha_eni, xonadon_joylashgan_kocha_tamirlanganligi, xonadon_joylashgan_kocha_yoritilishi,kocha_qoplamasi','required'),
			array('fuqaro_uy_id, user_id', 'numerical', 'integerOnly'=>true),
			array('xonadon_joylashgan_kocha_uzunligi, xonadon_joylashgan_kocha_eni, xonadon_joylashgan_kocha_tamirlanganligi, xonadon_joylashgan_kocha_koliforniya_terak_soni, xonadon_joylashgan_kocha_yoritilishi, xonadon_joylashgan_kocha_ariqlarni_xolati, xonadon_joylashgan_kocha_ariqa_suv_kelishi, xonadon_joylashgan_kocha_koprik_xolati, xonadon_joylashgan_kocha_zovur_xolati, kocha_qoplamasi', 'length', 'max'=>255),
            array('anketa_ozgarish, user_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, xonadon_joylashgan_kocha_uzunligi, xonadon_joylashgan_kocha_eni, xonadon_joylashgan_kocha_tamirlanganligi, xonadon_joylashgan_kocha_koliforniya_terak_soni, xonadon_joylashgan_kocha_yoritilishi, xonadon_joylashgan_kocha_ariqlarni_xolati, xonadon_joylashgan_kocha_ariqa_suv_kelishi, xonadon_joylashgan_kocha_koprik_xolati, xonadon_joylashgan_kocha_zovur_xolati, fuqaro_uy_id, kocha_qoplamasi, anketa_ozgarish, user_id, user_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fuqaroUy' => array(self::BELONGS_TO, 'FuqaroUy', 'fuqaro_uy_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'xonadon_joylashgan_kocha_uzunligi' => Yii::t('strings','Хонадон жойлашган кўчанинг умумий узунлиги'),
			'xonadon_joylashgan_kocha_eni' => Yii::t('strings','Хонадон жойлашган кўчанинг умумий эни'),
			'xonadon_joylashgan_kocha_tamirlanganligi' => Yii::t('strings','Хонадон жойлашган кўчанинг таъмирга мухтожлиги(капитал ёки жорий, мухтож ёки мухтож эмас)'),
			'xonadon_joylashgan_kocha_koliforniya_terak_soni' => Yii::t('strings','Хонадон жойлашган кўчадаги калифорния тераклар сони'),
			'xonadon_joylashgan_kocha_yoritilishi' => Yii::t('strings','Кўчани ёритиш тизими'),
			'xonadon_joylashgan_kocha_ariqlarni_xolati' => Yii::t('strings','Хонадон жойлашган кўчадаги ариқларнинг холати(чопилган ёки чопилмаган)'),
			'xonadon_joylashgan_kocha_ariqa_suv_kelishi' => Yii::t('strings','Хонадон жойлашган кўчанинг ариқларига сувни етиб келиши'),
			'xonadon_joylashgan_kocha_koprik_xolati' => Yii::t('strings','Хонадон жойлашган кўчадаги кўприкларнинг холати'),
			'xonadon_joylashgan_kocha_zovur_xolati' => Yii::t('strings','Хонадон жойлашган кўчадаги зовурларнинг холати'),
			'fuqaro_uy_id' => Yii::t('strings','Fuqaro Uy'),
			'kocha_qoplamasi' => Yii::t('strings','Кўчанинг қопламаси'),
            'anketa_ozgarish' => Yii::t('strings','Анкета ўзгариш санаси'),
			'user_id' => Yii::t('strings','Фойдалунувчи'),
			'user_date' => Yii::t('strings','Сана'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('xonadon_joylashgan_kocha_uzunligi',$this->xonadon_joylashgan_kocha_uzunligi,true);
		$criteria->compare('xonadon_joylashgan_kocha_eni',$this->xonadon_joylashgan_kocha_eni,true);
		$criteria->compare('xonadon_joylashgan_kocha_tamirlanganligi',$this->xonadon_joylashgan_kocha_tamirlanganligi,true);
		$criteria->compare('xonadon_joylashgan_kocha_koliforniya_terak_soni',$this->xonadon_joylashgan_kocha_koliforniya_terak_soni,true);
		$criteria->compare('xonadon_joylashgan_kocha_yoritilishi',$this->xonadon_joylashgan_kocha_yoritilishi,true);
		$criteria->compare('xonadon_joylashgan_kocha_ariqlarni_xolati',$this->xonadon_joylashgan_kocha_ariqlarni_xolati,true);
		$criteria->compare('xonadon_joylashgan_kocha_ariqa_suv_kelishi',$this->xonadon_joylashgan_kocha_ariqa_suv_kelishi,true);
		$criteria->compare('xonadon_joylashgan_kocha_koprik_xolati',$this->xonadon_joylashgan_kocha_koprik_xolati,true);
		$criteria->compare('xonadon_joylashgan_kocha_zovur_xolati',$this->xonadon_joylashgan_kocha_zovur_xolati,true);
		$criteria->compare('fuqaro_uy_id',$this->fuqaro_uy_id);
        $criteria->compare('kocha_qoplamasi',$this->kocha_qoplamasi,true);
        $criteria->compare('anketa_ozgarish',$this->anketa_ozgarish,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_date',$this->user_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Kocha_Xolati the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
