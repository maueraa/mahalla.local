<?php

/**
 * This is the model class for table "{{kochalar}}".
 *
 * The followings are the available columns in table '{{kochalar}}':
 * @property integer $id
 * @property string $tbl_kocha_nomi
 * @property integer $tbl_mfy_id
 *
 * The followings are the available model relations:
 * @property FuqaroUy[] $fuqaroUys
 * @property Mfy $tblMfy
 */
class Kochalar extends CActiveRecord
{
    public function getfviloyat()
    {
        $fviloyat = Viloyatlar::model()->findAll();
        $fviloyat = CHtml::listData($fviloyat,'id','viloyat_nomi');
        foreach ($fviloyat as $key=>$val){
            $fviloyat[$key]=Yii::t("strings",CHtml::encode($val));

        }
        return $fviloyat;
    }
    public function getmfy_nomii()
    {
        $mfy_nomiii = Mfy::model()->findAll();
        $mfy_nomiii = CHtml::listData($mfy_nomiii,'id','mfy_nomi');
        return $mfy_nomiii;
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kochalar}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('tbl_mfy_id, user_id, viloyat_id, tuman_id', 'numerical', 'integerOnly'=>true),
			array('tbl_kocha_nomi', 'length', 'max'=>255),
            array('user_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tbl_kocha_nomi, tbl_mfy_id, user_id, user_date, viloyat_id, tuman_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fuqaroUys' => array(self::HAS_MANY, 'FuqaroUy', 'kocha_id'),
			'tblMfy' => array(self::BELONGS_TO, 'Mfy', 'tbl_mfy_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'viloyat' => array(self::BELONGS_TO, 'Viloyatlar', 'viloyat_id'),
            'tuman' => array(self::BELONGS_TO, 'Tuman', 'tuman_id'),
            'muammos' => array(self::HAS_MANY, 'Muammo', 'kocha_id'),
            'tashkilots' => array(self::HAS_MANY, 'Tashkilot', 'tkocha_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'tbl_kocha_nomi' => Yii::t('strings','Кўчанинг номи'),
			'tbl_mfy_id' => Yii::t('strings','МФЙни танланг'),
            'user_id' => Yii::t('strings','Фойдаланувчи'),
            'user_date' => Yii::t('strings','Бугунги сана'),
            'viloyat_id' => Yii::t('strings','Вилоят'),
            'tuman_id' => Yii::t('strings','Туман'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tbl_kocha_nomi',$this->tbl_kocha_nomi,true);
		$criteria->compare('tbl_mfy_id',$this->tbl_mfy_id);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('user_date',$this->user_date,true);
        $criteria->compare('viloyat_id',$this->viloyat_id);
        $criteria->compare('tuman_id',$this->tuman_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Kochalar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
