<?php

/**
 * This is the model class for table "{{kollejlar}}".
 *
 * The followings are the available columns in table '{{kollejlar}}':
 * @property integer $id
 * @property string $kollej_nomi
 * @property string $manzili
 * @property string $kollej_direktori
 */
class Kollejlar extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kollejlar}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('kollej_nomi, manzili, kollej_direktori', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, kollej_nomi, manzili, kollej_direktori', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'kollej_nomi' => Yii::t('strings','Kollej Nomi'),
			'manzili' => Yii::t('strings','Manzili'),
			'kollej_direktori' => Yii::t('strings','Kollej Direktori'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('kollej_nomi',$this->kollej_nomi,true);
		$criteria->compare('manzili',$this->manzili,true);
		$criteria->compare('kollej_direktori',$this->kollej_direktori,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Kollejlar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
