<?php

/**
 * This is the model class for table "{{manaviy}}".
 *
 * The followings are the available columns in table '{{manaviy}}':
 * @property integer $id
 * @property string $xonadon_bahosi
 * @property string $kutubxona
 * @property string $obuna
 * @property string $farzandi_davomad
 * @property string $manaviy_muhit
 * @property string $jamoat_ishlarida_ishtiroki
 * @property string $kayfiyat
 * @property string $muhit_boshqalar
 * @property integer $fuqaro_uy_id
 *
 * The followings are the available model relations:
 * @property FuqaroUy $fuqaroUy
 */
class Manaviy extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{manaviy}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('farzandi_davomad,kutubxona,obuna','required'),
			array('fuqaro_uy_id, user_id', 'numerical', 'integerOnly'=>true),
			array('xonadon_bahosi, kutubxona, obuna, farzandi_davomad, manaviy_muhit, jamoat_ishlarida_ishtiroki, kayfiyat, muhit_boshqalar', 'length', 'max'=>255),
            array('anketa_ozgarish, user_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, xonadon_bahosi, kutubxona, obuna, farzandi_davomad, manaviy_muhit, jamoat_ishlarida_ishtiroki, kayfiyat, muhit_boshqalar, fuqaro_uy_id, anketa_ozgarish, user_id, user_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fuqaroUy' => array(self::BELONGS_TO, 'FuqaroUy', 'fuqaro_uy_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'xonadon_bahosi' => Yii::t('strings','Хонодон баҳоси (яхши, ўрта, қониқарли)'),
			'kutubxona' => Yii::t('strings','Кутубхона борлиги'),
			'obuna' => Yii::t('strings','Газeта ва журналларга обуна бўлганлиги'),
			'farzandi_davomad' => Yii::t('strings','Фарзандлар давомади'),
			'manaviy_muhit' => Yii::t('strings','Хонадондаги маънавий муҳит'),
			'jamoat_ishlarida_ishtiroki' => Yii::t('strings','Жамоат ишларида иштироки'),
			'kayfiyat' => Yii::t('strings','Кайфияти'),
			'muhit_boshqalar' => Yii::t('strings','Бошқалар'),
			'fuqaro_uy_id' => Yii::t('strings','Fuqaro Uy'),
            'anketa_ozgarish' => Yii::t('strings','Анкетани ўзгариш санаси'),
            'user_id' => Yii::t('strings','Фойдаланувчи'),
            'user_date' => Yii::t('strings','Сана'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('xonadon_bahosi',$this->xonadon_bahosi,true);
		$criteria->compare('kutubxona',$this->kutubxona,true);
		$criteria->compare('obuna',$this->obuna,true);
		$criteria->compare('farzandi_davomad',$this->farzandi_davomad,true);
		$criteria->compare('manaviy_muhit',$this->manaviy_muhit,true);
		$criteria->compare('jamoat_ishlarida_ishtiroki',$this->jamoat_ishlarida_ishtiroki,true);
		$criteria->compare('kayfiyat',$this->kayfiyat,true);
		$criteria->compare('muhit_boshqalar',$this->muhit_boshqalar,true);
		$criteria->compare('fuqaro_uy_id',$this->fuqaro_uy_id);
        $criteria->compare('anketa_ozgarish',$this->anketa_ozgarish,true);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('user_date',$this->user_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Manaviy the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
