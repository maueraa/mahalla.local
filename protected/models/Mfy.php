<?php

/**
 * This is the model class for table "{{mfy}}".
 *
 * The followings are the available columns in table '{{mfy}}':
 * @property integer $id
 * @property string $mfy_nomi
 * @property integer $tuman_id
 *
 * The followings are the available model relations:
 * @property FuqaroUy[] $fuqaroUys
 * @property Kochalar[] $kochalars
 * @property Tuman $tuman
 */
class Mfy extends CActiveRecord
{
    public function gettuman_nomii()
    {
        $tuman_nomii = Tuman::model()->findAll();
        $tuman_nomii = CHtml::listData($tuman_nomii,'id','tuman_nomi');
        return $tuman_nomii;
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{mfy}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('tuman_id', 'numerical', 'integerOnly'=>true),
			array('mfy_nomi', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, mfy_nomi, tuman_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fuqaroUys' => array(self::HAS_MANY, 'FuqaroUy', 'mfy_id'),
			'kochalars' => array(self::HAS_MANY, 'Kochalar', 'tbl_mfy_id'),
			'tuman' => array(self::BELONGS_TO, 'Tuman', 'tuman_id'),
            'viloyat' => array(self::BELONGS_TO, 'Viloyatlar', 'viloyat_id'),
            'muammos' => array(self::HAS_MANY, 'Muammo', 'mfy_id'),
            'sektors' => array(self::HAS_MANY, 'Sektor', 'sektor_mfy_id'),
            'tashkilots' => array(self::HAS_MANY, 'Tashkilot', 'tmfy_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'mfy_nomi' => Yii::t('strings','МФЙ'),
			'tuman_id' => Yii::t('strings','Туман'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('mfy_nomi',$this->mfy_nomi,true);
		$criteria->compare('tuman_id',$this->tuman_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mfy the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
