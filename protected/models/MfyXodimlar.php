<?php

/**
 * This is the model class for table "{{mfy_xodimlar}}".
 *
 * The followings are the available columns in table '{{mfy_xodimlar}}':
 * @property integer $id
 * @property integer $viloyat_id
 * @property integer $tuman_id
 * @property integer $mfy_id
 * @property string $mfy_rais
 * @property integer $mfy_rais_tel
 * @property string $mfy_kotiba
 * @property integer $mfy_kotiba_tel
 * @property string $mfy_posbon
 * @property integer $mfy_posbon_tel
 * @property string $email
 * @property integer $user_id
 * @property string $user_date
 */
class MfyXodimlar extends CActiveRecord
{
    public function getfviloyat()
    {
        $fviloyat = Viloyatlar::model()->findAll();
        $fviloyat = CHtml::listData($fviloyat,'id','viloyat_nomi');
        foreach ($fviloyat as $key=>$val){
            $fviloyat[$key]=Yii::t("strings",CHtml::encode($val));

        }
        return $fviloyat;
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{mfy_xodimlar}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('viloyat_id, tuman_id, mfy_id,mfy_rais,mfy_kotiba,mfy_posbon','required'),
			array('viloyat_id, tuman_id, mfy_id, mfy_rais_tel, mfy_kotiba_tel, mfy_posbon_tel, user_id', 'numerical', 'integerOnly'=>true),
			array('mfy_rais, mfy_kotiba, mfy_posbon, email', 'length', 'max'=>255),
			array('user_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, viloyat_id, tuman_id, mfy_id, mfy_rais, mfy_rais_tel, mfy_kotiba, mfy_kotiba_tel, mfy_posbon, mfy_posbon_tel, email, user_id, user_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'mfy' => array(self::BELONGS_TO, 'Mfy', 'mfy_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'viloyat' => array(self::BELONGS_TO, 'Viloyatlar', 'viloyat_id'),
            'tuman' => array(self::BELONGS_TO, 'Tuman', 'tuman_id'),

        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'viloyat_id' => Yii::t('strings','Вилоят номи'),
			'tuman_id' => Yii::t('strings','Туман номи'),
			'mfy_id' => Yii::t('strings','МФЙ номи'),
			'mfy_rais' => Yii::t('strings','МФЙ раисининг Ф.И.Ш.'),
			'mfy_rais_tel' => Yii::t('strings','МФЙ раисининг телефон рақами'),
			'mfy_kotiba' => Yii::t('strings','МФЙ котибасининг Ф.И.Ш.'),
			'mfy_kotiba_tel' => Yii::t('strings','МФЙ котибасининг телефон рақами'),
			'mfy_posbon' => Yii::t('strings','МФЙ Посбонини Ф.И.Ш.'),
			'mfy_posbon_tel' => Yii::t('strings','МФЙ Посбоннинг телефон рақами'),
			'email' => Yii::t('strings','E-mail'),
			'user_id' => Yii::t('strings','Фойдаланувчи:'),
			'user_date' => Yii::t('strings','Бугунги сана:'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('viloyat_id',$this->viloyat_id);
		$criteria->compare('tuman_id',$this->tuman_id);
		$criteria->compare('mfy_id',$this->mfy_id);
		$criteria->compare('mfy_rais',$this->mfy_rais,true);
		$criteria->compare('mfy_rais_tel',$this->mfy_rais_tel);
		$criteria->compare('mfy_kotiba',$this->mfy_kotiba,true);
		$criteria->compare('mfy_kotiba_tel',$this->mfy_kotiba_tel);
		$criteria->compare('mfy_posbon',$this->mfy_posbon,true);
		$criteria->compare('mfy_posbon_tel',$this->mfy_posbon_tel);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_date',$this->user_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MfyXodimlar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
