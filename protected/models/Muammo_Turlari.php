<?php

/**
 * This is the model class for table "{{muammo_turlari}}".
 *
 * The followings are the available columns in table '{{muammo_turlari}}':
 * @property integer $id
 * @property string $muammo_turlari
 * @property integer $user_id
 * @property string $user_date
 *
 * The followings are the available model relations:
 * @property Muammo[] $muammos
 * @property User $user
 */
class Muammo_Turlari extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{muammo_turlari}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('muammo_turlari', 'length', 'max'=>255),
			array('user_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, muammo_turlari, user_id, user_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'muammos' => array(self::HAS_MANY, 'Muammo', 'muammo_turlari'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'muammo_turlari' => Yii::t('strings','Муаммо турини киритинг'),
			'user_id' => Yii::t('strings','Фойдаланувчи'),
			'user_date' => Yii::t('strings','Сана'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('muammo_turlari',$this->muammo_turlari,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_date',$this->user_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Muammo_Turlari the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
