<?php

/**
 * This is the model class for table "{{qarz}}".
 *
 * The followings are the available columns in table '{{qarz}}':
 * @property integer $id
 * @property string $umumiy_qarz
 * @property string $eletr_qarz
 * @property string $gaz_qarz
 * @property string $suv_qarz
 * @property string $soliq_yer_qarz
 * @property string $soliq_mulk_qarz
 * @property string $schr_qarz
 * @property string $chiqindi_qarz
 * @property string $aliment_qarz
 * @property string $qarz_boshqalar
 * @property integer $tbl_fuqaro_uy_id
 * @property integer $tbl_fuqaro_id
 *
 * The followings are the available model relations:
 * @property FuqaroUy $tblFuqaroUy
 * @property Fuqaro $tblFuqaro
 */
class Qarz extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{qarz}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('umumiy_qarz','required'),
			array('tbl_fuqaro_uy_id, tbl_fuqaro_id, user_id', 'numerical', 'integerOnly'=>true),
			array('umumiy_qarz, eletr_qarz, gaz_qarz, suv_qarz, soliq_yer_qarz, soliq_mulk_qarz, schr_qarz, chiqindi_qarz, aliment_qarz, qarz_boshqalar', 'length', 'max'=>255),
            array('anketa_ozgarish, user_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, umumiy_qarz, eletr_qarz, gaz_qarz, suv_qarz, soliq_yer_qarz, soliq_mulk_qarz, schr_qarz, chiqindi_qarz, aliment_qarz, qarz_boshqalar, tbl_fuqaro_uy_id, tbl_fuqaro_id, anketa_ozgarish, user_id, user_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblFuqaroUy' => array(self::BELONGS_TO, 'FuqaroUy', 'tbl_fuqaro_uy_id'),
			'tblFuqaro' => array(self::BELONGS_TO, 'Fuqaro', 'tbl_fuqaro_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'umumiy_qarz' => Yii::t('strings','Умумий карздорлик суммаси'),
			'eletr_qarz' => Yii::t('strings','Электрдан карздорлик суммаси'),
			'gaz_qarz' => Yii::t('strings','Таббий газ карздорлик суммаси'),
			'suv_qarz' => Yii::t('strings','Ичимлик сувидан қарздорлик суммаси'),
			'soliq_yer_qarz' => Yii::t('strings','Ер солоғидан қарздорлик суммаси'),
			'soliq_mulk_qarz' => Yii::t('strings','Мулк соғлиғидан қарздорлик суммаси'),
			'schr_qarz' => Yii::t('strings','Сафарбарлик чақирув резевига тўланадиган бадалдан карздорлик суммаси(СЧР)'),
			'chiqindi_qarz' => Yii::t('strings','Чиқиндидан қарздорлик суммаси'),
			'aliment_qarz' => Yii::t('strings','Алимент тўлўвидан қарздорлик суммаси'),
			'qarz_boshqalar' => Yii::t('strings','Бошқа қарздорлик суммаси'),
			'tbl_fuqaro_uy_id' => Yii::t('strings','Tbl Fuqaro Uy'),
			'tbl_fuqaro_id' => Yii::t('strings','Tbl Fuqaro'),
            'anketa_ozgarish' => Yii::t('strings','Анкетани ўзгариш санаси'),
			'user_id' => Yii::t('strings','Фойдаланувчи'),
			'user_date' => Yii::t('strings','Сана'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('umumiy_qarz',$this->umumiy_qarz,true);
		$criteria->compare('eletr_qarz',$this->eletr_qarz,true);
		$criteria->compare('gaz_qarz',$this->gaz_qarz,true);
		$criteria->compare('suv_qarz',$this->suv_qarz,true);
		$criteria->compare('soliq_yer_qarz',$this->soliq_yer_qarz,true);
		$criteria->compare('soliq_mulk_qarz',$this->soliq_mulk_qarz,true);
		$criteria->compare('schr_qarz',$this->schr_qarz,true);
		$criteria->compare('chiqindi_qarz',$this->chiqindi_qarz,true);
		$criteria->compare('aliment_qarz',$this->aliment_qarz,true);
		$criteria->compare('qarz_boshqalar',$this->qarz_boshqalar,true);
		$criteria->compare('tbl_fuqaro_uy_id',$this->tbl_fuqaro_uy_id);
		$criteria->compare('tbl_fuqaro_id',$this->tbl_fuqaro_id);
        $criteria->compare('anketa_ozgarish',$this->anketa_ozgarish,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_date',$this->user_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Qarz the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
