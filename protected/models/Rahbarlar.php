<?php

/**
 * This is the model class for table "{{rahbarlar}}".
 *
 * The followings are the available columns in table '{{rahbarlar}}':
 * @property integer $id
 * @property string $r_fio
 * @property string $ish_joy_nomi
 * @property string $lavozimi
 * @property integer $viloyat_id
 * @property integer $tuman_id
 *
 * The followings are the available model relations:
 * @property Tuman $tuman
 * @property Viloyatlar $viloyat
 * @property Sektor[] $sektors
 */
class Rahbarlar extends CActiveRecord
{
    public function getfviloyat()
    {
        $fviloyat = Viloyatlar::model()->findAll();
        $fviloyat = CHtml::listData($fviloyat,'id','viloyat_nomi');
        foreach ($fviloyat as $key=>$val){
            $fviloyat[$key]=Yii::t("strings",CHtml::encode($val));

        }
        return $fviloyat;
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{rahbarlar}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('viloyat_id, tuman_id', 'numerical', 'integerOnly'=>true),
			array('r_fio, ish_joy_nomi, lavozimi', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, r_fio, ish_joy_nomi, lavozimi, viloyat_id, tuman_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tuman' => array(self::BELONGS_TO, 'Tuman', 'tuman_id'),
			'viloyat' => array(self::BELONGS_TO, 'Viloyatlar', 'viloyat_id'),
			'sektors' => array(self::HAS_MANY, 'Sektor', 'sektor_rahbari'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'r_fio' => Yii::t('strings','Сектор рахбарининг Ф.И.Ш.'),
			'ish_joy_nomi' => Yii::t('strings','Иш жойи'),
			'lavozimi' => Yii::t('strings','Лоавозими'),
			'viloyat_id' => Yii::t('strings','Вилоят'),
			'tuman_id' => Yii::t('strings','Туман'),
			'sektor' => Yii::t('strings','Сектор рақами'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('r_fio',$this->r_fio,true);
		$criteria->compare('ish_joy_nomi',$this->ish_joy_nomi,true);
		$criteria->compare('lavozimi',$this->lavozimi,true);
		$criteria->compare('viloyat_id',$this->viloyat_id);
		$criteria->compare('tuman_id',$this->tuman_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Rahbarlar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
