<?php

/**
 * This is the model class for table "{{sektor}}".
 *
 * The followings are the available columns in table '{{sektor}}':
 * @property integer $id
 * @property integer $sektor_raqami
 * @property integer $sektor_viloyat_id
 * @property integer $sektor_tuman_id
 * @property integer $sektor_mfy_id
 * @property integer $sektor_rahbari
 * @property string $sektor_ish_joyi
 *
 * The followings are the available model relations:
 * @property Mfy $sektorMfy
 * @property Viloyatlar $sektorViloyat
 * @property Tuman $sektorTuman
 * @property Rahbarlar $sektorRahbari
 */
class Sektor extends CActiveRecord
{
    public function getfviloyat()
    {
        $fviloyat = Viloyatlar::model()->findAll();
        $fviloyat = CHtml::listData($fviloyat,'id','viloyat_nomi');
        foreach ($fviloyat as $key=>$val){
            $fviloyat[$key]=Yii::t("strings",CHtml::encode($val));
        }
        return $fviloyat;
    }
    public function getrahbar()
    {
        $rahbar = Rahbarlar::model()->findAll();
        $rahbar = CHtml::listData($rahbar,'id','r_fio');
        foreach ($rahbar as $key=>$val){
            $rahbar[$key]=Yii::t("strings",CHtml::encode($val));
        }
        return $rahbar;
    }


    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sektor}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('sektor_raqami, sektor_viloyat_id, sektor_tuman_id, sektor_mfy_id, sektor_rahbari', 'numerical', 'integerOnly'=>true),
			array('sektor_ish_joyi', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sektor_raqami, sektor_viloyat_id, sektor_tuman_id, sektor_mfy_id, sektor_rahbari, sektor_ish_joyi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'sektorMfy' => array(self::BELONGS_TO, 'Mfy', 'sektor_mfy_id'),
			'sektorViloyat' => array(self::BELONGS_TO, 'Viloyatlar', 'sektor_viloyat_id'),
			'sektorTuman' => array(self::BELONGS_TO, 'Tuman', 'sektor_tuman_id'),
			'sektorRahbari' => array(self::BELONGS_TO, 'Rahbarlar', 'sektor_rahbari'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'sektor_raqami' => Yii::t('strings','Сектор рақами'),
			'sektor_viloyat_id' => Yii::t('strings','Вилоят'),
			'sektor_tuman_id' => Yii::t('strings','Туман'),
			'sektor_mfy_id' => Yii::t('strings','МФЙ'),
			'sektor_rahbari' => Yii::t('strings','Сектор рахбари'),
			'sektor_ish_joyi' => Yii::t('strings','Иш жой номи'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sektor_raqami',$this->sektor_raqami);
		$criteria->compare('sektor_viloyat_id',$this->sektor_viloyat_id);
		$criteria->compare('sektor_tuman_id',$this->sektor_tuman_id);
		$criteria->compare('sektor_mfy_id',$this->sektor_mfy_id);
		$criteria->compare('sektor_rahbari',$this->sektor_rahbari);
		$criteria->compare('sektor_ish_joyi',$this->sektor_ish_joyi,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sektor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
