<?php

/**
 * This is the model class for table "{{sektorlar}}".
 *
 * The followings are the available columns in table '{{sektorlar}}':
 * @property integer $id
 * @property integer $sektor_raqami
 * @property integer $viloyat_id
 * @property integer $tuman_id
 *
 * The followings are the available model relations:
 * @property Tuman $tuman
 * @property Viloyatlar $viloyat
 */
class Sektorlar extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sektorlar}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('sektor_raqami, viloyat_id, tuman_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sektor_raqami, viloyat_id, tuman_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tuman' => array(self::BELONGS_TO, 'Tuman', 'tuman_id'),
			'viloyat' => array(self::BELONGS_TO, 'Viloyatlar', 'viloyat_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'sektor_raqami' => Yii::t('strings','Сектор рақами'),
			'viloyat_id' => Yii::t('strings','Вилоят'),
			'tuman_id' => Yii::t('strings','Туман'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sektor_raqami',$this->sektor_raqami);
		$criteria->compare('viloyat_id',$this->viloyat_id);
		$criteria->compare('tuman_id',$this->tuman_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sektorlar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
