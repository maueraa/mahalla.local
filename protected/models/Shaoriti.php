<?php

/**
 * This is the model class for table "{{shaoriti}}".
 *
 * The followings are the available columns in table '{{shaoriti}}':
 * @property integer $id
 * @property integer $vosita_soni
 * @property integer $televizor
 * @property integer $kir_yuvish_vositasi
 * @property integer $sovutgich
 * @property integer $DVD
 * @property integer $Konditsioner
 * @property integer $kompyuter
 * @property string $internet
 * @property integer $dazmol
 * @property string $vosita_boshqalar
 * @property integer $tbl_fuqaro_uy_id
 *
 * The followings are the available model relations:
 * @property FuqaroUy $tblFuqaroUy
 */
class Shaoriti extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{shaoriti}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vosita_soni','required'),
			array('vosita_soni, televizor, kir_yuvish_vositasi, sovutgich, DVD, Konditsioner, kompyuter, dazmol, tbl_fuqaro_uy_id, user_id', 'numerical', 'integerOnly'=>true),
			array('internet, vosita_boshqalar', 'length', 'max'=>255),
            array('anketa_ozgarish, user_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, vosita_soni, televizor, kir_yuvish_vositasi, sovutgich, DVD, Konditsioner, kompyuter, internet, dazmol, vosita_boshqalar, tbl_fuqaro_uy_id, anketa_ozgarish, user_id, user_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblFuqaroUy' => array(self::BELONGS_TO, 'FuqaroUy', 'tbl_fuqaro_uy_id'),
			 'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'vosita_soni' => Yii::t('strings','Мавжуд воситалар сони'),
			'televizor' => Yii::t('strings','Телевизор сони'),
			'kir_yuvish_vositasi' => Yii::t('strings','Кир ювиш машинаси сони'),
			'sovutgich' => Yii::t('strings','Совутгич(музлатгич) сони'),
			'DVD' => Yii::t('strings','DVD сони'),
			'Konditsioner' => Yii::t('strings','Кондиционер сони'),
			'kompyuter' => Yii::t('strings','Компютер ёки ноутбук сони'),
			'internet' => Yii::t('strings','Интернет мавжудлиги(бор/йўқ)'),
			'dazmol' => Yii::t('strings','Дазмол сони'),
			'vosita_boshqalar' => Yii::t('strings','Бошқа воситалар номи ва сони'),
			'tbl_fuqaro_uy_id' => Yii::t('strings','Tbl Fuqaro Uy'),
            'anketa_ozgarish' => Yii::t('strings','Анкетани ўзгариш санаси'),
			'user_id' => Yii::t('strings','Фойдаланувчи'),
			'user_date' => Yii::t('strings','Сана')
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vosita_soni',$this->vosita_soni);
		$criteria->compare('televizor',$this->televizor);
		$criteria->compare('kir_yuvish_vositasi',$this->kir_yuvish_vositasi);
		$criteria->compare('sovutgich',$this->sovutgich);
		$criteria->compare('DVD',$this->DVD);
		$criteria->compare('Konditsioner',$this->Konditsioner);
		$criteria->compare('kompyuter',$this->kompyuter);
		$criteria->compare('internet',$this->internet,true);
		$criteria->compare('dazmol',$this->dazmol);
		$criteria->compare('vosita_boshqalar',$this->vosita_boshqalar,true);
		$criteria->compare('tbl_fuqaro_uy_id',$this->tbl_fuqaro_uy_id);
        $criteria->compare('anketa_ozgarish',$this->anketa_ozgarish,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_date',$this->user_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Shaoriti the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
