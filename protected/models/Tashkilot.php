<?php

/**
 * This is the model class for table "{{tashkilot}}".
 *
 * The followings are the available columns in table '{{tashkilot}}':
 * @property integer $id
 * @property integer $tviloyat_id
 * @property integer $ttuman_id
 * @property integer $tmfy_id
 * @property integer $tkocha_id
 * @property string $tnomi
 * @property string $trahbari
 * @property integer $ttelefon_raqami
 * @property string $email
 * @property integer $user_id
 * @property string $user_date
 *
 * The followings are the available model relations:
 * @property Viloyatlar $tviloyat
 * @property Tuman $ttuman
 * @property Mfy $tmfy
 * @property Kochalar $tkocha
 */
class Tashkilot extends CActiveRecord
{
    public function getfviloyat()
    {
        $fviloyat = Viloyatlar::model()->findAll();
        $fviloyat = CHtml::listData($fviloyat,'id','viloyat_nomi');
        foreach ($fviloyat as $key=>$val){
            $fviloyat[$key]=Yii::t("strings",CHtml::encode($val));

        }
        return $fviloyat;
    }


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tashkilot}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('tviloyat_id, ttuman_id, tmfy_id, tkocha_id, ttelefon_raqami, user_id', 'numerical', 'integerOnly'=>true),
			array('tnomi, trahbari, email', 'length', 'max'=>255),
			array('user_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tviloyat_id, ttuman_id, tmfy_id, tkocha_id, tnomi, trahbari, ttelefon_raqami, email, user_id, user_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tviloyat' => array(self::BELONGS_TO, 'Viloyatlar', 'tviloyat_id'),
			'ttuman' => array(self::BELONGS_TO, 'Tuman', 'ttuman_id'),
			'tmfy' => array(self::BELONGS_TO, 'Mfy', 'tmfy_id'),
			'tkocha' => array(self::BELONGS_TO, 'Kochalar', 'tkocha_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'tviloyat_id' => Yii::t('strings','Вилоят'),
			'ttuman_id' => Yii::t('strings','Туман'),
			'tmfy_id' => Yii::t('strings','МФЙ'),
			'tkocha_id' => Yii::t('strings','Кўча номи'),
			'tnomi' => Yii::t('strings','Ташкилот номи'),
			'trahbari' => Yii::t('strings','Ташкилот рахбари ФИШ'),
			'ttelefon_raqami' => Yii::t('strings','Тел'),
			'email' => Yii::t('strings','Email'),
			'user_id' => Yii::t('strings','Фойдаланувчи номи:'),
			'user_date' => Yii::t('strings','Ташкилот киритилаётган санаси'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tviloyat_id',$this->tviloyat_id);
		$criteria->compare('ttuman_id',$this->ttuman_id);
		$criteria->compare('tmfy_id',$this->tmfy_id);
		$criteria->compare('tkocha_id',$this->tkocha_id);
		$criteria->compare('tnomi',$this->tnomi,true);
		$criteria->compare('trahbari',$this->trahbari,true);
		$criteria->compare('ttelefon_raqami',$this->ttelefon_raqami);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_date',$this->user_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tashkilot the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
