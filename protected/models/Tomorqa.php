	<?php

/**
 * This is the model class for table "{{tomorqa}}".
 *
 * The followings are the available columns in table '{{tomorqa}}':
 * @property integer $id
 * @property integer $tbl_fuqaro_uy_id
 * @property string $umumiy_yer_maydoni
 * @property string $yer_ekin_maydoni
 * @property string $yer_ekin_xolati
 * @property integer $issiq_xona_mavjud_id
 * @property integer $issiq_xona_ekin_nomi_soni
 * @property string $yer_foy_dalanish_xolati
 * @property string $ekilgan_ekin_maydoni
 * @property integer $kartoshka
 * @property integer $piyoz
 * @property integer $sarimsoq
 * @property integer $sabzi
 * @property integer $pomidor
 * @property integer $bodiring
 * @property integer $balgariskiy
 * @property integer $baqlajon
 * @property integer $loviya
 * @property integer $mosh
 * @property double $makka
 * @property double $beda
 * @property integer $tamaki
 * @property string $ekin_boshqalar
 * @property integer $ekilgan_meva_soni
 * @property integer $yongoq_soni
 * @property string $yongoq_yoshi
 * @property string $jiyda_soni
 * @property string $jiyda_yoshi
 * @property string $uzum_soni
 * @property string $uzum_yoshi
 * @property string $shaftoli_soni
 * @property string $shaftoli_yoshi
 * @property string $olma_soni
 * @property string $anor_soni
 * @property string $nok_soni
 * @property string $nok_yoshi
 * @property string $orik_soni
 * @property string $orik_yoshi
 *
 * The followings are the available model relations:
 * @property FuqaroUy $tblFuqaroUy
 * @property Issiqxona $issiqXonaMavjud
 */
class Tomorqa extends CActiveRecord
{
    public function getissiqxona_turi()
    {
        $issiqxona_t = Issiqxona::model()->findAll();
        $issiqxona_t = CHtml::listData($issiqxona_t,'id','issiqxona_turi');
        return $issiqxona_t;
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tomorqa}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('umumiy_yer_maydoni,yer_ekin_maydoni,yer_foy_dalanish_xolati,ekilgan_ekin_maydoni,yer_ekin_xolati,issiq_xona_mavjud_id,ekilgan_meva_soni,yer_foy_dalanish_xolati','required'),
			array('tbl_fuqaro_uy_id, issiq_xona_mavjud_id, ekilgan_meva_soni, yongoq_soni', 'numerical', 'integerOnly'=>true),
			//array('', 'numerical'),
			array('umumiy_yer_maydoni, yer_ekin_maydoni, yer_ekin_xolati, yer_foy_dalanish_xolati, ekilgan_ekin_maydoni, issiq_xona_ekin_nomi_soni, kartoshka, piyoz, sarimsoq, sabzi, pomidor, bodiring, balgariskiy, baqlajon, loviya, mosh, makka, beda, tamaki, ekin_boshqalar, yongoq_yoshi, jiyda_soni, jiyda_yoshi, uzum_soni, uzum_yoshi, shaftoli_soni, shaftoli_yoshi, olma_soni, olm_yoshi, anor_soni, anor_yoshi, nok_soni, nok_yoshi, orik_soni, orik_yoshi, anjir_soni, anjiir_yoshi, bexi_soni, bexi_yoshi, gilos_soni, gilos_yoshi, sliva_soni, sliva_yoshi, humro_soni, hurmo_yoshi, bodom_soni, bodom_yoshi, terak_soni, terak_yoshi, tol_soni, tol_yoshi, boshqa_ekinlar,kokat', 'length', 'max'=>255),
            array('anketa_ozgarish', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tbl_fuqaro_uy_id, umumiy_yer_maydoni, yer_ekin_maydoni, yer_ekin_xolati, issiq_xona_mavjud_id, issiq_xona_ekin_nomi_soni, yer_foy_dalanish_xolati, ekilgan_ekin_maydoni, kartoshka, piyoz, sarimsoq, sabzi, pomidor, bodiring, balgariskiy, baqlajon, loviya, mosh, makka, beda, tamaki, ekin_boshqalar, ekilgan_meva_soni, yongoq_soni, yongoq_yoshi, jiyda_soni, jiyda_yoshi, uzum_soni, uzum_yoshi, shaftoli_soni, shaftoli_yoshi, olma_soni, olm_yoshi, anor_soni, anor_yoshi, nok_soni, nok_yoshi, orik_soni, orik_yoshi, anjir_soni, anjiir_yoshi, bexi_soni, bexi_yoshi, gilos_soni, gilos_yoshi, sliva_soni, sliva_yoshi, humro_soni, hurmo_yoshi, bodom_soni, bodom_yoshi, terak_soni, terak_yoshi, tol_soni, tol_yoshi, boshqa_ekinlar, anketa_ozgarish,kokat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblFuqaroUy' => array(self::BELONGS_TO, 'FuqaroUy', 'tbl_fuqaro_uy_id'),
			'issiqXonaMavjud' => array(self::BELONGS_TO, 'Issiqxona', 'issiq_xona_mavjud_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'tbl_fuqaro_uy_id' => Yii::t('strings','Tbl Fuqaro Uy'),
			'umumiy_yer_maydoni' => Yii::t('strings','Умумий ер майдони'),
			'yer_ekin_maydoni' => Yii::t('strings','Ер экин майдони'),
			'yer_ekin_xolati' => Yii::t('strings','Томорқадан фойдаланиш холати'),
			'issiq_xona_mavjud_id' => Yii::t('strings','Иссиқхона мавжудлиги'),
			'issiq_xona_ekin_nomi_soni' => Yii::t('strings','Иссикхона экин номи ва сони (мисол:лимон- 5 туп)'),
			'yer_foy_dalanish_xolati' => Yii::t('strings','Ердан фойдаланиш холати'),
			'ekilgan_ekin_maydoni' => Yii::t('strings','Экилган экин майдони'),
			'kartoshka' => Yii::t('strings','Картошка майдони'),
			'piyoz' => Yii::t('strings','Пиёз майдони'),
			'sarimsoq' => Yii::t('strings','Саримсоқ пиёз майдони'),
			'sabzi' => Yii::t('strings','Сабзи майдони'),
			'pomidor' => Yii::t('strings','Помидор майдони'),
			'bodiring' => Yii::t('strings','Бодиринг майдони'),
			'balgariskiy' => Yii::t('strings','Булғори калампири майдони'),
			'baqlajon' => Yii::t('strings','Бақлажон майдони'),
			'loviya' => Yii::t('strings','Ловия майдони'),
			'mosh' => Yii::t('strings','Мош майдони'),
			'makka' => Yii::t('strings','Макка майдони'),
            'kokat' => Yii::t('strings','Кўкат майдони'),
            'beda' => Yii::t('strings','Беда майдони'),
			'tamaki' => Yii::t('strings','Тамаки майдони'),
			'ekin_boshqalar' => Yii::t('strings','Бошка экинлар майдони ва номи'),
			'ekilgan_meva_soni' => Yii::t('strings','Экилган мева сони қанча'),
			'yongoq_soni' => Yii::t('strings','Ёнғоқ сони'),
			'yongoq_yoshi' => Yii::t('strings','Ёнғоқ ёши'),
			'jiyda_soni' => Yii::t('strings','Жийда сони'),
			'jiyda_yoshi' => Yii::t('strings','Жийда ёши'),
			'uzum_soni' => Yii::t('strings','Узум сони'),
			'uzum_yoshi' => Yii::t('strings','Узум ёши'),
			'shaftoli_soni' => Yii::t('strings','Шафтоли сони'),
			'shaftoli_yoshi' => Yii::t('strings','Шафтоли ёши'),
			'olma_soni' => Yii::t('strings','Олма сони'),
			'olm_yoshi' => Yii::t('strings','Олма ёши'),
#			'olm_yoshi' => Yii::t('strings','Olm Yoshi'),
			'anor_soni' => Yii::t('strings','Анор сони'),
			'anor_yoshi' => Yii::t('strings','Анор ёши'),
			'nok_soni' => Yii::t('strings','Нок сони'),
			'nok_yoshi' => Yii::t('strings','Нок ёши'),
			'orik_soni' => Yii::t('strings','Ўрик сони'),
			'orik_yoshi' => Yii::t('strings','Ўрик ёши'),
			'anjir_soni' => Yii::t('strings','Анжир сони'),
			'anjiir_yoshi' => Yii::t('strings','Анжир ёши'),
			'bexi_soni' => Yii::t('strings','Бехи сони'),
			'bexi_yoshi' => Yii::t('strings','Бехи ёши'),
			'gilos_soni' => Yii::t('strings','Гилос сони'),
			'gilos_yoshi' => Yii::t('strings','Гилос ёши'),
			'sliva_soni' => Yii::t('strings','Слива сони'),
			'sliva_yoshi' => Yii::t('strings','Слива ёши'),
			'humro_soni' => Yii::t('strings','Хурмо сони'),
			'hurmo_yoshi' => Yii::t('strings','Хурмо ёши'),
			'bodom_soni' => Yii::t('strings','Бодом сони'),
			'bodom_yoshi' => Yii::t('strings','Бодом ёши'),
			'terak_soni' => Yii::t('strings','Терак сони'),
			'terak_yoshi' => Yii::t('strings','Терак ёши'),
			'tol_soni' => Yii::t('strings','Тол сони'),
			'tol_yoshi' => Yii::t('strings','Тол ёши'),
			'boshqa_ekinlar' => Yii::t('strings','Бошқа экинлар'),
            'anketa_ozgarish' => Yii::t('strings','Анкетани ўзгариш санаси'),
			'user_id' => Yii::t('strings','Фойдаланувчи'),
			'user_date' => Yii::t('strings','Сана'),

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tbl_fuqaro_uy_id',$this->tbl_fuqaro_uy_id);
		$criteria->compare('umumiy_yer_maydoni',$this->umumiy_yer_maydoni,true);
		$criteria->compare('yer_ekin_maydoni',$this->yer_ekin_maydoni,true);
		$criteria->compare('yer_ekin_xolati',$this->yer_ekin_xolati,true);
		$criteria->compare('issiq_xona_mavjud_id',$this->issiq_xona_mavjud_id);
		$criteria->compare('issiq_xona_ekin_nomi_soni',$this->issiq_xona_ekin_nomi_soni);
		$criteria->compare('yer_foy_dalanish_xolati',$this->yer_foy_dalanish_xolati,true);
		$criteria->compare('ekilgan_ekin_maydoni',$this->ekilgan_ekin_maydoni,true);
		$criteria->compare('kartoshka',$this->kartoshka,true);
		$criteria->compare('piyoz',$this->piyoz,true);
		$criteria->compare('sarimsoq',$this->sarimsoq,true);
		$criteria->compare('sabzi',$this->sabzi,true);
		$criteria->compare('pomidor',$this->pomidor,true);
		$criteria->compare('bodiring',$this->bodiring,true);
		$criteria->compare('balgariskiy',$this->balgariskiy,true);
		$criteria->compare('baqlajon',$this->baqlajon,true);
		$criteria->compare('loviya',$this->loviya,true);
		$criteria->compare('mosh',$this->mosh,true);
		$criteria->compare('makka',$this->makka,true);
		$criteria->compare('beda',$this->beda,true);
		$criteria->compare('tamaki',$this->tamaki,true);
		$criteria->compare('ekin_boshqalar',$this->ekin_boshqalar,true);
		$criteria->compare('ekilgan_meva_soni',$this->ekilgan_meva_soni);
		$criteria->compare('yongoq_soni',$this->yongoq_soni);
		$criteria->compare('yongoq_yoshi',$this->yongoq_yoshi,true);
		$criteria->compare('jiyda_soni',$this->jiyda_soni,true);
		$criteria->compare('jiyda_yoshi',$this->jiyda_yoshi,true);
		$criteria->compare('uzum_soni',$this->uzum_soni,true);
		$criteria->compare('uzum_yoshi',$this->uzum_yoshi,true);
		$criteria->compare('shaftoli_soni',$this->shaftoli_soni,true);
		$criteria->compare('shaftoli_yoshi',$this->shaftoli_yoshi,true);
		$criteria->compare('olma_soni',$this->olma_soni,true);
		$criteria->compare('olm_yoshi',$this->olm_yoshi,true);
		$criteria->compare('anor_soni',$this->anor_soni,true);
		$criteria->compare('anor_yoshi',$this->anor_yoshi,true);
		$criteria->compare('nok_soni',$this->nok_soni,true);
		$criteria->compare('nok_yoshi',$this->nok_yoshi,true);
		$criteria->compare('orik_soni',$this->orik_soni,true);
		$criteria->compare('orik_yoshi',$this->orik_yoshi,true);
		$criteria->compare('anjir_soni',$this->anjir_soni,true);
		$criteria->compare('anjiir_yoshi',$this->anjiir_yoshi,true);
		$criteria->compare('bexi_soni',$this->bexi_soni,true);
		$criteria->compare('bexi_yoshi',$this->bexi_yoshi,true);
		$criteria->compare('gilos_soni',$this->gilos_soni,true);
		$criteria->compare('gilos_yoshi',$this->gilos_yoshi,true);
		$criteria->compare('sliva_soni',$this->sliva_soni,true);
		$criteria->compare('sliva_yoshi',$this->sliva_yoshi,true);
		$criteria->compare('humro_soni',$this->humro_soni,true);
		$criteria->compare('hurmo_yoshi',$this->hurmo_yoshi,true);
		$criteria->compare('bodom_soni',$this->bodom_soni,true);
		$criteria->compare('bodom_yoshi',$this->bodom_yoshi,true);
		$criteria->compare('terak_soni',$this->terak_soni,true);
		$criteria->compare('terak_yoshi',$this->terak_yoshi,true);
		$criteria->compare('tol_soni',$this->tol_soni,true);
		$criteria->compare('tol_yoshi',$this->tol_yoshi,true);
		$criteria->compare('boshqa_ekinlar',$this->boshqa_ekinlar,true);
        $criteria->compare('anketa_ozgarish',$this->anketa_ozgarish,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tomorqa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
