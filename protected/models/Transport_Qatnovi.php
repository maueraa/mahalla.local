<?php

/**
 * This is the model class for table "{{transport_qatnovi}}".
 *
 * The followings are the available columns in table '{{transport_qatnovi}}':
 * @property integer $id
 * @property string $xonadonga_yaqin_tran_qatnov_masofa
 * @property string $xonadondan_mahalla_markaz_masofa
 * @property integer $xonadon_mahalla_trans_soni
 * @property string $xonadon_tuman_masofa
 * @property integer $xonadon_tuman_trans_soni
 * @property string $xonadon_viloyat_masofa
 * @property integer $xonadon_viloyat_trans_soni
 * @property string $yol_kira_mahalla
 * @property string $yol_kira_tuman
 * @property string $yol_kira_viloyat
 * @property integer $fuqaro_uy_id
 *
 * The followings are the available model relations:
 * @property FuqaroUy $fuqaroUy
 */
class Transport_Qatnovi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{transport_qatnovi}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('xonadon_mahalla_trans_soni, xonadon_tuman_trans_soni, xonadon_viloyat_trans_soni, fuqaro_uy_id', 'numerical', 'integerOnly'=>true),
			array('xonadonga_yaqin_tran_qatnov_masofa, xonadondan_mahalla_markaz_masofa, xonadon_tuman_masofa, xonadon_viloyat_masofa, yol_kira_mahalla, yol_kira_tuman, yol_kira_viloyat', 'length', 'max'=>255),
            array('anketa_ozgarish', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, xonadonga_yaqin_tran_qatnov_masofa, xonadondan_mahalla_markaz_masofa, xonadon_mahalla_trans_soni, xonadon_tuman_masofa, xonadon_tuman_trans_soni, xonadon_viloyat_masofa, xonadon_viloyat_trans_soni, yol_kira_mahalla, yol_kira_tuman, yol_kira_viloyat, fuqaro_uy_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fuqaroUy' => array(self::BELONGS_TO, 'FuqaroUy', 'fuqaro_uy_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'xonadonga_yaqin_tran_qatnov_masofa' => Yii::t('strings','Хонадонга яқин бўлган транспорт қатновига бўлган масофа'),
			'xonadondan_mahalla_markaz_masofa' => Yii::t('strings','Хонадондан махалла марказига бўлган масофа'),
			'xonadon_mahalla_trans_soni' => Yii::t('strings','Хонадондан махалла марказига нечта транспорт воситасида борилади(масалан:пиёда ёки 1 та транспортда деб фақатгина рақам билан ёзилсин'),
			'xonadon_tuman_masofa' => Yii::t('strings','Хонадондан туман марказигача бўлган масофа метр ёки км ли кўрсатилсин'),
			'xonadon_tuman_trans_soni' => Yii::t('strings','Хонадондан туман марказигача нечта транспортда борилади'),
			'xonadon_viloyat_masofa' => Yii::t('strings','Хонадондан вилоят марказигача масофа'),
			'xonadon_viloyat_trans_soni' => Yii::t('strings','Хонадондан вилоят марказигача нечта транспортда борилади'),
			'yol_kira_mahalla' => Yii::t('strings','Махалла марказига йўл кира нархи'),
			'yol_kira_tuman' => Yii::t('strings','Туман марказигача йўл кира нархи'),
			'yol_kira_viloyat' => Yii::t('strings','Вилоят марказигача йўл кира нархи'),
			'fuqaro_uy_id' => Yii::t('strings','Fuqaro Uy'),
            'anketa_ozgarish' => Yii::t('strings','Анкетани ўзгариш санаси'),
            'user_id' => Yii::t('strings','Фойдаланувчи'),
            'user_date' => Yii::t('strings','Сана'),

        );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('xonadonga_yaqin_tran_qatnov_masofa',$this->xonadonga_yaqin_tran_qatnov_masofa,true);
		$criteria->compare('xonadondan_mahalla_markaz_masofa',$this->xonadondan_mahalla_markaz_masofa,true);
		$criteria->compare('xonadon_mahalla_trans_soni',$this->xonadon_mahalla_trans_soni);
		$criteria->compare('xonadon_tuman_masofa',$this->xonadon_tuman_masofa,true);
		$criteria->compare('xonadon_tuman_trans_soni',$this->xonadon_tuman_trans_soni);
		$criteria->compare('xonadon_viloyat_masofa',$this->xonadon_viloyat_masofa,true);
		$criteria->compare('xonadon_viloyat_trans_soni',$this->xonadon_viloyat_trans_soni);
		$criteria->compare('yol_kira_mahalla',$this->yol_kira_mahalla,true);
		$criteria->compare('yol_kira_tuman',$this->yol_kira_tuman,true);
		$criteria->compare('yol_kira_viloyat',$this->yol_kira_viloyat,true);
		$criteria->compare('fuqaro_uy_id',$this->fuqaro_uy_id);
        $criteria->compare('anketa_ozgarish',$this->anketa_ozgarish,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Transport_Qatnovi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
