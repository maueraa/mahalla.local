<?php

/**
 * This is the model class for table "{{tuman}}".
 *
 * The followings are the available columns in table '{{tuman}}':
 * @property integer $id
 * @property string $tuman_nomi
 * @property integer $viloyat_id
 *
 * The followings are the available model relations:
 * @property Fuqaro[] $fuqaros
 * @property Fuqaro[] $fuqaros1
 * @property Mfy[] $mfies
 * @property Viloyatlar $viloyat
 */
class Tuman extends CActiveRecord
{

    public function getvil()
{
    $vil = Viloyatlar::model()->findAll();
    $vil = CHtml::listData($vil,'id','viloyat_nomi');
    return $vil;
}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tuman}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('viloyat_id', 'numerical', 'integerOnly'=>true),
			array('tuman_nomi', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tuman_nomi, viloyat_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fuqaros' => array(self::HAS_MANY, 'Fuqaro', 'passport_kim_tomonidan_id'),
			'fuqaros1' => array(self::HAS_MANY, 'Fuqaro', 'tugilgan_joy_tuman_id'),
			'mfies' => array(self::HAS_MANY, 'Mfy', 'tuman_id'),
			'viloyat' => array(self::BELONGS_TO, 'Viloyatlar', 'viloyat_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'tuman_nomi' => Yii::t('strings','Туман'),
			'viloyat_id' => Yii::t('strings','Вилоят'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tuman_nomi',$this->tuman_nomi,true);
		$criteria->compare('viloyat_id',$this->viloyat_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tuman the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
