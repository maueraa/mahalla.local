<?php

/**
 * This is the model class for table "{{user}}".
 *
 * The followings are the available columns in table '{{user}}':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property integer $role
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('name','required'),
			array('username, password, email', 'required'),
			array('viloyat, tuman, role, user_id, tasgkilot_id, mfy_id', 'numerical', 'integerOnly'=>true),
			array('username, password, email', 'length', 'max'=>128),
            array('user_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, viloyat, tuman, username, password, email, role, user_id, user_date, tasgkilot_id, mfy_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'aliments' => array(self::HAS_MANY, 'Aliment', 'user_id'),
            'binos' => array(self::HAS_MANY, 'Bino', 'user_id'),
            'bogchalars' => array(self::HAS_MANY, 'Bogchalar', 'user_id'),
            'daromads' => array(self::HAS_MANY, 'Daromad', 'user_id'),
            'daromadTuris' => array(self::HAS_MANY, 'DaromadTuri', 'user_id'),
            'davlats' => array(self::HAS_MANY, 'Davlat', 'user_id'),
            'deos' => array(self::HAS_MANY, 'Deo', 'user_id'),
            'fuqaros' => array(self::HAS_MANY, 'Fuqaro', 'user_id'),
            'fuqaroUys' => array(self::HAS_MANY, 'FuqaroUy', 'user_id'),
            'gazs' => array(self::HAS_MANY, 'Gaz', 'user_id'),
            'hayvons' => array(self::HAS_MANY, 'Hayvon', 'user_id'),
            'iibs' => array(self::HAS_MANY, 'Iib', 'user_id'),
            'ijtimoiyMuhits' => array(self::HAS_MANY, 'IjtimoiyMuhit', 'user_id'),
            'ishchiGuruhFios' => array(self::HAS_MANY, 'IshchiGuruhFio', 'user_id'),
            'issiqxonas' => array(self::HAS_MANY, 'Issiqxona', 'user_id'),
            'kassaliks' => array(self::HAS_MANY, 'Kassalik', 'user_id'),
            'kochaXolatis' => array(self::HAS_MANY, 'KochaXolati', 'user_id'),
            'kochalars' => array(self::HAS_MANY, 'Kochalar', 'user_id'),
            'kollejlars' => array(self::HAS_MANY, 'Kollejlar', 'user_id'),
            'maktabs' => array(self::HAS_MANY, 'Maktab', 'user_id'),
            'malumotis' => array(self::HAS_MANY, 'Malumoti', 'user_id'),
            'manaviys' => array(self::HAS_MANY, 'Manaviy', 'user_id'),
            'mfies' => array(self::HAS_MANY, 'Mfy', 'user_id'),
            'millatis' => array(self::HAS_MANY, 'Millati', 'user_id'),
            'muammos' => array(self::HAS_MANY, 'Muammo', 'user_id'),
            'muammoTurlaris' => array(self::HAS_MANY, 'MuammoTurlari', 'user_id'),
            'muassasas' => array(self::HAS_MANY, 'Muassasa', 'user_id'),
            'notinches' => array(self::HAS_MANY, 'Notinch', 'user_id'),
            'oliygohlars' => array(self::HAS_MANY, 'Oliygohlar', 'user_id'),
            'qarzs' => array(self::HAS_MANY, 'Qarz', 'user_id'),
            'rahbarlars' => array(self::HAS_MANY, 'Rahbarlar', 'user_id'),
            'sektors' => array(self::HAS_MANY, 'Sektor', 'user_id'),
            'shahars' => array(self::HAS_MANY, 'Shahar', 'user_id'),
            'shaoritis' => array(self::HAS_MANY, 'Shaoriti', 'user_id'),
            'tomorqas' => array(self::HAS_MANY, 'Tomorqa', 'user_id'),
            'transportQatnovis' => array(self::HAS_MANY, 'TransportQatnovi', 'user_id'),
            'tuman2' => array(self::HAS_MANY, 'Tuman', 'tuman'),
            'tumen' => array(self::BELONGS_TO, 'Tuman', 'tuman'),
            'user' => array(self::BELONGS_TO, 'Tashkilot', 'user_id'),
            'mfy' => array(self::BELONGS_TO, 'Mfy', 'mfy_id'),
            'viloyatlars' => array(self::BELONGS_TO, 'Viloyatlar', 'viloyat'),
            'ximzatKorsatishes' => array(self::HAS_MANY, 'XimzatKorsatish', 'user_id'),
            'xonadonTransports' => array(self::HAS_MANY, 'XonadonTransport', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
            'viloyat' =>Yii::t('strings','Вилоят'),
            'tuman' => Yii::t('strings','Туман'),
			'username' => Yii::t('strings','Фойдаланувчи номи'),
			'password' => Yii::t('strings','Парол'),
			'email' => Yii::t('strings','Почта манзили'),
			'role' => Yii::t('strings','Фойдаланувчининг даражаси'),
            'user_id' => Yii::t('strings','Фойдаланувчи'),
            'user_date' => Yii::t('strings','Фойдаланувчи рўйхатга олинган сана'),
            'tasgkilot_id' => Yii::t('strings','Ташкилот'),
            'mfy_id' => Yii::t('strings','МФЙ'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
        $criteria->compare('viloyat',$this->viloyat);
        $criteria->compare('tuman',$this->tuman);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('role',$this->role);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('user_date',$this->user_date,true);
        $criteria->compare('tasgkilot_id',$this->tasgkilot_id);
        $criteria->compare('mfy_id',$this->mfy_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
