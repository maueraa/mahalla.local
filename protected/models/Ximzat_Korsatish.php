<?php

/**
 * This is the model class for table "{{ximzat_korsatish}}".
 *
 * The followings are the available columns in table '{{ximzat_korsatish}}':
 * @property integer $id
 * @property integer $xondonga_yaqin_maktab_id
 * @property string $xondonga_yaqin_maktab_masofa
 * @property integer $xonadonga_yaqin_bogcha_id
 * @property string $xonadonga_yaqin_bogcha_masofa
 * @property string $xonadonga_yaqin_savdo_obyekt_masofa
 * @property string $xonadonga_yaqin_xizmat_korsatish_masofa
 * @property string $xonadonga_yaqin_bozorcha_masofa
 * @property string $xonadonga_yaqin_sport_majmuasi_masofa
 * @property string $xonadonga_yaqin_togarak_obyekt_masofa
 * @property string $xonadonga_yaqin_bogacha_masofa
 * @property integer $tbl_fuqaro_uy_id
 *
 * The followings are the available model relations:
 * @property FuqaroUy $tblFuqaroUy
 * @property Maktab $xondongaYaqinMaktab
 * @property Bogchalar $xonadongaYaqinBogcha
 */
class Ximzat_Korsatish extends CActiveRecord
{
    public function getmaktab()
    {
        $maktab_raqam = Maktab::model()->findAll();
        $maktab_raqam = CHtml::listData($maktab_raqam,'id','maktab_raqami');
        return $maktab_raqam;
    }
    public function getbogcha()
    {
        $bogcha_raqamii = Bogchalar::model()->findAll();
        $bogcha_raqamii = CHtml::listData($bogcha_raqamii,'id','bogcha_raqami');
        return $bogcha_raqamii;

    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{ximzat_korsatish}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('xondonga_yaqin_maktab_id,xonadonga_yaqin_bogcha_id,xonadonga_yaqin_kollej_nomi','required'),
			array('xondonga_yaqin_maktab_id, xonadonga_yaqin_bogcha_id, tbl_fuqaro_uy_id, user_id', 'numerical', 'integerOnly'=>true),
			array('xondonga_yaqin_maktab_masofa, xonadonga_yaqin_bogcha_masofa, xonadonga_yaqin_kollej_masofa, xonadonga_yaqin_kollej_nomi, xonadonga_yaqin_savdo_obyekt_masofa, xonadonga_yaqin_xizmat_korsatish_masofa, xonadonga_yaqin_bozorcha_masofa, xonadonga_yaqin_xamom_masofa,  xonadonga_yaqin_sport_majmuasi_masofa, xonadonga_yaqin_togarak_obyekt_masofa, xonadonga_yaqin_bogacha_masofa, xonadonga_yaqin_bogacha_masofa, istiroxat_bog, istiroxat_bog_masofa', 'length', 'max'=>255),
            array('anketa_ozgarish, user_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, xondonga_yaqin_maktab_id, xondonga_yaqin_maktab_masofa, xonadonga_yaqin_bogcha_id, xonadonga_yaqin_bogcha_masofa, xonadonga_yaqin_savdo_obyekt_masofa, xonadonga_yaqin_xizmat_korsatish_masofa, xonadonga_yaqin_bozorcha_masofa, xonadonga_yaqin_sport_majmuasi_masofa, xonadonga_yaqin_togarak_obyekt_masofa, xonadonga_yaqin_bogacha_masofa, tbl_fuqaro_uy_id,istiroxat_bog, istiroxat_bog_masofa, anketa_ozgarish, user_id, user_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblFuqaroUy' => array(self::BELONGS_TO, 'FuqaroUy', 'tbl_fuqaro_uy_id'),
			'xondongaYaqinMaktab' => array(self::BELONGS_TO, 'Maktab', 'xondonga_yaqin_maktab_id'),
			'xonadongaYaqinBogcha' => array(self::BELONGS_TO, 'Bogchalar', 'xonadonga_yaqin_bogcha_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'xondonga_yaqin_maktab_id' => Yii::t('strings','Хонаднога яқин мактаб (мактаб рақами)'),
			'xondonga_yaqin_maktab_masofa' => Yii::t('strings','Хонаднога яқин мактаб обьектигача бўлган масофа метрда киритинг (масалан:100), метр ёки м сўзини ёзиш шарт эмас'),
			'xonadonga_yaqin_bogcha_id' => Yii::t('strings','Хонаднога яқин боғча'),
			'xonadonga_yaqin_bogcha_masofa' => Yii::t('strings','Хонаднога яқин боғча обьектигача бўлган масофа'),
            'xonadonga_yaqin_kollej_masofa' => Yii::t('strings','Хонадонга яқин коллежгача масофа'),
            'xonadonga_yaqin_kollej_nomi' => Yii::t('strings','Хонадонга яқин коллеж номи'),
			'xonadonga_yaqin_savdo_obyekt_masofa' => Yii::t('strings','Хонаднога яқин савдо обьектигача бўлган масофа'),
			'xonadonga_yaqin_xizmat_korsatish_masofa' => Yii::t('strings','Хонаднога хизмат кўрсатиш обьектигача бўлган масофа'),
			'xonadonga_yaqin_bozorcha_masofa' => Yii::t('strings','Хонаднога яқин бозорга обьектигача бўлган масофа'),
            'xonadonga_yaqin_xamom_masofa' => Yii::t('strings','Хонадонга яқин хамомгача масофа'),
			'xonadonga_yaqin_sport_majmuasi_masofa' => Yii::t('strings','Хонаднога яқин спорт обьектигача бўлган масофа'),
			'xonadonga_yaqin_togarak_obyekt_masofa' => Yii::t('strings','Хонаднога яқин тўгарак обьектигача бўлган масофа'),
			'xonadonga_yaqin_bogacha_masofa' => Yii::t('strings','Xonadonga Yaqin Bogacha Masofa'),
			'tbl_fuqaro_uy_id' => Yii::t('strings','Tbl Fuqaro Uy'),
			'istiroxat_bog' => Yii::t('strings','Истирохат боғи номи'),
			'istiroxat_bog_masofa' => Yii::t('strings','Истирохат боғигача бўлган масофа'),
            'anketa_ozgarish' => Yii::t('strings','Анкета ўзгариш санаси'),
			'user_id' => Yii::t('strings','Фойдаланувчи'),
			'user_date' => Yii::t('strings','Сана'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('xondonga_yaqin_maktab_id',$this->xondonga_yaqin_maktab_id);
		$criteria->compare('xondonga_yaqin_maktab_masofa',$this->xondonga_yaqin_maktab_masofa,true);
		$criteria->compare('xonadonga_yaqin_bogcha_id',$this->xonadonga_yaqin_bogcha_id);
		$criteria->compare('xonadonga_yaqin_bogcha_masofa',$this->xonadonga_yaqin_bogcha_masofa,true);
        $criteria->compare('xonadonga_yaqin_kollej_nomi',$this->xonadonga_yaqin_kollej_nomi,true);
        $criteria->compare('xonadonga_yaqin_kollej_masofa',$this->xonadonga_yaqin_kollej_masofa,true);
        $criteria->compare('xonadonga_yaqin_savdo_obyekt_masofa',$this->xonadonga_yaqin_savdo_obyekt_masofa,true);
		$criteria->compare('xonadonga_yaqin_xizmat_korsatish_masofa',$this->xonadonga_yaqin_xizmat_korsatish_masofa,true);
		$criteria->compare('xonadonga_yaqin_bozorcha_masofa',$this->xonadonga_yaqin_bozorcha_masofa,true);
        $criteria->compare('xonadonga_yaqin_xamom_masofa',$this->xonadonga_yaqin_xamom_masofa,true);
		$criteria->compare('xonadonga_yaqin_sport_majmuasi_masofa',$this->xonadonga_yaqin_sport_majmuasi_masofa,true);
		$criteria->compare('xonadonga_yaqin_togarak_obyekt_masofa',$this->xonadonga_yaqin_togarak_obyekt_masofa,true);
		$criteria->compare('xonadonga_yaqin_bogacha_masofa',$this->xonadonga_yaqin_bogacha_masofa,true);
		$criteria->compare('tbl_fuqaro_uy_id',$this->tbl_fuqaro_uy_id);
		$criteria->compare('istiroxat_bog',$this->istiroxat_bog,true);
		$criteria->compare('istiroxat_bog_masofa',$this->istiroxat_bog_masofa,true);
        $criteria->compare('anketa_ozgarish',$this->anketa_ozgarish,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_date',$this->user_date,true);

        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ximzat_Korsatish the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
