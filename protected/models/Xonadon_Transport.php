<?php

/**
 * This is the model class for table "{{xonadon_transport}}".
 *
 * The followings are the available columns in table '{{xonadon_transport}}':
 * @property integer $id
 * @property integer $xonadon_transport_soni
 * @property string $yengil_avto_rusumi
 * @property string $yengil_avto_raqami
 * @property string $yengil_avto_yili
 * @property string $yengil_avto_tex_pas
 * @property string $yengil_prvara_raqami
 * @property string $yengil_sugurta_mavjudligi
 * @property string $yengil_ishonchnoma
 * @property string $yengi_avto_egasi
 * @property string $yuk_avto_rusumi
 * @property string $yuk_avto_raqami
 * @property string $yuk_avto_yili
 * @property string $yuk_avto_tex_pas
 * @property string $yuk_avto_egasi
 * @property string $yuk_avto_prva
 * @property string $yuk_avto_sugurta
 * @property string $yuk_avto_ishonchnoma
 * @property string $trans_boshqalar
 * @property integer $fuqaro_uy_id
 * @property integer $fuqaro_id
 *
 * The followings are the available model relations:
 * @property FuqaroUy $fuqaroUy
 * @property Fuqaro $fuqaro
 */
class Xonadon_Transport extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{xonadon_transport}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('xonadon_transport_soni','required'),
			array('xonadon_transport_soni, fuqaro_uy_id, fuqaro_id, user_id', 'numerical', 'integerOnly'=>true),
			array('yengil_avto_raqami, yengil_avto_yili, yengil_avto_tex_pas, yengil_prvara_raqami, yengil_sugurta_mavjudligi, yengil_ishonchnoma, yengi_avto_egasi, yuk_avto_rusumi, yuk_avto_raqami, yuk_avto_yili, yuk_avto_tex_pas, yuk_avto_egasi, yuk_avto_prva, yuk_avto_sugurta, yuk_avto_ishonchnoma, trans_boshqalar', 'length', 'max'=>255),
			array('yengil_avto_rusumi, anketa_ozgarish,user_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, xonadon_transport_soni, yengil_avto_rusumi, yengil_avto_raqami, yengil_avto_yili, yengil_avto_tex_pas, yengil_prvara_raqami, yengil_sugurta_mavjudligi, yengil_ishonchnoma, yengi_avto_egasi, yuk_avto_rusumi, yuk_avto_raqami, yuk_avto_yili, yuk_avto_tex_pas, yuk_avto_egasi, yuk_avto_prva, yuk_avto_sugurta, yuk_avto_ishonchnoma, trans_boshqalar, fuqaro_uy_id, fuqaro_id,  anketa_ozgarish, user_id, user_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fuqaroUy' => array(self::BELONGS_TO, 'FuqaroUy', 'fuqaro_uy_id'),
			'fuqaro' => array(self::BELONGS_TO, 'Fuqaro', 'fuqaro_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('strings','ID'),
			'xonadon_transport_soni' => Yii::t('strings','Мавжуд техник воситалар ва сони (мавжуд бўлмаса 0 рақамини киритинг)'),
			'yengil_avto_rusumi' => Yii::t('strings','Енгил автомашина русуми '),
			'yengil_avto_raqami' => Yii::t('strings','Енгил автомашина рақами'),
			'yengil_avto_yili' => Yii::t('strings','Енгил автомашина ишлаб чиқарилган йили'),
			'yengil_avto_tex_pas' => Yii::t('strings','Енгил автомашина техник праспорт'),
            'yengi_avto_egasi' => Yii::t('strings','Енгил автомашинанинг эгасин ФИШ'),
            'yengil_prvara_raqami' => Yii::t('strings','Енгил машина эгаси хақида маълумот  ва хайдовчилик гувохномаси'),
            'yengil_sugurta_mavjudligi' => Yii::t('strings','Енгил машина суғурта мавжудлиги'),
            'yengil_ishonchnoma' => Yii::t('strings','Енгил автомашина ишончномаси мавжудлиги'),
			'yuk_avto_rusumi' => Yii::t('strings','Юк автомашинаси русуми'),
			'yuk_avto_raqami' => Yii::t('strings','Юк автомашинаси рақами'),
			'yuk_avto_yili' => Yii::t('strings','Юк автомашинаси ишлаб чиқарилган йили'),
			'yuk_avto_tex_pas' => Yii::t('strings','Юк автомашинаси техник паспорт рақами'),
			'yuk_avto_egasi' => Yii::t('strings','Юк автомашинаси эгаси'),
			'yuk_avto_prva' => Yii::t('strings','Юк автомашинаси хайдовчилик гувохномаси'),
			'yuk_avto_sugurta' => Yii::t('strings','Юк автомашинаси суғуртаси мавжудлиги'),
			'yuk_avto_ishonchnoma' => Yii::t('strings','Юк автомашинаси ишонч қоғози мавжудлиги'),
			'trans_boshqalar' => Yii::t('strings','Бошққа транспорт воситалари сони'),
			'fuqaro_uy_id' => Yii::t('strings','Fuqaro Uy'),
			'fuqaro_id' => Yii::t('strings','Fuqaro'),
            'anketa_ozgarish' => Yii::t('strings','Анкета ўзгариш санаси'),
			'user_id' => Yii::t('strings','Фойдаланувчи'),
			'user_date' => Yii::t('strings','Сана'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('xonadon_transport_soni',$this->xonadon_transport_soni);
		$criteria->compare('yengil_avto_rusumi',$this->yengil_avto_rusumi,true);
		$criteria->compare('yengil_avto_raqami',$this->yengil_avto_raqami,true);
		$criteria->compare('yengil_avto_yili',$this->yengil_avto_yili,true);
		$criteria->compare('yengil_avto_tex_pas',$this->yengil_avto_tex_pas,true);
        $criteria->compare('yengi_avto_egasi',$this->yengi_avto_egasi,true);
        $criteria->compare('yengil_prvara_raqami',$this->yengil_prvara_raqami,true);
        $criteria->compare('yengil_sugurta_mavjudligi',$this->yengil_sugurta_mavjudligi,true);
        $criteria->compare('yengil_ishonchnoma',$this->yengil_ishonchnoma,true);
		$criteria->compare('yuk_avto_rusumi',$this->yuk_avto_rusumi,true);
		$criteria->compare('yuk_avto_raqami',$this->yuk_avto_raqami,true);
		$criteria->compare('yuk_avto_yili',$this->yuk_avto_yili,true);
		$criteria->compare('yuk_avto_tex_pas',$this->yuk_avto_tex_pas,true);
		$criteria->compare('yuk_avto_egasi',$this->yuk_avto_egasi,true);
		$criteria->compare('yuk_avto_prva',$this->yuk_avto_prva,true);
		$criteria->compare('yuk_avto_sugurta',$this->yuk_avto_sugurta,true);
		$criteria->compare('yuk_avto_ishonchnoma',$this->yuk_avto_ishonchnoma,true);
		$criteria->compare('trans_boshqalar',$this->trans_boshqalar,true);
		$criteria->compare('fuqaro_uy_id',$this->fuqaro_uy_id);
		$criteria->compare('fuqaro_id',$this->fuqaro_id);
        $criteria->compare('anketa_ozgarish',$this->anketa_ozgarish,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_date',$this->user_date,true);



		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Xonadon_Transport the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
