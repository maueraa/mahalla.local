<?php
/* @var $this AlimentController */
/* @var $data Aliment */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomi')); ?>:</b>
	<?php echo CHtml::encode($data->nomi); ?>
	<br />


</div>