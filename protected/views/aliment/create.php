<?php
/* @var $this AlimentController */
/* @var $model Aliment */

$this->breadcrumbs=array(
	Yii::t('strings','Aliments')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Aliment'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Aliment'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Aliment')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>