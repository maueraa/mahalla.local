<?php
/* @var $this BinoController */
/* @var $data Bino */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xolati')); ?>:</b>
	<?php echo CHtml::encode($data->xolati); ?>
	<br />


</div>