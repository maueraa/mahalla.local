<?php
/* @var $this BinoController */
/* @var $model Bino */

$this->breadcrumbs=array(
	Yii::t('strings','Binos')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Bino'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Bino'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Bino')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>