<?php
/* @var $this BogchalarController */
/* @var $data Bogchalar */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bogcha_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->bogcha_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bogcha_nomi')); ?>:</b>
	<?php echo CHtml::encode($data->bogcha_nomi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('manzili')); ?>:</b>
	<?php echo CHtml::encode($data->manzili); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bogcha_direktor')); ?>:</b>
	<?php echo CHtml::encode($data->bogcha_direktor); ?>
	<br />


</div>