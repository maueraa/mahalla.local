<?php
/* @var $this BogchalarController */
/* @var $model Bogchalar */

$this->breadcrumbs=array(
	Yii::t('strings','Bogchalars')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Bogchalar'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Bogchalar'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Bogchalar')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>