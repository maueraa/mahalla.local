<?php
/* @var $this BogchalarController */
/* @var $model Bogchalar */

$this->breadcrumbs=array(
	Yii::t('strings','Bogchalars')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Bogchalar'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Bogchalar'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Bogchalar'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Bogchalar'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Bogchalar'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Bogchalar') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'bogcha_raqami',
		'bogcha_nomi',
		'manzili',
		'bogcha_direktor',
/**
		array(
			'label'=>$model->getAttributeLabel('bogcha_direktor'),
			'type'=>'raw',
			'value'=>$model->bogcha_direk->name,
		),
/**/
	),
)); ?>
