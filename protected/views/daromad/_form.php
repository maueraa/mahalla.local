<?php
/* @var $this DaromadController */
/* @var $model Daromad */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'daromad-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'kam_taminlangan_sifatida'); ?>
		<?php echo $form->dropdownlist($model,'kam_taminlangan_sifatida',array('1'=>'Туради','2'=>'Турмайди'),array('prompt'=>Yii::t("strings",'Камтаминланган оила рўйхатида туриши'))); ?>
		<?php echo $form->error($model,'kam_taminlangan_sifatida'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'oylik_ortacha_daromadi'); ?>
		<?php echo $form->textField($model,'oylik_ortacha_daromadi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'oylik_ortacha_daromadi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ish_xaq_oluvchi_soni'); ?>
		<?php echo $form->textField($model,'ish_xaq_oluvchi_soni'); ?>
		<?php echo $form->error($model,'ish_xaq_oluvchi_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ish_haqi_summasi'); ?>
		<?php echo $form->textField($model,'ish_haqi_summasi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ish_haqi_summasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ish_xaqi_muddati'); ?>
		<?php echo $form->textField($model,'ish_xaqi_muddati',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ish_xaqi_muddati'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pensiya_oluvchi_soni'); ?>
		<?php echo $form->textField($model,'pensiya_oluvchi_soni'); ?>
		<?php echo $form->error($model,'pensiya_oluvchi_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pensiya_summa'); ?>
		<?php echo $form->textField($model,'pensiya_summa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'pensiya_summa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pensiya_vaqti'); ?>
		<?php echo $form->textField($model,'pensiya_vaqti',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'pensiya_vaqti'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nogiron_nafaqa_soni'); ?>
		<?php echo $form->textField($model,'nogiron_nafaqa_soni'); ?>
		<?php echo $form->error($model,'nogiron_nafaqa_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nogiron_summasi'); ?>
		<?php echo $form->textField($model,'nogiron_summasi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nogiron_summasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nogiron_nafaqa_vaqti'); ?>
		<?php echo $form->textField($model,'nogiron_nafaqa_vaqti'); ?>
		<?php echo $form->error($model,'nogiron_nafaqa_vaqti'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kamtaminlangan_nafaqachilar_soni'); ?>
		<?php echo $form->textField($model,'kamtaminlangan_nafaqachilar_soni'); ?>
		<?php echo $form->error($model,'kamtaminlangan_nafaqachilar_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kamtaminlangan_nafaqa_summasi'); ?>
		<?php echo $form->textField($model,'kamtaminlangan_nafaqa_summasi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'kamtaminlangan_nafaqa_summasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kamtaminlangan_nafaqa_vaqti'); ?>
		<?php echo $form->textField($model,'kamtaminlangan_nafaqa_vaqti',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'kamtaminlangan_nafaqa_vaqti'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ikki_yoshgacha_nafaqa_soni'); ?>
		<?php echo $form->textField($model,'ikki_yoshgacha_nafaqa_soni'); ?>
		<?php echo $form->error($model,'ikki_yoshgacha_nafaqa_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ikki_yosh_nafaqa_summasi'); ?>
		<?php echo $form->textField($model,'ikki_yosh_nafaqa_summasi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ikki_yosh_nafaqa_summasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ikki_yosh_nafaqa_vaqti'); ?>
		<?php echo $form->textField($model,'ikki_yosh_nafaqa_vaqti',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ikki_yosh_nafaqa_vaqti'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'on_tort_yosh_nafaqa_soni'); ?>
		<?php echo $form->textField($model,'on_tort_yosh_nafaqa_soni'); ?>
		<?php echo $form->error($model,'on_tort_yosh_nafaqa_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'on_tort_nafaqa_summasi'); ?>
		<?php echo $form->textField($model,'on_tort_nafaqa_summasi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'on_tort_nafaqa_summasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'on_tort_nafaqa_vaqti'); ?>
		<?php echo $form->textField($model,'on_tort_nafaqa_vaqti',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'on_tort_nafaqa_vaqti'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'daromad_boshqa'); ?>
		<?php echo $form->textField($model,'daromad_boshqa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'daromad_boshqa'); ?>
	</div>

	<div class="row">
		
		<?php echo $form->hiddenField($model,'fuqaro_uy_id',array('value'=>$fid)); ?>
		<?php echo $form->error($model,'fuqaro_uy_id'); ?>
	</div>

	<div class="row">
		
		<?php echo $form->hiddenField($model,'fuqaro_id'); ?>
		<?php echo $form->error($model,'fuqaro_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id').Yii::app()->user->name; ?>
		<?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_date').date("d.m.Y"); ?>
		<?php echo $form->hiddenField($model,'user_date',array('value'=>date("Y-m-d"))); ?>
		<?php echo $form->error($model,'user_date'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array("class"=>"btn btn-primary")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->