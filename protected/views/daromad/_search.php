<?php
/* @var $this DaromadController */
/* @var $model Daromad */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kam_taminlangan_sifatida'); ?>
		<?php echo $form->textField($model,'kam_taminlangan_sifatida',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'oylik_ortacha_daromadi'); ?>
		<?php echo $form->textField($model,'oylik_ortacha_daromadi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ish_xaq_oluvchi_soni'); ?>
		<?php echo $form->textField($model,'ish_xaq_oluvchi_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ish_haqi_summasi'); ?>
		<?php echo $form->textField($model,'ish_haqi_summasi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ish_xaqi_muddati'); ?>
		<?php echo $form->textField($model,'ish_xaqi_muddati',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pensiya_oluvchi_soni'); ?>
		<?php echo $form->textField($model,'pensiya_oluvchi_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pensiya_summa'); ?>
		<?php echo $form->textField($model,'pensiya_summa',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pensiya_vaqti'); ?>
		<?php echo $form->textField($model,'pensiya_vaqti',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nogiron_nafaqa_soni'); ?>
		<?php echo $form->textField($model,'nogiron_nafaqa_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nogiron_summasi'); ?>
		<?php echo $form->textField($model,'nogiron_summasi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nogiron_nafaqa_vaqti'); ?>
		<?php echo $form->textField($model,'nogiron_nafaqa_vaqti'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kamtaminlangan_nafaqachilar_soni'); ?>
		<?php echo $form->textField($model,'kamtaminlangan_nafaqachilar_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kamtaminlangan_nafaqa_summasi'); ?>
		<?php echo $form->textField($model,'kamtaminlangan_nafaqa_summasi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kamtaminlangan_nafaqa_vaqti'); ?>
		<?php echo $form->textField($model,'kamtaminlangan_nafaqa_vaqti',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ikki_yoshgacha_nafaqa_soni'); ?>
		<?php echo $form->textField($model,'ikki_yoshgacha_nafaqa_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ikki_yosh_nafaqa_summasi'); ?>
		<?php echo $form->textField($model,'ikki_yosh_nafaqa_summasi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ikki_yosh_nafaqa_vaqti'); ?>
		<?php echo $form->textField($model,'ikki_yosh_nafaqa_vaqti',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'on_tort_yosh_nafaqa_soni'); ?>
		<?php echo $form->textField($model,'on_tort_yosh_nafaqa_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'on_tort_nafaqa_summasi'); ?>
		<?php echo $form->textField($model,'on_tort_nafaqa_summasi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'on_tort_nafaqa_vaqti'); ?>
		<?php echo $form->textField($model,'on_tort_nafaqa_vaqti',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'daromad_boshqa'); ?>
		<?php echo $form->textField($model,'daromad_boshqa',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fuqaro_uy_id'); ?>
		<?php echo $form->textField($model,'fuqaro_uy_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fuqaro_id'); ?>
		<?php echo $form->textField($model,'fuqaro_id'); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model,'anketa_ozgarish'); ?>
        <?php echo $form->textField($model,'anketa_ozgarish'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'user_id'); ?>
        <?php echo $form->textField($model,'user_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'user_date'); ?>
        <?php echo $form->textField($model,'user_date'); ?>
    </div>

    <div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->