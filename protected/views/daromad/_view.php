<?php
/* @var $this DaromadController */
/* @var $data Daromad */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kam_taminlangan_sifatida')); ?>:</b>
	<?php echo CHtml::encode($data->kam_taminlangan_sifatida); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('oylik_ortacha_daromadi')); ?>:</b>
	<?php echo CHtml::encode($data->oylik_ortacha_daromadi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ish_xaq_oluvchi_soni')); ?>:</b>
	<?php echo CHtml::encode($data->ish_xaq_oluvchi_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ish_haqi_summasi')); ?>:</b>
	<?php echo CHtml::encode($data->ish_haqi_summasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ish_xaqi_muddati')); ?>:</b>
	<?php echo CHtml::encode($data->ish_xaqi_muddati); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pensiya_oluvchi_soni')); ?>:</b>
	<?php echo CHtml::encode($data->pensiya_oluvchi_soni); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('pensiya_summa')); ?>:</b>
	<?php echo CHtml::encode($data->pensiya_summa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pensiya_vaqti')); ?>:</b>
	<?php echo CHtml::encode($data->pensiya_vaqti); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nogiron_nafaqa_soni')); ?>:</b>
	<?php echo CHtml::encode($data->nogiron_nafaqa_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nogiron_summasi')); ?>:</b>
	<?php echo CHtml::encode($data->nogiron_summasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nogiron_nafaqa_vaqti')); ?>:</b>
	<?php echo CHtml::encode($data->nogiron_nafaqa_vaqti); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kamtaminlangan_nafaqachilar_soni')); ?>:</b>
	<?php echo CHtml::encode($data->kamtaminlangan_nafaqachilar_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kamtaminlangan_nafaqa_summasi')); ?>:</b>
	<?php echo CHtml::encode($data->kamtaminlangan_nafaqa_summasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kamtaminlangan_nafaqa_vaqti')); ?>:</b>
	<?php echo CHtml::encode($data->kamtaminlangan_nafaqa_vaqti); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ikki_yoshgacha_nafaqa_soni')); ?>:</b>
	<?php echo CHtml::encode($data->ikki_yoshgacha_nafaqa_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ikki_yosh_nafaqa_summasi')); ?>:</b>
	<?php echo CHtml::encode($data->ikki_yosh_nafaqa_summasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ikki_yosh_nafaqa_vaqti')); ?>:</b>
	<?php echo CHtml::encode($data->ikki_yosh_nafaqa_vaqti); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('on_tort_yosh_nafaqa_soni')); ?>:</b>
	<?php echo CHtml::encode($data->on_tort_yosh_nafaqa_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('on_tort_nafaqa_summasi')); ?>:</b>
	<?php echo CHtml::encode($data->on_tort_nafaqa_summasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('on_tort_nafaqa_vaqti')); ?>:</b>
	<?php echo CHtml::encode($data->on_tort_nafaqa_vaqti); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('daromad_boshqa')); ?>:</b>
	<?php echo CHtml::encode($data->daromad_boshqa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fuqaro_uy_id')); ?>:</b>
	<?php echo CHtml::encode($data->fuqaro_uy_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fuqaro_id')); ?>:</b>
	<?php echo CHtml::encode($data->fuqaro_id); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('anketa_ozgarish')); ?>:</b>
	<?php echo CHtml::encode($data->anketa_ozgarish); ?>
	<br />
<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_date')); ?>:</b>
	<?php echo CHtml::encode($data->user_date); ?>
	<br />
	*/ ?>

</div>