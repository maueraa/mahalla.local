<?php
/* @var $this DaromadController */
/* @var $model Daromad */

$this->breadcrumbs=array(
	Yii::t('strings','Daromads')=>array('index'),
	Yii::t('strings','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Daromad'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Daromad'))), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#daromad-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Daromad'))) ?></h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link(Yii::t('zii','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'daromad-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'kam_taminlangan_sifatida',
		'oylik_ortacha_daromadi',
		'ish_xaq_oluvchi_soni',
		'ish_haqi_summasi',
		'ish_xaqi_muddati',
		/*
		'pensiya_oluvchi_soni',
		'pensiya_summa',
		'pensiya_vaqti',
		'nogiron_nafaqa_soni',
		'nogiron_summasi',
		'nogiron_nafaqa_vaqti',
		'kamtaminlangan_nafaqachilar_soni',
		'kamtaminlangan_nafaqa_summasi',
		'kamtaminlangan_nafaqa_vaqti',
		'ikki_yoshgacha_nafaqa_soni',
		'ikki_yosh_nafaqa_summasi',
		'ikki_yosh_nafaqa_vaqti',
		'on_tort_yosh_nafaqa_soni',
		'on_tort_nafaqa_summasi',
		'on_tort_nafaqa_vaqti',
		'daromad_boshqa',
		'fuqaro_uy_id',
		'user_id',
		'user_date',
		'fuqaro_id',
		//'fuqaro.name',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
