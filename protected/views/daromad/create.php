﻿<?php
/* @var $this DaromadController */
/* @var $model Daromad */

$this->breadcrumbs=array(
	Yii::t('strings','Daromads')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Daromad'))), 'url'=>array('index')),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Daromad'))), 'url'=>array('admin')),

);
?>

<h1><?php echo Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон аъзоларининг даромади ва пенсия олиши хақидаги маълумотларни киритиш ойнаси')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'fid'=>$fid)); ?>