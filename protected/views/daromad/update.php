<?php
/* @var $this DaromadController */
/* @var $model Daromad */

$this->breadcrumbs=array(
	Yii::t('strings','Оила даромадлари')=>array('index'),
	Yii::t('strings',$model->id)=>array('view','id'=>$model->id),
	Yii::t('strings','Ўзгартириш'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Daromad'))), 'url'=>array('index')),
#	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Daromad'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Daromad'))), 'url'=>array('admin')),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон маълумотлари'))), 'url'=>array("/fuqaro_Uy/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон аъзолари'))), 'url'=>array("/fuqarolar?fuq=$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Маънавий мухит'))), 'url'=>array("/manaviy/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Даромади хақидаги маълумотларни кўриш'))), 'url'=>array('view', 'id'=>$model->fuqaro_uy_id)),
#    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Даромади'))), 'url'=>array('/daromad/update', 'fid'=>$fid)),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Қарздоллик'))), 'url'=>array("/qarz/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт қатнови'))), 'url'=>array("/transport_qatnovi/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт воситалари'))), 'url'=>array("/xonadon_transport/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ижтимоий мухит'))), 'url'=>array("/ijtimoiy_muhit/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Шарт-шароити'))), 'url'=>array("/shaoriti/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Томорқа'))), 'url'=>array("/tomorqa/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Чорва хайвонлари'))), 'url'=>array("/hayvon/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хизмат кўрсатиш объектлари'))), 'url'=>array("/ximzat_korsatish/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчанинг холати'))), 'url'=>array("/kocha_xolati/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Барча маълумотларни кўриш'))), 'url'=>array('/jamlama/index', 'id'=>$model->fuqaro_uy_id)),

);
$fuqaro_m = Fuqaro_Uy::model()->find("id=$model->fuqaro_uy_id");
?>

<h1><?php if (isset($fuqaro_m->ftuman->tuman_nomi))echo $fuqaro_m->ftuman->tuman_nomi; ?>
    <?php if (isset($fuqaro_m->mfy->mfy_nomi))	echo $fuqaro_m->mfy->mfy_nomi;	?>
    <?php if (isset($fuqaro_m->kochalar->tbl_kocha_nomi))echo $fuqaro_m->kochalar->tbl_kocha_nomi; ?>
    <?php if (isset($fuqaro_m->uy_raqami))echo CHtml::encode($fuqaro_m->uy_raqami); ?>
    <?php echo Yii::t('strings','{table} "{record}" (id={id})', array(
	'{table}'=>Yii::t('strings','хонадон аъзоларининг даромади ва пенсия олиши хақидаги маълумотларни тахрирлаш ойнаси'),
	'{record}'=>$model->id,
	'{id}'=>$model->id));?></h1>
<?php $this->renderPartial('_form_upd', array('model'=>$model,'fid'=>$fid)); ?>