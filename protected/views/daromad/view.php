<?php
/* @var $this DaromadController */
/* @var $model Daromad */

$this->breadcrumbs=array(
	Yii::t('strings','Оила даромадлари')=>array('index'),
	$model->id,
);

$this->menu=array(
#	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Daromad'))), 'url'=>array('index')),
#	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Daromad'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Daromad'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Daromad'))), 'url'=>array('admin')),
#   array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Даромад'))), 'url'=>array('update', 'id'=>$model->id)),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон маълумотлари'))), 'url'=>array("/fuqaro_Uy/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон аъзолари'))), 'url'=>array("/fuqarolar?fuq=$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Даромад хақидаги маълумотларини ўзгартириш'))), 'url'=>array('update', 'fid'=>$model->fuqaro_uy_id)),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Қарздоллик'))), 'url'=>array("/qarz/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт қатнови'))), 'url'=>array("/transport_qatnovi/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт воситалари'))), 'url'=>array("/xonadon_transport/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ижтимоий мухит'))), 'url'=>array("/ijtimoiy_muhit/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Шарт-шароити'))), 'url'=>array("/shaoriti/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Томорқа'))), 'url'=>array("/tomorqa/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Чорва хайвонлари'))), 'url'=>array("/hayvon/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хизмат кўрсатиш объектлари'))), 'url'=>array("/ximzat_korsatish/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчанинг холати'))), 'url'=>array("/kocha_xolati/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Барча маълумотларни кўриш'))), 'url'=>array('/jamlama/index', 'id'=>$model->fuqaro_uy_id)),

);
$kk=array('1'=>'Туради','2'=>'Турмайди');
$fuqaro_m = Fuqaro_Uy::model()->find("id=$model->fuqaro_uy_id");
?>

<h1><?php if (isset($fuqaro_m->ftuman->tuman_nomi))echo $fuqaro_m->ftuman->tuman_nomi; ?>
    <?php if (isset($fuqaro_m->mfy->mfy_nomi))	echo $fuqaro_m->mfy->mfy_nomi;	?>
    <?php if (isset($fuqaro_m->kochalar->tbl_kocha_nomi))echo $fuqaro_m->kochalar->tbl_kocha_nomi; ?>
    <?php if (isset($fuqaro_m->uy_raqami))echo CHtml::encode($fuqaro_m->uy_raqami); ?>
    <?php echo Yii::t('strings','хонадон аъзоларининг даромади ва пенсия олиши хақидаги маълумотлар ойнаси') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
			'label'=>$model->getAttributeLabel('kam_taminlangan_sifatida'),
			'type'=>'raw',
			'value'=>$kk[$model->kam_taminlangan_sifatida],
		),
		'kam_taminlangan_sifatida',
		'oylik_ortacha_daromadi',
		'ish_xaq_oluvchi_soni',
		'ish_haqi_summasi',
		'ish_xaqi_muddati',
		'pensiya_oluvchi_soni',
		'pensiya_summa',
		'pensiya_vaqti',
		'nogiron_nafaqa_soni',
		'nogiron_summasi',
		'nogiron_nafaqa_vaqti',
		'kamtaminlangan_nafaqachilar_soni',
		'kamtaminlangan_nafaqa_summasi',
		'kamtaminlangan_nafaqa_vaqti',
		'ikki_yoshgacha_nafaqa_soni',
		'ikki_yosh_nafaqa_summasi',
		'ikki_yosh_nafaqa_vaqti',
		'on_tort_yosh_nafaqa_soni',
		'on_tort_nafaqa_summasi',
		'on_tort_nafaqa_vaqti',
		'daromad_boshqa',
		'fuqaro_uy_id',
		'fuqaro_id',
        'user_id',
        'user_date',
/**
		array(
			'label'=>$model->getAttributeLabel('fuqaro_id'),
			'type'=>'raw',
			'value'=>$model->fuqaro->name,
		),
/**/
	),
)); ?>
