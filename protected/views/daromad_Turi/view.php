<?php
/* @var $this Daromad_TuriController */
/* @var $model Daromad_Turi */

$this->breadcrumbs=array(
	Yii::t('strings','Daromad  Turis')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Daromad_Turi'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Daromad_Turi'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Daromad_Turi'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Daromad_Turi'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Daromad_Turi'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Daromad_Turi') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nomi',
/**
		array(
			'label'=>$model->getAttributeLabel('nomi'),
			'type'=>'raw',
			'value'=>$model->n->name,
		),
/**/
	),
)); ?>
