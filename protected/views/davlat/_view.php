<?php
/* @var $this DavlatController */
/* @var $data Davlat */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('davlat_nomi')); ?>:</b>
	<?php echo CHtml::encode($data->davlat_nomi); ?>
	<br />


</div>