<?php
/* @var $this DavlatController */
/* @var $model Davlat */

$this->breadcrumbs=array(
	Yii::t('strings','Davlats')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Davlat'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Davlat'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Davlat')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>