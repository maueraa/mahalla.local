<?php
/* @var $this DavlatController */
/* @var $model Davlat */

$this->breadcrumbs=array(
	Yii::t('strings','Davlats')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Davlat'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Davlat'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Davlat'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Davlat'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Davlat'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Davlat') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'davlat_nomi',
/**
		array(
			'label'=>$model->getAttributeLabel('davlat_nomi'),
			'type'=>'raw',
			'value'=>$model->davlat_n->name,
		),
/**/
	),
)); ?>
