<?php
/* @var $this Deo_azosiController */
/* @var $data Deo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deo_xolati')); ?>:</b>
	<?php echo CHtml::encode($data->deo_xolati); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_date')); ?>:</b>
	<?php echo CHtml::encode($data->user_date); ?>
	<br />


</div>