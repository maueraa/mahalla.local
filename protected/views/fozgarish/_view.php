<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fuy_id')); ?>:</b>
	<?php echo CHtml::encode($data->fuy_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fuq_id')); ?>:</b>
	<?php echo CHtml::encode($data->fuq_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fuq_maydon_nomi')); ?>:</b>
	<?php echo CHtml::encode($data->fuq_maydon_nomi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fuq_maydon_ozgaruvchi')); ?>:</b>
	<?php echo CHtml::encode($data->fuq_maydon_ozgaruvchi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_date')); ?>:</b>
	<?php echo CHtml::encode($data->user_date); ?>
	<br />


</div>