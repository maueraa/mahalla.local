<?php
$this->breadcrumbs=array(
	'Fozgarishes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Fozgarish','url'=>array('index')),
	array('label'=>'Manage Fozgarish','url'=>array('admin')),
);
?>

<h1>Create Fozgarish</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>