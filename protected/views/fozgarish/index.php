<?php
$this->breadcrumbs=array(
	'Fozgarishes',
);

$this->menu=array(
	array('label'=>'Create Fozgarish','url'=>array('create')),
	array('label'=>'Manage Fozgarish','url'=>array('admin')),
);
?>

<h1>Fozgarishes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
