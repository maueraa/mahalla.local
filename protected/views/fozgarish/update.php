<?php
$this->breadcrumbs=array(
	'Fozgarishes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Fozgarish','url'=>array('index')),
	array('label'=>'Create Fozgarish','url'=>array('create')),
	array('label'=>'View Fozgarish','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Fozgarish','url'=>array('admin')),
);
?>

<h1>Update Fozgarish <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>