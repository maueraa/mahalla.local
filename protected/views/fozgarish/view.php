<?php
$this->breadcrumbs=array(
	'Fozgarishes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Fozgarish','url'=>array('index')),
	array('label'=>'Create Fozgarish','url'=>array('create')),
	array('label'=>'Update Fozgarish','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Fozgarish','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Fozgarish','url'=>array('admin')),
);
?>

<h1>View Fozgarish #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'fuy_id',
		'fuq_id',
		'fuq_maydon_nomi',
		'fuq_maydon_ozgaruvchi',
		'user_id',
		'user_date',
	),
)); ?>
