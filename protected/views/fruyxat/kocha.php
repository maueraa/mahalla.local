﻿<?php
/* @var $this FuqaroController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Фуқаролар'),
);

$this->menu=array(
	//array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Янги фуқаро қиритиш'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Фуқарони қидириш'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{kocha}сидаги фуқаролар хақида маълумот', array('{kocha}'=>$kocha))?></h1>

<h3>Сариқ ранг билан белгиланган фуқаролар 16 ёшга тўлмаган ёки паспорт маълумотлари киритилмаган</h3>

<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'itemsTagName'=>'table',
    'itemsCssClass'=>'table table-bordered table-striped',
    'itemsHeader'=>"
	<tr>
	<th>ID рақами</th>
	<th>Фамалия исм шариф</th>
	<th>Жинси</th>
	<th>Туғилган санаси</th>
	<th>Яшаш манзили</th>
	<th>Пасспорт ёки метирка рақами</th>
	<th>Иш ёки ўқиш жойи </th>

	</tr>
	",
)); ?>
