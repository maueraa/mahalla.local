<?php
/* @var $this FuqaroController */
/* @var $model Fuqaro */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'fuqaro-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    'errorMessageCssClass'=>'alert alert-danger',
));
CHtml::$errorCss='alert alert-danger';
CHtml::$errorMessageCss='alert alert-danger';
?>

<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

<?php echo $form->errorSummary($model); ?>

<div class="row">
    <?php echo $form->labelEx($model,'fio'); ?>
    <?php echo $form->textField($model,'fio',array('size'=>60,'maxlength'=>255)); ?>
    <?php echo $form->error($model,'fio'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'jinsi_id'); ?>
    <?php echo $form->dropDownList($model,'jinsi_id',$model->getjinsi(),array('prompt'=>Yii::t("strings",'Жинси танланг'))); ?>
    <?php echo $form->error($model,'jinsi_id'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'millati_id'); ?>
    <?php echo $form->dropDownList($model,'millati_id',$model->getmillati()); ?>
    <?php echo $form->error($model,'millati_id'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'tugilgan_sanasi'); ?>
    <?php echo $form->textField($model,'tugilgan_sanasi'); ?>
    <?php echo $form->error($model,'tugilgan_sanasi'); ?>
</div>

<div class="row">
    <?php #echo $form->labelEx($model,'rasm'); ?>
    <?php# echo $form->fileField($model,'rasm',array('size'=>60,'maxlength'=>255)); ?>
    <?php #echo $form->error($model,'rasm'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'davlat_id'); ?>
    <?php echo $form->dropDownList($model,'davlat_id',$model->getdavlat(),array('prompt'=>Yii::t("strings",'Давлатни танланг'))); ?>
    <?php echo $form->error($model,'davlat_id'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'tugilgan_joy_viloyat_id'); ?>
    <?php echo $form->dropDownList($model,'tugilgan_joy_viloyat_id',$model->getjoyvil(), array('id'=>'id_province','prompt'=>Yii::t("strings",'Вилоятни танланг'))); ?>
    <?php echo $form->error($model,'tugilgan_joy_viloyat_id'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'tugilgan_joy_tuman_id'); ?>
    <?php echo $form->dropDownList($model,'tugilgan_joy_tuman_id',CHtml::listData(Tuman::model()->findAll("viloyat_id like '$model->tugilgan_joy_viloyat_id'"),'id','tuman_nomi'),array('id'=>'id_tuman','prompt'=>Yii::t("strings",'Туманни танланг'))); ?>
    <?php echo $form->error($model,'tugilgan_joy_tuman_id'); ?>
</div>
<?php ECascadeDropDown::master('id_province')->setDependent('id_tuman',array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro/loadtuman');?>

<div class="row">
    <?php echo $form->labelEx($model,'passport_seyiya'); ?>
    <?php echo $form->textField($model,'passport_seyiya',array('size'=>11,'maxlength'=>11)); ?>
    <?php echo $form->error($model,'passport_seyiya'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'passport_raqami'); ?>
    <?php echo $form->textField($model,'passport_raqami',array('size'=>60,'maxlength'=>255)); ?>
    <?php echo $form->error($model,'passport_raqami'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'passport_vaqti'); ?>
    <?php echo $form->textField($model,'passport_vaqti',array('size'=>60,'maxlength'=>255)); ?>
    <?php echo $form->error($model,'passport_vaqti'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'passport_kim_tomonidan_id'); ?>
    <?php echo $form->dropDownList($model,'passport_kim_tomonidan_id',$model->getjoytumanpas(),array('prompt'=>Yii::t("strings",'Туманни танланг'))); ?>
    <?php echo $form->error($model,'passport_kim_tomonidan_id'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'metirka_raqami'); ?>
    <?php echo $form->textField($model,'metirka_raqami',array('size'=>60,'maxlength'=>255)); ?>
    <?php echo $form->error($model,'metirka_raqami'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'metirka_kim_tomonidan'); ?>
    <?php echo $form->textField($model,'metirka_kim_tomonidan',array('size'=>60,'maxlength'=>255)); ?>
    <?php echo $form->error($model,'metirka_kim_tomonidan'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'nikox_turi'); ?>
    <?php echo $form->dropdownlist($model,'nikox_turi',array('1'=>'Никохланмаган','2'=>'қонуний', '3'=>'шаръий'),array('prompt'=>Yii::t('strings',"Никох турини танланг"))); ?>
    <?php echo $form->error($model,'nikox_turi'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'nikox_guvohnom_raqami'); ?>
    <?php echo $form->textField($model,'nikox_guvohnom_raqami',array('size'=>60,'maxlength'=>255)); ?>
    <?php echo $form->error($model,'nikox_guvohnom_raqami'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'ish_joy_nomi'); ?>
    <?php echo $form->textField($model,'ish_joy_nomi',array('size'=>60,'maxlength'=>255)); ?>
    <?php echo $form->error($model,'ish_joy_nomi'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'lavozimi'); ?>
    <?php echo $form->textField($model,'lavozimi',array('size'=>60,'maxlength'=>255)); ?>
    <?php echo $form->error($model,'lavozimi'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'daromad_turi_id'); ?>
    <?php echo $form->dropDownList($model,'daromad_turi_id',$model->getdaromad_turi(),array('prompt'=>Yii::t("strings",'Даромад турини белгиланг'))); ?>
    <?php echo $form->error($model,'daromad_turi_id'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'daromad_miqdori'); ?>
    <?php echo $form->textField($model,'daromad_miqdori',array('size'=>60,'maxlength'=>255)); ?>
    <?php echo $form->error($model,'daromad_miqdori'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'daromad_olish_sanasi'); ?>
    <?php echo $form->textField($model,'daromad_olish_sanasi',array('size'=>60,'maxlength'=>255)); ?>
    <?php echo $form->error($model,'daromad_olish_sanasi'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'uzoq_muddatga_ketgan_vaqti'); ?>
    <?php echo $form->textField($model,'uzoq_muddatga_ketgan_vaqti'); ?>
    <?php echo $form->error($model,'uzoq_muddatga_ketgan_vaqti'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'uzoq_muddatga_kelgan_vaqti'); ?>
    <?php echo $form->textField($model,'uzoq_muddatga_kelgan_vaqti'); ?>
    <?php echo $form->error($model,'uzoq_muddatga_kelgan_vaqti'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'uzoq_muddatga_ketgan_davlati'); ?>
    <?php echo $form->dropdownlist($model,'uzoq_muddatga_ketgan_davlati',$model->getdavlat(),array('prompt'=>Yii::t("strings",'Давлатни танланг'))); ?>
    <?php echo $form->error($model,'uzoq_muddatga_ketgan_davlati'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'uzoq_muddat_ish'); ?>
    <?php echo $form->textField($model,'uzoq_muddat_ish',array('size'=>60,'maxlength'=>255)); ?>
    <?php echo $form->error($model,'uzoq_muddat_ish'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'uzoq_muddat_suhbat_otgani'); ?>
    <?php echo $form->textField($model,'uzoq_muddat_suhbat_otgani',array('size'=>60,'maxlength'=>255)); ?>
    <?php echo $form->error($model,'uzoq_muddat_suhbat_otgani'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'malumoti_id'); ?>
    <?php echo $form->dropdownlist($model,'malumoti_id',$model->getmalumoti(),array('prompt'=>Yii::t("strings",'Маълумотини киритинг'))); ?>
    <?php echo $form->error($model,'malumoti_id'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'bitirgan_joy_id'); ?>
    <?php echo $form->textField($model,'bitirgan_joy_id'); ?>
    <?php echo $form->error($model,'bitirgan_joy_id'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'bitirgan_mutaxasisligi'); ?>
    <?php echo $form->textField($model,'bitirgan_mutaxasisligi',array('size'=>60,'maxlength'=>255)); ?>
    <?php echo $form->error($model,'bitirgan_mutaxasisligi'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'bitirgan_yili'); ?>
    <?php echo $form->textField($model,'bitirgan_yili',array('size'=>60,'maxlength'=>255)); ?>
    <?php echo $form->error($model,'bitirgan_yili'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'aliment_id'); ?>
    <?php echo $form->dropdownlist($model,'aliment_id',$model->getalimenti(),array('prompt'=>Yii::t("strings",'Алиментини бор ёки йўқлигини танланг'))); ?>
    <?php echo $form->error($model,'aliment_id'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'aliment_qarz'); ?>
    <?php echo $form->textField($model,'aliment_qarz',array('size'=>60,'maxlength'=>255)); ?>
    <?php echo $form->error($model,'aliment_qarz'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'notinch_oila_id'); ?>
    <?php echo $form->dropdownlist($model,'notinch_oila_id', $model->getnotinch(),array('prompt'=>Yii::t("strings",'Оиланинг холатини белгиланг'))); ?>
    <?php echo $form->error($model,'notinch_oila'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'sudlanganligi'); ?>
    <?php echo $form->textArea($model,'sudlanganligi',array('rows'=>6, 'cols'=>50)); ?>
    <?php echo $form->error($model,'sudlanganligi'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'iib_xisobida_turadimi'); ?>
    <?php echo $form->dropdownlist($model,'iib_xisobida_turadimi',$model->getiib(),array('prompt'=>Yii::t("strings",'ИИБ хисобида туришини белгиланг'))); ?>
    <?php echo $form->error($model,'iib_xisobida_turadimi'); ?>
</div>
<div class="row">
    <?php  echo $form->labelEx($model,'deo_azoligi_id'); ?>
    <?php echo $form->dropDownList($model,'deo_azoligi_id',$model->getdeoazo(),array('prompt'=>Yii::t("strings",'ДЭО аъзолиги'))); ?>
    <?php echo $form->error($model,'deo_azoligi_id'); ?>
</div>
<div class="row">
    <?php  $form->labelEx($model,'fuqaro_uy_id'); ?>
    <?php echo $form->hiddenField($model,'fuqaro_uy_id',array('value'=>$fid)); ?>
    <?php echo $form->error($model,'fuqaro_uy_id'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'kassalik_id'); ?>
    <?php echo $form->dropdownlist($model,'kassalik_id',$model->getkasallik(),array('prompt'=>Yii::t("strings",'Касаллик турини белгиланг'))); ?>
    <?php echo $form->error($model,'kassalik_id'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'kassalik_xolati'); ?>
    <?php echo $form->dropdownlist($model,'kassalik_xolati',array('0'=>'Соғлом','1'=>'Даволанишга мухтож', '2'=>'Оғир касал'),array('prompt'=>Yii::t("strings",'Касаллик холатини кўрсатинг'))); ?>
    <?php echo $form->error($model,'kassalik_xolati'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'sogligi'); ?>
    <?php echo $form->textField($model,'sogligi',array('size'=>60,'maxlength'=>255)); ?>
    <?php echo $form->error($model,'sogligi'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'oila_boshligi'); ?>
    <?php echo $form->dropdownlist($model,'oila_boshligi',array('0'=>'Оила бошлиғи эмас', '1'=>'Оила бошлиғи'),array('prompt'=>Yii::t("strings",'Оила аъзолигини белгиланг'))); ?>
    <?php echo $form->error($model,'oila_boshligi'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'user_id').Yii::app()->user->name; ?>
    <?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
    <?php echo $form->error($model,'user_id'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'user_date').date("d.m.Y"); ?>
    <?php echo $form->hiddenField($model,'user_date',array('value'=>date("Y-m-d"))); ?>
    <?php echo $form->error($model,'user_date'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'otasi_id'); ?>
    <?php echo $form->dropdownlist($model,'otasi_id',$model->getota($fid),array('prompt'=>Yii::t("strings",'Отасини танланг'))); ?>
    <?php echo $form->error($model,'otasi_id'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'onasi_id'); ?>
    <?php echo $form->dropdownlist($model,'onasi_id',$model->getona($fid),array('prompt'=>Yii::t("strings",'Онасини танланг'))); ?>
    <?php echo $form->error($model,'onasi_id'); ?>
</div>
<div class="row buttons">
    <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array("class"=>"btn btn-primary")); ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->