﻿<?php
/* @var $this FuqaroController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Fuqaros'),
);

$this->menu=array(
	//array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Янги фуқаро қиритиш'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Фуқарони қидириш'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Махаллада яшовчи фуқаролар рўйхати')?></h1>

<h3>Сариқ ранг билан белгиланган фуқаролар 16 ёшга тўлмаган ёки пасспорт маълумотлари киритилмаган</h3>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'itemsTagName'=>'table',
    'itemsCssClass'=>'table table-bordered table-striped',
    'itemsHeader'=>"
	<thead><tr>
	<th>ID рақами</th>
	<th>Фамалия исм шариф</th>
	<th>Жинси</th>
	<th>Туғилган санаси</th>
	<th>Вилоят</th>
	<th>Туман, МФЙ, кўча ва уй рақами</th>
	<th>Пасспорт рақами</th>

	</tr></thead>
	",
)); ?>
