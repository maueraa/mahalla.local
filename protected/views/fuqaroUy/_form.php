<?php
/* @var $this FuqaroUyController */
/* @var $model FuqaroUy */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fuqaro-uy-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'fviloyat_id'); ?>
		<?php echo $form->textField($model,'fviloyat_id'); ?>
		<?php echo $form->error($model,'fviloyat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ftuman_id'); ?>
		<?php echo $form->textField($model,'ftuman_id'); ?>
		<?php echo $form->error($model,'ftuman_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mfy_id'); ?>
		<?php echo $form->textField($model,'mfy_id'); ?>
		<?php echo $form->error($model,'mfy_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kocha_id'); ?>
		<?php echo $form->textField($model,'kocha_id'); ?>
		<?php echo $form->error($model,'kocha_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'uy_raqami'); ?>
		<?php echo $form->textField($model,'uy_raqami',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'uy_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kadastr_mavjudligi'); ?>
		<?php echo $form->textField($model,'kadastr_mavjudligi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'kadastr_mavjudligi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kadastr_seriyasi_raqami'); ?>
		<?php echo $form->textField($model,'kadastr_seriyasi_raqami',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'kadastr_seriyasi_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kadastr_buyruq_raqami'); ?>
		<?php echo $form->textField($model,'kadastr_buyruq_raqami',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'kadastr_buyruq_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ijara'); ?>
		<?php echo $form->textField($model,'ijara',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ijara'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'azo_soni'); ?>
		<?php echo $form->textField($model,'azo_soni'); ?>
		<?php echo $form->error($model,'azo_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jon_soni'); ?>
		<?php echo $form->textField($model,'jon_soni'); ?>
		<?php echo $form->error($model,'jon_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'voya_yetmagan_soni'); ?>
		<?php echo $form->textField($model,'voya_yetmagan_soni'); ?>
		<?php echo $form->error($model,'voya_yetmagan_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'taklif'); ?>
		<?php echo $form->textField($model,'taklif',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'taklif'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'muammosi'); ?>
		<?php echo $form->textField($model,'muammosi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'muammosi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ishchi_guruh_fio_id'); ?>
		<?php echo $form->textField($model,'ishchi_guruh_fio_id'); ?>
		<?php echo $form->error($model,'ishchi_guruh_fio_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tel_raqami'); ?>
		<?php echo $form->textField($model,'tel_raqami',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'tel_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'anketa_sana'); ?>
		<?php echo $form->textField($model,'anketa_sana'); ?>
		<?php echo $form->error($model,'anketa_sana'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->