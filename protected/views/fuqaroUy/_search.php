<?php
/* @var $this FuqaroUyController */
/* @var $model FuqaroUy */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fviloyat_id'); ?>
		<?php echo $form->textField($model,'fviloyat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ftuman_id'); ?>
		<?php echo $form->textField($model,'ftuman_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mfy_id'); ?>
		<?php echo $form->textField($model,'mfy_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kocha_id'); ?>
		<?php echo $form->textField($model,'kocha_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uy_raqami'); ?>
		<?php echo $form->textField($model,'uy_raqami',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kadastr_mavjudligi'); ?>
		<?php echo $form->textField($model,'kadastr_mavjudligi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kadastr_seriyasi_raqami'); ?>
		<?php echo $form->textField($model,'kadastr_seriyasi_raqami',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kadastr_buyruq_raqami'); ?>
		<?php echo $form->textField($model,'kadastr_buyruq_raqami',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ijara'); ?>
		<?php echo $form->textField($model,'ijara',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'azo_soni'); ?>
		<?php echo $form->textField($model,'azo_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jon_soni'); ?>
		<?php echo $form->textField($model,'jon_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'voya_yetmagan_soni'); ?>
		<?php echo $form->textField($model,'voya_yetmagan_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'taklif'); ?>
		<?php echo $form->textField($model,'taklif',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'muammosi'); ?>
		<?php echo $form->textField($model,'muammosi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ishchi_guruh_fio_id'); ?>
		<?php echo $form->textField($model,'ishchi_guruh_fio_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tel_raqami'); ?>
		<?php echo $form->textField($model,'tel_raqami',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'anketa_sana'); ?>
		<?php echo $form->textField($model,'anketa_sana'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->