<?php
/* @var $this FuqaroUyController */
/* @var $data FuqaroUy */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fviloyat_id')); ?>:</b>
	<?php echo CHtml::encode($data->fviloyat_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ftuman_id')); ?>:</b>
	<?php echo CHtml::encode($data->ftuman_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mfy_id')); ?>:</b>
	<?php echo CHtml::encode($data->mfy_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kocha_id')); ?>:</b>
	<?php echo CHtml::encode($data->kocha_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uy_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->uy_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kadastr_mavjudligi')); ?>:</b>
	<?php echo CHtml::encode($data->kadastr_mavjudligi); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kadastr_seriyasi_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->kadastr_seriyasi_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kadastr_buyruq_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->kadastr_buyruq_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ijara')); ?>:</b>
	<?php echo CHtml::encode($data->ijara); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('azo_soni')); ?>:</b>
	<?php echo CHtml::encode($data->azo_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jon_soni')); ?>:</b>
	<?php echo CHtml::encode($data->jon_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('voya_yetmagan_soni')); ?>:</b>
	<?php echo CHtml::encode($data->voya_yetmagan_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('taklif')); ?>:</b>
	<?php echo CHtml::encode($data->taklif); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('muammosi')); ?>:</b>
	<?php echo CHtml::encode($data->muammosi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ishchi_guruh_fio_id')); ?>:</b>
	<?php echo CHtml::encode($data->ishchi_guruh_fio_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tel_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->tel_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anketa_sana')); ?>:</b>
	<?php echo CHtml::encode($data->anketa_sana); ?>
	<br />

	*/ ?>

</div>