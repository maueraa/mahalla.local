<?php
/* @var $this FuqaroUyController */
/* @var $model FuqaroUy */

$this->breadcrumbs=array(
	Yii::t('strings','Fuqaro Uys')=>array('index'),
	Yii::t('strings','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','FuqaroUy'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','FuqaroUy'))), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fuqaro-uy-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','FuqaroUy'))) ?></h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link(Yii::t('zii','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fuqaro-uy-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'fviloyat_id',
		'ftuman_id',
		'mfy_id',
		'kocha_id',
		'uy_raqami',
		/*
		'kadastr_mavjudligi',
		'kadastr_seriyasi_raqami',
		'kadastr_buyruq_raqami',
		'ijara',
		'azo_soni',
		'jon_soni',
		'voya_yetmagan_soni',
		'taklif',
		'muammosi',
		'ishchi_guruh_fio_id',
		'tel_raqami',
		'anketa_sana',
		//'anketa_s.name',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
