<?php
/* @var $this FuqaroUyController */
/* @var $model FuqaroUy */

$this->breadcrumbs=array(
	Yii::t('strings','Fuqaro Uys')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','FuqaroUy'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','FuqaroUy'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','FuqaroUy')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>