<?php
/* @var $this FuqaroUyController */
/* @var $model FuqaroUy */

$this->breadcrumbs=array(
	Yii::t('strings','Fuqaro Uys')=>array('index'),
	Yii::t('strings',$model->id)=>array('view','id'=>$model->id),
	Yii::t('strings','Ўзгартириш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','FuqaroUy'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','FuqaroUy'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','View {label}', array('{label}'=>Yii::t('strings','FuqaroUy'))), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','FuqaroUy'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Update {table} "{record}" (id={id})', array(
	'{table}'=>Yii::t('strings','FuqaroUy'),
	'{record}'=>$model->id,
	'{id}'=>$model->id));?></h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>