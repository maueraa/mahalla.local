<?php
/* @var $this Fuqaro_MuammolarController */
/* @var $model Fuqaro_Muammolar */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'fuqaro--muammolar-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'viloyat_id'); ?>
		<?php echo $form->dropDownList($model,'viloyat_id',$model->getfviloyat(),array('id'=>'id_province','prompt'=>Yii::t("strings",'Вилоятни танланг'))); ?>
		<?php echo $form->error($model,'viloyat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tuman_id'); ?>
		<?php echo $form->dropDownList($model,'tuman_id',array(),array('id'=>'id_tuman','prompt'=>Yii::t("strings",'Туманни танланг'))); ?>
		<?php echo $form->error($model,'tuman_id'); ?>
	</div>
    <?php
    ECascadeDropDown::master('id_province')->setDependent('id_tuman',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadtuman');
    ?>
	<div class="row">
		<?php echo $form->labelEx($model,'mfy_id'); ?>
		<?php echo $form->dropDownList($model,'mfy_id',array(),array('id'=>'id_mfy','prompt'=>Yii::t("strings",'МФЙ танланг'))); ?>
		<?php echo $form->error($model,'mfy_id'); ?>
	</div>
    <?php
    ECascadeDropDown::master('id_tuman')->setDependent('id_mfy',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadmfy');
    ?>
	<div class="row">
		<?php echo $form->labelEx($model,'kocha_id'); ?>
		<?php echo $form->dropDownList($model,'kocha_id',array(),array('id'=>'id_kocha','prompt'=>Yii::t("strings",'Кўчани танланг'))); ?>
		<?php echo $form->error($model,'kocha_id'); ?>
	</div>
    <?php
    ECascadeDropDown::master('id_mfy')->setDependent('id_kocha',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadkocha');
    ?>
    <div class="row">
        <?php echo $form->labelEx($model,'fuqaro_uy_id'); ?>
        <?php echo $form->dropDownList($model,'fuqaro_uy_id',array(),array('id'=>'id_fuqaro_uy','prompt'=>Yii::t("strings",'Уйни танланг'))); ?>
        <?php echo $form->error($model,'fuqaro_uy_id'); ?>
    </div>
    <?php
    ECascadeDropDown::master('id_kocha')->setDependent('id_fuqaro_uy',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadfuqaro_uy');
    ?>

    <div class="row">
        <?php echo $form->labelEx($model,'fuqaro_id'); ?>
        <?php echo $form->dropDownList($model,'fuqaro_id',array(),array('id'=>'id_fuqaro','prompt'=>Yii::t("strings",'Фуқарони танланг'))); ?>
        <?php echo $form->error($model,'fuqaro_id'); ?>
    </div>
    <?php
    ECascadeDropDown::master('id_fuqaro_uy')->setDependent('id_fuqaro',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadfuqaro');
    ?>

	<div class="row">
		<?php echo $form->labelEx($model,'muammo_mazmuni'); ?>
		<?php echo $form->textArea($model,'muammo_mazmuni',array('size'=>60,'maxlength'=>5000, 'style'=>'width:500px; height:200px')); ?>
		<?php echo $form->error($model,'muammo_mazmuni'); ?>
	</div>


    <div class="row">
        <?php echo $form->labelEx($model,'viloyat_id'); ?>
        <?php echo CHtml::DropDownList('m8643','',CHtml::listData(Viloyatlar::model()->findAll(),'id','viloyat_nomi'),array('id'=>'id_vil2','prompt'=>Yii::t("strings",'Вилоятни танланг')))  ?>
    </div>
    <?php
    ECascadeDropDown::master('id_vil2')->setDependent('id_tuman2',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadtuman');
    ?>
	<div class="row">
        <?php echo $form->labelEx($model,'tuman_id'); ?>
        <?php echo CHtml::DropDownList('m3492','',CHtml::listData(Tuman::model()->findAll(),'id','tuman_nomi'),array('id'=>'id_tuman2','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    </div>
        <div class="row">

		<?php echo $form->labelEx($model,'tashkilot_id'); ?>
		<?php echo $form->dropDownList($model,'tashkilot_id',array(),array('id'=>'id_tashkilot','prompt'=>Yii::t("strings",'Ташкилотни танланг'))); ?>
		<?php echo $form->error($model,'tashkilot_id'); ?>
	</div>
    <?php
    ECascadeDropDown::master('id_tuman2')->setDependent('id_tashkilot',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadtashkilot');
    ?>
	<div class="row">
		<?php echo $form->labelEx($model,'qayd_sana').":".date("d.m.Y"); ?>
		<?php echo $form->hiddenField($model,'qayd_sana',array('value'=>date("Y-m-d H:i:s"))); ?>
		<?php echo $form->error($model,'qayd_sana'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'muddat_sana'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'name' => 'muddat_sana',
        'model' => $model,
        'attribute' => 'muddat_sana',
        'language' => 'ru',
        'options' => array(
            'showAnim' => 'fold',
        ),
        'htmlOptions' => array(
            'style' => 'height:20px;'
        ),
    ));?>

        <?php echo $form->error($model,'muddat_sana'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'user_id').Yii::app()->user->name;; ?>
		<?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array("class"=>"btn btn-primary")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->