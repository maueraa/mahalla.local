<?php
/* @var $this Fuqaro_MuammolarController */
/* @var $model Fuqaro_Muammolar */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'fuqaro--muammolar-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

    <p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'viloyat_id'); ?>
        <?php echo $model->viloyat->viloyat_nomi; ?>
        <?php echo $form->error($model,'viloyat_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'tuman_id'); ?>
        <?php echo $model->tuman->tuman_nomi; ?>
        <?php echo $form->error($model,'tuman_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'mfy_id'); ?>
        <?php echo $model->mfy->mfy_nomi; ?>
        <?php echo $form->error($model,'mfy_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'kocha_id'); ?>
        <?php echo $model->kocha->tbl_kocha_nomi; ?>
        <?php echo $form->error($model,'kocha_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'fuqaro_id'); ?>
        <?php echo $model->fuqaro->fio; ?>
        <?php echo $form->error($model,'fuqaro_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'muammo_mazmuni'); ?>
        <?php echo $model->muammo_mazmuni; ?>
        <?php echo $form->error($model,'muammo_mazmuni'); ?>
    </div>

    <div class="row">

        <?php $tash=Tashkilot::model()->findByPk(Yii::app()->user->tashkilot_id);
        echo $form->labelEx($model,'tashkilot_id')?>
        <?php echo $model->viloyat->viloyat_nomi; ?>
        <?php echo $model->tuman->tuman_nomi; ?>
        <?php echo $tash->tnomi; ?>
        <?php echo $form->hiddenField($model,'tashkilot_id',array('value'=>$tash->id)); ?>
        <?php echo $form->error($model,'tashkilot_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->hiddenField($model,'xal_etuvchi_fio',array('value'=>$tash->trahbari)); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'xulosa'); ?>
        <?php echo $form->textArea($model,'xulosa',array('size'=>60,'maxlength'=>5000,'style'=>'width:500px; height:300px')); ?>
        <?php echo $form->error($model,'xulosa'); ?>

    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'qayd_sana').date("d.m.Y"); ?>
        <?php echo $form->hiddenField($model,'qayd_sana',array('value'=>date("Y-m-d"))); ?>
        <?php echo $form->error($model,'qayd_sana'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'muddat_sana'); ?>
        <?php echo $model->muddat_sana; ?>

        <?php echo $form->error($model,'muddat_sana'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'xal_etilgan_sana').date("d.m.Y"); ?>
        <?php echo $form->hiddenField($model,'xal_etilgan_sana',array('value'=>date("Y-m-d H:i:s"))); ?>
        <?php echo $form->error($model,'xal_etilgan_sana'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'user_id').Yii::app()->user->name; ?>
        <?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
        <?php echo $form->error($model,'user_id'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array("class"=>"btn btn-primary")); ?>
    </div>

    <?php $this->endWidget(); ?>
<form action="/fuqaro_Muammolar/yechim/<?=$model->id?>" method="post">

    <div class="row">
    <?php echo $form->hiddenField($model,'etiroz'); ?>
    <input type='hidden' name="Fuqaro_Muammolar[etiroz]" value="1">
    <?php echo CHtml::submitButton(Yii::t('strings',"Эътироз билдириш"),array('confirm'=>'Сиз хақиқатан муаммога эътироз билдирасизми'),array("class"=>"btn btn-warning"));?>

</form>

</div>

</div><!-- form -->