<?php
/* @var $this Fuqaro_MuammolarController */
/* @var $model Fuqaro_Muammolar */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'viloyat_id'); ?>
		<?php echo $form->textField($model,'viloyat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tuman_id'); ?>
		<?php echo $form->textField($model,'tuman_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mfy_id'); ?>
		<?php echo $form->textField($model,'mfy_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kocha_id'); ?>
		<?php echo $form->textField($model,'kocha_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'muammo_mazmuni'); ?>
		<?php echo $form->textField($model,'muammo_mazmuni',array('size'=>60,'maxlength'=>5000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'muammo_xolati_id'); ?>
		<?php echo $form->textField($model,'muammo_xolati_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xulosa'); ?>
		<?php echo $form->textField($model,'xulosa',array('size'=>60,'maxlength'=>5000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fuqaro_uy_id'); ?>
		<?php echo $form->textField($model,'fuqaro_uy_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fuqaro_id'); ?>
		<?php echo $form->textField($model,'fuqaro_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tashkilot_id'); ?>
		<?php echo $form->textField($model,'tashkilot_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qayd_sana'); ?>
		<?php echo $form->textField($model,'qayd_sana'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'muddat_sana'); ?>
		<?php echo $form->textField($model,'muddat_sana'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'korilgan_sana'); ?>
		<?php echo $form->textField($model,'korilgan_sana'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xal_etilgan_sana'); ?>
		<?php echo $form->textField($model,'xal_etilgan_sana'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
	</div>
    <div class="row">
        <?php echo $form->label($model,'xal_etuvchi_fio'); ?>
        <?php echo $form->textField($model,'xal_etuvchi_fio',array('size'=>30,'maxlength'=>30)); ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'etiroz'); ?>
        <?php echo $form->textField($model,'etiroz'); ?>
    </div>

    <div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->