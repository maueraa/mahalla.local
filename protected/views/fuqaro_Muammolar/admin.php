<?php
/* @var $this Fuqaro_MuammolarController */
/* @var $model Fuqaro_Muammolar */

$this->breadcrumbs=array(
	Yii::t('strings','Fuqaro  Muammolars')=>array('index'),
	Yii::t('strings','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Fuqaro_Muammolar'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Fuqaro_Muammolar'))), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fuqaro--muammolar-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Fuqaro_Muammolar'))) ?></h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link(Yii::t('zii','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fuqaro--muammolar-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'viloyat_id',
		'tuman_id',
		'mfy_id',
		'kocha_id',
		'muammo_mazmuni',
		/*
		'muammo_xolati_id',
		'xulosa',
		'fuqaro_uy_id',
		'fuqaro_id',
		'tashkilot_id',
		'qayd_sana',
		'muddat_sana',
		'korilgan_sana',
		'xal_etilgan_sana',
		'user_id',
		'xal_etuvchi_fio',
		'etiroz',
		//'user.name',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
