<?php
/* @var $this Fuqaro_MuammolarController */
/* @var $model Fuqaro_Muammolar */

$this->breadcrumbs=array(
	Yii::t('strings','Муаммолар')=>array('index'),
	Yii::t('strings','Киритиш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Fuqaro_Muammolar'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Fuqaro_Muammolar'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Фуқаронинг муаммоларини киритиш')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>