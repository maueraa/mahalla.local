<?php
/* @var $this Fuqaro_MuammolarController */
/* @var $model Fuqaro_Muammolar */

$this->breadcrumbs=array(
	Yii::t('strings','Фуқаро муаммолари')=>array('index'),
	Yii::t('strings',$model->fuqaro->fio)=>array('view','id'=>$model->id),
	Yii::t('strings','Ўзгартириш'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Fuqaro_Muammolar'))), 'url'=>array('index')),
#	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Fuqaro_Muammolar'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings','View {label}', array('{label}'=>Yii::t('strings','Fuqaro_Muammolar'))), 'url'=>array('view', 'id'=>$model->id)),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Fuqaro_Muammolar'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{record}{table}', array(
	'{table}'=>Yii::t('strings','нинг муаммосига жавоб қайтариш'),
	'{record}'=>$model->fuqaro->fio,
	'{id}'=>$model->id));?></h1>
<?php $this->renderPartial('_form_answer', array('model'=>$model)); ?>