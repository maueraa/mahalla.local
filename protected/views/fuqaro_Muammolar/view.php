<?php
/* @var $this Fuqaro_MuammolarController */
/* @var $model Fuqaro_Muammolar */

$this->breadcrumbs=array(
	Yii::t('strings','Фуқаро муаммолари')=>array('index'),
	$model->fuqaro->fio,
);

$this->menu=array(
#	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Fuqaro_Muammolar'))), 'url'=>array('index')),
#	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Fuqaro_Muammolar'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Fuqaro_Muammolar'))), 'url'=>array('update', 'id'=>$model->id)),
#	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Fuqaro_Muammolar'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Fuqaro_Muammolar'))), 'url'=>array('admin')),
);
?>

<h1 style="text-align: center; text-transform: uppercase; text-shadow: 1px 0px 1px;"><?php echo Yii::t('strings','Фуқаро') .' : '. Yii::t('strings',$model->fuqaro->fio).Yii::t('strings',' 	муаммоси'); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        array(
            'label'=>$model->getAttributeLabel('viloyat_id'),
            'type'=>'raw',
            'value'=>$model->viloyat->viloyat_nomi,
        ),
        array(
            'label'=>$model->getAttributeLabel('tuman_id'),
            'type'=>'raw',
            'value'=>$model->tuman->tuman_nomi,
        ),
        array(
            'label'=>$model->getAttributeLabel('mfy_id'),
            'type'=>'raw',
            'value'=>$model->mfy->mfy_nomi,
        ),
        array(
            'label'=>$model->getAttributeLabel('kocha_id'),
            'type'=>'raw',
            'value'=>$model->kocha->tbl_kocha_nomi,
        ),
		'muammo_mazmuni',
        array(
            'label'=>$model->getAttributeLabel('muammo_xolati_id'),
            'type'=>'raw',
            'value'=>isset($model->muammoXolati->xulosa)?$model->muammoXolati->xulosa:$model->muammo_xolati_id,
        ),
		'xulosa',
		array(
			'label'=>$model->getAttributeLabel('fuqaro_uy_id'),
			'type'=>'raw',
			'value'=>isset($model->fuqaroUy->uy_raqami)?$model->fuqaroUy->uy_raqami:"",
		),
		array(
			'label'=>$model->getAttributeLabel('fuqaro_id'),
			'type'=>'raw',
			'value'=>isset($model->fuqaro->fio)?$model->fuqaro->fio:$model->fuqaro->fio,
		),
		array(
			'label'=>$model->getAttributeLabel('tashkilot_id'),
			'type'=>'raw',
			'value'=>isset($model->tashkilot->tnomi)?$model->tashkilot->tnomi:"",
		),

		'qayd_sana',
		'muddat_sana',
		'korilgan_sana',
		'xal_etilgan_sana',
		array(
			'label'=>$model->getAttributeLabel('user_id'),
			'type'=>'raw',
			'value'=>isset($model->user->username)?$model->user->username:"",
		),
        'xal_etuvchi_fio',
        'etiroz'
/**
		array(
			'label'=>$model->getAttributeLabel('user_id'),
			'type'=>'raw',
			'value'=>$model->user->name,
		),
/**/
	),
)); ?>
