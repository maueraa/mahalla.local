<?php
/* @var $this Fuqaro_UyController */
/* @var $model Fuqaro_Uy */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'fuqaro--uy-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,

));
    ?>

	<p class="help-block"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'fviloyat_id'); ?>
        <?php echo $form->dropDownList($model,'fviloyat_id',$model->getfviloyat(),array('id'=>'id_province','prompt'=>Yii::t("strings",'Вилоятни танланг'))); ?>
        <?php echo $form->error($model,'fviloyat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ftuman_id'); ?>
        <?php echo $form->dropDownList($model,'ftuman_id',array(),array('id'=>'id_tuman','prompt'=>Yii::t("strings",'Туманни танланг'))); ?>
        <?php echo $form->error($model,'ftuman_id'); ?>
	</div>
    <?php
    ECascadeDropDown::master('id_province')->setDependent('id_tuman',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadtuman');
    ?>
	<div class="row">
		<?php echo $form->labelEx($model,'mfy_id'); ?>
        <?php echo $form->dropDownList($model,'mfy_id',array(),array('id'=>'id_mfy','prompt'=>Yii::t("strings",'МФЙ танланг'))); ?>
        <?php echo $form->error($model,'mfy_id'); ?>
    </div>
    <?php
    ECascadeDropDown::master('id_tuman')->setDependent('id_mfy',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadmfy');
    ?>
	<div class="row">
		<?php echo $form->labelEx($model,'kocha_id'); ?>
        <?php echo $form->dropDownList($model,'kocha_id',array(),array('id'=>'id_kocha','prompt'=>Yii::t("strings",'Кўчани танланг'))); ?>
		<?php echo $form->error($model,'kocha_id'); ?>
	</div>
    <?php
    ECascadeDropDown::master('id_mfy')->setDependent('id_kocha',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadkocha');
    ?>
	<div class="row">
		<?php echo Yii::t('strings','Уй рақами, агар уй рақамида қўшимча белги бўлса қўшимча белгисини пастки.<br> Уй рақамининг индекс майдонига ёзинг<br>'); ?>
		<?php echo$form->textField($model,'uy_raqami',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'uy_raqami'); ?>
	</div>
	<div class="row">
		<?php echo Yii::t('strings','Уй рақами индекси <br> (Бу майдонга уй рақамидан сўнг бирон бир сони ёки белги бўлса тўлдиринг,<br/> агар мавжуд бўлмаса тўлдириш шарт эмас: Масалан:34а бўлса майдонга фақат "а" харфи ёзилади)'); ?>
		<?php echo $form->textField($model,'uy_raqami_position',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'uy_raqami_position'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'kadastr_mavjudligi'); ?>
		<?php echo $form->dropDownList($model,'kadastr_mavjudligi',array('Bor'=>'Бор', "Yo'q"=>'Йўқ'),array('prompt'=>Yii::t("strings",'Кадастр мавжудлигини кўрсатинг'))); ?>
		<?php echo $form->error($model,'kadastr_mavjudligi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kadastr_seriyasi_raqami'); ?>
		<?php echo $form->textField($model,'kadastr_seriyasi_raqami',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'kadastr_seriyasi_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kadastr_buyruq_raqami'); ?>
		<?php echo $form->textField($model,'kadastr_buyruq_raqami',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'kadastr_buyruq_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ijara'); ?>
		<?php echo $form->textField($model,'ijara',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ijara'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'azo_soni'); ?>
		<?php echo $form->textField($model,'azo_soni'); ?>
		<?php echo $form->error($model,'azo_soni'); ?>
	</div>
<?php /*
	<div class="row">
		<?php echo $form->labelEx($model,'jon_soni'); ?>
		<?php echo $form->textField($model,'jon_soni'); ?>
		<?php echo $form->error($model,'jon_soni'); ?>
	</div>*/
?>
	<div class="row">
		<?php echo $form->labelEx($model,'voya_yetmagan_soni'); ?>
		<?php echo $form->textField($model,'voya_yetmagan_soni'); ?>
		<?php echo $form->error($model,'voya_yetmagan_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'taklif'); ?>
		<?php echo $form->textField($model,'taklif',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'taklif'); ?>
	</div>

	<div class="row">
		<?php #echo $form->labelEx($model,'muammosi'); ?>
		<?php #echo $form->textField($model,'muammosi',array('size'=>60,'maxlength'=>255)); ?>
		<?php #echo $form->error($model,'muammosi'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'oila_xolati'); ?>
        <?php echo $form->dropdownlist($model,'oila_xolati',$model->getnotinch2(),array('prompt'=>Yii::t("strings",'Оилавий холати'))); ?>
        <?php echo $form->error($model,'oila_xolati'); ?>
    </div>
    <div class="row">
	<?php echo $form->labelEx($model,'fviloyat_id'); ?>
	<?php echo CHtml::DropDownList('viloyat','',CHtml::listData(Viloyatlar::model()->findAll(),'id','viloyat_nomi'),array('id'=>'id_viloyat2','prompt'=>Yii::t("strings",'Вилоят танланг')))  ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'ftuman_id'); ?>
	<?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman2','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    </div>
	<div class="row">
		<?php echo $form->labelEx($model,'ishchi_guruh_fio_id'); ?>
		<?php echo $form->dropdownlist($model,'ishchi_guruh_fio_id',array(),array('id'=>'igf_id','prompt'=>Yii::t("strings",'Ташкилотни танланг'))); ?>
		<?php echo $form->error($model,'ishchi_guruh_fio_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ishchi_guruh_fio'); ?>
		<?php echo $form->dropDownlist($model,'ishchi_guruh_fio',array(),array('id'=>'id_fio','prompt'=>Yii::t("strings",'ФИШ танланг'))); ?>
		<?php echo $form->error($model,'ishchi_guruh_fio'); ?>
	</div>

	<?php
	ECascadeDropDown::master('id_viloyat2')->setDependent('id_tuman2',
		array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_uy/loadtuman');
	?>
	<?php
	ECascadeDropDown::master('id_tuman2')->setDependent('igf_id',
		array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_uy/loadigf');
	?>
	<?php
	ECascadeDropDown::master('igf_id')->setDependent('id_fio',
		array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_uy/loadigffio');
	?>
	<div class="row">
		<?php echo $form->labelEx($model,'tel_raqami'); ?>
		<?php echo $form->textField($model,'tel_raqami',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'tel_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'anketa_sana').date("d.m.Y"); ?>
		<?php echo $form->hiddenField($model,'anketa_sana',array('value'=>date("Y-m-d"))); ?>
		<?php echo $form->error($model,'anketa_sana'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'user_id').Yii::app()->user->username; ?>
        <?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
        <?php echo $form->error($model,'user_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'user_date').date("d.m.Y"); ?>
        <?php echo $form->hiddenField($model,'user_date',array('value'=>date("Y-m-d"))); ?>
        <?php echo $form->error($model,'user_date'); ?>
    </div>

	<div class="form-actions">
        <?php
        /*
        $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'type'=>'primary',
        'label'=>$model->isNewRecord ?'Қўшиш' :'Save',
        )); */?>
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array("class"=>"btn btn-primary")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->