﻿<?php
/* @var $this Fuqaro_UyController */
/* @var $data Fuqaro_Uy */

$uyegasi = Fuqaro::model()->find("fuqaro_uy_id=$data->id");

$kadastr=array('Bor'=>'Бор', "Yo'q"=>'Йўқ');
?>

<ul class="nav nav-tabs" id="myTab<?php  echo CHtml::encode($data->id); ?>">
    <li class="active"><a href="#home<?php  echo CHtml::encode($data->id); ?>" data-toggle="tab">Умумий маълумот</a></li>
    <li class=""><a href="#profile<?php  echo CHtml::encode($data->id); ?>" data-toggle="tab">Хонадон аъзолари</a></li>
    <li class=""><a href="#messages<?php  echo CHtml::encode($data->id); ?>" data-toggle="tab">Маълумотларни тахрирлаш</a></li>
    <li class=""><a href="#settings<?php  echo CHtml::encode($data->id); ?>" data-toggle="tab">Барча маълумотларни кўриш</a></li>
</ul>

<div class="tab-content">
    <div class="tab-pane active" id="home<?php  echo CHtml::encode($data->id); ?>">
        home<?php  echo CHtml::encode($data->id); ?>

    </div>
    <div class="tab-pane" id="profile<?php  echo CHtml::encode($data->id); ?>"><?php  echo CHtml::encode($data->id); ?>profile</div>
    <div class="tab-pane" id="messages<?php  echo CHtml::encode($data->id); ?>">messages<?php  echo CHtml::encode($data->id); ?></div>
    <div class="tab-pane" id="settings<?php  echo CHtml::encode($data->id); ?>">settings<?php  echo CHtml::encode($data->id); ?></div>
</div>

<script>
    $(function () {
        $('#myTab a:last').tab('show');
    })
</script>
<tr class="view"  colspan="6">
	<td>
        <?php #echo CHtml::link(CHtml::encode($data->id), array('/jamlama/index', 'id'=>$data->id)); ?>
        <?php echo CHtml::link(CHtml::encode($data->id), array("/fuqaro_Uy/$data->id")); ?>
    </td>
	<td>	<?php if (isset($uyegasi->fio))	echo $uyegasi->fio;	?></td>
	<td>
        <?php if (isset($data->mfy->mfy_nomi))echo $data->mfy->mfy_nomi; ?>
        <?php if (isset($data->kochalar->tbl_kocha_nomi))echo $data->kochalar->tbl_kocha_nomi; ?>
        <?php echo CHtml::encode($data->uy_raqami); ?>
    </td>
	<td>	<?php echo CHtml::encode($kadastr[$data->kadastr_mavjudligi]); ?></td>
	<td>	<?php echo  CHtml::link("Хонадон аъзолари", array("/fuqarolar?fuq=$data->id", ));  ?></td>
	<td>	<?php echo  CHtml::link("Янги фуқаро", array('/fuqaro/create', 'fid'=>$data->id));  ?></td>
	<td>
        <?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
        'collapsed'=>true,
        'legend'=>Yii::t('strings','Тахрирлаш'),
        'fieldsetHtmlOptions'=>array('id'=>"muslim$data->id"),
        'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
    ));
        ?>
        <div class="list-group">

            <?php echo  CHtml::link("Хонадон маълумотлари", array('/fuqaro_uy/update', 'id'=>$data->id),array("class"=>'list-group-item'));  ?>

            <?php echo  CHtml::link("Маънавий мухит", array('/manaviy/update', 'fid'=>$data->id),array("class"=>'list-group-item'));  ?>
        <?php echo  CHtml::link("Даромади", array('/daromad/update', 'fid'=>$data->id),array("class"=>'list-group-item'));  ?>
            <?php echo  CHtml::link("Қарздоллик", array('/qarz/update', 'fid'=>$data->id),array("class"=>'list-group-item'));  ?>
        <?php echo  CHtml::link("Транспорт қатнови", array('/transport_qatnovi/update', 'fid'=>$data->id),array("class"=>'list-group-item'));  ?>
            <?php echo  CHtml::link("Транспорт воситалари", array('/xonadon_transport/update', 'fid'=>$data->id),array("class"=>'list-group-item'));  ?>
            <?php echo  CHtml::link("Ижтимоий мухит", array('/ijtimoiy_muhit/update', 'fid'=>$data->id),array("class"=>'list-group-item'));  ?>
                <?php echo  CHtml::link("Шарт-шароити", array('/shaoriti/update', 'fid'=>$data->id),array("class"=>'list-group-item'));  ?>
                <?php echo  CHtml::link("Томорқа", array('/tomorqa/update', 'fid'=>$data->id),array("class"=>'list-group-item'));  ?>
               <?php echo  CHtml::link("Чорва хайвонлари", array('/hayvon/update', 'fid'=>$data->id),array("class"=>'list-group-item'));  ?>
                <?php echo  CHtml::link("Хизмат кўрсатиш объектлари", array('/ximzat_korsatish/update', 'fid'=>$data->id),array("class"=>'list-group-item'));  ?>
                <?php echo  CHtml::link("Кўчанинг холати", array('/kocha_xolati/update', 'fid'=>$data->id),array("class"=>'list-group-item'));  ?>
        <?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->
    </div>
    </td>
</tr>


<?php /*
<div class="taxrirlash">
	<h9>Хонадон маълумотларини тахрирлаш</h9>
		<br>
	<?php echo  CHtml::link("Хонадон маълумотлари", array('/fuqaro_uy/update', 'id'=>$data->id));  ?>
	<?php echo  CHtml::link("Маънавий мухит", array('/manaviy/update', 'fid'=>$data->id));  ?>
	<?php echo  CHtml::link("Даромади", array('/daromad/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Қарздоллик", array('/qarz/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Транспорт қатнови", array('/transport_qatnovi/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Транспорт воситалари", array('/xonadon_transport/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Ижтимоий мухит", array('/ijtimoiy_muhit/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Шарт-шароити", array('/shaoriti/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Томорқа", array('/tomorqa/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Чорва хайвонлари", array('/hayvon/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Хизмат кўрсатиш объектлари", array('/ximzat_korsatish/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Кўчанинг холати", array('/kocha_xolati/update', 'fid'=>$data->id));  ?>
	</div>
	<br />

    <?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kadastr_seriyasi_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->kadastr_seriyasi_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kadastr_buyruq_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->kadastr_buyruq_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ijara')); ?>:</b>
	<?php echo CHtml::encode($data->ijara); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('azo_soni')); ?>:</b>
	<?php echo CHtml::encode($data->azo_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jon_soni')); ?>:</b>
	<?php echo CHtml::encode($data->jon_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('voya_yetmagan_soni')); ?>:</b>
	<?php echo CHtml::encode($data->voya_yetmagan_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('taklif')); ?>:</b>
	<?php echo CHtml::encode($data->taklif); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('muammosi')); ?>:</b>
	<?php echo CHtml::encode($data->muammosi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ishchi_guruh_fio_id')); ?>:</b>
	<?php echo CHtml::encode($data->ishchi_guruh_fio_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tel_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->tel_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anketa_sana')); ?>:</b>
	<?php echo CHtml::encode($data->anketa_sana); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('uy_raqami_position')); ?>:</b>
	<?php echo CHtml::encode($data->uy_raqami_position); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_date')); ?>:</b>
	<?php echo CHtml::encode($data->user_date); ?>
	<br />


	*/ ?>

