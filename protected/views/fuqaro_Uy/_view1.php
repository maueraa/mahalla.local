﻿<?php
/* @var $this Fuqaro_UyController */
/* @var $data Fuqaro_Uy */

$uyegasi = Fuqaro::model()->find("fuqaro_uy_id=$data->id");
?>

<div class="view">
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('/jamlama/index', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo "Хонадон бошлиғи" ?>:</b>
	<?php if (isset($uyegasi->fio))
	echo $uyegasi->fio;

	?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('Вилоят')); ?>:</b>
	<?php if (isset($data->fviloyat->viloyat_nomi))
	echo $data->fviloyat->viloyat_nomi;

	?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Туман')); ?>:</b>
	<?php
	if (isset($data->ftuman->tuman_nomi))
		echo $data->ftuman->tuman_nomi;

		?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('МФЙ')); ?>:</b>
	<?php if (isset($data->mfy->mfy_nomi))
		echo $data->mfy->mfy_nomi; ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Кўча номи')); ?>:</b>
	<?php if (isset($data->kochalar->tbl_kocha_nomi))
		echo $data->kochalar->tbl_kocha_nomi; ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Уй рақами')); ?>:</b>
	<?php echo CHtml::encode($data->uy_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Кадастр хужжати')); ?>:</b>
	<?php echo CHtml::encode($data->kadastr_mavjudligi); ?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('Хонадонга янги фуқаро қўшиш')); ?>:</b>
	<?php echo  CHtml::link("Янги фуқаро", array('/fuqaro/create', 'fid'=>$data->id));  ?>
	<div class="taxrirlash">
	<h9>Хонадон маълумотларини тахрирлаш</h9>
		<br>
	<?php echo  CHtml::link("Хонадон маълумотлари", array('/fuqaro_uy/update', 'id'=>$data->id));  ?>
	<?php echo  CHtml::link("Маънавий мухит", array('/manaviy/update', 'fid'=>$data->id));  ?>
	<?php echo  CHtml::link("Даромади", array('/daromad/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Қарздоллик", array('/qarz/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Транспорт қатнови", array('/transport_qatnovi/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Транспорт воситалари", array('/xonadon_transport/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Ижтимоий мухит", array('/ijtimoiy_muhit/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Шарт-шароити", array('/shaoriti/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Томорқа", array('/tomorqa/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Чорва хайвонлари", array('/hayvon/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Хизмат кўрсатиш объектлари", array('/ximzat_korsatish/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Кўчанинг холати", array('/kocha_xolati/update', 'fid'=>$data->id));  ?>
	</div>
	<br />

    <?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kadastr_seriyasi_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->kadastr_seriyasi_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kadastr_buyruq_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->kadastr_buyruq_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ijara')); ?>:</b>
	<?php echo CHtml::encode($data->ijara); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('azo_soni')); ?>:</b>
	<?php echo CHtml::encode($data->azo_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jon_soni')); ?>:</b>
	<?php echo CHtml::encode($data->jon_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('voya_yetmagan_soni')); ?>:</b>
	<?php echo CHtml::encode($data->voya_yetmagan_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('taklif')); ?>:</b>
	<?php echo CHtml::encode($data->taklif); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('muammosi')); ?>:</b>
	<?php echo CHtml::encode($data->muammosi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ishchi_guruh_fio_id')); ?>:</b>
	<?php echo CHtml::encode($data->ishchi_guruh_fio_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tel_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->tel_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anketa_sana')); ?>:</b>
	<?php echo CHtml::encode($data->anketa_sana); ?>
	<br />

	*/ ?>

</div>