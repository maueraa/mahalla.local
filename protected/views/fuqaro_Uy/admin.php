﻿<?php
/* @var $this Fuqaro_UyController */
/* @var $model Fuqaro_Uy */

$this->breadcrumbs=array(
	Yii::t('strings','Хонадонлар')=>array('index'),
	Yii::t('strings','Қидириш'),
);

$this->menu=array(
	#array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','қидириш'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Янги хонадон киритиш'))), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('fuqaro--uy-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадонлари қидириш'))) ?></h1>
<div class="tugma">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
        Қўшимча маълумотлар орқали қидириш
    </button>
</div>

<div class="modal fade" id="myModal">
        <div class="modal-header">
           <button class="close" data-dismiss="modal">×</button>
           <h3>
               Қўшимча маълмуотлар орқали қидириш
           </h3>
       </div>
       <div class="modal-body">
           <?php $this->renderPartial('_search',array(
           'model'=>$model,
       ));
           #   $uyegasi = Fuqaro::model()->find("fuqaro_uy_id=$model->id");
           ?>

       </div>
       <div class="modal-footer">
           <a href="" class="btn btn-default" data-dismiss="modal">Close</a>
       </div>
</div>
<!--search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'fuqaro--uy-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		#array('name'=>'','header'=>'Хонадон болшилиғи', 'value'=>'$data->fuqaro->fio'),
		array('name'=>'fviloyat_id','header'=>'Вилоят', 'value'=>'$data->fviloyat->viloyat_nomi' ),
		array('name'=>'ftuman_id','header'=>'Туман', 'value'=>'$data->ftuman->tuman_nomi' ),
		array('name'=>'mfy_id','header'=>'МФЙ', 'value'=>'$data->mfy->mfy_nomi' ),
		array('name'=>'kocha_id','header'=>'Кўча номи', 'value'=>'$data->kochalar->tbl_kocha_nomi' ),
		array('name'=>'uy_raqami','header'=>'Уй рақами'),
        array('name'=>'kadastr_mavjudligi','header'=>'Кадастр'),
        array('name'=>'muammosi','header'=>'Муаммолилар'),
        array('name'=>'taklif','header'=>'Таклифлар'),
        array('name'=>'tel_raqami','header'=>'Телефон рақами'),

		/*
		'kadastr_mavjudligi',
		'kadastr_seriyasi_raqami',
		'kadastr_buyruq_raqami',
		'ijara',
		'azo_soni',
		'jon_soni',
		'voya_yetmagan_soni',
		'taklif',
		'muammosi',
		'oila_xolati',
		'ishchi_guruh_fio_id',
		'tel_raqami',
		'anketa_sana',
		'anketa_ozgarish',
		'ishchi_guruh_fio',
		'uy_raqami_position',
		'user_id',
		'user_date',
		//'anketa_s.name',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
