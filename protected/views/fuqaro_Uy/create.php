﻿<?php
/* @var $this Fuqaro_UyController */
/* @var $model Fuqaro_Uy */

$this->breadcrumbs=array(
	Yii::t('strings','Fuqaro  Uys')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадонлар рўйхати'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Хонадон қидириш'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Фуқаро уйининг маълумотларини қиритиш ойнаси')?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>