﻿<?php
/* @var $this Fuqaro_UyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Хонадонлар рўйхати'),
);
$this->menu=array(
	array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Хонадон қўшиш'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Хонадон қидириш'))), 'url'=>array('admin')),
);
?>
<h1>
    <?php if(isset($dataProvider->data[0]->mfy->mfy_nomi)) echo Yii::t('strings',$dataProvider->data[0]->mfy->mfy_nomi.' ');?>
    <?php if(isset($dataProvider->data[0]->kochalar->tbl_kocha_nomi))echo Yii::t('strings',$dataProvider->data[0]->kochalar->tbl_kocha_nomi.'да жойлашган хонадонлар')?>

</h1>

	<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'sortableAttributes'=>array('id','uy_raqami','anketa_sana'),
	'itemsTagName'=>'table',
    'itemsCssClass'=>'table table-bordered table-striped',
    'itemsHeader'=>"
	<thead><tr>
	<th>№</th>
	<th>Хонадон бошлиғи</th>
	<th>МФЙ, кўчанинг номи, уй рақами</th>
	<th>Кадастр мавжудлиги</th>
	<th>Хонадонда яшовчи фуқаролар</th>
	<th>Янги фуқаро қўшиш</th>
	<th>Маълумотларни тахрирлаш</th>
	</tr></thead>
	",
)); ?>





