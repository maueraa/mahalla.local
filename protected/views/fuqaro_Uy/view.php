<?php
/* @var $this Fuqaro_UyController */
/* @var $model Fuqaro_Uy */

$this->breadcrumbs=array(
	Yii::t('strings','Хонадонлар')=>array('#'),
	$model->id,
);

$this->menu=array(
	#array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Fuqaro_Uy'))), 'url'=>array('index')),
	#array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон киритиш'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон маълумотларни ўзгартириш'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон маълумотларни ўчириш'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон аъзолари'))), 'url'=>array("/fuqarolar?fuq=$model->id")),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Маънавий мухит'))), 'url'=>array("/manaviy/$model->id")),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Даромади'))), 'url'=>array("/daromad/$model->id")),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Қарздоллик'))), 'url'=>array("/qarz/$model->id")),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт қатнови'))), 'url'=>array("/transport_qatnovi/$model->id")),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт воситалари'))), 'url'=>array("/xonadon_transport/$model->id")),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ижтимоий мухит'))), 'url'=>array("/ijtimoiy_muhit/$model->id")),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Шарт-шароити'))), 'url'=>array("/shaoriti/$model->id")),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Томорқа'))), 'url'=>array("/tomorqa/$model->id")),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Чорва хайвонлари'))), 'url'=>array("/hayvon/$model->id")),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хизмат кўрсатиш объектлари'))), 'url'=>array("/ximzat_korsatish/$model->id")),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчанинг холати'))), 'url'=>array("/kocha_xolati/$model->id")),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Барча маълумотларни кўриш'))), 'url'=>array('/jamlama/index', 'id'=>$model->id)),
);
$fuq=Fuqaro::model()->find("fuqaro_uy_id = $model->id and oila_boshligi=1");
?>
<div id="yuklash"><b><i class="icon-download-alt"></i><?php echo CHtml::link(CHtml::encode('Хонадоннинг барча маълумотларини PDF орқали юклаб олиш'), array('/jamlama/index', 'id'=>$model->id)); ?></b></div>
<h1><?php 

if(isset($model->ftuman->tuman_nomi) ) echo Yii::t('strings',$model->ftuman->tuman_nomi);   if(isset($model->mfy->mfy_nomi)) echo Yii::t('strings',$model->mfy->mfy_nomi,'');
if(isset($model->kochalar->tbl_kocha_nomi)) echo Yii::t('strings',$model->kochalar->tbl_kocha_nomi);
if(isset($fuq->fio)) echo Yii::t('strings',$fuq->fio) .' ('. Yii::t('strings',$model->uy_raqami) .')  '.	Yii::t('strings','хонадонининг <br>маълумотларини ўзгартирилган холати');
	?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
			'label'=>$model->getAttributeLabel('fviloyat_id'),
			'type'=>'raw',
			'value'=>$model->fviloyat->viloyat_nomi,
		),
		array(
			'label'=>$model->getAttributeLabel('ftuman_id'),
			'type'=>'raw',
			'value'=>$model->ftuman->tuman_nomi,
		),
		array(
			'label'=>$model->getAttributeLabel('mfy_id'),
			'type'=>'raw',
			'value'=>$model->mfy->mfy_nomi,
		),
		array(
			'label'=>$model->getAttributeLabel('kocha_id'),
			'type'=>'raw',
			'value'=>$model->kochalar->tbl_kocha_nomi,
		),
		'uy_raqami',
        'uy_raqami_position',
		'kadastr_mavjudligi',
		'kadastr_seriyasi_raqami',
		'kadastr_buyruq_raqami',
		'ijara',
		'azo_soni',
		'voya_yetmagan_soni',
		'taklif',
		array(
			'label'=>$model->getAttributeLabel('oila_xolati'),
			'type'=>'raw',
			#'value'=>$model->oilaXolati->oila_xolati,
		),
		array(
			'label'=>$model->getAttributeLabel('ishchi_guruh_fio_id'),
			'type'=>'raw',
			'value'=>$model->ishchiGuruhFio->ishchi_guruh_fio,
		),
		'tel_raqami',
		'anketa_sana',
        'anketa_ozgarish',
		'ishchi_guruh_fio',
		array(
			'label'=>$model->getAttributeLabel('user_id'),
			'type'=>'raw',
			#'value'=>$model->user->username,
		),
        'user_date',

/**
		array(
			'label'=>$model->getAttributeLabel('anketa_sana'),
			'type'=>'raw',
			'value'=>$model->anketa_s->name,
		),
/**/
	),
)); ?>
