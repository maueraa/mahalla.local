<?php
/* @var $this Fuqaro_kasallarController */
/* @var $model Fuqaro */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fuqaro-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'fio'); ?>
		<?php echo $form->textField($model,'fio',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'fio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jinsi_id'); ?>
		<?php echo $form->textField($model,'jinsi_id'); ?>
		<?php echo $form->error($model,'jinsi_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tugilgan_sanasi'); ?>
		<?php echo $form->textField($model,'tugilgan_sanasi'); ?>
		<?php echo $form->error($model,'tugilgan_sanasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tugilgan_joy_viloyat_id'); ?>
		<?php echo $form->textField($model,'tugilgan_joy_viloyat_id'); ?>
		<?php echo $form->error($model,'tugilgan_joy_viloyat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tugilgan_joy_tuman_id'); ?>
		<?php echo $form->textField($model,'tugilgan_joy_tuman_id'); ?>
		<?php echo $form->error($model,'tugilgan_joy_tuman_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'passport_raqami'); ?>
		<?php echo $form->textField($model,'passport_raqami',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'passport_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'passport_vaqti'); ?>
		<?php echo $form->textField($model,'passport_vaqti',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'passport_vaqti'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'passport_kim_tomonidan_id'); ?>
		<?php echo $form->textField($model,'passport_kim_tomonidan_id'); ?>
		<?php echo $form->error($model,'passport_kim_tomonidan_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'metirka_raqami'); ?>
		<?php echo $form->textField($model,'metirka_raqami',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'metirka_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'metirka_kim_tomonidan'); ?>
		<?php echo $form->textField($model,'metirka_kim_tomonidan',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'metirka_kim_tomonidan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ish_joy_nomi'); ?>
		<?php echo $form->textField($model,'ish_joy_nomi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ish_joy_nomi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lavozimi'); ?>
		<?php echo $form->textField($model,'lavozimi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'lavozimi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'daromad_turi_id'); ?>
		<?php echo $form->textField($model,'daromad_turi_id'); ?>
		<?php echo $form->error($model,'daromad_turi_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'daromad_miqdori'); ?>
		<?php echo $form->textField($model,'daromad_miqdori',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'daromad_miqdori'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'daromad_olish_sanasi'); ?>
		<?php echo $form->textField($model,'daromad_olish_sanasi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'daromad_olish_sanasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'uzoq_muddatga_ketgan_vaqti'); ?>
		<?php echo $form->textField($model,'uzoq_muddatga_ketgan_vaqti'); ?>
		<?php echo $form->error($model,'uzoq_muddatga_ketgan_vaqti'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'uzoq_muddatga_kelgan_vaqti'); ?>
		<?php echo $form->textField($model,'uzoq_muddatga_kelgan_vaqti'); ?>
		<?php echo $form->error($model,'uzoq_muddatga_kelgan_vaqti'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'uzoq_muddatga_ketgan_davlati'); ?>
		<?php echo $form->textField($model,'uzoq_muddatga_ketgan_davlati',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'uzoq_muddatga_ketgan_davlati'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'uzoq_muddat_suhbat_otgani'); ?>
		<?php echo $form->textField($model,'uzoq_muddat_suhbat_otgani',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'uzoq_muddat_suhbat_otgani'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'malumoti_id'); ?>
		<?php echo $form->textField($model,'malumoti_id'); ?>
		<?php echo $form->error($model,'malumoti_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bitirgan_joy_id'); ?>
		<?php echo $form->textField($model,'bitirgan_joy_id',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'bitirgan_joy_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bitirgan_mutaxasisligi'); ?>
		<?php echo $form->textField($model,'bitirgan_mutaxasisligi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'bitirgan_mutaxasisligi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bitirgan_yili'); ?>
		<?php echo $form->textField($model,'bitirgan_yili',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'bitirgan_yili'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'aliment_id'); ?>
		<?php echo $form->textField($model,'aliment_id'); ?>
		<?php echo $form->error($model,'aliment_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'aliment_qarz'); ?>
		<?php echo $form->textField($model,'aliment_qarz',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'aliment_qarz'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'notinch_oila'); ?>
		<?php echo $form->textField($model,'notinch_oila',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'notinch_oila'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sudlanganligi'); ?>
		<?php echo $form->textArea($model,'sudlanganligi',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'sudlanganligi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'iib_xisobida_turadimi'); ?>
		<?php echo $form->textField($model,'iib_xisobida_turadimi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'iib_xisobida_turadimi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fuqaro_uy_id'); ?>
		<?php echo $form->textField($model,'fuqaro_uy_id'); ?>
		<?php echo $form->error($model,'fuqaro_uy_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kassalik_id'); ?>
		<?php echo $form->textField($model,'kassalik_id'); ?>
		<?php echo $form->error($model,'kassalik_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kassalik_xolati'); ?>
		<?php echo $form->textField($model,'kassalik_xolati',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'kassalik_xolati'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sogligi'); ?>
		<?php echo $form->textField($model,'sogligi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'sogligi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'oila_boshligi'); ?>
		<?php echo $form->textField($model,'oila_boshligi'); ?>
		<?php echo $form->error($model,'oila_boshligi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nikox_turi'); ?>
		<?php echo $form->textField($model,'nikox_turi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nikox_turi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nikox_guvohnom_raqami'); ?>
		<?php echo $form->textField($model,'nikox_guvohnom_raqami',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nikox_guvohnom_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rasm'); ?>
		<?php echo $form->textField($model,'rasm',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'rasm'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->