<?php
/* @var $this Fuqaro_kasallarController */
/* @var $model Fuqaro */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fio'); ?>
		<?php echo $form->textField($model,'fio',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jinsi_id'); ?>
		<?php echo $form->textField($model,'jinsi_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tugilgan_sanasi'); ?>
		<?php echo $form->textField($model,'tugilgan_sanasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tugilgan_joy_viloyat_id'); ?>
		<?php echo $form->textField($model,'tugilgan_joy_viloyat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tugilgan_joy_tuman_id'); ?>
		<?php echo $form->textField($model,'tugilgan_joy_tuman_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'passport_raqami'); ?>
		<?php echo $form->textField($model,'passport_raqami',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'passport_vaqti'); ?>
		<?php echo $form->textField($model,'passport_vaqti',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'passport_kim_tomonidan_id'); ?>
		<?php echo $form->textField($model,'passport_kim_tomonidan_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'metirka_raqami'); ?>
		<?php echo $form->textField($model,'metirka_raqami',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'metirka_kim_tomonidan'); ?>
		<?php echo $form->textField($model,'metirka_kim_tomonidan',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ish_joy_nomi'); ?>
		<?php echo $form->textField($model,'ish_joy_nomi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lavozimi'); ?>
		<?php echo $form->textField($model,'lavozimi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'daromad_turi_id'); ?>
		<?php echo $form->textField($model,'daromad_turi_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'daromad_miqdori'); ?>
		<?php echo $form->textField($model,'daromad_miqdori',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'daromad_olish_sanasi'); ?>
		<?php echo $form->textField($model,'daromad_olish_sanasi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uzoq_muddatga_ketgan_vaqti'); ?>
		<?php echo $form->textField($model,'uzoq_muddatga_ketgan_vaqti'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uzoq_muddatga_kelgan_vaqti'); ?>
		<?php echo $form->textField($model,'uzoq_muddatga_kelgan_vaqti'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uzoq_muddatga_ketgan_davlati'); ?>
		<?php echo $form->textField($model,'uzoq_muddatga_ketgan_davlati',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uzoq_muddat_suhbat_otgani'); ?>
		<?php echo $form->textField($model,'uzoq_muddat_suhbat_otgani',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'malumoti_id'); ?>
		<?php echo $form->textField($model,'malumoti_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bitirgan_joy_id'); ?>
		<?php echo $form->textField($model,'bitirgan_joy_id',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bitirgan_mutaxasisligi'); ?>
		<?php echo $form->textField($model,'bitirgan_mutaxasisligi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bitirgan_yili'); ?>
		<?php echo $form->textField($model,'bitirgan_yili',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'aliment_id'); ?>
		<?php echo $form->textField($model,'aliment_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'aliment_qarz'); ?>
		<?php echo $form->textField($model,'aliment_qarz',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'notinch_oila'); ?>
		<?php echo $form->textField($model,'notinch_oila',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sudlanganligi'); ?>
		<?php echo $form->textArea($model,'sudlanganligi',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'iib_xisobida_turadimi'); ?>
		<?php echo $form->textField($model,'iib_xisobida_turadimi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fuqaro_uy_id'); ?>
		<?php echo $form->textField($model,'fuqaro_uy_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kassalik_id'); ?>
		<?php echo $form->textField($model,'kassalik_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kassalik_xolati'); ?>
		<?php echo $form->textField($model,'kassalik_xolati',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sogligi'); ?>
		<?php echo $form->textField($model,'sogligi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'oila_boshligi'); ?>
		<?php echo $form->textField($model,'oila_boshligi'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nikox_turi'); ?>
		<?php echo $form->textField($model,'nikox_turi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nikox_guvohnom_raqami'); ?>
		<?php echo $form->textField($model,'nikox_guvohnom_raqami',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rasm'); ?>
		<?php echo $form->textField($model,'rasm',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->