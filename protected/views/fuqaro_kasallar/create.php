<?php
/* @var $this Fuqaro_kasallarController */
/* @var $model Fuqaro */

$this->breadcrumbs=array(
	Yii::t('strings','Fuqaros')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Fuqaro'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Fuqaro'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Fuqaro')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>