﻿<?php
/* @var $this FuqaroController */
/* @var $model Fuqaro */

$this->breadcrumbs=array(
	Yii::t('strings','Fuqaros')=>array('index'),
	Yii::t('strings','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Махаллада яшовчи фуқаролар рўйхати'))), 'url'=>array('index')),
	//array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Fuqaro'))), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fuqaro-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Фуқарони қидириш'))) ?></h1>


<?php echo CHtml::link(Yii::t('zii','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fuqaro-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'columns'=>array(
		array('name'=>'id', 'header'=>'ID'),
		array('name'=>'fio','header'=>'ФИШ'),
		array('name'=>'tugilgan_sanasi', 'header'=>'Туғилган санаси'),
		array('name'=>'passport_raqami', 'header'=>'Пасспорт рақами'),
		array('name'=>'tugilgan_joy_viloyat_id', 'header'=>'Вилоят', 'value'=>'$data->tugilganJoyViloyat->viloyat_nomi'),
		array('name'=>'tugilgan_joy_tuman_id', 'header'=>'Туман','value'=>'$data->tugilganJoyTuman->tuman_nomi'),
		array('name'=>'daromad_turi_id', 'header'=>'Бандлиги','value'=>'$data->daromad_turi->nomi'),


		/*
		'passport_raqami',
		'passport_vaqti',
		'passport_kim_tomonidan_id',
		'metirka_raqami',
		'metirka_kim_tomonidan',
		'ish_joy_nomi',
		'lavozimi',
		'daromad_turi_id',
		'daromad_miqdori',
		'daromad_olish_sanasi',
		'uzoq_muddatga_ketgan_vaqti',
		'uzoq_muddatga_kelgan_vaqti',
		'uzoq_muddatga_ketgan_davlati',
		'uzoq_muddat_suhbat_otgani',
		'malumoti_id',
		'bitirgan_joy_id',
		'bitirgan_mutaxasisligi',
		'bitirgan_yili',
		'aliment_id',
		'aliment_qarz',
		'notinch_oila_id',
		'sudlanganligi',
		'iib_xisobida_turadimi',
		'fuqaro_uy_id',
		'kassalik_id',
		'kassalik_xolati',
		'sogligi',
		'nikox_turi',
		'nikox_guvohnom_raqami',
		'rasm','passport_seyiya','uzoq_muddat_ish','user_id',
		'user_date',
		'otasi_id',
		'onasi_id',
		//'sogl.name',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
