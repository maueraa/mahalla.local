﻿<?php
/* @var $this FuqaroController */
/* @var $model Fuqaro */

$this->breadcrumbs=array(
	Yii::t('strings','Fuqaros')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	#array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Fuqaro'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Фуқарони қидириш'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Хонадонга фуқаронининг маълумотларини киритиш ойнаси')?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'fid'=>$fid)); ?>