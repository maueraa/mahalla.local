﻿<?php
/* @var $this FuqaroController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Фуқаролар'),
);
if(isset($fuqs))
{
    $this->menu=array(
        //array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Янги фуқаро қиритиш'))), 'url'=>array('create')),
        array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадонни маълумотини кўриш'))), 'url'=>array('fuqaro_Uy/view', 'id'=>$fuqs)),
        array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Маънавий мухит'))), 'url'=>array("/manaviy/$fuqs")),
        array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Даромади'))), 'url'=>array("/daromad/$fuqs")),
        array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Қарздоллик'))), 'url'=>array("/qarz/$fuqs")),
        array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт қатнови'))), 'url'=>array("/transport_qatnovi/$fuqs")),
        array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт воситалари'))), 'url'=>array("/xonadon_transport/$fuqs")),
        array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ижтимоий мухит'))), 'url'=>array("/ijtimoiy_muhit/$fuqs")),
        array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Шарт-шароити'))), 'url'=>array("/shaoriti/$fuqs")),
        array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Томорқа'))), 'url'=>array("/tomorqa/$fuqs")),
        array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Чорва хайвонлари'))), 'url'=>array("/hayvon/$fuqs")),
        array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хизмат кўрсатиш объектлари'))), 'url'=>array("/ximzat_korsatish/$fuqs")),
        array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчанинг холати'))), 'url'=>array("/kocha_xolati/$fuqs")),
        array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Барча маълумотларни кўриш'))), 'url'=>array('/jamlama/index', 'id'=>$fuqs)),

    );
}
else
{
$this->menu=array(
	//array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Янги фуқаро қиритиш'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Фуқарони қидириш'))), 'url'=>array('admin')),
);}
?>

<h1><?php echo Yii::t('strings','Махаллада яшовчи фуқаролар рўйхати')?></h1>

<h3>Сариқ ранг билан белгиланган фуқаролар 16 ёшга тўлмаган ёки пасспорт маълумотлари киритилмаган</h3>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'itemsTagName'=>'table',
    'itemsHeader'=>"
	<tr>
	<th>ID рақами</th>
	<th>Фамалия исм шариф</th>
	<th>Жинси</th>
	<th>Туғилган санаси</th>
	<th>Вилоят</th>
	<th>Туман, МФЙ, кўча ва уй рақами</th>
	<th>Пасспорт рақами</th>

	</tr>
	",
)); ?>
