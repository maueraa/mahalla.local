<?php
/* @var $this FuqarolarController */
/* @var $model Fuqarolar */

$this->breadcrumbs=array(
	Yii::t('strings','Фуқаролар')=>array('index'),
	$model->id,
);

$this->menu=array(
	#array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Fuqarolar'))), 'url'=>array('index')),
	#array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Fuqarolar'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Тахрирлаш'))), 'url'=>array('/fuqaro/update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ўчириш'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	#array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Fuqarolar'))), 'url'=>array('admin')),
);
?>

<?php #echo Yii::t('strings','Fuqarolar') .' - '. Yii::t('strings',$model->id);
$fuqaro_m = Fuqaro_Uy::model()->find("id=$model->fuqaro_uy_id");
?>

<h1 align="center" ID="kirish"><?php echo (CHtml::encode($model->fio)); ?> хақида умумий маълумот</h1>

<br />
<div class="view">
    <table width='640'  border='0'>
        <tr>
            <td width='100' height='23' align='left' valign='top' style="font-size: 14px; ">Исми шаърифи:</td>
            <td width='237' valign='top' colspan="2">
                <?php echo (CHtml::encode($model->fio)); ?>
            </td>
            <td width='151' rowspan='3' valign='top' align='right'><IMG src='../images/<?php echo CHtml::encode($model->rasm); ?>' height='120' width='100' ></td>
        </tr>
        <tr>
            <td width='100' height='27' align='left' valign='top'>Ту&#1171;илган санаси:</td>
            <td valign='top' colspan="2">
                <?php echo (CHtml::encode($model->tugilgan_sanasi)); ?>
            </td>
        </tr>
        <tr>
            <td width='100' align='right' valign='top'>Ту&#1171;илган жойи:</td>
            <td valign='top' colspan="2">
                <?php echo (CHtml::encode($model->tugilganJoyViloyat->viloyat_nomi)); ?>,
                <?php if (isset($model->tugilganJoyTuman->tuman_nomi))echo $model->tugilganJoyTuman->tuman_nomi; ?>
            </td>
        </tr>
        <tr>
            <td width='100' align='right' valign='top'>Пасспорт рақами:</td>
            <td valign='top' colspan="2">
                <?php echo (CHtml::encode($model->passport_raqami)); ?>,
                <?php if (isset($fuqaro_m->ftuman->tuman_nomi))echo $fuqaro_m->ftuman->tuman_nomi; ?>
                <?php if (isset($model->passport_vaqti))echo $model->passport_vaqti; ?>
            </td>
        </tr>
        <tr>
            <td height='28'  colspan='2' align='right' valign='top'>Яшаш манзили</td>
            <td height='28'  colspan='2' align='right' valign='top'>
                <?php if (isset($fuqaro_m->fviloyat->viloyat_nomi))	echo $fuqaro_m->fviloyat->viloyat_nomi;	?>
                <?php if (isset($fuqaro_m->ftuman->tuman_nomi))	echo $fuqaro_m->ftuman->tuman_nomi;	?>
                <?php if (isset($fuqaro_m->mfy->mfy_nomi))	echo $fuqaro_m->mfy->mfy_nomi;	?>
                <?php if (isset($fuqaro_m->kochalar->tbl_kocha_nomi))echo $fuqaro_m->kochalar->tbl_kocha_nomi; ?>
                <?php if (isset($fuqaro_m->uy_raqami))echo CHtml::encode($fuqaro_m->uy_raqami); ?>
            </td>
            </tr>
        <tr>
            <td width='100' height='24' align='left' valign='top'>Маълумоти:</td>
            <td valign='top' colspan="2"><?php echo (CHtml::encode($model->malumoti->malumot_turi)); ?></td>
            <td valign='top'>&nbsp;</td>
        </tr>
        <tr>
            <td width='100' height='24' align='left' valign='top'>Битирган жойи, мутахассислиги ва ўқишни тамомлаган йили:</td>
            <td valign='top' colspan="2">
                <?php echo (CHtml::encode($model->bitirgan_joy_id)); ?>
                <?php echo (CHtml::encode($model->bitirgan_mutaxasisligi)); ?>
                <?php echo (CHtml::encode($model->bitirgan_yili)); ?>
            </td>
            <td valign='top'>&nbsp;</td>
        </tr>
        <tr>
            <td width='100' height='24' align='left' valign='top'>Иш жойи ва лавозими:</td>
            <td valign='top' colspan="2">
                <?php echo (CHtml::encode($model->ish_joy_nomi)); ?>
                <?php echo (CHtml::encode($model->lavozimi)); ?>
            </td>
            <td valign='top'>&nbsp;</td>
        </tr>
        <tr>
            <td width='100' height='24' align='left' valign='top'>Судланганлиги:</td>
            <td valign='top' colspan="2"><?php echo (CHtml::encode($model->sudlanganligi)); ?></td>
            <td valign='top'>&nbsp;</td>
        </tr>
        <tr>
            <td width='100' height='24' align='left' valign='top'>ИИБ хисобида турадими:</td>
            <td valign='top' colspan="2"><?php echo (CHtml::encode($model->iib_xisobida_turadimi)); ?></td>
            <td valign='top'>&nbsp;</td>
        </tr>
        <tr>
            <td width='100' height='24' align='left' valign='top'>Даромад тури:</td>
            <td valign='top' colspan="2">
                <?php if(isset($model->daromad_turi->nomi))echo (CHtml::encode($model->daromad_turi->nomi)); ?>
            </td>
            <td valign='top'>&nbsp;</td>
        </tr>
        <tr>
            <td width='100' height='24' align='left' valign='top'>Даромад миқдори:</td>
            <td valign='top' colspan="2">
                <?php if(isset($model->daromad_miqdori))echo (CHtml::encode($model->daromad_miqdori)); ?>
            </td>
            <td valign='top'>&nbsp;</td>
        </tr>
        <tr>
            <td width='100' height='24' align='left' valign='top'>Даромад ёки иш хаки олиш вақти:</td>
            <td valign='top' colspan="2">
                <?php if(isset($model->daromad_olish_sanasi))echo (CHtml::encode($model->daromad_olish_sanasi)); ?>
            </td>
            <td valign='top'>&nbsp;</td>
        </tr>
        <tr>
            <td width='100' height='24' align='left' valign='top'>Узоқ муддатга кетган ва келган вакти:</td>
            <td valign='top' colspan="2">
                <?php #if(isset($model->uzoq_muddatga_ketgan_vaqti))echo (CHtml::encode($model->uzoq_muddatga_ketgan_vaqti)); ?>
                <?php if(isset($model->uzoq_muddatga_ketgan_vaqti) and $model->uzoq_muddatga_ketgan_vaqti!=='1970-01-01') {echo "";}else {echo $model->uzoq_muddatga_ketgan_vaqti;}?>
                <?php if(isset($model->uzoq_muddatga_kelgan_vaqti) and $model->uzoq_muddatga_kelgan_vaqti!=='1970-01-01') {echo "";}else {echo $model->uzoq_muddatga_kelgan_vaqti;}?>
            </td>
            <td valign='top'>&nbsp;</td>
        </tr>
        <tr>
            <td width='100' height='24' align='left' valign='top'>Узоқ муддатга кетган давлати:</td>
            <td valign='top' colspan="2">
                <?php if(isset($model->uzoq_muddatga_ketgan_davlati))echo (CHtml::encode($model->uzoq_muddatga_ketgan_davlati)); ?>

            </td>
            <td valign='top'>&nbsp;</td>
        </tr>
        <tr>
            <td width='100' height='24' align='left' valign='top'>Узоқ муддатга кетган давлати:</td>
            <td valign='top' colspan="2">
                <?php #if(isset($model->uzoq_muddat_suhbat_otgani))echo (CHtml::encode($model->uzoq_muddat_suhbat_otgani)); ?>
            </td>
            <td valign='top'>&nbsp;</td>
        </tr>
        <tr>
            <td width='100' height='24' align='left' valign='top'>Никох тур:</td>
            <td valign='top' colspan="2">

                <?php if(isset($model->nikox_turi)) {$n=array('1'=>'','2'=>'қонуний', '3'=>'шаръий');  echo $n[$model->nikox_turi];} ?>
                <?php if(isset($model->nikox_guvohnom_raqami)) echo $model->nikox_guvohnom_raqami; ?>
            </td>
            <td valign='top'>&nbsp;</td>
        </tr>
        <tr>
            <td width='100' height='24' align='left' valign='top'>Алимент(бор/йўқ ва қарздорлиги):</td>
            <td valign='top' colspan="2">
                <?php if(isset($model->aliment->nomi))echo (CHtml::encode($model->aliment->nomi)); ?>
                <?php if(isset($model->aliment_qarz))echo (CHtml::encode($model->aliment_qarz)); ?>
            </td>
            <td valign='top'>&nbsp;</td>
        </tr>
        <tr>
            <td width='100' height='24' align='left' valign='top'>Соғлиги:</td>
            <td valign='top' colspan="2">
                <?php if(isset($model->sogligi))echo (CHtml::encode($model->sogligi)); ?>

            </td>
            <td valign='top'>&nbsp;</td>
        </tr>
    </table>

</div>
