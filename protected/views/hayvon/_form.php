<?php
/* @var $this HayvonController */
/* @var $model Hayvon */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'hayvon-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'hayvon_soni'); ?>
		<?php echo $form->textField($model,'hayvon_soni'); ?>
		<?php echo $form->error($model,'hayvon_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'qora_mol'); ?>
		<?php echo $form->textField($model,'qora_mol'); ?>
		<?php echo $form->error($model,'qora_mol'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'qoy'); ?>
		<?php echo $form->textField($model,'qoy'); ?>
		<?php echo $form->error($model,'qoy'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'echki'); ?>
		<?php echo $form->textField($model,'echki'); ?>
		<?php echo $form->error($model,'echki'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ot'); ?>
		<?php echo $form->textField($model,'ot'); ?>
		<?php echo $form->error($model,'ot'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kuchuk'); ?>
		<?php echo $form->textField($model,'kuchuk'); ?>
		<?php echo $form->error($model,'kuchuk'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mushuk'); ?>
		<?php echo $form->textField($model,'mushuk'); ?>
		<?php echo $form->error($model,'mushuk'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'quyon'); ?>
		<?php echo $form->textField($model,'quyon'); ?>
		<?php echo $form->error($model,'quyon'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tovuq'); ?>
		<?php echo $form->textField($model,'tovuq'); ?>
		<?php echo $form->error($model,'tovuq'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kurka'); ?>
		<?php echo $form->textField($model,'kurka'); ?>
		<?php echo $form->error($model,'kurka'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ordak'); ?>
		<?php echo $form->textField($model,'ordak'); ?>
		<?php echo $form->error($model,'ordak'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'goz'); ?>
		<?php echo $form->textField($model,'goz'); ?>
		<?php echo $form->error($model,'goz'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'boshqalar'); ?>
		<?php echo $form->textField($model,'boshqalar',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'boshqalar'); ?>
	</div>

	<div class="row">
		<?php  $form->labelEx($model,'tbl_fuqaro_uy_id'); ?>
		<?php echo $form->hiddenField($model,'tbl_fuqaro_uy_id',array('value'=>$fid)); ?>
		<?php echo $form->error($model,'tbl_fuqaro_uy_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id').Yii::app()->user->name; ?>
		<?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_date').date("d.m.Y"); ?>
		<?php echo $form->hiddenField($model,'user_date',array('value'=>date("Y-m-d"))); ?>
		<?php echo $form->error($model,'user_date'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array("class"=>"btn btn-primary")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->