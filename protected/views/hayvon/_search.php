<?php
/* @var $this HayvonController */
/* @var $model Hayvon */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hayvon_soni'); ?>
		<?php echo $form->textField($model,'hayvon_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qora_mol'); ?>
		<?php echo $form->textField($model,'qora_mol'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qoy'); ?>
		<?php echo $form->textField($model,'qoy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'echki'); ?>
		<?php echo $form->textField($model,'echki'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ot'); ?>
		<?php echo $form->textField($model,'ot'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kuchuk'); ?>
		<?php echo $form->textField($model,'kuchuk'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mushuk'); ?>
		<?php echo $form->textField($model,'mushuk'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'quyon'); ?>
		<?php echo $form->textField($model,'quyon'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tovuq'); ?>
		<?php echo $form->textField($model,'tovuq'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kurka'); ?>
		<?php echo $form->textField($model,'kurka'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ordak'); ?>
		<?php echo $form->textField($model,'ordak'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'goz'); ?>
		<?php echo $form->textField($model,'goz'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'boshqalar'); ?>
		<?php echo $form->textField($model,'boshqalar',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tbl_fuqaro_uy_id'); ?>
		<?php echo $form->textField($model,'tbl_fuqaro_uy_id'); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model,'anketa_ozgarish'); ?>
        <?php echo $form->textField($model,'anketa_ozgarish'); ?>
    </div>

    <div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->