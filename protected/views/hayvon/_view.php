<?php
/* @var $this HayvonController */
/* @var $data Hayvon */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hayvon_soni')); ?>:</b>
	<?php echo CHtml::encode($data->hayvon_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qora_mol')); ?>:</b>
	<?php echo CHtml::encode($data->qora_mol); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qoy')); ?>:</b>
	<?php echo CHtml::encode($data->qoy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('echki')); ?>:</b>
	<?php echo CHtml::encode($data->echki); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ot')); ?>:</b>
	<?php echo CHtml::encode($data->ot); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kuchuk')); ?>:</b>
	<?php echo CHtml::encode($data->kuchuk); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('mushuk')); ?>:</b>
	<?php echo CHtml::encode($data->mushuk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quyon')); ?>:</b>
	<?php echo CHtml::encode($data->quyon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tovuq')); ?>:</b>
	<?php echo CHtml::encode($data->tovuq); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kurka')); ?>:</b>
	<?php echo CHtml::encode($data->kurka); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ordak')); ?>:</b>
	<?php echo CHtml::encode($data->ordak); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('goz')); ?>:</b>
	<?php echo CHtml::encode($data->goz); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('boshqalar')); ?>:</b>
	<?php echo CHtml::encode($data->boshqalar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tbl_fuqaro_uy_id')); ?>:</b>
	<?php echo CHtml::encode($data->tbl_fuqaro_uy_id); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('anketa_ozgarish')); ?>:</b>
	<?php echo CHtml::encode($data->anketa_ozgarish); ?>
	<br />

	*/ ?>

</div>