﻿<?php
/* @var $this HayvonController */
/* @var $model Hayvon */

$this->breadcrumbs=array(
	Yii::t('strings','Hayvons')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Hayvon'))), 'url'=>array('index')),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Hayvon'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Хонадондаги чорва хайвонлари ва паррандалар тўғрисида маълумотларни киритиш ойнаси')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'fid'=>$fid)); ?>