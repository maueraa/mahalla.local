<?php
/* @var $this IibController */
/* @var $data Iib */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('iib_xisobi')); ?>:</b>
	<?php echo CHtml::encode($data->iib_xisobi); ?>
	<br />


</div>