<?php
/* @var $this IibController */
/* @var $model Iib */

$this->breadcrumbs=array(
	Yii::t('strings','Iibs')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Iib'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Iib'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Iib')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>