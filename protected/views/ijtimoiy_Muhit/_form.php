<?php
/* @var $this Ijtimoiy_MuhitController */
/* @var $model Ijtimoiy_Muhit */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'ijtimoiy--muhit-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'obodonchilik'); ?>
		<?php echo $form->dropdownlist($model,'obodonchilik',array('1'=>'яхши','2'=>'ўртача', '3'=>'қониқарсиз')); ?>
		<?php echo $form->error($model,'obodonchilik'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hammom'); ?>
		<?php echo $form->dropdownlist($model,'hammom',array('1'=>'бор','2'=>'йўқ')); ?>
		<?php echo $form->error($model,'hammom'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'oshxona'); ?>
		<?php echo $form->dropdownlist($model,'oshxona',array('1'=>'бор','2'=>'йўқ')); ?>
		<?php echo $form->error($model,'oshxona'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tandir'); ?>
		<?php echo $form->dropdownlist($model,'tandir',array('1'=>'бор','2'=>'йўқ')); ?>
		<?php echo $form->error($model,'tandir'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bino_xolati_id'); ?>
		<?php echo $form->dropdownlist($model,'bino_xolati_id',$model->getbino_xolati()); ?>
		<?php echo $form->error($model,'bino_xolati_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'elektr_hisoblagich_mavjudligi'); ?>
		<?php echo $form->dropdownlist($model,'elektr_hisoblagich_mavjudligi',array('1'=>'бор','2'=>'йўқ')); ?>
		<?php echo $form->error($model,'elektr_hisoblagich_mavjudligi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'elektr_hisoblagich_raqami'); ?>
		<?php echo $form->textField($model,'elektr_hisoblagich_raqami',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'elektr_hisoblagich_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gaz_mavjudligi_id'); ?>
		<?php echo $form->dropdownlist($model,'gaz_mavjudligi_id',$model->getgaz_turi(),array('prompt'=>Yii::t("strings",'Табиий газ ёки пропан газ баллонини борлигини белгиланг'))); ?>
		<?php echo $form->error($model,'gaz_mavjudligi_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gaz_ballon_soni'); ?>
		<?php echo $form->textField($model,'gaz_ballon_soni',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'gaz_ballon_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gaz_hisoblagich_mavjudligii'); ?>
		<?php echo $form->dropdownlist($model,'gaz_hisoblagich_mavjudligii',array('1'=>'мавжуд','2'=>'мавжуд эмас',),array('prompt'=>Yii::t("strings",'Газ хисоблагичи'))); ?>
		<?php echo $form->error($model,'gaz_hisoblagich_mavjudligii'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gaz_hisoblagichi_raqami'); ?>
		<?php echo $form->textField($model,'gaz_hisoblagichi_raqami',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'gaz_hisoblagichi_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ichimlik_suvi_mavjud_hisoblagich_mavjudligi'); ?>
		<?php echo $form->dropdownlist($model,'ichimlik_suvi_mavjud_hisoblagich_mavjudligi',array('1'=>'Ичимлик суви хонадонга тортилган, хисоблагич мавжуд','2'=>'Ичимлик суви хонадонга тортилган, хисоблагич мавжуд эмас','3'=>'Ичимлик суви хонадонга тортилмаган'),array('prompt'=>Yii::t("strings",'Ичимлик суви'))); ?>
		<?php echo $form->error($model,'ichimlik_suvi_mavjud_hisoblagich_mavjudligi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ichimlik_suvi_mavjud_masofa'); ?>
		<?php echo $form->textField($model,'ichimlik_suvi_mavjud_masofa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ichimlik_suvi_mavjud_masofa'); ?>
	</div>

	<div class="row">
		<?php  $form->labelEx($model,'tbl_fuqaro_uy_id'); ?>
		<?php echo $form->hiddenField($model,'tbl_fuqaro_uy_id',array('value'=>$fid)); ?>
		<?php echo $form->error($model,'tbl_fuqaro_uy_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'user_id').Yii::app()->user->name; ?>
		<?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_date').date("d.m.Y"); ?>
		<?php echo $form->hiddenField($model,'user_date',array('value'=>date("Y-m-d"))); ?>
		<?php echo $form->error($model,'user_date'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array("class"=>"btn btn-primary")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->