<?php
/* @var $this Ijtimoiy_MuhitController */
/* @var $model Ijtimoiy_Muhit */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'obodonchilik'); ?>
		<?php echo $form->textField($model,'obodonchilik',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hammom'); ?>
		<?php echo $form->textField($model,'hammom',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'oshxona'); ?>
		<?php echo $form->textField($model,'oshxona',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tandir'); ?>
		<?php echo $form->textField($model,'tandir',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bino_xolati_id'); ?>
		<?php echo $form->textField($model,'bino_xolati_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'elektr_hisoblagich_mavjudligi'); ?>
		<?php echo $form->textField($model,'elektr_hisoblagich_mavjudligi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'elektr_hisoblagich_raqami'); ?>
		<?php echo $form->textField($model,'elektr_hisoblagich_raqami',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gaz_mavjudligi_id'); ?>
		<?php echo $form->textField($model,'gaz_mavjudligi_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gaz_ballon_soni'); ?>
		<?php echo $form->textField($model,'gaz_ballon_soni',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gaz_hisoblagichi_raqami'); ?>
		<?php echo $form->textField($model,'gaz_hisoblagichi_raqami',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gaz_hisoblagich_mavjudligii'); ?>
		<?php echo $form->textField($model,'gaz_hisoblagich_mavjudligii',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ichimlik_suvi_mavjud_hisoblagich_mavjudligi'); ?>
		<?php echo $form->textField($model,'ichimlik_suvi_mavjud_hisoblagich_mavjudligi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ichimlik_suvi_mavjud_masofa'); ?>
		<?php echo $form->textField($model,'ichimlik_suvi_mavjud_masofa',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tbl_fuqaro_uy_id'); ?>
		<?php echo $form->textField($model,'tbl_fuqaro_uy_id'); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model,'anketa_ozgarish'); ?>
        <?php echo $form->textField($model,'anketa_ozgarish'); ?>
    </div>

    <div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->