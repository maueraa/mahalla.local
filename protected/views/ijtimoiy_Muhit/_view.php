<?php
/* @var $this Ijtimoiy_MuhitController */
/* @var $data Ijtimoiy_Muhit */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('obodonchilik')); ?>:</b>
	<?php echo CHtml::encode($data->obodonchilik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hammom')); ?>:</b>
	<?php echo CHtml::encode($data->hammom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('oshxona')); ?>:</b>
	<?php echo CHtml::encode($data->oshxona); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tandir')); ?>:</b>
	<?php echo CHtml::encode($data->tandir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bino_xolati_id')); ?>:</b>
	<?php echo CHtml::encode($data->bino_xolati_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('elektr_hisoblagich_mavjudligi')); ?>:</b>
	<?php echo CHtml::encode($data->elektr_hisoblagich_mavjudligi); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('elektr_hisoblagich_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->elektr_hisoblagich_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gaz_mavjudligi_id')); ?>:</b>
	<?php echo CHtml::encode($data->gaz_mavjudligi_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gaz_ballon_soni')); ?>:</b>
	<?php echo CHtml::encode($data->gaz_ballon_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gaz_hisoblagichi_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->gaz_hisoblagichi_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gaz_hisoblagich_mavjudligii')); ?>:</b>
	<?php echo CHtml::encode($data->gaz_hisoblagich_mavjudligii); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ichimlik_suvi_mavjud_hisoblagich_mavjudligi')); ?>:</b>
	<?php echo CHtml::encode($data->ichimlik_suvi_mavjud_hisoblagich_mavjudligi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ichimlik_suvi_mavjud_masofa')); ?>:</b>
	<?php echo CHtml::encode($data->ichimlik_suvi_mavjud_masofa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tbl_fuqaro_uy_id')); ?>:</b>
	<?php echo CHtml::encode($data->tbl_fuqaro_uy_id); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('anketa_ozgarish')); ?>:</b>
	<?php echo CHtml::encode($data->anketa_ozgarish); ?>
	<br />

	*/ ?>

</div>