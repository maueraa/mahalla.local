<?php
/* @var $this Ijtimoiy_MuhitController */
/* @var $model Ijtimoiy_Muhit */

$this->breadcrumbs=array(
	Yii::t('strings','Ijtimoiy  Muhits')=>array('index'),
	Yii::t('strings','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Ijtimoiy_Muhit'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Ijtimoiy_Muhit'))), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#ijtimoiy--muhit-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Ijtimoiy_Muhit'))) ?></h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link(Yii::t('zii','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ijtimoiy--muhit-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'obodonchilik',
		'hammom',
		'oshxona',
		'tandir',
		'bino_xolati_id',
		/*
		'elektr_hisoblagich_mavjudligi',
		'elektr_hisoblagich_raqami',
		'gaz_mavjudligi_id',
		'gaz_ballon_soni',
		'gaz_hisoblagichi_raqami',
		'gaz_hisoblagich_mavjudligii',
		'ichimlik_suvi_mavjud_hisoblagich_mavjudligi',
		'ichimlik_suvi_mavjud_masofa',
		'tbl_fuqaro_uy_id',
		//'tbl_fuqaro_uy.name',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
