﻿<?php
/* @var $this Ijtimoiy_MuhitController */
/* @var $model Ijtimoiy_Muhit */

$this->breadcrumbs=array(
	Yii::t('strings','Ijtimoiy  Muhits')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Ijtimoiy_Muhit'))), 'url'=>array('index')),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Ijtimoiy_Muhit'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадондаги ижтимоий-маиший мухит хақида маълумотларни киритиш ойнаси ')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'fid'=>$fid)); ?>