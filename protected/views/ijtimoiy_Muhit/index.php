<?php
/* @var $this Ijtimoiy_MuhitController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Ijtimoiy  Muhits'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Ijtimoiy_Muhit'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Ijtimoiy_Muhit'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Ijtimoiy  Muhits')?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
