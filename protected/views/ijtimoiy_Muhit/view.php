<?php
/* @var $this Ijtimoiy_MuhitController */
/* @var $model Ijtimoiy_Muhit */

$this->breadcrumbs=array(
	Yii::t('strings','Ижтимоий мухит')=>array('index'),
	$model->id,
);

$this->menu=array(
#	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Ijtimoiy_Muhit'))), 'url'=>array('index')),
#	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Ijtimoiy_Muhit'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Ijtimoiy_Muhit'))), 'url'=>array('update', 'id'=>$model->id)),
#	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Ijtimoiy_Muhit'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Ijtimoiy_Muhit'))), 'url'=>array('admin')),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон маълумотлари'))), 'url'=>array("/fuqaro_Uy/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон аъзолари'))), 'url'=>array("/fuqarolar?fuq=$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Маънавий мухит'))), 'url'=>array("/manaviy/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Даромади'))), 'url'=>array("/daromad/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Қарздоллик'))), 'url'=>array("/qarz/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт қатнови'))), 'url'=>array("/transport_qatnovi/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ижтимоий мухит хақидаги маълумотларни ўзгартириш'))), 'url'=>array('update', 'fid'=>$model->tbl_fuqaro_uy_id)),
    #array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ижтимоий мухит'))), 'url'=>array("/ijtimoiy_muhit/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Шарт-шароити'))), 'url'=>array("/shaoriti/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Томорқа'))), 'url'=>array("/tomorqa/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Чорва хайвонлари'))), 'url'=>array("/hayvon/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хизмат кўрсатиш объектлари'))), 'url'=>array("/ximzat_korsatish/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчанинг холати'))), 'url'=>array("/kocha_xolati/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Барча маълумотларни кўриш'))), 'url'=>array('/jamlama/index', 'id'=>$model->tbl_fuqaro_uy_id)),

);
?>

<h1><?php echo Yii::t('strings','Хонадондаги ижтимоий-маиший мухит хақида маълумотларни янгиланган холати') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'obodonchilik',
		'hammom',
		'oshxona',
		'tandir',
		'bino_xolati_id',
		'elektr_hisoblagich_mavjudligi',
		'elektr_hisoblagich_raqami',
		'gaz_mavjudligi_id',
		'gaz_ballon_soni',
		'gaz_hisoblagichi_raqami',
		'gaz_hisoblagich_mavjudligii',
		'ichimlik_suvi_mavjud_hisoblagich_mavjudligi',
		'ichimlik_suvi_mavjud_masofa',
		'tbl_fuqaro_uy_id',
        'anketa_ozgarish',
		'user_id',
		'user_date',
/**
		array(
			'label'=>$model->getAttributeLabel('tbl_fuqaro_uy_id'),
			'type'=>'raw',
			'value'=>$model->tbl_fuqaro_uy->name,
		),
/**/
	),
)); ?>
