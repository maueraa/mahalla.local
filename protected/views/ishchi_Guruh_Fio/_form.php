<?php
/* @var $this Ishchi_Guruh_FioController */
/* @var $model Ishchi_Guruh_Fio */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'ishchi--guruh--fio-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'viloyat_id'); ?>
		<?php echo $form->dropDownList($model,'viloyat_id',$model->getvil(), array('id'=>'id_province','prompt'=>Yii::t("strings",'Вилоятни танланг'))); ?>
		<?php echo $form->error($model,'viloyat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tuman_id'); ?>
		<?php echo $form->dropDownList($model,'tuman_id',CHtml::listData(Tuman::model()->findAll("viloyat_id like '$model->viloyat_id'"),'id','tuman_nomi'),array('id'=>'id_tuman','prompt'=>Yii::t("strings",'Туманни танланг'))); ?>
		<?php echo $form->error($model,'tuman_id'); ?>
	</div>
	<?php ECascadeDropDown::master('id_province')->setDependent('id_tuman',array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro/loadtuman');?>

	<div class="row">
		<?php echo $form->labelEx($model,'ishchi_guruh_fio'); ?>
		<?php echo $form->textField($model,'ishchi_guruh_fio',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ishchi_guruh_fio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tashkilot_nomi'); ?>
		<?php echo $form->textField($model,'tashkilot_nomi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'tashkilot_nomi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id').Yii::app()->user->name;; ?>
		<?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_date').date("d.m.Y"); ?>
		<?php echo $form->hiddenField($model,'user_date',array('value'=>date("Y-m-d"))); ?>
		<?php echo $form->error($model,'user_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->