<?php
/* @var $this Ishchi_Guruh_FioController */
/* @var $model Ishchi_Guruh_Fio */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model,'viloyat_id'); ?>
		<?php echo $form->textField($model,'viloyat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tuman_id'); ?>
		<?php echo $form->textField($model,'tuman_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ishchi_guruh_fio'); ?>
		<?php echo $form->textField($model,'ishchi_guruh_fio',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tashkilot_nomi'); ?>
		<?php echo $form->textField($model,'tashkilot_nomi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_date'); ?>
		<?php echo $form->textField($model,'user_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->