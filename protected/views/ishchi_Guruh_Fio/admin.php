<?php
/* @var $this Ishchi_Guruh_FioController */
/* @var $model Ishchi_Guruh_Fio */

$this->breadcrumbs=array(
	Yii::t('strings','Ишчи гурух')=>array("index?guruh=".Yii::app()->user->tuman),
	Yii::t('strings','Тахрирлаш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label} рўйхати', array('{label}'=>Yii::t('strings','Ишчи гурух'))), 'url'=>array("index?guruh=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label}ни киритиш', array('{label}'=>Yii::t('strings','Ишчи гурух'))), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('fuqaro--uy-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('strings','{label} тахрирлаш', array('{label}'=>Yii::t('strings','Ишчи гурух'))) ?></h1>


<?php echo CHtml::link(Yii::t('zii','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'ishchi--guruh--fio-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'viloyat_id',
		'tuman_id',
		'ishchi_guruh_fio',
		'tashkilot_nomi',
		'user_id',
		'user_date',
		//'tashkilot_n.name',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
