<?php
/* @var $this Ishchi_Guruh_FioController */
/* @var $model Ishchi_Guruh_Fio */

$this->breadcrumbs=array(
	Yii::t('strings','Ишчи гурух')=>array("index?guruh=".Yii::app()->user->tuman),
	Yii::t('strings','Киритиш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ишчи гурух рўйхати'))), 'url'=>array("index?guruh=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Админ'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ишчи гурухни киритиш ойнаси')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>