<?php
/* @var $this Ishchi_Guruh_FioController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Ишчи гурух'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label} киритиш', array('{label}'=>Yii::t('strings','Ишчи гурух'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label} тахрирлаш', array('{label}'=>Yii::t('strings','Ишчи гурух'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Ишчи гурухлар')?></h1>

<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'itemsTagName'=>'table',
    'itemsCssClass'=>'table table-bordered table-striped',
    'itemsHeader'=>"
  	<thead><tr>
	<th>№</th>
	<th>Вилоят, туман</th>
	<th>Ташкилот рахбари</th>
	<th>Ташкилот номи</th>
	<th>Хонадонда яшовчи фуқаролар</th>
	<th>Телефон рақами</th>
	</tr></thead>

    ",
)); ?>
