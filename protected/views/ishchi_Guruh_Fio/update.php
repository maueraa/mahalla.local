<?php
/* @var $this Ishchi_Guruh_FioController */
/* @var $model Ishchi_Guruh_Fio */

$this->breadcrumbs=array(
	Yii::t('strings','Ишчи гурух')=>array("index?guruh=".Yii::app()->user->tuman),
	Yii::t('strings',$model->id)=>array('view','id'=>$model->id),
	Yii::t('strings','Ўзгартириш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label} рўйхати', array('{label}'=>Yii::t('strings','Ишчи гурух'))), 'url'=>array("index?guruh=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','Янги {label}ини киритиш', array('{label}'=>Yii::t('strings','Ишчи гурух'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label} кўриш', array('{label}'=>Yii::t('strings','Ишчи гурух'))), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','{label} тахрирлаш', array('{label}'=>Yii::t('strings','Ишчи гурух'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{table}ни ўзгартириш "{record}" (id={id})', array(
	'{table}'=>Yii::t('strings','Ишчи гурух'),
	'{record}'=>$model->id,
	'{id}'=>$model->id));?></h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>