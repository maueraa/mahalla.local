<?php
/* @var $this Ishchi_Guruh_FioController */
/* @var $model Ishchi_Guruh_Fio */

$this->breadcrumbs=array(
	Yii::t('strings','Ишчи гурух')=>array("index?guruh=".Yii::app()->user->tuman),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ишчи гурух'))), 'url'=>array("index?guruh=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ишчи гурух киритиш'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ўзгартиш'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ўчириш'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Админ'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Ишчи гурух'); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
			'label'=>$model->getAttributeLabel('viloyat_id'),
			'type'=>'raw',
			'value'=>$model->viloyat->viloyat_nomi,
		),
		array(
			'label'=>$model->getAttributeLabel('tuman_id'),
			'type'=>'raw',
			'value'=>$model->tuman->tuman_nomi,
		),
		'ishchi_guruh_fio',
		'tashkilot_nomi',
		array(
			'label'=>$model->getAttributeLabel('user_id'),
			'type'=>'raw',
			'value'=>$model->user->username,
		),
		'user_date',
/**
		array(
			'label'=>$model->getAttributeLabel('tashkilot_nomi'),
			'type'=>'raw',
			'value'=>$model->tashkilot_n->name,
		),
/**/
	),
)); ?>
