<?php
/* @var $this IssiqxonaController */
/* @var $data Issiqxona */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('issiqxona_turi')); ?>:</b>
	<?php echo CHtml::encode($data->issiqxona_turi); ?>
	<br />


</div>