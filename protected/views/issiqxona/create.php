<?php
/* @var $this IssiqxonaController */
/* @var $model Issiqxona */

$this->breadcrumbs=array(
	Yii::t('strings','Issiqxonas')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Issiqxona'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Issiqxona'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Issiqxona')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>