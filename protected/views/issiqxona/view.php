<?php
/* @var $this IssiqxonaController */
/* @var $model Issiqxona */

$this->breadcrumbs=array(
	Yii::t('strings','Issiqxonas')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Issiqxona'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Issiqxona'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Issiqxona'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Issiqxona'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Issiqxona'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Issiqxona') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'issiqxona_turi',
/**
		array(
			'label'=>$model->getAttributeLabel('issiqxona_turi'),
			'type'=>'raw',
			'value'=>$model->issiqxona_t->name,
		),
/**/
	),
)); ?>
