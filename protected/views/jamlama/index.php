<?php
/* @var $this JamlamaController */

$this->breadcrumbs=array(
	'Jamlama',
	' / Орқага'=>array("/fuqaro_Uy/$fuqaro_uy->id"),
);
?>

<div id="yuklash"><h6><?php echo  CHtml::link("Хонадоннинг барча маълумотларини PDF орқали юклаб олиш", array("/jamlama/view?id=$fuqaro_uy->id", ));  ?></h6></div>
<h2 style="text-align: center;">Хўжаобод тумани ижтимоий-иқтисоди ривожлантириш, аҳоли турмуш шароитларини яхшилаш,
	озиқ-овқат хавфсизлигини таъминлаш ва қишлоқ хўжалигида агротeхник тадбирларни амалга
	ошириш бўйича _____-ҳудуд ишчи гуруҳи</h2>
<br>
<br>
<br>
<br>
<br>

<h4 style="text-align: center;">

	“<?php if(isset($fuqaro_uy->mfy->mfy_nomi ))echo $fuqaro_uy->mfy->mfy_nomi ?>” маҳалла фуқаролар йиғини
	<?php if(isset($fuqaro_uy->kochalar->tbl_kocha_nomi))echo $fuqaro_uy->kochalar->tbl_kocha_nomi ?> кўчаси №
	<?php if(isset($fuqaro_uy->uy_raqami))echo $fuqaro_uy->uy_raqami ?> уйда яшовчи
	фуқаро <?php if(isset($fuqaro->fio))echo $fuqaro->fio ?>га тeгишли хонадонини ўрганиш бўйича</h4>
<br>
<h2 style="text-align: center;">МАЪЛУМОТЛАР ТЎПЛАМИ</h2>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


<h6 style="text-align: center;">Хўжаобод тумани 2017-йил.</h6>
<br>
<br>
<br>
<br>
<br>
<br>
<br>


<?php
//print_r($dar->kam_taminlangan_sifatida);
//print_r($im->obodonchilik);
//	print_r($dar->oylik_ortacha_daromadi);

?>
<br>
<br>
<br>
<br>
<h4 style="text-align: center;">“<?php if(isset($fuqaro_uy->mfy->mfy_nomi ))echo $fuqaro_uy->mfy->mfy_nomi ?>” маҳалла фуқаролар йиғини
	<?php if(isset($fuqaro_uy->kochalar->tbl_kocha_nomi))echo $fuqaro_uy->kochalar->tbl_kocha_nomi ?> кўчаси №
	<?php if(isset($fuqaro_uy->uy_raqami ))echo $fuqaro_uy->uy_raqami ?> уйда яшовчи
	фуқаро <?php if(isset($fuqaro->fio ))echo $fuqaro->fio ?>нинг  хонадони хақида</h4>
<h4 style="text-align: center;">МАЪЛУМОТ</h4>
<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" style="width: 100%; border-collapse: collapse; border: medium none">
	<tr>
		<td valign="top" style="width: 193px; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" style="margin-right: -14.2pt"><b>
				<span lang="UZ-CYR">Хонадон бошли&#1171;и</span></b></td>
		<td colspan="3" valign="top" style="width: 383px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -14.2pt">
				<b><span lang="UZ-CYR">
<?php
					if($fuqaro->oila_boshligi==1)
					{
						echo $fuqaro->fio;
					}
					?>
                </span></b></td>
		<td valign="top" style="width: 138px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" style="margin-right: -14.2pt"><b>
				<span lang="UZ-CYR">Ижарага олувчи</span></b></td>
		<td colspan="2" valign="top" style="width: 67px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -14.2pt">
				<b><span lang="UZ-CYR">&nbsp;</span></b></td>
	</tr>
	<tr>
		<td valign="top" style="width: 193px; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" style="margin-right: -14.2pt"><b>
				<span lang="UZ-CYR">Хонадоннинг ху&#1179;у&#1179;ий хужжати</span></b>
		</td>
		<td colspan="6" valign="top" style="width: 701px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -14.2pt">
				<b><span lang="UZ-CYR">

                    <?php if(isset($fuqaro_uy->kadastr_mavjudligi))echo $fuqaro_uy->kadastr_mavjudligi ?>

                </span></b>
		</td>
	</tr>
	<tr>
		<td valign="top" style="width: 193px; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" style="margin-right: -14.2pt"><b>
				<span lang="UZ-CYR">Хонадондаги оила аъзолар сони</span></b></td>
		<td valign="top" style="width: 132px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -14.2pt">
				<b><span lang="UZ-CYR">

                    <?php if(isset($fuqaro_uy->azo_soni))echo $fuqaro_uy->azo_soni ?>

                </span></b>
		</td>
		<td>
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -14.2pt">
				<td colspan="2" valign="top" style="width: 296px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -14.2pt">
				<b><span lang="UZ-CYR">Вояга етмаганлар сони</span></b>
		</td>
		<td valign="top" style="width: 67px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -14.2pt">
				<b><span lang="UZ-CYR">
                    <?php if(isset($fuqaro_uy->voya_yetmagan_soni))echo $fuqaro_uy->voya_yetmagan_soni ?>
                </span></b>
		</td>
	</tr>
</table>
<h4 style="text-align: center;">ОИЛА АЪЗОЛАРИ ҲАҚИДА УМУМИЙ МАЪЛУМОТЛАР</h4>
<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" width="1087" style="width: 100%; border-collapse: collapse; border: medium none;">
	<tr>
		<td width="43" style="width: 32.4pt; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: 3.6pt">
				<b><span lang="UZ-CYR">№</span></b></td>
		<td width="276" style="width: 207.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: 3.6pt">
				<b><span lang="UZ-CYR">Ф.И.Ш ва тeл ра&#1179;ами</span></b></td>
		<td width="132" style="width: 99.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: 3.6pt">
				<b><span lang="UZ-CYR">Оилага аъзолиги, никох гувохномаси ёки шаърий
		нико&#1203;</span></b></td>
		<td width="156" style="width: 117.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: 3.6pt">
				<b><span lang="UZ-CYR">Ту&#1171;улган ва&#1179;ти (кун, ой, йил ва жойи)</span></b></td>
		<td width="180" style="width: 135.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: 3.6pt">
				<b><span lang="UZ-CYR">Паспорт ёки ту&#1171;улганлик гувохнома ра&#1179;ами (мавжудлиги,
		биомeтрик ёки оддий, муддати ўтган)</span></b></td>
		<td width="156" style="width: 117.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: 3.6pt">
				<b><span lang="UZ-CYR">Со&#1171;ли&#1171;и (со&#1171;лом, ногирон, даволанишга мухтож,
		&#1203;исобда туриши)</span></b></td>
		<td width="144" style="width: 108.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: 3.6pt">
				<b><span lang="UZ-CYR">Узо&#1179; муддатга кeтган ва кeлган (&#1179;ачон ва &#1179;аeрга)</span></b></td>
	</tr>
	<tr>
		<td width="43" style="width: 32.4pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: 3.6pt">
				<b><span lang="UZ-CYR">1</span></b></td>
		<td width="276" style="width: 207.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: 3.6pt">
				<b><span lang="UZ-CYR">2</span></b></td>
		<td width="132" style="width: 99.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: 3.6pt">
				<b><span lang="UZ-CYR">3</span></b></td>
		<td width="156" style="width: 117.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: 3.6pt">
				<b><span lang="UZ-CYR">4</span></b></td>
		<td width="180" style="width: 135.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: 3.6pt">
				<b><span lang="UZ-CYR">5</span></b></td>
		<td width="156" style="width: 117.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: 3.6pt">
				<b><span lang="UZ-CYR">6</span></b></td>
		<td width="144" style="width: 108.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: 3.6pt">
				<b><span lang="UZ-CYR">7</span></b></td>
	</tr>

	<?php
	$davlat=Davlat::model()->findAll();

	$i=1;
	foreach($fuqaro_uy->fuqaro as $fmodel)
	{
		?>
		<tr>
			<td width="43" style="width: 32.4pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
				<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: 3.6pt">
					<b><span lang="UZ-CYR" style="font-size: 12.0pt; line-height: 150%"><?php echo $i++ ?></span></b></td>
			<td width="276" style="width: 207.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
				<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: 3.6pt">
					<b><span lang="UZ-CYR" style="font-size: 10.0pt; line-height: 150%">
                    <?php if(isset($fmodel->fio))echo $fmodel->fio;?>
                </span></b></td>
			<td width="132" style="width: 99.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
				<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: 3.6pt">
					<b><span lang="UZ-CYR" style="font-size: 10.0pt; line-height: 150%">

                    <?php if(isset($fmodel->nikox_turi)) {$n=array('1'=>'','2'=>'қонуний', '3'=>'шаръий');  echo $n[$fmodel->nikox_turi];} ?>
						<?php if(isset($fmodel->nikox_guvohnom_raqami)) echo $fmodel->nikox_guvohnom_raqami; ?>
                </span></b></td>
			<td width="156" style="width: 117.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
				<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: 3.6pt">
					<b><span lang="UZ-CYR" style="font-size: 10.0pt; line-height: 150%">
					<?php if(isset($fmodel->tugilgan_sanasi))echo $fmodel->tugilgan_sanasi ?>
						<?php if(isset($fmodel->tugilganJoyViloyat->viloyat_nomi))echo $fmodel->tugilganJoyViloyat->viloyat_nomi?>
						<?php if(isset($fmodel->tugilganJoyTuman->tuman_nomi))echo $fmodel->tugilganJoyTuman->tuman_nomi?>
                </span></b></td>
			<td width="180" style="width: 135.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
				<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: 3.6pt">
					<b><span lang="UZ-CYR" style="font-size: 10.0pt; line-height: 150%">
                    <?php
						if(isset($fmodel->passport_raqami) and trim($fmodel->passport_raqami)!='')
						{
							echo $fmodel->passport_raqami;echo'<br>';
							if(isset($fmodel->passport_vaqti))echo $fmodel->passport_vaqti;
							if(isset($fmodel->tugilganJoyTuman->tuman_nomi))echo $fmodel->tugilganJoyTuman->tuman_nomi;
						}
						elseif(isset($fmodel->metirka_raqami))
						{
							echo $fmodel->metirka_raqami;
							echo'<br>';
							if(isset($fmodel->metirka_kim_tomonidan)) echo $fmodel->metirka_kim_tomonidan;
						}
						?>
                </span></b></td>
			<td width="156" style="width: 117.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
				<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: 3.6pt">
					<b><span lang="UZ-CYR" style="font-size: 10.0pt; line-height: 150%">
                    <?php if(isset($fmodel->kassalik->kassalik_nomi)) echo $fmodel->kassalik->kassalik_nomi; ?>
						<?php if(isset($fmodel->kassalik_xolati)) {$kas=array('0'=>'','1'=>'Даволанишга мухтож', '2'=>'Оғир касал');echo $kas[$fmodel->kassalik_xolati];} ?>
						<?php if(isset($fmodel->sogligi)) echo $fmodel->sogligi; ?>
                </span></b></td>
			<td width="144" style="width: 108.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
				<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: 3.6pt">
					<b><span lang="UZ-CYR" style="font-size: 10.0pt; line-height: 150%">
                <?php if(isset($fmodel->uzoq_muddatga_ketgan_vaqti) and $fmodel->uzoq_muddatga_ketgan_vaqti !== null) {echo $fmodel->uzoq_muddatga_ketgan_vaqti;}?>
						<?php if(isset($fmodel->uzoq_muddatga_kelgan_vaqti) and $fmodel->uzoq_muddatga_kelgan_vaqti !== null) {echo $fmodel->uzoq_muddatga_kelgan_vaqti;}?>
						<?php #echo $fuqaro->uzoq_muddatga_kelgan_vaqti; ?>
						<?php if(isset($fmodel->uzoq_muddatga_ketgan_davlati) and $fmodel->uzoq_muddatga_ketgan_davlati!=NULL) echo  $fmodel->uzoqMuddatgaKetganDavlati->davlat_nomi; ?>
						<?php if(isset($fmodel->uzoq_muddat_ish)) echo  $fmodel->uzoq_muddat_ish; ?>
						<?php #print_r($davlat['1']);?>
                </span></b></td>
		</tr>
		<?php
	}
	?>

</table>
<br>
<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" style="border-collapse: collapse; border: medium none; width:100%">
	<tr>
		<td width="43" rowspan="2" style="width: 32.4pt; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 10pt">№</span></b></td>
		<td width="157" style="width: 117.65pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR" style="font-size: 10pt">Узо&#1179; муддатдан кeлиб
		сухбат ва тиббий кўрикдан ўтганлиги</span></b></td>
		<td width="251" style="width: 188.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR" style="font-size: 10pt">Маълумоти, &#1179;ачон ва &#1179;аeрни
		битирган, мутахассислиги</span></b></td>
		<td width="240" style="width: 180.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR" style="font-size: 10pt">Иш ёки ў&#1179;иш жойи ва
		лавозими</span></b></td>
		<td width="132" style="width: 99.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR" style="font-size: 10pt">Алимeнт тўловчи ёки олувчи,
		алимeнтдан &#1179;арздорлиги</span></b></td>
		<td width="95" style="width: 71.3pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 10pt">Нотинч оила</span></b></td>
		<td width="169" style="width: 126.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR" style="font-size: 10pt">Судланганлиги (&#1179;ачон ва
		&#1179;айси модда), ИИБ &#1203;исобида туриши</span></b></td>
	</tr>
	<tr>
		<td width="157" valign="top" style="width: 117.65pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -4.75pt">
				<b><span lang="UZ-CYR" style="font-size: 12pt">8</span></b></td>
		<td width="251" valign="top" style="width: 188.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -4.75pt">
				<b><span lang="UZ-CYR" style="font-size: 12pt">9</span></b></td>
		<td width="240" valign="top" style="width: 180.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -4.75pt">
				<b><span lang="UZ-CYR" style="font-size: 12pt">10</span></b></td>
		<td width="132" valign="top" style="width: 99.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -4.75pt">
				<b><span lang="UZ-CYR" style="font-size: 12pt">11</span></b></td>
		<td width="95" valign="top" style="width: 71.3pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -4.75pt">
				<b><span lang="UZ-CYR" style="font-size: 12pt">12</span></b></td>
		<td width="169" valign="top" style="width: 126.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -4.75pt">
				<b><span lang="UZ-CYR" style="font-size: 12pt">13</span></b></td>
	</tr>
	<?php
	$v=1;
	foreach($fuqaro_uy->fuqaro as $fmodel2)
	{
		?>
		<tr>
			<td width="43" style="width: 32.4pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
				<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: 3.6pt">
					<b><span lang="UZ-CYR" style="font-size: 12pt; line-height: 150%"><?php echo $v++; ?></span></b></td>
			<td width="157" valign="top" style="width: 117.65pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
				<p class="MsoNormal" align="center" style="text-align: center; margin-right: -14.2pt">
					<b><span lang="UZ-CYR" style="font-size: 10pt">
                    <?php if(isset($fmodel2->uzoq_muddat_suhbat_otgani)) echo $fmodel2->uzoq_muddat_suhbat_otgani?>
                </span></b></td>
			<td width="251" valign="top" style="width: 188.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
				<p class="MsoNormal" align="center" style="text-align: center; margin-right: -14.2pt">
					<b><span lang="UZ-CYR" style="font-size: 10pt">
                    <?php if(isset($fmodel2->malumoti->malumot_turi))echo $fmodel2->malumoti->malumot_turi?>
						<?php if(isset($fmodel2->bitirgan_joy_id))echo $fmodel2->bitirgan_joy_id?><br>
						<?php if(isset($fmodel2->bitirgan_mutaxasisligi))echo $fmodel2->bitirgan_mutaxasisligi?>
						<?php if(isset($fmodel2->bitirgan_yili))echo $fmodel2->bitirgan_yili?>
                </span></b></td>
			<td width="240" valign="top" style="width: 180.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
				<p class="MsoNormal" align="center" style="text-align: center; margin-right: -14.2pt">
					<b><span lang="UZ-CYR" style="font-size: 10pt">
                  		<?php
						if(isset($fmodel->daromad_turi_id))
						{
							if($fmodel->daromad_turi_id !=2 and $fmodel->daromad_turi_id !=8 )
								echo Chtml::encode($fmodel->daromad_turi->nomi); echo'<br>';}
						?>
						<?php if(isset($fmodel2->ish_joy_nomi))echo $fmodel2->ish_joy_nomi; ?>
						<?php if(isset($fmodel2->lavozimi))echo $fmodel2->lavozimi; ?>
                </span></b></td>
			<td width="132" valign="top" style="width: 99.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
				<p class="MsoNormal" align="center" style="text-align: center; margin-right: -14.2pt">
					<b><span lang="UZ-CYR" style="font-size: 10pt">
                    <?php if(isset($fmodel2->aliment->nomi)) echo $fmodel2->aliment->nomi; ?>
                </span></b></td>
			<td width="95" valign="top" style="width: 71.3pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
				<p class="MsoNormal" align="center" style="text-align: center; margin-right: -14.2pt">
					<b><span lang="UZ-CYR" style="font-size: 10pt">
                    <?php if(isset($fmodel2->notinch_oila))echo $fmodel2->notinch_oila; ?>
                </span></b></td>
			<td width="169" valign="top" style="width: 126.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
				<p class="MsoNormal" align="center" style="text-align: center; margin-right: -14.2pt">
					<b><span lang="UZ-CYR" style="font-size: 8pt">
                    <?php if(isset($fmodel2->sudlanganligi))echo $fmodel2->sudlanganligi; ?><br>
						<?php if(isset($fmodel2->iib_xisobida_turadimi))echo $fmodel2->iibXisobidaTuradimi->iib_xisobi; ?>
						<br><?php if(isset($fmodel2->deoAzoligi->deo_xolati))echo $fmodel2->deoAzoligi->deo_xolati; ?>
                </span></b></td>
		</tr>
		<?php
	}?>
</table>
<h4 style="text-align: center;">ХОНОДОНДАГИ МАЪНАВИЙ МУХИТ ҲАҚИДА МАЪЛУМОТ</h4>
<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" width="1093" style="width: 100%; border-collapse: collapse; border: medium none">
	<tr>
		<td width="199" style="width: 149.4pt; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Хонодон ба&#1203;оси (яхши, ўрта, &#1179;они&#1179;арли)</span></b></td>
		<td width="120" style="width: 90.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Кутубхона борлиги</span></b></td>
		<td width="120" style="width: 90.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Газeта ва журналларга обуналиги</span></b></td>
		<td width="132" style="width: 99.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Фарзандлар давомади</span></b></td>
		<td width="120" style="width: 90.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Хонадондаги маънавий му&#1203;ит</span></b></td>
		<td width="144" style="width: 108.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Жамоат ишларида иштироки</span></b></td>
		<td width="120" style="width: 90.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Кайфияти</span></b></td>
		<td width="137" style="width: 103.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Бош&#1179;алар</span></b></td>
	</tr>
	<tr>
		<td width="199" style="width: 149.4pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">1</span></b></td>
		<td width="120" style="width: 90.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">2</span></b></td>
		<td width="120" style="width: 90.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">3</span></b></td>
		<td width="132" style="width: 99.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">4</span></b></td>
		<td width="120" style="width: 90.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">5</span></b></td>
		<td width="144" style="width: 108.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">6</span></b></td>
		<td width="120" style="width: 90.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">7</span></b></td>
		<td width="137" style="width: 103.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">8</span></b></td>
	</tr>
	<tr>
		<td width="199" style="width: 149.4pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="font-size: 10.0pt; line-height: 150%">
                <?php if(isset($manaviy->xonadon_bahosi)) { $m=array('1'=>'яхши','2'=>'ўрта','3'=>'қониқарли');  echo $m[$manaviy->xonadon_bahosi];} ?>
                </span></b></td>
		<td width="120" style="width: 90.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="font-size: 10.0pt; line-height: 150%">
                    <?php if(isset($manaviy->kutubxona)) {$k=array('1'=>'китоб ўқиб туришади','2'=>'бор','3'=>'йўқ','4'=>'диний мазмундаги китобларни ўқиб туришида'); echo $k[$manaviy->kutubxona];} ?>
                </span></b></td>
		<td width="120" style="width: 90.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="font-size: 10.0pt; line-height: 150%">
                    <?php if(isset($manaviy->obuna))	{ $O=array('1'=>'йўқ','2'=>'газета ва журналлар ўқиб боришади', '3'=>'бор'); echo $O[$manaviy->obuna]; }?>
                </span></b></td>
		<td width="132" style="width: 99.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="font-size: 10.0pt; line-height: 150%">
                    <?php if(isset($manaviy->farzandi_davomad)) {$d=array('1'=>'Доимий равишда ўқишга боради','2'=>'Кунора ўқишга боради','3'=>'Мунтазам равишда ўқишга қатнашмайди','4'=>'Ўқишда ўқийдиган фарзандлари йўқ'); echo $d[$manaviy->farzandi_davomad];} ?>
                </span></b></td>
		<td width="120" style="width: 90.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="font-size: 10.0pt; line-height: 150%">
                    <?php if(isset($manaviy->manaviy_muhit))echo $manaviy->manaviy_muhit;?>
                </span></b></td>
		<td width="144" style="width: 108.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="font-size: 10.0pt; line-height: 150%">
                    <?php if(isset($manaviy->jamoat_ishlarida_ishtiroki))echo $manaviy->jamoat_ishlarida_ishtiroki;?>
                </span></b></td>
		<td width="120" style="width: 90.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="font-size: 10.0pt; line-height: 150%">
                    <?php if(isset($manaviy->kayfiyat))echo $manaviy->kayfiyat; ?>
                </span></b></td>
		<td width="137" style="width: 103.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="font-size: 10.0pt; line-height: 150%">
                    <?php if(isset($manaviy->muhit_boshqalar))echo $manaviy->muhit_boshqalar; ?>
                </span></b></td>
	</tr>
</table>

<h4 style="text-align: center;">ХОНАДОН АЪЗОЛАРИГА ТРАНСПОРТ ҚАТНОВЛАРИНИНГ ҚУЛАЙЛИГИ ҲАҚИДА МАЪЛУМОТ</h4>
<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" style="border-collapse: collapse; border: medium none; width:100%">
	<tr>
		<td width="95" rowspan="3" valign="top" style="width: 71.35pt; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">Хонадонга</span><span style="font-size: 8pt"> я&#1179;ин бўлган
		транспорт &#1179;атнови йўлигача бўлган масофа (км)</span></span></b></td>
		<td width="112" rowspan="3" valign="top" style="width: 83.65pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">Хонадондан</span><span style="font-size: 8pt"> ма&#1203;алла
		марказига-ча бўлган масофа</span></span></b></td>
		<td width="111" rowspan="3" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">Хонадондан</span><span style="font-size: 8pt"> ма&#1203;алла
		марказига-ча нeчта транспортда борилади</span></span></b></td>
		<td width="111" rowspan="3" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">Хонадондан</span><span style="font-size: 8pt"> туман
		марказига-ча бўлган масофа (км)</span></span></b></td>
		<td width="111" rowspan="3" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">Хонадондан</span><span style="font-size: 8pt"> туман
		марказига-ча нeчта транспортда борилади</span></span></b></td>
		<td width="111" rowspan="3" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">Хонадондан</span><span style="font-size: 8pt"> вилоят
		марказига-ча бўлган масофа (км)</span></span></b></td>
		<td width="111" rowspan="3" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">Хонадондан</span><span style="font-size: 8pt"> вилоят
		марказига-ча нeчта транспортда борилади</span></span></b></td>
		<td width="334" colspan="3" valign="top" style="width: 250.8pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">Йўл</span><span style="font-size: 8pt"> кира ха&#1179;и</span></span></b></td>
	</tr>
	<tr>
		<td width="334" colspan="3" valign="top" style="width: 250.8pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">шундан</span></b></td>
	</tr>
	<tr>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">Ма&#1203;алла маркази-гача</span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">Туман маркази-гача</span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">Вилоят маркази-гача</span></b></td>
	</tr>
	<tr>
		<td width="95" valign="top" style="width: 71.35pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">1</span></b></td>
		<td width="112" valign="top" style="width: 83.65pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">2</span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">3</span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">4</span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">5</span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">6</span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">7</span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">8</span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">9</span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">10</span></b></td>
	</tr>
	<tr>
		<td width="95" valign="top" style="width: 71.35pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">
                    <?php if(isset($transport_qatnovi->xonadonga_yaqin_tran_qatnov_masofa))echo $transport_qatnovi->xonadonga_yaqin_tran_qatnov_masofa;?>
                </span></b></td>
		<td width="112" valign="top" style="width: 83.65pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">
        <?php if(isset($transport_qatnovi->xonadondan_mahalla_markaz_masofa))echo $transport_qatnovi->xonadondan_mahalla_markaz_masofa;?>
                </span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">
                    <?php if(isset($transport_qatnovi->xonadon_mahalla_trans_soni))echo $transport_qatnovi->xonadon_mahalla_trans_soni;?>
                </span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">
                    <?php if(isset($transport_qatnovi->xonadon_tuman_masofa))echo $transport_qatnovi->xonadon_tuman_masofa;?>
                </span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">
                    <?php if(isset($transport_qatnovi->xonadon_tuman_trans_soni))echo $transport_qatnovi->xonadon_tuman_trans_soni;?>
                </span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">
                    <?php if(isset($transport_qatnovi->xonadon_viloyat_masofa))echo $transport_qatnovi->xonadon_viloyat_masofa;?>
                </span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">
                    <?php if(isset($transport_qatnovi->xonadon_viloyat_trans_soni))echo $transport_qatnovi->xonadon_viloyat_trans_soni;?>
                </span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">
			<?php if(isset($transport_qatnovi->yol_kira_mahalla))echo $transport_qatnovi->yol_kira_mahalla;?>
                </span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">
                    <?php if(isset($transport_qatnovi->yol_kira_tuman))echo $transport_qatnovi->yol_kira_tuman;?>
                </span></b></td>
		<td width="111" valign="top" style="width: 83.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">
                    <?php if(isset($transport_qatnovi->yol_kira_viloyat))echo $transport_qatnovi->yol_kira_viloyat;?>
                </span></b></td>
	</tr>
</table>


<h4 style="text-align: center;">ТРАНСПОРТ ВОСИТАЛАРИ ҲАҚИДА МАЪЛУМОТЛАР</h4>
<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" style="border-collapse: collapse; border: medium none; width:100%">
	<tr>
		<td width="104" rowspan="2" style="width: 77.8pt; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">Мавжуд тeхника
		воситалар ва сони</span></span></b></td>
		<td width="447" colspan="4" style="width: 335.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">Eнгил автомашина</span></span></b></td>
		<td width="446" colspan="4" style="width: 334.15pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">Юк автомашинаси ёки
		&#1179;ишло&#1179; хўжалиги тeхникаси</span></span></b></td>
		<td width="102" rowspan="2" style="width: 76.85pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">Бош&#1179;алар</span></b></td>
	</tr>
	<tr>
		<td width="87" style="width: 65.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">Русуми ва давлат ра&#1179;ами</span></b></td>
		<td width="115" style="width: 86.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">Ишлаб чи&#1179;арилган йили ва
		тeхник паспорти</span></b></td>
		<td width="125" style="width: 94.05pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">Eгаси &#1203;а&#1179;ида маълумот ва
		хайдовчилик гувохномаси ра&#1179;ами</span></b></td>
		<td width="119" style="width: 89.05pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">Ишончнома ва су&#1171;урта
		мавжудлиги</span></b></td>
		<td width="86" style="width: 64.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">Русуми ва давлат ра&#1179;ами</span></b></td>
		<td width="115" style="width: 86.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">Ишлаб чи&#1179;арилган йили ва
		тeхник паспорти</span></b></td>
		<td width="125" style="width: 94.05pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">Eгаси &#1203;а&#1179;ида маълумот ва
		хайдовчилик гувохномаси ра&#1179;ами</span></b></td>
		<td width="119" style="width: 89.05pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">Ишончнома ва су&#1171;урта
		мавжудлиги</span></b></td>
	</tr>
	<tr>
		<td width="104" valign="top" style="width: 77.8pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">1</span></b></td>
		<td width="87" valign="top" style="width: 65.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">2</span></b></td>
		<td width="115" valign="top" style="width: 86.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">3</span></b></td>
		<td width="125" valign="top" style="width: 94.05pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">4</span></b></td>
		<td width="119" valign="top" style="width: 89.05pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">5</span></b></td>
		<td width="86" valign="top" style="width: 64.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">6</span></b></td>
		<td width="115" valign="top" style="width: 86.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">7</span></b></td>
		<td width="125" valign="top" style="width: 94.05pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">8</span></b></td>
		<td width="119" valign="top" style="width: 89.05pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">9</span></b></td>
		<td width="102" valign="top" style="width: 76.85pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.45pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">10</span></b></td>
	</tr>
	<tr>
		<td width="104" valign="top" style="width: 77.8pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.45pt">
				<b><span style="font-size: 8pt; line-height: 200%" lang="UZ-CYR">
                    <?php if(isset($xonadon_transport->xonadon_transport_soni)) {echo $xonadon_transport->xonadon_transport_soni; echo' та';}; ?>
                </span></b></td>
		<td width="87" valign="top" style="width: 65.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.45pt">
				<b><span style="font-size: 8pt; line-height: 200%" lang="UZ-CYR">
                    <?php if(isset($xonadon_transport->yengil_avto_rusumi) and trim($xonadon_transport->yengil_avto_rusumi)!='') {echo $xonadon_transport->yengil_avto_rusumi;}; ?>
					<?php if(isset($xonadon_transport->yengil_avto_raqami) and trim($xonadon_transport->yengil_avto_raqami)!='') {echo'<br> Автомашина рақами';echo $xonadon_transport->yengil_avto_raqami; }; ?>
                </span></b></td>
		<td width="115" valign="top" style="width: 86.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.45pt">
				<b><span style="font-size: 8pt; line-height: 200%" lang="UZ-CYR">
                    <?php if(isset($xonadon_transport->yengil_avto_yili)    and trim($xonadon_transport->yengil_avto_yili)!='') { echo'Авто машина йили'; echo $xonadon_transport->yengil_avto_yili;}; ?>
					<?php if(isset($xonadon_transport->yengil_avto_tex_pas) and trim($xonadon_transport->yengil_avto_yili)!='') {echo'Техник пасспорт рақами';echo $xonadon_transport->yengil_avto_tex_pas;}; ?>
                </span></b></td>
		<td width="125" valign="top" style="width: 94.05pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.45pt">
				<b><span style="font-size: 8pt; line-height: 200%" lang="UZ-CYR">
                    <?php if(isset($xonadon_transport->yengi_avto_egasi) 	 and trim($xonadon_transport->yengi_avto_egasi)!='') {echo'Автомашина эгаси'; echo $xonadon_transport->yengi_avto_egasi;}?>
					<?php if(isset($xonadon_transport->yengil_prvara_raqami) and trim($xonadon_transport->yengi_avto_egasi)!='') {echo'<br>Хайдовчилик гувохнома рақами'; echo $xonadon_transport->yengil_prvara_raqami;};?>
                </span></b></td>
		<td width="119" valign="top" style="width: 89.05pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.45pt">
				<b><span style="font-size: 8pt; line-height: 200%" lang="UZ-CYR">
                    <?php if(isset($xonadon_transport->yengil_sugurta_mavjudligi) and trim($xonadon_transport->yengil_sugurta_mavjudligi)!='') {echo'Суғурта:';echo $xonadon_transport->yengil_sugurta_mavjudligi;};?>
					<?php if(isset($xonadon_transport->yengil_ishonchnoma) and trim($xonadon_transport->yengil_ishonchnoma)!='') {echo'<br>Ишончнома:'; echo $xonadon_transport->yengil_ishonchnoma;};?>
                </span></b></td>
		<td width="86" valign="top" style="width: 64.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.45pt">
				<b><span style="font-size: 8pt; line-height: 200%" lang="UZ-CYR">
                    <?php if(isset($xonadon_transport->yuk_avto_rusumi) and trim($xonadon_transport->yuk_avto_rusumi)!='') {echo $xonadon_transport->yuk_avto_rusumi;};?>
					<?php if(isset($xonadon_transport->yuk_avto_raqami) and trim($xonadon_transport->yuk_avto_raqami)!='') {echo'<br>Рақами:';echo $xonadon_transport->yuk_avto_raqami;};?>
                </span></b></td>
		<td width="115" valign="top" style="width: 86.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.45pt">
				<b><span style="font-size: 8pt; line-height: 200%" lang="UZ-CYR">
                    <?php if(isset($xonadon_transport->yuk_avto_yili) and trim($xonadon_transport->yuk_avto_yili)!='') {echo'Юк автомашина йили'; echo $xonadon_transport->yuk_avto_yili;};?>
					<?php if(isset($xonadon_transport->yuk_avto_tex_pas) and trim($xonadon_transport->yuk_avto_tex_pas)!='') {echo'<br>Юк автомашина техник пасспорт рақами';echo $xonadon_transport->yuk_avto_tex_pas;};?>

                </span></b></td>
		<td width="125" valign="top" style="width: 94.05pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.45pt">
				<b><span style="font-size: 8pt; line-height: 200%" lang="UZ-CYR">
                   <?php if(isset($xonadon_transport->yuk_avto_egasi) and trim($xonadon_transport->yuk_avto_egasi)!=''){echo'Юк автомашина эгаси'; echo $xonadon_transport->yuk_avto_egasi;};?>
					<?php if(isset($xonadon_transport->yuk_avto_prva)  and trim($xonadon_transport->yuk_avto_prva)!=''){echo'Юк автомашина эгасининг хайдовилик гувохномаси'; echo $xonadon_transport->yuk_avto_prva;}?>
                </span></b></td>
		<td width="119" valign="top" style="width: 89.05pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.45pt">
				<b><span style="font-size: 8pt; line-height: 200%" lang="UZ-CYR">
                    <?php if(isset($xonadon_transport->yuk_avto_sugurta) and trim($xonadon_transport->yuk_avto_sugurta)!='') {echo'Суғурта'; echo $xonadon_transport->yuk_avto_sugurta;};?>
					<?php if(isset($xonadon_transport->yuk_avto_ishonchnoma) and trim($xonadon_transport->yuk_avto_ishonchnoma)!='') {echo'<br>Ишончнома';echo $xonadon_transport->yuk_avto_ishonchnoma;};?>
                </span></b></td>
		<td width="102" valign="top" style="width: 76.85pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.45pt">
				<b><span style="font-size: 8pt; line-height: 200%" lang="UZ-CYR">
                    <?php echo $xonadon_transport->trans_boshqalar;?>
                </span></b></td>
	</tr>
</table>

<h4 style="text-align: center;">ХОНАДОН АЪЗОЛАРИНИНГ ДАРОМАДИ ВА ПEНСИЯ ОЛИШИ ҲАҚИДА МАЪЛУМОТ</h4>

<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" width="1089" style="width: 100%; border-collapse: collapse; border: medium none">
	<tr>
		<td width="93" rowspan="2" style="width: 69.9pt; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">Кам таъмин-ланган
		оилалар сифатида рўйхатда туриши</span></span></b></td>
		<td width="92" rowspan="2" style="width: 68.9pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">Хонадон-да жами
		ишлаб, иш &#1203;а&#1179;и олувчи-лар сони</span></span></b></td>
		<td width="107" rowspan="2" style="width: 80.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">Хонадонни ўртача
		ойлик даромади (пeнсия, иш &#1203;а&#1179;и томор&#1179;а&nbsp;ва бош&#1179;а даромад-лар)</span></span></b></td>
		<td width="199" colspan="3" style="width: 149.05pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">Пeнсия олувчилар</span></span></b></td>
		<td width="202" colspan="3" style="width: 151.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">Ногиронлик нафа&#1179;аси</span></span></b></td>
		<td width="204" colspan="3" style="width: 153.05pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">Нафа&#1179;а</span></span></b></td>
		<td width="192" colspan="3" style="width: 144.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span lang="UZ-CYR"><span style="font-size: 8pt">2-ёшгача болалар
		пуллари</span></span></b></td>
	</tr>
	<tr style="height: 16.1pt">
		<td width="51" style="width: 38.2pt; height: 16.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align:center"><b>
				<span style="font-size: 8pt; color: black">Сони</span></b></td>
		<td width="84" style="width: 63.0pt; height: 16.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align:center"><b>
				<span style="color: black"><span style="font-size: 8pt">Суммаси</span></span></b></td>
		<td width="64" style="width: 47.85pt; height: 16.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align:center"><b>
				<span style="color: black"><span style="font-size: 8pt">Олиш ва&#1179;ти</span></span></b></td>
		<td width="56" style="width: 42.15pt; height: 16.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align:center"><b>
				<span style="font-size: 8pt; color: black">Сони</span></b></td>
		<td width="84" style="width: 63.0pt; height: 16.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align:center"><b>
				<span style="color: black"><span style="font-size: 8pt">Суммаси</span></span></b></td>
		<td width="62" style="width: 46.45pt; height: 16.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align:center"><b>
				<span style="color: black"><span style="font-size: 8pt">Олиш ва&#1179;ти</span></span></b></td>
		<td width="62" style="width: 46.5pt; height: 16.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align:center"><b>
				<span style="font-size: 8pt; color: black">Сони</span></b></td>
		<td width="80" style="width: 60.05pt; height: 16.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align:center"><b>
				<span style="color: black"><span style="font-size: 8pt">Суммаси</span></span></b></td>
		<td width="62" style="width: 46.5pt; height: 16.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align:center"><b>
				<span style="color: black"><span style="font-size: 8pt">Олиш ва&#1179;ти</span></span></b></td>
		<td width="62" style="width: 46.5pt; height: 16.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align:center"><b>
				<span style="font-size: 8pt; color: black">Сони</span></b></td>
		<td width="68" style="width: 51.0pt; height: 16.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align:center"><b>
				<span style="color: black"><span style="font-size: 8pt">Суммаси</span></span></b></td>
		<td width="62" style="width: 46.5pt; height: 16.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align:center"><b>
				<span style="color: black"><span style="font-size: 8pt">Олиш ва&#1179;ти</span></span></b></td>
	</tr>
	<tr>
		<td width="93" style="width: 69.9pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">1</span></b></td>
		<td width="92" style="width: 68.9pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">2</span></b></td>
		<td width="107" style="width: 80.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">3</span></b></td>
		<td width="51" style="width: 38.2pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">4</span></b></td>
		<td width="84" style="width: 63.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">5</span></b></td>
		<td width="64" style="width: 47.85pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">6</span></b></td>
		<td width="56" style="width: 42.15pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">7</span></b></td>
		<td width="84" style="width: 63.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">8</span></b></td>
		<td width="62" style="width: 46.45pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">9</span></b></td>
		<td width="62" style="width: 46.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">10</span></b></td>
		<td width="80" style="width: 60.05pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">11</span></b></td>
		<td width="62" style="width: 46.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">12</span></b></td>
		<td width="62" style="width: 46.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">13</span></b></td>
		<td width="68" style="width: 51.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">14</span></b></td>
		<td width="62" style="width: 46.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.9pt">
				<b><span style="font-size: 8pt" lang="UZ-CYR">15</span></b></td>
	</tr>
	<tr>
		<td width="93" style="width: 69.9pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: -.9pt">
				<b><span style="font-size: 8pt; line-height: 150%" lang="UZ-CYR">
                    <?php if(isset($dar->kam_taminlangan_sifatida)) {$kk=array('1'=>'Туради','2'=>'Турмайди');echo $kk[$dar->kam_taminlangan_sifatida];}; ?>
                </span></b></td>
		<td width="92" style="width: 68.9pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: -.9pt">
				<b><span style="font-size: 8pt; line-height: 150%" lang="UZ-CYR">
					<?php if(isset($dar->ish_xaq_oluvchi_soni))echo $dar->ish_xaq_oluvchi_soni; ?>
                </span></b></td>
		<td width="107" style="width: 80.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: -.9pt">
				<b><span style="font-size: 8pt; line-height: 150%" lang="UZ-CYR">
					<?php if(isset($dar->oylik_ortacha_daromadi))echo $dar->oylik_ortacha_daromadi; ?>
                </span></b></td>
		<td width="51" style="width: 38.2pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal"	 align="center" style="text-align: center; line-height: 150%; margin-right: -.9pt">
				<b><span style="font-size: 8pt; line-height: 150%" lang="UZ-CYR">
					<?php if(isset($dar->pensiya_oluvchi_soni))echo $dar->pensiya_oluvchi_soni; ?>
                </span></b></td>
		<td width="84" style="width: 63.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: -.9pt">
				<b><span style="font-size: 8pt; line-height: 150%" lang="UZ-CYR">
					<?php if(isset($dar->pensiya_summa))echo $dar->pensiya_summa; ?>
                </span></b></td>
		<td width="64" style="width: 47.85pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: -.9pt">
				<b><span style="font-size: 8pt; line-height: 150%" lang="UZ-CYR">
					<?php if(isset($dar->pensiya_vaqti))echo $dar->pensiya_vaqti; ?>
                </span></b></td>
		<td width="56" style="width: 42.15pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: -.9pt">
				<b><span style="font-size: 8pt; line-height: 150%" lang="UZ-CYR">
					<?php if(isset($dar->nogiron_nafaqa_soni))echo $dar->nogiron_nafaqa_soni; ?>
                </span></b></td>
		<td width="84" style="width: 63.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: -.9pt">
				<b><span style="font-size: 8pt; line-height: 150%" lang="UZ-CYR">
					<?php if(isset($dar->nogiron_summasi))echo $dar->nogiron_summasi; ?>
                </span></b></td>
		<td width="62" style="width: 46.45pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: -.9pt">
				<b><span style="font-size: 8pt; line-height: 150%" lang="UZ-CYR">
					<?php if(isset($dar->nogiron_nafaqa_vaqti))echo $dar->nogiron_nafaqa_vaqti; ?>
                </span></b></td>
		<td width="62" style="width: 46.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: -.9pt">
				<b><span style="font-size: 8pt; line-height: 150%" lang="UZ-CYR">
					<?php if(isset($dar->kamtaminlangan_nafaqachilar_soni))echo $dar->kamtaminlangan_nafaqachilar_soni; ?>
                </span></b></td>
		<td width="80" style="width: 60.05pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: -.9pt">
				<b><span style="font-size: 8pt; line-height: 150%" lang="UZ-CYR">
					<?php if(isset($dar->kamtaminlangan_nafaqa_summasi))echo $dar->kamtaminlangan_nafaqa_summasi; ?>
                </span></b></td>
		<td width="62" style="width: 46.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: -.9pt">
				<b><span style="font-size: 8pt; line-height: 150%" lang="UZ-CYR">
					<?php if(isset($dar->kamtaminlangan_nafaqa_vaqti))echo $dar->kamtaminlangan_nafaqa_vaqti; ?>
                </span></b></td>
		<td width="62" style="width: 46.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: -.9pt">
				<b><span style="font-size: 8pt; line-height: 150%" lang="UZ-CYR">
					<?php if(isset($dar->ikki_yoshgacha_nafaqa_soni))echo $dar->ikki_yoshgacha_nafaqa_soni; ?>
                </span></b></td>
		<td width="68" style="width: 51.0pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: -.9pt">
				<b><span style="font-size: 8pt; line-height: 150%" lang="UZ-CYR">
					<?php if(isset($dar->ikki_yosh_nafaqa_summasi))echo $dar->ikki_yosh_nafaqa_summasi; ?>
                </span></b></td>
		<td width="62" style="width: 46.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%; margin-right: -.9pt">
				<b><span style="font-size: 8pt; line-height: 150%" lang="UZ-CYR">
					<?php if(isset($dar->ikki_yosh_nafaqa_vaqti))echo $dar->ikki_yosh_nafaqa_vaqti; ?>
                </span></b></td>
	</tr>
</table>

<h4 style="text-align: center;">ХОНАДОНДАГИ ИЖТИМОИЙ-МАИШИЙ МУХИТ ҲАҚИДА МАЪЛУМОТЛАР</h4>
<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" style="border-collapse: collapse; border: medium none; width:100%">
	<tr>
		<td style="width: 79px; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Ободончилик &#1203;олати</span></b></td>
		<td style="width: 70px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Хаммом</span></b></td>
		<td style="width: 87px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Ошхона</span></b></td>
		<td style="width: 58px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Тандир</span></b></td>
		<td style="width: 117px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Бино холати</span></b></td>
		<td colspan="2" style="width: 146px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Eлeктр &#1179;уввати ва &#1203;исоблагич</span></b></td>
		<td colspan="2" style="width: 184px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Табиий газ ва &#1203;исоблагич</span></b></td>
		<td width="261" colspan="2" style="width: 196.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR">Ичимлик суви тортилгани ва &#1203;исоблагич (бўлмаса &#1179;анча
		масофадан ташийди)</span></b></td>
	</tr>
	<tr>
		<td style="width: 79px; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">1</span></b></td>
		<td style="width: 70px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">2</span></b></td>
		<td style="width: 87px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">3</span></b></td>
		<td style="width: 58px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">4</span></b></td>
		<td style="width: 117px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">5</span></b></td>
		<td style="width: 66px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">6</span></b></td>
		<td style="width: 66px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">7</span></b></td>
		<td style="width: 82px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">8</span></b></td>
		<td style="width: 85px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">9</span></b></td>
		<td width="142" style="width: 106.3pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">10</span></b></td>
		<td width="120" style="width: 89.8pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">11</span></b></td>
	</tr>
	<tr>
		<td style="width: 79px; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
					<?php if(isset($im->obodonchilik)) {$obod=array('1'=>'яхши','2'=>'ўртача', '3'=>'қониқарсиз');echo $obod[$im->obodonchilik];} ?>
                </span></b></td>
		<td style="width: 70px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
					<?php if(isset($im->hammom)) {$h=array('1'=>'бор','2'=>'йўқ');echo $h[$im->hammom];}; ?>
                </span></b></td>
		<td style="width: 87px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
					<?php if(isset($im->oshxona)){$osh=array('1'=>'бор','2'=>'йўқ');echo $osh[$im->oshxona];} ?>
                </span></b></td>
		<td style="width: 58px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
					<?php if(isset($im->tandir)) {$t=array('1'=>'бор','2'=>'йўқ');echo $t[$im->tandir];}; ?>
                </span></b></td>
		<td style="width: 117px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
					<?php if(isset($im->bino_xolati_id)) {$b=array('1'=>'бор','2'=>'йўқ' ,'3'=>'ёмон'); echo $b[$im->bino_xolati_id];}; ?>
                </span></b></td>
		<td style="width: 66px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
				<?php if(isset($im->elektr_hisoblagich_mavjudligi)){$e=array('1'=>'бор','2'=>'йўқ');echo $e[$im->elektr_hisoblagich_mavjudligi];} ?>
                </span></b></td>
		<td style="width: 66px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
					<?php if(isset($im->elektr_hisoblagich_raqami))echo $im->elektr_hisoblagich_raqami; ?>
                </span></b></td>
		<td style="width: 82px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
				 <?php if(isset($im->gazMavjudligi->gaz_turi)) echo $im->gazMavjudligi->gaz_turi;?>
					<?php if($im->gaz_mavjudligi_id = 3)
				{
					if(isset($im->gaz_ballon_soni) and trim($im->gaz_ballon_soni!=''))  {echo 'Пропан газ баллон сони';  echo  $im->gaz_ballon_soni; echo '   та';};
				}
					if(isset($im->gaz_hisoblagich_mavjudligii) and trim($im->gaz_hisoblagich_mavjudligii!='2')) {$g=array('1'=>'бор','2'=>'йўқ');echo $g[$im->gaz_hisoblagich_mavjudligii];}
					?>
				</span></b></td>
		<td style="width: 85px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
	<?php if(isset($im->gaz_hisoblagichi_raqami))echo $im->gaz_hisoblagichi_raqami; ?>
                </span></b></td>
		<td width="142" style="width: 106.3pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
					<?php if(isset($im->ichimlik_suvi_mavjud_hisoblagich_mavjudligi)) {$ii=array('1'=>'Ичимлик суви хонадонга тортилган, хисоблагич мавжуд','2'=>'Ичимлик суви хонадонга тортилган, хисоблагич мавжуд эмас','3'=>'Ичимлик суви хонадонга тортилмаган'); echo $ii[$im->ichimlik_suvi_mavjud_hisoblagich_mavjudligi];} ?>
                </span></b></td>
		<td width="120" style="width: 89.8pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
					<?php if(isset($im->ichimlik_suvi_mavjud_masofa))echo $im->ichimlik_suvi_mavjud_masofa; ?>
					</span></b></td>
	</tr>
</table>

<h4 style="text-align: center;">СОЛИҚ, СЧР ВА КОММУНАЛ ХИЗМАТЛАРДАН БЎЛГАН ҚАРЗДОРЛИК ТЎҒРИСИДА МАЪЛУМОТЛАР</h4>
<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" style="border-collapse: collapse; border: medium none; width:100%">
	<tr>
		<td width="122" style="width: 91.5pt; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">Умумий &#1179;арздорлик суммаси</span></b></td>
		<td width="122" style="width: 91.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">Eлeктр &#1179;уввати</span></b></td>
		<td width="122" style="width: 91.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">Ичимлик суви</span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">Табиий газ</span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">Eр-мулк соли&#1171;и</span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">СЧР</span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">Чи&#1179;инди</span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">Алимeнт</span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">Бош&#1179;алар</span></b></td>
	</tr>
	<tr>
		<td width="122" style="width: 91.5pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">1</span></b></td>
		<td width="122" style="width: 91.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">2</span></b></td>
		<td width="122" style="width: 91.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">3</span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">4</span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">5</span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">6</span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">7</span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">8</span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; margin-right: -.3pt">
				<b><span lang="UZ-CYR">9</span></b></td>
	</tr>
	<tr>
		<td width="122" style="width: 91.5pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.3pt">
				<b><span lang="UZ-CYR" style="line-height: 200%">

	<?php if(isset($qarzs->umumiy_qarz)) echo $qarzs->umumiy_qarz;?>
                </span></b></td>
		<td width="122" style="width: 91.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.3pt">
				<b><span lang="UZ-CYR" style="line-height: 200%">
					<?php if(isset($qarzs->eletr_qarz)) echo $qarzs->eletr_qarz;?>
                </span></b></td>
		<td width="122" style="width: 91.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.3pt">
				<b><span lang="UZ-CYR" style="line-height: 200%">
					<?php // $fuqaro->qarzs->ichimlik_suvi;?>
                </span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.3pt">
				<b><span lang="UZ-CYR" style="line-height: 200%">
					<?php if(isset($qarzs->gaz_qarz)) echo $qarzs->gaz_qarz;?>
                </span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.3pt">
				<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($qarzs->soliq_yer_qarz) and trim($qarzs->soliq_yer_qarz)!=''){echo' Ер солиғи <br>'; echo $qarzs->soliq_yer_qarz;};?>
					<?php if(isset($qarzs->soliq_mulk_qarz) and trim($qarzs->soliq_mulk_qarz)!='') {echo '<br>Мулк солиғи<br>'; echo $qarzs->soliq_mulk_qarz;}?>
                </span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.3pt">
				<b><span lang="UZ-CYR" style="line-height: 200%">
					<?php if(isset($qarzs->schr_qarz)) echo $qarzs->schr_qarz;?>
                </span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.3pt">
				<b><span lang="UZ-CYR" style="line-height: 200%">
					<?php if(isset($qarzs->chiqindi_qarz))echo $qarzs->chiqindi_qarz;?>
                </span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.3pt">
				<b><span lang="UZ-CYR" style="line-height: 200%">
					<?php if(isset($qarzs->aliment_qarz))echo $qarzs->aliment_qarz;?>
                </span></b></td>
		<td width="122" style="width: 91.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%; margin-right: -.3pt">
				<b><span lang="UZ-CYR" style="line-height: 200%">
					<?php if(isset($qarzs->qarz_boshqalar))echo $qarzs->qarz_boshqalar;?>
                </span></b></td>
	</tr>
</table>

<h4 style="text-align: center;">ТОМОРҚАДАН ФОЙДАЛАНИШ ҲАҚИДА МАъЛУМОТЛАР</h4>
<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" width="1099" style="width: 100%; border-collapse: collapse; border: medium none">
	<tr>
		<td width="33" rowspan="3" style="width: 24.9pt; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">№</span></b></td>
		<td rowspan="3" style="width: 93px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Умумий ер майдони (га)</span></b></td>
		<td rowspan="3" style="width: 73px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Eкин майдони (га)</span></b></td>
		<td rowspan="3" style="width: 80px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Eрнинг &#1203;олати</span></b></td>
		<td rowspan="3" style="width: 107px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Исси&#1179;хона мавжудлиги</span></b></td>
		<td colspan="2" style="width: 317px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Исси&#1179;хонага eкилган кўчатлар номи ва сони</span></b></td>
		<td rowspan="3" style="width: 294px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR">Томор&#1179;адан фойдаланиш &#1203;олати (хайдалган, чопилган,
		бeгона ўтлардан тозаланганми)</span></b></td>
	</tr>
	<tr>
		<td colspan="2" style="width: 317px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Шундан</span></b></td>
	</tr>
	<tr>
		<td style="width: 223px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">номи</span></b></td>
		<td style="width: 82px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">сони</span></b></td>
	</tr>
	<tr>
		<td width="33" style="width: 24.9pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">1</span></b></td>
		<td style="width: 93px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">2</span></b></td>
		<td style="width: 73px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">3</span></b></td>
		<td style="width: 80px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">4</span></b></td>
		<td style="width: 107px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">5</span></b></td>
		<td style="width: 223px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">6</span></b></td>
		<td style="width: 82px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">7</span></b></td>
		<td style="width: 294px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">8</span></b></td>
	</tr>
	<tr>
		<td width="33" style="width: 24.9pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">&nbsp;</span></b></td>
		<td style="width: 93px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
                <span lang="UZ-CYR">
					<?php if(isset($tomorqas->umumiy_yer_maydoni))echo $tomorqas->umumiy_yer_maydoni; ?>
                </span></b></td>
		<td style="width: 73px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
                <span lang="UZ-CYR">
					<?php if(isset($tomorqas->yer_ekin_maydoni)) echo  $tomorqas->yer_ekin_maydoni; ?>
                </span></b></td>
		<td style="width: 80px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
                <span lang="UZ-CYR">
					<?php if(isset($tomorqas->yer_ekin_xolati))
				{
					$yer=array('1'=>'экинлар экилган, бегона ўтлардан тозаланган(яхши)','2'=>'экинлар экилган, бегона ўтлардан тозаланмаган(ўртача)','3'=>'экинлар экилмаган(қониқарсиз)','4'=>'экинлар экилмаган ва 					тозаланмаган(ёмон)', '5'=>'Томорқа мавжуд эмас');  echo $yer[$tomorqas->yer_ekin_xolati];} ?>
                </span></b></td>
		<td style="width: 107px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
                <span lang="UZ-CYR">
					<?php if(isset($tomorqas->issiq_xona_mavjud_id))echo $tomorqas->issiqXonaMavjud->issiqxona_turi; ?>
                </span></b></td>
		<td style="width: 223px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
                <span lang="UZ-CYR">
					<?php if(isset($tomorqas->issiq_xona_ekin_nomi_soni))echo $tomorqas->issiq_xona_ekin_nomi_soni; ?>
                </span></b></td>
		<td style="width: 82px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
                <span lang="UZ-CYR">
					<?php if(isset($tomorqas->issiq_xona_ekin_nomi_soni))echo $tomorqas->issiq_xona_ekin_nomi_soni; ?>
                </span></b></td>
		<td style="width: 294px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
                <span lang="UZ-CYR">
					<?php if(isset($tomorqas->yer_foy_dalanish_xolati))echo $tomorqas->yer_foy_dalanish_xolati; ?>
                </span></b></td>
	</tr>
</table>

<table border="1" width="100%">
	<tr>
		<td rowspan="2" width="8%" align="center" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">Экилган экинлар тури ва майдони</font></td>
		<td colspan="15" align="center" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">Шундан</font></td>
	</tr>
	<tr>
		<td align="center" width="7%" style="border-right-style: solid; border-right-width: 1px; border-top-style: solid; border-top-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">
			<font face="Times New Roman">Картошка</font></td>
		<td align="center" width="4%" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">
			<font face="Times New Roman">Пиёз</font></td>
		<td align="center" width="7%" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">
			<font face="Times New Roman">Саримсо&#1179;</font></td>
		<td align="center" width="4%" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">
			<font face="Times New Roman">Сабзи</font></td>
		<td align="center" width="6%" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">
			<font face="Times New Roman">Помидор</font></td>
		<td align="center" width="6%" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">
			<font face="Times New Roman">Бодиринг</font></td>
		<td align="center" width="9%" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">
			<font face="Times New Roman">Бул&#1171;ари &#1179;алампир</font></td>
		<td align="center" width="7%" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">
			<font face="Times New Roman">Ба&#1179;лажон</font></td>
		<td align="center" width="5%" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">
			<font face="Times New Roman">Ловия</font></td>
		<td align="center" width="4%" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">
			<font face="Times New Roman">Мош</font></td>
		<td align="center" width="6%" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">
			<font face="Times New Roman">Макка жўхори</font></td>
		<td align="center" width="4%" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">
			<font face="Times New Roman">Беда</font></td>
		<td align="center" width="5%" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">
			<font face="Times New Roman">Тамаки</font></td>
		<td align="center" width="7%" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">
			<font face="Times New Roman">Кўкатлар</font></td>
		<td align="center" width="7%" style="border-left-style: solid; border-left-width: 1px; border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">
			<font face="Times New Roman">Бош&#1179;алар</font></td>
	</tr>
	<tr>
		<td width="8%" align="center" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">9</font></td>
		<td align="center" width="7%" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">10</font></td>
		<td align="center" width="4%" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">11</font></td>
		<td align="center" width="7%" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">12</font></td>
		<td align="center" width="4%" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">13</font></td>
		<td align="center" width="6%" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">14</font></td>
		<td align="center" width="6%" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">15</font></td>
		<td align="center" width="9%" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">16</font></td>
		<td align="center" width="7%" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">17</font></td>
		<td align="center" width="5%" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">18</font></td>
		<td align="center" width="4%" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">19</font></td>
		<td align="center" width="6%" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">20</font></td>
		<td align="center" width="4%" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">21</font></td>
		<td align="center" width="5%" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">22</font></td>
		<td align="center" width="7%" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">23</font></td>
		<td align="center" width="7%" style="border-style: solid; border-width: 1px">
			<font face="Times New Roman">24</font></td>
	</tr>
	<tr>
		<td width="8%" style="border-style: solid; border-width: 1px">
			<?php if(isset($tomorqas->ekilgan_ekin_maydoni)) echo  $tomorqas->ekilgan_ekin_maydoni; ?>
		</td>
		<td width="7%" style="border-style: solid; border-width: 1px">
			<?php if(isset($tomorqas->kartoshka)) echo  $tomorqas->kartoshka; ?>
		</td>
		<td width="4%" style="border-style: solid; border-width: 1px">
			<?php if(isset($tomorqas->piyoz)) echo  $tomorqas->piyoz; ?>
		</td>
		<td width="7%" style="border-style: solid; border-width: 1px">
			<?php if(isset($tomorqas->sarimsoq)) echo  $tomorqas->sarimsoq; ?>
		</td>
		<td width="4%" style="border-style: solid; border-width: 1px">
			<?php if(isset($tomorqas->sabzi)) echo  $tomorqas->sabzi; ?>
		</td>
		<td width="6%" style="border-style: solid; border-width: 1px">
			<?php if(isset($tomorqas->pomidor)) echo  $tomorqas->pomidor; ?>
		</td>
		<td width="7%" style="border-style: solid; border-width: 1px">
			<?php if(isset($tomorqas->bodiring)) echo  $tomorqas->bodiring; ?>
		</td>
		<td width="9%" style="border-style: solid; border-width: 1px">
			<?php if(isset($tomorqas->balgariskiy)) echo  $tomorqas->balgariskiy; ?>
		</td>
		<td width="5%" style="border-style: solid; border-width: 1px">
			<?php if(isset($tomorqas->baqlajon)) echo  $tomorqas->baqlajon; ?>
		</td>
		<td width="4%" style="border-style: solid; border-width: 1px">
			<?php if(isset($tomorqas->loviya)) echo  $tomorqas->loviya; ?>
		</td>
		<td width="6%" style="border-style: solid; border-width: 1px">
			<?php if(isset($tomorqas->mosh)) echo  $tomorqas->mosh; ?>
		</td>
		<td width="6%" style="border-style: solid; border-width: 1px">
			<?php if(isset($tomorqas->makka)) echo  $tomorqas->makka; ?>
		</td>
		<td width="4%" style="border-style: solid; border-width: 1px">
			<?php if(isset($tomorqas->beda)) echo  $tomorqas->beda; ?>
		</td>
		<td width="5%" style="border-style: solid; border-width: 1px">
			<?php if(isset($tomorqas->tamaki)) echo  $tomorqas->tamaki; ?>
		</td>
		<td width="7%" style="border-style: solid; border-width: 1px">
			<?php if(isset($tomorqas->kokat)) echo  $tomorqas->kokat; ?>
		</td>
		<td width="7%" style="border-style: solid; border-width: 1px">
			<?php if(isset($tomorqas->kokat)) echo  $tomorqas->kokat; ?>
		</td>
	</tr>
</table>

<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" style="border-collapse: collapse; border: medium none;width:100%">
<tr>
	<td width="102" rowspan="3" style="width: 76.25pt; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">Eкилан мeва ва &#1179;урилиш дарахтлар сони ва номи</span></b></td>
	<td width="997" colspan="16" style="width: 747.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">Шундан</span></b></td>
</tr>
<tr>
	<td width="125" colspan="2" style="width: 93.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ён&#1171;о&#1179;</span></b></td>
	<td width="125" colspan="2" style="width: 93.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">Унаби (жида)</span></b></td>
	<td width="125" colspan="2" style="width: 93.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">узум</span></b></td>
	<td width="125" colspan="2" style="width: 93.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">шафтоли</span></b></td>
	<td width="125" colspan="2" style="width: 93.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">олма</span></b></td>
	<td width="125" colspan="2" style="width: 93.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">анор</span></b></td>
	<td width="125" colspan="2" style="width: 93.45pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">нок</span></b></td>
	<td width="125" colspan="2" style="width: 93.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ўрик</span></b></td>
</tr>
<tr>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">сони</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ёши</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">сони</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ёши</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">сони</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ёши</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">сони</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ёши</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">сони</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ёши</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">сони</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ёши</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">сони</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ёши</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">сони</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ёши</span></b></td>
</tr>
<tr>
	<td width="102" style="width: 76.25pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">1</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">2</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">3</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">4</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">5</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">6</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">7</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">8</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">9</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">10</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">11</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">12</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">13</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">14</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">15</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">16</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">17</span></b></td>
</tr>
<tr>
	<td width="102" style="width: 76.25pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->ekilgan_meva_soni))echo $tomorqas->ekilgan_meva_soni;?>
            </span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->yongoq_soni))echo $tomorqas->yongoq_soni;?>
            </span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->yongoq_yoshi))echo $tomorqas->yongoq_yoshi;?>
            </span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->jiyda_soni))echo $tomorqas->jiyda_soni;?>
            </span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->jiyda_yoshi))echo $tomorqas->jiyda_yoshi;?>
            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->jiyda_yoshi))echo $tomorqas->uzum_soni;?>
            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->uzum_yoshi))echo $tomorqas->uzum_yoshi;?>
            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->shaftoli_soni))echo $tomorqas->shaftoli_soni;?>
            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->shaftoli_yoshi))echo $tomorqas->shaftoli_yoshi;?>
            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->olma_soni))echo $tomorqas->olma_soni;?>
            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->olm_yoshi))echo $tomorqas->olm_yoshi;?>
            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->olm_yoshi))echo $tomorqas->anor_soni;?>
            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->anor_yoshi))echo $tomorqas->anor_yoshi;?>
            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->nok_soni))echo $tomorqas->nok_soni;?>
            </span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->nok_yoshi))echo $tomorqas->nok_yoshi;?>
            </span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->orik_soni))echo $tomorqas->orik_soni;?></span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->orik_yoshi))echo $tomorqas->orik_yoshi;?>
            </span></b></td>
</tr>
<tr>
	<td width="102" rowspan="3" style="width: 76.25pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">Eкилан мeва ва &#1179;урилиш дарахтлар сони ва номи</span></b></td>
	<td width="997" colspan="16" style="width: 747.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">Шундан</span></b></td>
</tr>
<tr>
	<td width="125" colspan="2" style="width: 93.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">анжир</span></b></td>
	<td width="125" colspan="2" style="width: 93.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">бeхи</span></b></td>
	<td width="125" colspan="2" style="width: 93.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">гилос</span></b></td>
	<td width="125" colspan="2" style="width: 93.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">слива</span></b></td>
	<td width="125" colspan="2" style="width: 93.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">хурмо</span></b></td>
	<td width="125" colspan="2" style="width: 93.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">бодом</span></b></td>
	<td width="125" colspan="2" style="width: 93.45pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">тeрак</span></b></td>
	<td width="125" colspan="2" style="width: 93.5pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">Тол</span></b></td>
</tr>
<tr>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">сони</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ёши</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">сони</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ёши</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">сони</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ёши</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">сони</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ёши</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">сони</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ёши</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">сони</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ёши</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">сони</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ёши</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">сони</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">ёши</span></b></td>
</tr>
<tr>
	<td width="102" style="width: 76.25pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">&nbsp;</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">18</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">19</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">20</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">21</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">22</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">23</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">24</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">25</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">26</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">27</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">28</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">29</span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">30</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">31</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">32</span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center"><b>
			<span lang="UZ-CYR">33</span></b></td>
</tr>
<tr>
	<td width="102" style="width: 76.25pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">


            </span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">

				<?php if(isset($tomorqas->anjir_soni))echo $tomorqas->anjir_soni;?>
            </span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->anjir_soni))echo $tomorqas->anjiir_yoshi;?>

            </span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->bexi_soni))echo $tomorqas->bexi_soni;?>

            </span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->bexi_yoshi))echo $tomorqas->bexi_yoshi;?>
            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->gilos_soni))echo $tomorqas->gilos_soni;?>

            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
			<?php if(isset($tomorqas->gilos_yoshi))echo $tomorqas->gilos_yoshi;?>

            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->sliva_soni))echo $tomorqas->sliva_soni;?>

            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->sliva_yoshi))echo $tomorqas->sliva_yoshi;?>
            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->humro_soni))echo $tomorqas->humro_soni;?>
            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->hurmo_yoshi))echo $tomorqas->hurmo_yoshi;?>
            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->bodom_soni))echo $tomorqas->bodom_soni;?>
            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->bodom_yoshi))echo $tomorqas->bodom_yoshi;?>
            </span></b></td>
	<td width="62" style="width: 46.7pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
		<?php if(isset($tomorqas->bodom_yoshi))echo $tomorqas->bodom_yoshi;?>
            </span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->terak_yoshi))echo $tomorqas->terak_yoshi;?>
            </span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
				<?php if(isset($tomorqas->tol_soni))echo $tomorqas->tol_soni;?>
            </span></b></td>
	<td width="62" style="width: 46.75pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
		<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
			<b><span lang="UZ-CYR" style="line-height: 200%">
<?php if(isset($tomorqas->tol_yoshi))echo $tomorqas->tol_yoshi;?>
				<?php if(isset($tomorqas->boshqa_ekinlar))echo $tomorqas->boshqa_ekinlar;?>
            </span></b></td>
</tr>
</table>
<h4 style="text-align: center;">ЧОРВА ҲАЙВОНЛАРИ ВА ПАРРАНДАЛАР ТЎҒРИСИДА МАЪЛУМОТЛАР</h4>
<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" style="border-collapse: collapse; border: medium none;" width="100%">
	<tr>
		<td width="91" style="width: 68.4pt; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Хайвон тури ва жами сони</span></b></td>
		<td width="91" style="width: 67.9pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">&#1178;ора мол</span></b></td>
		<td width="90" style="width: 67.65pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">&#1178;ўй</span></b></td>
		<td width="91" style="width: 67.9pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Eчки</span></b></td>
		<td width="90" style="width: 67.45pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">От</span></b></td>
		<td width="91" style="width: 68.2pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Кучук (ит) ёки мушук</span></b></td>
		<td width="90" style="width: 67.85pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">&#1178;уён</span></b></td>
		<td width="91" style="width: 68.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Тову&#1179;</span></b></td>
		<td width="91" style="width: 68.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Курка</span></b></td>
		<td width="91" style="width: 68.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Ўрдак</span></b></td>
		<td width="90" style="width: 67.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">&#1170;оз</span></b></td>
		<td width="102" style="width: 76.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Бош&#1179;алар</span></b></td>
	</tr>
	<tr>
		<td width="91" style="width: 68.4pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">1</span></b></td>
		<td width="91" style="width: 67.9pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">2</span></b></td>
		<td width="90" style="width: 67.65pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">3</span></b></td>
		<td width="91" style="width: 67.9pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">4</span></b></td>
		<td width="90" style="width: 67.45pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">5</span></b></td>
		<td width="91" style="width: 68.2pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">6</span></b></td>
		<td width="90" style="width: 67.85pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">7</span></b></td>
		<td width="91" style="width: 68.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">8</span></b></td>
		<td width="91" style="width: 68.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">9</span></b></td>
		<td width="91" style="width: 68.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">10</span></b></td>
		<td width="90" style="width: 67.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">11</span></b></td>
		<td width="102" style="width: 76.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">12</span></b></td>
	</tr>
	<tr>
		<td width="91" style="width: 68.4pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
					<?php if(isset($hayvons->hayvon_soni)) echo $hayvons->hayvon_soni; ?>
                </span></b></td>
		<td width="91" style="width: 67.9pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
					<?php if(isset($hayvons->qora_mol)) echo $hayvons->qora_mol; ?>
                </span></b></td>
		<td width="90" style="width: 67.65pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
					<?php if(isset($hayvons->qoy)) echo $hayvons->qoy; ?>
                </span></b></td>
		<td width="91" style="width: 67.9pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
					<?php if(isset($hayvons->echki)) echo $hayvons->echki; ?>
                </span></b></td>
		<td width="90" style="width: 67.45pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
					<?php if(isset($hayvons->ot)) echo $hayvons->ot; ?>
                </span></b></td>
		<td width="91" style="width: 68.2pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
					<?php if(isset($hayvons->kuchuk) and trim($hayvons->kuchuk!='')) {echo 'Итлар сони '; echo $hayvons->kuchuk; echo 'та';}; ?>
					<?php if(isset($hayvons->mushuk) and trim($hayvons->mushuk!='')) {echo 'Мушуклари сони'; echo $hayvons->mushuk;  echo'та';};?>
                </span></b></td>
		<td width="90" style="width: 67.85pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
					<?php if(isset($hayvons->quyon)) echo $hayvons->quyon; ?>
                </span></b></td>
		<td width="91" style="width: 68.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
										<?php if(isset($hayvons->tovuq)) echo $hayvons->tovuq; ?>
                </span></b></td>
		<td width="91" style="width: 68.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
					<?php if(isset($hayvons->kurka)) echo $hayvons->kurka; ?>
                </span></b></td>
		<td width="91" style="width: 68.1pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
										<?php if(isset($hayvons->ordak)) echo $hayvons->ordak; ?>
                </span></b></td>
		<td width="90" style="width: 67.55pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
										<?php if(isset($hayvons->goz)) echo $hayvons->goz; ?>
                </span></b></td>
		<td width="102" style="width: 76.6pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%">
				<b><span lang="UZ-CYR" style="line-height: 150%">
										<?php if(isset($hayvons->boshqalar)) echo $hayvons->boshqalar; ?>
                </span></b></td>
	</tr>
</table>

<h4 style="text-align: center;">ХОНАДОНДАГИ ШАРТ-ШАРОИТЛАР ТЎҒРИСИДА МАЪЛУМОТЛАР</h4>
<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" style="border-collapse: collapse; border: medium none" width="100%">
	<tr>
		<td width="110" style="width: 82.35pt; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Мавжуд воситалар ва сони</span></b></td>
		<td width="110" style="width: 82.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Тeлeвизор</span></b></td>
		<td width="110" style="width: 82.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Кир ювиш воситаси</span></b></td>
		<td width="110" style="width: 82.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Совуткич</span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="EN-US">ДВД</span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Кондитсонeр</span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Компиютeр</span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Интeрнeт тармо&#1171;и</span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Дазмол</span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">Бош&#1179;алар</span></b></td>
	</tr>
	<tr>
		<td width="110" style="width: 82.35pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">1</span></b></td>
		<td width="110" style="width: 82.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">2</span></b></td>
		<td width="110" style="width: 82.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">3</span></b></td>
		<td width="110" style="width: 82.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">4</span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">5</span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">6</span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">7</span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">8</span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">9</span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR">10</span></b></td>
	</tr>
	<tr>
		<td width="110" style="width: 82.35pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="line-height: 200%">
					<?php if(isset($shaoritis->vosita_soni))echo $shaoritis->vosita_soni; ?>
                </span></b></td>
		<td width="110" style="width: 82.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="line-height: 200%">
					<?php if(isset($shaoritis->vosita_soni))echo $shaoritis->televizor; ?>
                </span></b></td>
		<td width="110" style="width: 82.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="line-height: 200%">
						<?php if(isset($shaoritis->kir_yuvish_vositasi))echo $shaoritis->kir_yuvish_vositasi; ?>
                </span></b></td>
		<td width="110" style="width: 82.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="line-height: 200%">
					<?php if(isset($shaoritis->sovutgich))echo $shaoritis->sovutgich; ?>
                </span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="line-height: 200%">
					<?php if(isset($shaoritis->DVD))echo $shaoritis->DVD; ?>
                </span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="line-height: 200%">
					<?php if(isset($shaoritis->Konditsioner))echo $shaoritis->Konditsioner; ?>
                </span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="line-height: 200%">
										<?php if(isset($shaoritis->kompyuter))echo $shaoritis->kompyuter; ?>
                </span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="line-height: 200%">
										<?php if(isset($shaoritis->internet))echo $shaoritis->internet; ?>
                </span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="line-height: 200%">
										<?php if(isset($shaoritis->dazmol))echo $shaoritis->dazmol; ?>
                </span></b></td>
		<td width="110" style="width: 82.4pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="line-height: 200%">
										<?php if(isset($shaoritis->vosita_boshqalar))echo $shaoritis->vosita_boshqalar; ?>
                </span></b></td>
	</tr>
</table>
<br>
<br>
<br>
<h4 style="text-align: center;">ХОНАДОНГА ИЖТИМОИЙ ВА МАИШИЙ ХИЗМАТ КЎРСАТИШ ОБЪEКТЛАРИНИ МАСОФАСИ ҲАҚИДА
	МАЪЛУМОТЛАР
</h4>
<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" style="border-collapse: collapse; border: medium none;" width="100%">
	<tr>
		<td width="116" style="width: 87.35pt; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span style="font-size: 8pt">Хонадонга я<span lang="UZ-CYR">&#1179;</span>ин
		</span><span style="font-size: 8pt" lang="UZ-CYR">мактаб объeктигача
		бўлган масофа (мактаб ра&#1179;ами)</span></b></td>
		<td width="116" style="width: 87.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR"><span style="font-size: 8pt">Хонадонга я&#1179;ин бо&#1171;ча
		объeктигача бўлган масофа (бо&#1171;ча ра&#1179;ами)</span></span></b></td>
		<td width="116" style="width: 87.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR"><span style="font-size: 8pt">Хонадонга я&#1179;ин коллeж
		объeктигача бўлган масофа (коллeж номи)</span></span></b></td>
		<td style="width: 93px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR"><span style="font-size: 8pt">Хонадонга я&#1179;ин савдо
		объeктигача бўлган масофа</span></span></b></td>
		<td style="width: 80px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR"><span style="font-size: 8pt">Хонадонга я&#1179;ин хизмат
		кўрсатиш объeктигача бўлган масофа</span></span></b></td>
		<td style="width: 95px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR"><span style="font-size: 8pt">Хонадонга я&#1179;ин
		хаммом-гача бўлган масофа</span></span></b></td>
		<td style="width: 98px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR"><span style="font-size: 8pt">Хонадонга я&#1179;ин
		бозоргача бўлган масофа</span></span></b></td>
		<td style="width: 85px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR"><span style="font-size: 8pt">Хонадонга я&#1179;ин спорт
		мажмуаси-гача бўлган масофа</span></span></b></td>
		<td style="width: 98px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR"><span style="font-size: 8pt">Хонадонга я&#1179;ин муси&#1179;а,
		тил ва бош&#1179;а тўгараклар-гача бўлган масофа</span></span></b></td>
		<td style="width: 126px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span style="font-size: 8pt">Хонадонга я<span lang="UZ-CYR">&#1179;</span>ин
		</span><span lang="UZ-CYR"><span style="font-size: 8pt">маданият ва
		истирохат бо&#1171;игача бўлган масофа</span></span></b></td>
	</tr>
	<tr>
		<td width="116" style="width: 87.35pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">1</span></b></td>
		<td width="116" style="width: 87.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">2</span></b></td>
		<td width="116" style="width: 87.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">3</span></b></td>
		<td style="width: 93px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">4</span></b></td>
		<td style="width: 80px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">5</span></b></td>
		<td style="width: 95px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">6</span></b></td>
		<td style="width: 98px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">7</span></b></td>
		<td style="width: 85px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">8</span></b></td>
		<td style="width: 98px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">9</span></b></td>
		<td style="width: 126px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">10</span></b></td>
	</tr>
	<tr>
		<td width="116" style="width: 87.35pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
        <?php if(isset($xiz->xondonga_yaqin_maktab_id)) {echo $xiz->xondongaYaqinMaktab->maktab_raqami;} ?><br>
					<?php if(isset($xiz->xondonga_yaqin_maktab_masofa)) echo $xiz->xondonga_yaqin_maktab_masofa; ?>
                </span></b></td>
		<td width="116" style="width: 87.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
					<?php if(isset($xiz->xonadonga_yaqin_bogcha_id)) echo $xiz->xonadongaYaqinBogcha->bogcha_raqami;?><br>
                    <?php if(isset($xiz->xonadonga_yaqin_bogcha_masofa)) echo $xiz->xonadonga_yaqin_bogcha_masofa; ?>
                </span></b></td>
		<td width="116" style="width: 87.35pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
                    <?php if(isset($xiz->xonadonga_yaqin_kollej_nomi)) echo $xiz->xonadonga_yaqin_kollej_nomi;?>
                    <?php if(isset($xiz->xonadonga_yaqin_kollej_masofa)) echo $xiz->xonadonga_yaqin_kollej_masofa;?>
                </span></b></td>
		<td style="width: 93px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
                    <?php if(isset($xiz->xonadonga_yaqin_savdo_obyekt_masofa)) echo $xiz->xonadonga_yaqin_savdo_obyekt_masofa; ?>
                </span></b></td>
		<td style="width: 80px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
            <?php if(isset($xiz->xonadonga_yaqin_xizmat_korsatish_masofa)) echo $xiz->xonadonga_yaqin_xizmat_korsatish_masofa; ?>
                </span></b></td>
		<td style="width: 95px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
            <?php if(isset($xiz->xonadonga_yaqin_xamom_masofa)) echo $xiz->xonadonga_yaqin_xamom_masofa; ?>
                </span></b></td>
		<td style="width: 98px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
         <?php if(isset($xiz->xonadonga_yaqin_bozorcha_masofa)) echo $xiz->xonadonga_yaqin_bozorcha_masofa; ?>
                </span></b></td>
		<td style="width: 85px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
               <?php if(isset($xiz->xonadonga_yaqin_sport_majmuasi_masofa)) echo $xiz->xonadonga_yaqin_sport_majmuasi_masofa; ?>
                </span></b></td>
		<td style="width: 98px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
                            <?php if(isset($xiz->xonadonga_yaqin_togarak_obyekt_masofa)) echo $xiz->xonadonga_yaqin_togarak_obyekt_masofa; ?>
                </span></b></td>
		<td style="width: 126px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
                         <?php if(isset($xiz->istiroxat_bog)) echo $xiz->istiroxat_bog; ?>
					<?php if(isset($xiz->istiroxat_bog_masofa)) echo $xiz->istiroxat_bog_masofa; ?>
                </span></b></td>
	</tr>
</table>

<h4 style="text-align: center;">ХОНАДАОН ЖОЙЛАШГАН КЎЧАНИНГ ҲОЛАТИ ҲАҚИДА МАЪЛУМОТЛАР</h4>
<table class="MsoTableGrid" border="1" cellspacing="0" cellpadding="0" style="border-collapse: collapse; border: medium none" width="100%">
	<tr>
		<td width="110" style="width: 82.35pt; border: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR"><span style="font-size: 8pt">Хонадон жойлашган
		кўчанинг умумий узунлиги</span></span></b></td>
		<td style="width: 87px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR"><span style="font-size: 8pt">Хонадон жойлашган
		кўчанинг умумий eни</span></span></b></td>
		<td style="width: 76px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR"><span style="font-size: 8pt">Хонадон жойлашган
		кўчанинг &#1179;опламаси</span></span></b></td>
		<td style="width: 131px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR"><span style="font-size: 8pt">Хонадон жойлашган
		кўчанинг таъмирга мухтожлиги (жорий ёки копитал)</span></span></b></td>
		<td width="112" style="width: 84.2pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR"><span style="font-size: 8pt">Кўчада жойлашган
		колифорния тeраклари сони</span></span></b></td>
		<td style="width: 60px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR"><span style="font-size: 8pt">Кўчани ёритиш тизими</span></span></b></td>
		<td style="width: 88px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR"><span style="font-size: 8pt">Кўчадаги ари&#1179;ларни
		чопилганлиги</span></span></b></td>
		<td style="width: 117px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR"><span style="font-size: 8pt">Кўчадаги ари&#1179;ларга сув
		етиб кeлиши (&#1179;аeрдан, канал, зовур ёки насос)</span></span></b></td>
		<td style="width: 81px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR"><span style="font-size: 8pt">Кўчадаги кўприклар-нинг
		&#1203;олати</span></span></b></td>
		<td style="width: 111px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: 1.0pt solid windowtext; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
		<span lang="UZ-CYR"><span style="font-size: 8pt">Кўчадаги зовурларни
		чопилган-лиги</span></span></b></td>
	</tr>
	<tr>
		<td width="110" style="width: 82.35pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">1</span></b></td>
		<td style="width: 87px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">2</span></b></td>
		<td style="width: 76px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">3</span></b></td>
		<td style="width: 131px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">4</span></b></td>
		<td width="112" style="width: 84.2pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">5</span></b></td>
		<td style="width: 60px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">6</span></b></td>
		<td style="width: 88px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">7</span></b></td>
		<td style="width: 117px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">8</span></b></td>
		<td style="width: 81px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">9</span></b></td>
		<td style="width: 111px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center"><b>
				<span lang="UZ-CYR" style="font-size: 8pt">10</span></b></td>
	</tr>
	<tr>
		<td width="110" style="width: 82.35pt; border-left: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
<?php if(isset($kocha->xonadon_joylashgan_kocha_uzunligi)) echo $kocha->xonadon_joylashgan_kocha_uzunligi; ?>
                </span></b></td>
		<td style="width: 87px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
					<?php if(isset($kocha->xonadon_joylashgan_kocha_eni)) echo $kocha->xonadon_joylashgan_kocha_eni ?>
                </span></b></td>
		<td style="width: 76px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
					<?php if(isset($kocha->kocha_qoplamasi)) echo $kocha->kocha_qoplamasi?>
				</span></b></td>
		<td style="width: 131px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
					<?php if(isset($kocha->xonadon_joylashgan_kocha_tamirlanganligi)) echo $kocha->xonadon_joylashgan_kocha_tamirlanganligi ?>
                </span></b></td>
		<td width="112" style="width: 84.2pt; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
				<?php if(isset($koch->xonadon_joylashgan_kocha_koliforniya_terak_soni)) echo $kocha->xonadon_joylashgan_kocha_koliforniya_terak_soni ?>
                </span></b></td>
		<td style="width: 60px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
				<?php if(isset($kocha->xonadon_joylashgan_kocha_yoritilishi)) {
					$yoritish=array('1'=>'Ёритиш тизими мавжуд','2'=>'Ёритиш тизими мавжуд лекин таъмирталаб','3'=>'Ёритиш тизими мавжуд эмас'); echo $yoritish[$kocha->xonadon_joylashgan_kocha_yoritilishi]; }?>
                </span></b></td>
		<td style="width: 88px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
				<?php if(isset($kocha->xonadon_joylashgan_kocha_ariqlarni_xolati))  echo $kocha->xonadon_joylashgan_kocha_ariqlarni_xolati;  ?>
                </span></b></td>
		<td style="width: 117px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
				<?php if(isset($kocha->xonadon_joylashgan_kocha_ariqa_suv_kelishi)) {$ariq=array('1'=>'Канал','2'=>'Зовур','3'=>'Насос','4'=>'Сув йўқ'); echo $ariq[$kocha->xonadon_joylashgan_kocha_ariqa_suv_kelishi];} ?>
                </span></b></td>
		<td style="width: 81px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">
	<?php if(isset($kocha->xonadon_joylashgan_kocha_koprik_xolati)) {$koprik=array('1'=>'йўқ','2'=>'бор,яхши','3'=>'бор, таъмирталаб', '4'=>'янги кўприк қуриш керак '); echo $koprik[$kocha->xonadon_joylashgan_kocha_koprik_xolati];}?>
				</span></b></td>
		<td style="width: 111px; border-left: medium none; border-right: 1.0pt solid windowtext; border-top: medium none; border-bottom: 1.0pt solid windowtext; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; padding-bottom: 0cm">
			<p class="MsoNormal" align="center" style="text-align: center; line-height: 200%">
				<b><span lang="UZ-CYR" style="font-size: 8pt; line-height: 200%">

		<?php if(isset($kocha->xonadon_joylashgan_kocha_zovur_xolati)) {$zovur=array('1'=>'Зовур йўқ','2'=>'бор,тозаланган','3'=>'бор, тозаланмаган'); echo $zovur[$kocha->xonadon_joylashgan_kocha_zovur_xolati];}?>
                </span></b></td>
	</tr>
</table>
<br>
<br>

Хонадон аъзолари томонидан билдирилган таклиф (ариза ва шикоят)лар <?php if(isset($fuqaro_uy->taklif)) echo $fuqaro_uy->taklif?>
<?php #echo $muammolar->muammo_mazmuni; ?>
<br>


<br>Ишчи гуруҳ аъзоси Ф.И.Ш ва имзоси :<?php if(isset($fuqaro_uy->ishchi_guruh_fio_id->ishchi_guruh_fio)) echo $fuqaro_uy->ishchi_guruh_fio_id->ishchi_guruh_fio;?>
<br>
<br>Хонадон eгасининг Ф.И.Ш ва имзоси :<?php if(isset($fuqaro->oila_boshligi) and ($fuqaro->oila_boshligi==1)) {if(isset($fuqaro->fio)) echo $fuqaro->fio;}?>       тeл:+998<?php if(isset($fuqaro_uy->tel_raqami)) echo $fuqaro_uy->tel_raqami; ?>

