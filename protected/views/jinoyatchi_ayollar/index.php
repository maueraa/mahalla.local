<?php
/* @var $this Fuqaro_kasallarController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Жиноят содир этган аёллар'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Fuqaro'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Fuqaro'))), 'url'=>array('admin')),
);

?>
<h1><?php echo Yii::t('strings','{tuman}даги истиқомат қилувчи <br>Жиноят содир этган аёллар<br> хақида маълумот', array('{tuman}'=>$tuman))?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'itemsTagName'=>'table',
    'itemsCssClass'=>'table table-bordered table-striped',
    'itemsHeader'=>"
	<thead><tr>
	<th>ID</th>
	<th>Фамалия исм шариф</th>
	<th>Туғилган санаси ва пасспорт ёки метирка рақами </th>
	<th>Вилоят, туман, МФЙ, кўча ва уй рақами</th>
	<th>ИИБ хисобида</th>
	<th>Судланганлиги қачон, қайси модда</th>

	<th>Хозирги даврдаги мехнат фаолияти: иш жойи,лавозими ва телефон рақами</th>

	</tr></thead>",
)); ?>
