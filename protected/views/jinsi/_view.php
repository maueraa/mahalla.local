<?php
/* @var $this JinsiController */
/* @var $data Jinsi */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jins')); ?>:</b>
	<?php echo CHtml::encode($data->jins); ?>
	<br />


</div>