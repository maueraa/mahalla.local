<?php
/* @var $this JinsiController */
/* @var $model Jinsi */

$this->breadcrumbs=array(
	Yii::t('strings','Jinsis')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Jinsi'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Jinsi'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Jinsi')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>