<?php
/* @var $this JinsiController */
/* @var $model Jinsi */

$this->breadcrumbs=array(
	Yii::t('strings','Jinsis')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Jinsi'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Jinsi'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Jinsi'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Jinsi'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Jinsi'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Jinsi') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'jins',
/**
		array(
			'label'=>$model->getAttributeLabel('jins'),
			'type'=>'raw',
			'value'=>$model->j->name,
		),
/**/
	),
)); ?>
