<?php
/* @var $this Fuqaro_UyController */
/* @var $model Fuqaro_Uy */

$this->breadcrumbs=array(
	Yii::t('strings','Fuqaro  Uys')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Fuqaro_Uy'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Fuqaro_Uy'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Fuqaro_Uy'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Fuqaro_Uy'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Fuqaro_Uy'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Fuqaro_Uy') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'fviloyat_id',
		'ftuman_id',
		'mfy_id',
		'kocha_id',
		'uy_raqami',
		'kadastr_mavjudligi',
		'kadastr_seriyasi_raqami',
		'kadastr_buyruq_raqami',
		'ijara',
		'azo_soni',
		'jon_soni',
		'voya_yetmagan_soni',
		'taklif',
		'muammosi',
		'ishchi_guruh_fio_id',
		'tel_raqami',
		'anketa_sana',
/**
		array(
			'label'=>$model->getAttributeLabel('anketa_sana'),
			'type'=>'raw',
			'value'=>$model->anketa_s->name,
		),
/**/
	),
)); ?>
