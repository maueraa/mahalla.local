<?php
/* @var $this KassalikController */
/* @var $data Kassalik */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kassalik_nomi')); ?>:</b>
	<?php echo CHtml::encode($data->kassalik_nomi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kassalik_xolati')); ?>:</b>
	<?php echo CHtml::encode($data->kassalik_xolati); ?>
	<br />


</div>