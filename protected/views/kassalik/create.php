<?php
/* @var $this KassalikController */
/* @var $model Kassalik */

$this->breadcrumbs=array(
	Yii::t('strings','Kassaliks')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Kassalik'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Kassalik'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Kassalik')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>