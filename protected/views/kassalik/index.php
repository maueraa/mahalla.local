<?php
/* @var $this KassalikController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Kassaliks'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Kassalik'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Kassalik'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Kassaliks')?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
