<?php
/* @var $this KassalikController */
/* @var $model Kassalik */

$this->breadcrumbs=array(
	Yii::t('strings','Kassaliks')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Kassalik'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Kassalik'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Kassalik'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Kassalik'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Kassalik'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Kassalik') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'kassalik_nomi',
		'kassalik_xolati',
/**
		array(
			'label'=>$model->getAttributeLabel('kassalik_xolati'),
			'type'=>'raw',
			'value'=>$model->kassalik_xol->name,
		),
/**/
	),
)); ?>
