﻿<?php
/* @var $this Kocha_XolatiController */
/* @var $model Kocha_Xolati */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'kocha--xolati-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadon_joylashgan_kocha_uzunligi'); ?>
		<?php echo $form->textField($model,'xonadon_joylashgan_kocha_uzunligi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'xonadon_joylashgan_kocha_uzunligi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadon_joylashgan_kocha_eni'); ?>
		<?php echo $form->textField($model,'xonadon_joylashgan_kocha_eni',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'xonadon_joylashgan_kocha_eni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kocha_qoplamasi'); ?>
		<?php echo $form->textField($model,'kocha_qoplamasi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'kocha_qoplamasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadon_joylashgan_kocha_tamirlanganligi'); ?>
		<?php echo $form->textField($model,'xonadon_joylashgan_kocha_tamirlanganligi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'xonadon_joylashgan_kocha_tamirlanganligi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadon_joylashgan_kocha_koliforniya_terak_soni'); ?>
		<?php echo $form->textField($model,'xonadon_joylashgan_kocha_koliforniya_terak_soni',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'xonadon_joylashgan_kocha_koliforniya_terak_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadon_joylashgan_kocha_yoritilishi'); ?>
		<?php echo $form->dropdownlist($model,'xonadon_joylashgan_kocha_yoritilishi',array('1'=>'Ёритиш тизими мавжуд','2'=>'Ёритиш тизими мавжуд лекин таъмирталаб','3'=>'Ёритиш тизими мавжуд эмас'),array('prompt'=>Yii::t("strings",'Кўчанинг ёритиш тизими'))); ?>
		<?php echo $form->error($model,'xonadon_joylashgan_kocha_yoritilishi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadon_joylashgan_kocha_ariqlarni_xolati'); ?>
		<?php echo $form->textField($model,'xonadon_joylashgan_kocha_ariqlarni_xolati',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'xonadon_joylashgan_kocha_ariqlarni_xolati'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadon_joylashgan_kocha_ariqa_suv_kelishi'); ?>
		<?php echo $form->dropdownlist($model,'xonadon_joylashgan_kocha_ariqa_suv_kelishi',array('1'=>'Канал','2'=>'Зовур','3'=>'Насос','4'=>'Сув йўқ'),array('prompt'=>Yii::t("strings",'Хонадон жойлашган кўчага сувни етиб келиши'))); ?>
		<?php echo $form->error($model,'xonadon_joylashgan_kocha_ariqa_suv_kelishi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadon_joylashgan_kocha_koprik_xolati'); ?>
		<?php echo $form->dropdownlist($model,'xonadon_joylashgan_kocha_koprik_xolati',array('1'=>'йўқ','2'=>'бор,яхши','3'=>'бор, таъмирталаб', '4'=>'янги кўприк қуриш керак '),array('prompt'=>Yii::t("strings",'Кўприкни холати'))); ?>
		<?php echo $form->error($model,'xonadon_joylashgan_kocha_koprik_xolati'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadon_joylashgan_kocha_zovur_xolati'); ?>
		<?php echo $form->dropdownlist($model,'xonadon_joylashgan_kocha_zovur_xolati',array('1'=>'Зовур йўқ','2'=>'бор,тозаланган','3'=>'бор, тозаланмаган'),array('prompt'=>Yii::t("strings",'Зовур холати'))); ?>
		<?php echo $form->error($model,'xonadon_joylashgan_kocha_zovur_xolati'); ?>
	</div>

	<div class="row">
		<?php  $form->labelEx($model,'fuqaro_uy_id'); ?>
		<?php echo $form->hiddenField($model,'fuqaro_uy_id',array('value'=>$fid)); ?>
		<?php echo $form->error($model,'fuqaro_uy_id'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'anketa_ozgarish').":".date("d.m.Y"); ?>
        <?php echo $form->hiddenField($model,'anketa_ozgarish',array('value'=>date("Y-m-d"))); ?>
        <?php echo $form->error($model,'anketa_ozgarish'); ?>
    </div>
	<div class="row">
		<?php echo $form->labelEx($model,'user_id').Yii::app()->user->name; ?>
		<?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_date').date("d.m.Y"); ?>
		<?php echo $form->hiddenField($model,'user_date',array('value'=>date("Y-m-d"))); ?>
		<?php echo $form->error($model,'user_date'); ?>
	</div>

    <div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array("class"=>"btn btn-primary")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->