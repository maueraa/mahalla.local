<?php
/* @var $this Kocha_XolatiController */
/* @var $model Kocha_Xolati */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadon_joylashgan_kocha_uzunligi'); ?>
		<?php echo $form->textField($model,'xonadon_joylashgan_kocha_uzunligi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadon_joylashgan_kocha_eni'); ?>
		<?php echo $form->textField($model,'xonadon_joylashgan_kocha_eni',array('size'=>60,'maxlength'=>255)); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model,'kocha_qoplamasi'); ?>
		<?php echo $form->textField($model,'kocha_qoplamasi',array('size'=>60,'maxlength'=>255)); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model,'xonadon_joylashgan_kocha_tamirlanganligi'); ?>
		<?php echo $form->textField($model,'xonadon_joylashgan_kocha_tamirlanganligi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadon_joylashgan_kocha_koliforniya_terak_soni'); ?>
		<?php echo $form->textField($model,'xonadon_joylashgan_kocha_koliforniya_terak_soni',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadon_joylashgan_kocha_yoritilishi'); ?>
		<?php echo $form->textField($model,'xonadon_joylashgan_kocha_yoritilishi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadon_joylashgan_kocha_ariqlarni_xolati'); ?>
		<?php echo $form->textField($model,'xonadon_joylashgan_kocha_ariqlarni_xolati',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadon_joylashgan_kocha_ariqa_suv_kelishi'); ?>
		<?php echo $form->textField($model,'xonadon_joylashgan_kocha_ariqa_suv_kelishi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadon_joylashgan_kocha_koprik_xolati'); ?>
		<?php echo $form->textField($model,'xonadon_joylashgan_kocha_koprik_xolati',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadon_joylashgan_kocha_zovur_xolati'); ?>
		<?php echo $form->textField($model,'xonadon_joylashgan_kocha_zovur_xolati',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fuqaro_uy_id'); ?>
		<?php echo $form->textField($model,'fuqaro_uy_id'); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model,'kocha_qoplamasi'); ?>
        <?php echo $form->textField($model,'kocha_qoplamasi',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'anketa_ozgarish'); ?>
        <?php echo $form->textField($model,'anketa_ozgarish'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->