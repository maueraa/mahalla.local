<?php
/* @var $this Kocha_XolatiController */
/* @var $data Kocha_Xolati */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadon_joylashgan_kocha_uzunligi')); ?>:</b>
	<?php echo CHtml::encode($data->xonadon_joylashgan_kocha_uzunligi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadon_joylashgan_kocha_eni')); ?>:</b>
	<?php echo CHtml::encode($data->xonadon_joylashgan_kocha_eni); ?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('kocha_qoplamasi')); ?>:</b>
	<?php echo CHtml::encode($data->kocha_qoplamasi); ?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadon_joylashgan_kocha_tamirlanganligi')); ?>:</b>
	<?php echo CHtml::encode($data->xonadon_joylashgan_kocha_tamirlanganligi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadon_joylashgan_kocha_koliforniya_terak_soni')); ?>:</b>
	<?php echo CHtml::encode($data->xonadon_joylashgan_kocha_koliforniya_terak_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadon_joylashgan_kocha_yoritilishi')); ?>:</b>
	<?php echo CHtml::encode($data->xonadon_joylashgan_kocha_yoritilishi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadon_joylashgan_kocha_ariqlarni_xolati')); ?>:</b>
	<?php echo CHtml::encode($data->xonadon_joylashgan_kocha_ariqlarni_xolati); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadon_joylashgan_kocha_ariqa_suv_kelishi')); ?>:</b>
	<?php echo CHtml::encode($data->xonadon_joylashgan_kocha_ariqa_suv_kelishi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadon_joylashgan_kocha_koprik_xolati')); ?>:</b>
	<?php echo CHtml::encode($data->xonadon_joylashgan_kocha_koprik_xolati); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadon_joylashgan_kocha_zovur_xolati')); ?>:</b>
	<?php echo CHtml::encode($data->xonadon_joylashgan_kocha_zovur_xolati); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fuqaro_uy_id')); ?>:</b>
	<?php echo CHtml::encode($data->fuqaro_uy_id); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('kocha_qoplamasi')); ?>:</b>
	<?php echo CHtml::encode($data->kocha_qoplamasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anketa_ozgarish')); ?>:</b>
	<?php echo CHtml::encode($data->anketa_ozgarish); ?>
	<br />

	*/ ?>

</div>