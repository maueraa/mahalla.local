<?php
/* @var $this Kocha_XolatiController */
/* @var $model Kocha_Xolati */

$this->breadcrumbs=array(
	Yii::t('strings','Kocha  Xolatis')=>array('index'),
	Yii::t('strings','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Kocha_Xolati'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Kocha_Xolati'))), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#kocha--xolati-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Kocha_Xolati'))) ?></h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link(Yii::t('zii','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'kocha--xolati-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'xonadon_joylashgan_kocha_uzunligi',
		'xonadon_joylashgan_kocha_eni',
		'xonadon_joylashgan_kocha_tamirlanganligi',
		'xonadon_joylashgan_kocha_koliforniya_terak_soni',
		'xonadon_joylashgan_kocha_yoritilishi',
		'kocha_qoplamasi',
		/*
		'xonadon_joylashgan_kocha_ariqlarni_xolati',
		'xonadon_joylashgan_kocha_ariqa_suv_kelishi',
		'xonadon_joylashgan_kocha_koprik_xolati',
		'xonadon_joylashgan_kocha_zovur_xolati',
		'fuqaro_uy_id',
		//'fuqaro_uy.name',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
