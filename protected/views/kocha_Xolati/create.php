﻿<?php
/* @var $this Kocha_XolatiController */
/* @var $model Kocha_Xolati */

$this->breadcrumbs=array(
	Yii::t('strings','Kocha  Xolatis')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	#array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Kocha_Xolati'))), 'url'=>array('index')),
	#array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Kocha_Xolati'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Хонадон жойлашган кўчанинг холати хақида маълумтларни киритиш ойнаси')?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'fid'=>$fid)); ?>