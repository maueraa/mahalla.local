<?php
/* @var $this Kocha_XolatiController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Kocha  Xolatis'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Kocha_Xolati'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Kocha_Xolati'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Kocha  Xolatis')?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
