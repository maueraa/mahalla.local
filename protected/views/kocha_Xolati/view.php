<?php
/* @var $this Kocha_XolatiController */
/* @var $model Kocha_Xolati */

$this->breadcrumbs=array(
	Yii::t('strings','Кўчанинг холати')=>array('index'),
	$model->id,
);

$this->menu=array(
#	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Kocha_Xolati'))), 'url'=>array('index')),
#	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Kocha_Xolati'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Kocha_Xolati'))), 'url'=>array('update', 'id'=>$model->id)),
#	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Kocha_Xolati'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Kocha_Xolati'))), 'url'=>array('admin')),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон маълумотлари'))), 'url'=>array("/fuqaro_Uy/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон аъзолари'))), 'url'=>array("/fuqarolar?fuq=$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Маънавий мухит'))), 'url'=>array("/manaviy/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Даромади'))), 'url'=>array("/daromad/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Қарздоллик'))), 'url'=>array("/qarz/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт қатнови'))), 'url'=>array("/transport_qatnovi/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ижтимоий мухит'))), 'url'=>array("/ijtimoiy_muhit/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Шарт-шароити'))), 'url'=>array("/shaoriti/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Томорқа'))), 'url'=>array("/tomorqa/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Чорва хайвонлари'))), 'url'=>array("/hayvon/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хизмат кўрсатиш объектлари'))), 'url'=>array("/ximzat_korsatish/$model->fuqaro_uy_id")),
    #array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчанинг холати'))), 'url'=>array("/kocha_xolati/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчанинг холати хақидаги маълумотларни ўзгартириш'))), 'url'=>array('update', 'fid'=>$model->fuqaro_uy_id)),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Барча маълумотларни кўриш'))), 'url'=>array('/jamlama/index', 'id'=>$model->fuqaro_uy_id)),

);
?>

<h1><?php echo Yii::t('strings','Хонадон жойлашган кўчанинг холати хақида маълумтларни янгиланган холати ойнаси') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'xonadon_joylashgan_kocha_uzunligi',
		'xonadon_joylashgan_kocha_eni',
		'xonadon_joylashgan_kocha_tamirlanganligi',
		'xonadon_joylashgan_kocha_koliforniya_terak_soni',
		'xonadon_joylashgan_kocha_yoritilishi',
		'xonadon_joylashgan_kocha_ariqlarni_xolati',
		'xonadon_joylashgan_kocha_ariqa_suv_kelishi',
		'xonadon_joylashgan_kocha_koprik_xolati',
		'xonadon_joylashgan_kocha_zovur_xolati',
		'fuqaro_uy_id',
		'kocha_qoplamasi',
        'anketa_ozgarish',
		'user_id',
		'user_date',
/**
		array(
			'label'=>$model->getAttributeLabel('fuqaro_uy_id'),
			'type'=>'raw',
			'value'=>$model->fuqaro_uy->name,
		),
/**/
	),
)); ?>
