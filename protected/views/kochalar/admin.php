<?php
/* @var $this KochalarController */
/* @var $model Kochalar */

$this->breadcrumbs=array(
    Yii::t('strings','Кўчалар')=>array("kocha?val_mfy=".Yii::app()->user->tuman),
	Yii::t('strings','Қидириш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчалар'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Янги кўча қўшиш'))), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('fuqaro--uy-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчаларни қидириш ва тахрирлаш'))) ?></h1>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'kochalar-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'tbl_kocha_nomi',
		'mfy.mfy_nomi',
		'tuman.tuman_nomi',
		'viloyat.viloyat_nomi',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
