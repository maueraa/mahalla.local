<?php
/* @var $this KochalarController */
/* @var $model Kochalar */

$this->breadcrumbs=array(
	Yii::t('strings','Кўчалар')=>array("kocha?val_mfy=".Yii::app()->user->tuman),
	Yii::t('strings','Киритиш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчалар'))), 'url'=>array("kocha?val_mfy=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Тахрирлаш'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Махалла фуқаролар йиғинлари учун кўчалар қўшиш')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>