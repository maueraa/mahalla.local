<?php
/* @var $this KochalarController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Кўчалар'),
);

$this->menu=array(
	array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Янги кўча қўшиш'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчани қидириш'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings',$dataProvider->data[0]->tblMfy->mfy_nomi.' таркибидаги кўчалар рўйхати')?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'viewData'=>array('cp'=>$dataProvider),
    'itemsTagName'=>'table',
    'itemsCssClass'=>'table table-bordered table-striped',
    'itemsHeader'=>"
    <thead>
    	<tr>
	<th style='width: 20px;'>№</th>
	<th style='width: 625px;'>Кўчалар номи</th>

	</tr>
	</thead>
	",
)); ?>

