<?php
/* @var $this KochalarController */
/* @var $model Kochalar */

$this->breadcrumbs=array(
    Yii::t('strings','Кўчалар')=>array("kocha?val_mfy=".Yii::app()->user->tuman),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчалар'))), 'url'=>array("kocha?val_mfy=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Янги кўча қўшиш'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ўзгартириш'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ўчириш'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Қидириш'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings',$model->tbl_kocha_nomi).'  '. Yii::t('strings',''); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'tbl_kocha_nomi',
		'tbl_mfy_id',
/**
		array(
			'label'=>$model->getAttributeLabel('tbl_mfy_id'),
			'type'=>'raw',
			'value'=>$model->tbl_mfy->name,
		),
/**/
	),
)); ?>
