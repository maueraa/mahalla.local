<?php
/* @var $this KollejlarController */
/* @var $model Kollejlar */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'kollejlar-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'kollej_nomi'); ?>
		<?php echo $form->textField($model,'kollej_nomi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'kollej_nomi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'manzili'); ?>
		<?php echo $form->textField($model,'manzili',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'manzili'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kollej_direktori'); ?>
		<?php echo $form->textField($model,'kollej_direktori',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'kollej_direktori'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->