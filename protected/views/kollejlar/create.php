<?php
/* @var $this KollejlarController */
/* @var $model Kollejlar */

$this->breadcrumbs=array(
	Yii::t('strings','Kollejlars')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Kollejlar'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Kollejlar'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Kollejlar')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>