<?php
/* @var $this KollejlarController */
/* @var $model Kollejlar */

$this->breadcrumbs=array(
	Yii::t('strings','Kollejlars')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Kollejlar'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Kollejlar'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Kollejlar'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Kollejlar'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Kollejlar'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Kollejlar') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'kollej_nomi',
		'manzili',
		'kollej_direktori',
/**
		array(
			'label'=>$model->getAttributeLabel('kollej_direktori'),
			'type'=>'raw',
			'value'=>$model->kollej_direkt->name,
		),
/**/
	),
)); ?>
