<?php
/* @var $this MahallarController */
/* @var $data Mahallar */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomi')); ?>:</b>
	<?php echo CHtml::encode($data->nomi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transport_id')); ?>:</b>
	<?php echo CHtml::encode($data->transport_id); ?>
	<br />


</div>