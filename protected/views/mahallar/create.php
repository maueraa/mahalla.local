<?php
/* @var $this MahallarController */
/* @var $model Mahallar */

$this->breadcrumbs=array(
	Yii::t('strings','Mahallars')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Mahallar'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Mahallar'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Mahallar')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>