<?php
/* @var $this MahallarController */
/* @var $model Mahallar */

$this->breadcrumbs=array(
	Yii::t('strings','Mahallars')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Mahallar'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Mahallar'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Mahallar'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Mahallar'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Mahallar'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Mahallar') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nomi',
		'transport.transport_narxi',
/**
		array(
			'label'=>$model->getAttributeLabel('transport_id'),
			'type'=>'raw',
			'value'=>$model->transport->name,
		),
/**/
	),
)); ?>
