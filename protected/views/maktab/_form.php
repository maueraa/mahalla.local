<?php
/* @var $this MaktabController */
/* @var $model Maktab */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'maktab-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'maktab_raqami'); ?>
		<?php echo $form->textField($model,'maktab_raqami',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'maktab_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'manzili'); ?>
		<?php echo $form->textField($model,'manzili',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'manzili'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'maktab_direktor'); ?>
		<?php echo $form->textField($model,'maktab_direktor',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'maktab_direktor'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->