<?php
/* @var $this MaktabController */
/* @var $data Maktab */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('maktab_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->maktab_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('manzili')); ?>:</b>
	<?php echo CHtml::encode($data->manzili); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('maktab_direktor')); ?>:</b>
	<?php echo CHtml::encode($data->maktab_direktor); ?>
	<br />


</div>