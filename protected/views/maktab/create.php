<?php
/* @var $this MaktabController */
/* @var $model Maktab */

$this->breadcrumbs=array(
	Yii::t('strings','Maktabs')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Maktab'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Maktab'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Maktab')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>