<?php
/* @var $this MaktabController */
/* @var $model Maktab */

$this->breadcrumbs=array(
	Yii::t('strings','Maktabs')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Maktab'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Maktab'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Maktab'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Maktab'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Maktab'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Maktab') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'maktab_raqami',
		'manzili',
		'maktab_direktor',
/**
		array(
			'label'=>$model->getAttributeLabel('maktab_direktor'),
			'type'=>'raw',
			'value'=>$model->maktab_direk->name,
		),
/**/
	),
)); ?>
