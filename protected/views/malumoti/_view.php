<?php
/* @var $this MalumotiController */
/* @var $data Malumoti */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('malumot_turi')); ?>:</b>
	<?php echo CHtml::encode($data->malumot_turi); ?>
	<br />


</div>