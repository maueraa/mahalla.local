<?php
/* @var $this MalumotiController */
/* @var $model Malumoti */

$this->breadcrumbs=array(
	Yii::t('strings','Malumotis')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Malumoti'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Malumoti'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Malumoti')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>