<?php
/* @var $this ManaviyController */
/* @var $model Manaviy */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'manaviy-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadon_bahosi'); ?>
		<?php echo $form->dropdownlist($model,'xonadon_bahosi',array('1'=>'яхши','2'=>'ўрта','3'=>'қониқарли')); ?>
		<?php echo $form->error($model,'xonadon_bahosi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kutubxona'); ?>
			<?php echo $form->dropdownlist($model,'kutubxona',array('1'=>'китоб ўқиб туришади','2'=>'бор','3'=>'йўқ','4'=>'диний мазмундаги китобларни ўқиб туришида'),array('prompt'=>Yii::t("strings",'Кутубхона борлиги'))); ?>
		<?php echo $form->error($model,'kutubxona'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'obuna'); ?>
		<?php echo $form->dropdownlist($model,'obuna',array('1'=>'йўқ','2'=>'газета ва журналлар ўқиб боришади', '3'=>'бор'),array('prompt'=>Yii::t("strings",'Газета ва журналларга обуна бўлганлиги'))); ?>
		<?php echo $form->error($model,'obuna'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'farzandi_davomad'); ?>
		<?php echo $form->dropdownlist($model,'farzandi_davomad',array('1'=>'Доимий равишда ўқишга боради','2'=>'Кунора ўқишга боради','3'=>'Мунтазам равишда ўқишга қатнашмайди','4'=>'Ўқишда ўқийдиган фарзандлари йўқ'),array('prompt'=>Yii::t("strings",'Фарзандини давомади'))); ?>
		<?php echo $form->error($model,'farzandi_davomad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'manaviy_muhit'); ?>
		<?php echo $form->textField($model,'manaviy_muhit',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'manaviy_muhit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jamoat_ishlarida_ishtiroki'); ?>
		<?php echo $form->textField($model,'jamoat_ishlarida_ishtiroki',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'jamoat_ishlarida_ishtiroki'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kayfiyat'); ?>
		<?php echo $form->textField($model,'kayfiyat',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'kayfiyat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'muhit_boshqalar'); ?>
		<?php echo $form->textField($model,'muhit_boshqalar',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'muhit_boshqalar'); ?>
	</div>

	<div class="row">
		<?php  $form->labelEx($model,'fuqaro_uy_id'); ?>
		<?php echo $form->hiddenField($model,'fuqaro_uy_id',array('value'=>$fid)); ?>
		<?php echo $form->error($model,'fuqaro_uy_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'user_id').Yii::app()->user->name; ?>
		<?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_date').date("d.m.Y"); ?>
		<?php echo $form->hiddenField($model,'user_date',array('value'=>date("Y-m-d"))); ?>
		<?php echo $form->error($model,'user_date'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array("class"=>"btn btn-primary")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->