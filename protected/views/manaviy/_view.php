<?php
/* @var $this ManaviyController */
/* @var $data Manaviy */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadon_bahosi')); ?>:</b>
	<?php echo CHtml::encode($data->xonadon_bahosi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kutubxona')); ?>:</b>
	<?php echo CHtml::encode($data->kutubxona); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('obuna')); ?>:</b>
	<?php echo CHtml::encode($data->obuna); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('farzandi_davomad')); ?>:</b>
	<?php echo CHtml::encode($data->farzandi_davomad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('manaviy_muhit')); ?>:</b>
	<?php echo CHtml::encode($data->manaviy_muhit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jamoat_ishlarida_ishtiroki')); ?>:</b>
	<?php echo CHtml::encode($data->jamoat_ishlarida_ishtiroki); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kayfiyat')); ?>:</b>
	<?php echo CHtml::encode($data->kayfiyat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('muhit_boshqalar')); ?>:</b>
	<?php echo CHtml::encode($data->muhit_boshqalar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fuqaro_uy_id')); ?>:</b>
	<?php echo CHtml::encode($data->fuqaro_uy_id); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('anketa_ozgarish')); ?>:</b>
	<?php echo CHtml::encode($data->anketa_ozgarish); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_date')); ?>:</b>
	<?php echo CHtml::encode($data->user_date); ?>
	<br />
	*/ ?>

</div>