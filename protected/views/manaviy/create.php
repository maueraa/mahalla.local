<?php
/* @var $this ManaviyController */
/* @var $model Manaviy */

$this->breadcrumbs=array(
	Yii::t('strings','Manaviys')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	#array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Manaviy'))), 'url'=>array('index')),
	#array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Manaviy'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадондаги маънавий мухит хақидаги маълумотларни киритиш ойнаси')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'fid'=>$fid)); ?>