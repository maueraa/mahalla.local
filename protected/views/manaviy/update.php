<?php
/* @var $this ManaviyController */
/* @var $model Manaviy */

$this->breadcrumbs=array(
	Yii::t('strings','Маънавий мухит')=>array('index'),
	Yii::t('strings',$model->id)=>array('view','id'=>$model->id),
	Yii::t('strings','Ўзгартириш'),
);

$this->menu=array(
	#array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Manaviy'))), 'url'=>array('index')),
	#array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Manaviy'))), 'url'=>array('create')),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадонни маълумоти'))), 'url'=>array('view', 'id'=>$model->fuqaro_uy_id)),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон аъзолари'))), 'url'=>array("/fuqarolar?fuq=$model->id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Маънавий мухит маълумотларини кўриш'))), 'url'=>array('view', 'id'=>$model->id)),
	#array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Manaviy'))), 'url'=>array('admin')),
    #array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Маънавий мухит'))), 'url'=>array('/manaviy/update', 'fid'=>$fid)),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Даромади'))), 'url'=>array("/daromad/$fid")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Қарздоллик'))), 'url'=>array("/qarz/$fid")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт қатнови'))), 'url'=>array("/transport_qatnovi/$fid")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт воситалари'))), 'url'=>array("/xonadon_transport/$fid")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ижтимоий мухит'))), 'url'=>array("/ijtimoiy_muhit/$fid")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Шарт-шароити'))), 'url'=>array("/shaoriti/$fid")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Томорқа'))), 'url'=>array("/tomorqa/$fid")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Чорва хайвонлари'))), 'url'=>array("/hayvon/$fid")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хизмат кўрсатиш объектлари'))), 'url'=>array("/ximzat_korsatish/$fid")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчанинг холати'))), 'url'=>array("/kocha_xolati/$fid")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Барча маълумотларни кўриш'))), 'url'=>array('/jamlama/index', 'id'=>$fid)),

);$fuqaro_m = Fuqaro_Uy::model()->find("id=$model->fuqaro_uy_id");
?>

<h1><?php if (isset($fuqaro_m->ftuman->tuman_nomi))echo $fuqaro_m->ftuman->tuman_nomi; ?>
    <?php if (isset($fuqaro_m->mfy->mfy_nomi))	echo $fuqaro_m->mfy->mfy_nomi;	?>
    <?php if (isset($fuqaro_m->kochalar->tbl_kocha_nomi))echo $fuqaro_m->kochalar->tbl_kocha_nomi; ?>
    <?php if (isset($fuqaro_m->uy_raqami))echo CHtml::encode($fuqaro_m->uy_raqami); ?>
    <?php echo Yii::t('strings','{table} "{record}" (id={id})', array(
	'{table}'=>Yii::t('strings','хонадонининг маънавий мухит хақидаги маълумотларни тахрирлаш ойнаси'),
	'{record}'=>$model->id,
	'{id}'=>$model->id));?></h1>
<?php $this->renderPartial('_form_upd', array('model'=>$model,'fid'=>$fid)); ?>