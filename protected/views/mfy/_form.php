<?php
/* @var $this MfyController */
/* @var $model Mfy */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'mfy-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'mfy_nomi'); ?>
		<?php echo $form->textField($model,'mfy_nomi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'mfy_nomi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tuman_id'); ?>
		<?php echo $form->dropDownList($model,'tuman_id',$model->gettuman_nomii(    )); ?>
		<?php echo $form->error($model,'tuman_id'); ?>
	</div>

	<div class="form-actions">
		<?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' :'Save',
        ));
        ; ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->