<?php
/* @var $this MfyController */
/* @var $model Mfy */

$this->breadcrumbs=array(
	Yii::t('strings','МФЙ')=>array('index'),
	Yii::t('strings','Қидириш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ рўйхати'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ қўшиш'))), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('mfy-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙлар рўйхати'))) ?></h1>


<?php #echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn'));?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'mfy-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'mfy_nomi',
		'tuman.tuman_nomi',
		//'tuman.name',
		array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
