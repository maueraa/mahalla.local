<?php
/* @var $this MfyController */
/* @var $model Mfy */

$this->breadcrumbs=array(
	Yii::t('strings','МФЙ')=>array('index?val_mfy='.Yii::app()->user->tuman),
	Yii::t('strings','Қўшиш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ рўйхати'))), 'url'=>array('index?val_mfy='.Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Қидириш'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ қўшиш')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>