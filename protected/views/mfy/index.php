<?php
/* @var $this MfyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','МФЙ'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','Янги {label}', array('{label}'=>Yii::t('strings','МФЙ қўшиш'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Админ'))), 'url'=>array('admin')),
);
?>

<h1><?php
    if(Yii::app()->user->role == 5)
    {
    echo Yii::t('strings','{vil}даги махалла фуқаролар йиғинлари', array('{vil}'=>$vil));
    }
    else
        echo Yii::t('strings','<h2 style="text-align: center;">{vil}даги махалла фуқаролар йиғинларининг умумий рўйхати</h2>', array('{vil}'=>$vil));

    ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'viewData'=>array('cp'=>$dataProvider),
	'itemsTagName'=>'table',
    'itemsCssClass'=>'table table-bordered table-striped',
	'itemsHeader'=>"
    <thead>
    <tr>
	<th style='width: 20px;'>№</th>
	<th style='width: 625px;'>Махалла фуқаролар йиғини номи</th>
    <th style='text-align: center;'>Хонадон сони</th>
    <th style='text-align: center;'>Кам таъминланган хонадонлар сони</th>
    <th style='text-align: center;'>Кадастр хужжати йўқ хонаднолар сони</th>
    <th style='text-align: center;'>Аҳоли сони</th>
    <th style='text-align: center;'>Эркаклар сони</th>
    <th style='text-align: center;'>Аёллар сони</th>
    <th style='text-align: center;'>Ногирон фуқаролар сони</th>
    <th style='text-align: center;'>Вояга етмаган ёшлар сони</th>
	</tr>
	</thead>
	",

)); ?>
