<?php
/* @var $this MfyXodimlarController */
/* @var $model MfyXodimlar */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'mfy-xodimlar-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'viloyat_id'); ?>
        <?php echo $form->dropDownList($model,'viloyat_id',$model->getfviloyat(),array('id'=>'id_province','prompt'=>Yii::t("strings",'Вилоятни танланг'))); ?>
        <?php echo $form->error($model,'viloyat_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'tuman_id'); ?>
        <?php echo $form->dropDownList($model,'tuman_id',array(),array('id'=>'id_tuman','prompt'=>Yii::t("strings",'Туманни танланг'))); ?>
        <?php echo $form->error($model,'tuman_id'); ?>
    </div>
    <?php
    ECascadeDropDown::master('id_province')->setDependent('id_tuman',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadtuman');
    ?>
    <div class="row">
        <?php echo $form->labelEx($model,'mfy_id'); ?>
        <?php echo $form->dropDownList($model,'mfy_id',array(),array('id'=>'id_mfy','prompt'=>Yii::t("strings",'МФЙ танланг'))); ?>
        <?php echo $form->error($model,'mfy_id'); ?>
    </div>
    <?php
    ECascadeDropDown::master('id_tuman')->setDependent('id_mfy',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadmfy');
    ?>

	<div class="row">
		<?php echo $form->labelEx($model,'mfy_rais'); ?>
		<?php echo $form->textField($model,'mfy_rais',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'mfy_rais'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mfy_rais_tel'); ?>
		<?php echo $form->textField($model,'mfy_rais_tel'); ?>
		<?php echo $form->error($model,'mfy_rais_tel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mfy_kotiba'); ?>
		<?php echo $form->textField($model,'mfy_kotiba',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'mfy_kotiba'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mfy_kotiba_tel'); ?>
		<?php echo $form->textField($model,'mfy_kotiba_tel'); ?>
		<?php echo $form->error($model,'mfy_kotiba_tel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mfy_posbon'); ?>
		<?php echo $form->textField($model,'mfy_posbon',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'mfy_posbon'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mfy_posbon_tel'); ?>
		<?php echo $form->textField($model,'mfy_posbon_tel'); ?>
		<?php echo $form->error($model,'mfy_posbon_tel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id').Yii::app()->user->name;; ?>
		<?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_date').date("d.m.Y"); ?>
		<?php echo $form->hiddenField($model,'user_date',array('value'=>date("Y-m-d"))); ?>
		<?php echo $form->error($model,'user_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->