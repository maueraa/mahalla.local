<?php
/* @var $this MfyXodimlarController */
/* @var $model MfyXodimlar */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'viloyat_id'); ?>
		<?php echo $form->textField($model,'viloyat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tuman_id'); ?>
		<?php echo $form->textField($model,'tuman_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mfy_id'); ?>
		<?php echo $form->textField($model,'mfy_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mfy_rais'); ?>
		<?php echo $form->textField($model,'mfy_rais',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mfy_rais_tel'); ?>
		<?php echo $form->textField($model,'mfy_rais_tel'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mfy_kotiba'); ?>
		<?php echo $form->textField($model,'mfy_kotiba',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mfy_kotiba_tel'); ?>
		<?php echo $form->textField($model,'mfy_kotiba_tel'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mfy_posbon'); ?>
		<?php echo $form->textField($model,'mfy_posbon',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mfy_posbon_tel'); ?>
		<?php echo $form->textField($model,'mfy_posbon_tel'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_date'); ?>
		<?php echo $form->textField($model,'user_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->