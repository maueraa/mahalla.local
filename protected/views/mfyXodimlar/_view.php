<?php
/* @var $this MfyXodimlarController */
/* @var $data MfyXodimlar */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('viloyat_id')); ?>:</b>
	<?php echo CHtml::encode($data->viloyat_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tuman_id')); ?>:</b>
	<?php echo CHtml::encode($data->tuman_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mfy_id')); ?>:</b>
	<?php echo CHtml::encode($data->mfy_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mfy_rais')); ?>:</b>
	<?php echo CHtml::encode($data->mfy_rais); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mfy_rais_tel')); ?>:</b>
	<?php echo CHtml::encode($data->mfy_rais_tel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mfy_kotiba')); ?>:</b>
	<?php echo CHtml::encode($data->mfy_kotiba); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('mfy_kotiba_tel')); ?>:</b>
	<?php echo CHtml::encode($data->mfy_kotiba_tel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mfy_posbon')); ?>:</b>
	<?php echo CHtml::encode($data->mfy_posbon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mfy_posbon_tel')); ?>:</b>
	<?php echo CHtml::encode($data->mfy_posbon_tel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_date')); ?>:</b>
	<?php echo CHtml::encode($data->user_date); ?>
	<br />

	*/ ?>

</div>