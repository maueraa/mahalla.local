<?php
/* @var $this MfyXodimlarController */
/* @var $model MfyXodimlar */

$this->breadcrumbs=array(
	Yii::t('strings','МФЙ ходимлари')=>array("index?tuman_id=".Yii::app()->user->tuman),
	Yii::t('strings','МФЙ ходимларини қидириш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ ходимлари'))), 'url'=>array("index?tuman_id=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ ходимларини қиритиш'))), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('fuqaro--uy-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ ходимларини қидириш'))) ?></h1>

<?php echo CHtml::link(Yii::t('zii','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'mfy-xodimlar-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'viloyat_id',
		'tuman_id',
		'mfy_id',
		'mfy_rais',
		'mfy_rais_tel',
		/*
		'mfy_kotiba',
		'mfy_kotiba_tel',
		'mfy_posbon',
		'mfy_posbon_tel',
		'email',
		'user_id',
		'user_date',
		//'user_d.name',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
