<?php
/* @var $this MfyXodimlarController */
/* @var $model MfyXodimlar */

$this->breadcrumbs=array(
	Yii::t('strings','МФЙ ходимлари')=>array("index?tuman_id=".Yii::app()->user->tuman),
	Yii::t('strings','Киритиш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ ходимлари'))), 'url'=>array("index?tuman_id=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','МФЙ ходимларини қидирш'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ ходимларини киритиш')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>