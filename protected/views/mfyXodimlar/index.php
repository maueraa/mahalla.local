<?php
/* @var $this MfyXodimlarController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','МФЙ ходимлари'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ ходимларини киритиш'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ ходимларини қидириш'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','МФЙ ходимлари')?></h1>

<?php $this->widget('ext.bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
