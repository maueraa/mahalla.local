<?php
/* @var $this MfyXodimlarController */
/* @var $model MfyXodimlar */

$this->breadcrumbs=array(
	Yii::t('strings','МФЙ ходимлари')=>array("index?tuman_id=".Yii::app()->user->tuman),
	Yii::t('strings',$model->id)=>array('view','id'=>$model->id),
	Yii::t('strings','Ўзгартириш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ ходимлари'))), 'url'=>array("index?tuman_id=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ ходимларини киритиш'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ ходимини кўриш'))), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ ходимларини қидириш'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{table} "{record}" (id={id})', array(
	'{table}'=>Yii::t('strings','МФЙ ходимлари ўзгартириш'),
	'{record}'=>$model->id,
	'{id}'=>$model->id));?></h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>