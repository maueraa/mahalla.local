<?php
/* @var $this MfyXodimlarController */
/* @var $model MfyXodimlar */

$this->breadcrumbs=array(
	Yii::t('strings','МФЙ ходимларини')=>array("index?tuman_id=".Yii::app()->user->tuman),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ ходимлари'))), 'url'=>array("index?tuman_id=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ ходимларини киритиш'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ ходимини ўзгартириш'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ ходимини ўчириш'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙ ходимларини қидириш'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings',$model->id) .' - '.Yii::t('strings','МФЙ ходимидининг маълумотлари'); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'viloyat_id',
		'tuman_id',
		'mfy_id',
		'mfy_rais',
		'mfy_rais_tel',
		'mfy_kotiba',
		'mfy_kotiba_tel',
		'mfy_posbon',
		'mfy_posbon_tel',
		'email',
		'user_id',
		'user_date',
/**
		array(
			'label'=>$model->getAttributeLabel('user_date'),
			'type'=>'raw',
			'value'=>$model->user_d->name,
		),
/**/
	),
)); ?>
