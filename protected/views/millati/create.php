<?php
/* @var $this MillatiController */
/* @var $model Millati */

$this->breadcrumbs=array(
	Yii::t('strings','Millatis')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Millati'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Millati'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Millati')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>