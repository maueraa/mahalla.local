<?php
/* @var $this MillatiController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Millatis'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Millati'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Millati'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Millatis')?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
