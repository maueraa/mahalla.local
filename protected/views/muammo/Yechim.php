<?php
/* @var $this Fuqaro_UyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    Yii::t('strings','Муаммолар'),
);
$this->menu=array(
#	array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Хонадон қўшиш'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Хонадон қидириш'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Хўжаобод туманидаги хонадонларда мавжуд муаммолар')?></h1>
<div class="view">
    <?php $this->widget('bootstrap.widgets.TbListView', array(
    'dataProvider'=>$dataProvider,
    'itemView'=>'_view_yechim',
    'sortableAttributes'=>array('id','uy_raqami','anketa_sana'),
    'itemsTagName'=>'table',
    'itemsCssClass'=>'table table-bordered table-striped',
    'itemsHeader'=>"
	<tr>
	<th>№</th>
	<th>Мурожаат қилган фуқаронинг ФИШ ва манзили</th>
	<th>Муаммолар</th>
	<th>Бириктирилган ташкилот</th>
	<th>Белгиланган муддати</th>
	<th>Хал этилган сана</th>
	<th>Натижа</th>
	<th>Муаммони хал этилганлиги тасдиқлаш</th>



	</tr>
	",
)); ?>
</div>
