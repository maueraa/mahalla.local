﻿<?php
/* @var $this Fuqaro_UyController */
/* @var $data Fuqaro_Uy */

$uyegasi = Fuqaro::model()->find("fuqaro_uy_id=$data->id and oila_boshligi = '1' ");
$etiroz = ($data->etiroz == "1")? "etiroz":"";
$xal = (strtotime($data->muddat_sana)< time() )? "xal":"";
$korilmagan = ($data->korilgan_sana)=='' ? "korilmagan":"";
$xal_etildi= ($data->muammo_xolati_id)=='1' ? "xal_etildi":"";
$biriktirmagan= ($data->tashkilot_id)=='' ? "biriktirmagan":"";
?>

<tr class="view <?=$etiroz?> <?=$xal?> <?=$korilmagan?> <?=$xal_etildi?><?=$biriktirmagan?>">
    <td>    <?php echo CHtml::link(CHtml::encode($data->id), array('/jamlama/index', 'id'=>$data->id)); ?></td>
    <td>	<?php echo $data->fuqaro->fio; ?></td>
    <td>
        <?php if (isset($data->viloyat->viloyat_nomi))echo $data->viloyat->viloyat_nomi; ?>
        <?php if (isset($data->tuman->tuman_nomi))echo $data->tuman->tuman_nomi; ?>
        <?php if (isset($data->mfy->mfy_nomi))echo $data->mfy->mfy_nomi; ?>
        <?php if (isset($data->kocha->tbl_kocha_nomi))echo $data->kocha->tbl_kocha_nomi; ?>
        <?php if(isset($data->uy_raqami))echo CHtml::encode($data->uy_raqami); ?>
        <?php if(isset($data->tel_raqami))echo CHtml::encode($data->tel_raqami); ?>
    </td>
    <td><?php echo CHtml::encode($data->muammo_mazmuni); ?></td>
    <td>
        <?php echo CHtml::DropDownList('m8643','',CHtml::listData(Viloyatlar::model()->findAll(),'id','viloyat_nomi'),array('id'=>"id_vil2$data->id",'prompt'=>Yii::t("strings",'Вилоятни танланг')))  ?>
        <?php
        ECascadeDropDown::master("id_vil2$data->id")->setDependent("id_tuman2$data->id",
            array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadtuman');
        ?>
        <?php echo CHtml::DropDownList('m3492','',array(),array('id'=>"id_tuman2$data->id",'prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
        <form target="muammo" action='<?=Yii::app()->createUrl('/fuqaro_Muammolar/update',array('id'=>$data->id))?>' method='post'>
            <?php echo CHtml::DropDownList('Fuqaro_Muammolar[tashkilot_id]','',array(),array('id'=>"id_tashkilot$data->id",'prompt'=>Yii::t("strings",'Ташкилотни танланг')))  ?>
            <?php
            ECascadeDropDown::master("id_tuman2$data->id")->setDependent("id_tashkilot$data->id",
                array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadtashkilot');
            ?>
            <?php echo Yii::t('strings',"") ?><br>
            <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'Fuqaro_Muammolar[muddat_sana]',
            'attribute' => "muddat_sana",
            'language' => 'ru',
            'options' => array(
                'showAnim' => 'fold',
            ),
            'htmlOptions' => array(
                'style' => 'height:20px;',
                'id'=>"muddat_sana$data->id",
            ),
            ));?>
              <input type='submit' value="<?=Yii::t('strings','Бириктириш')?>">
        </form>
     </td>
</tr>


<?php /*
<div class="taxrirlash">
	<h9>Хонадон маълумотларини тахрирлаш</h9>
		<br>
	<?php echo  CHtml::link("Хонадон маълумотлари", array('/fuqaro_uy/update', 'id'=>$data->id));  ?>
	<?php echo  CHtml::link("Маънавий мухит", array('/manaviy/update', 'fid'=>$data->id));  ?>
	<?php echo  CHtml::link("Даромади", array('/daromad/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Қарздоллик", array('/qarz/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Транспорт қатнови", array('/transport_qatnovi/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Транспорт воситалари", array('/xonadon_transport/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Ижтимоий мухит", array('/ijtimoiy_muhit/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Шарт-шароити", array('/shaoriti/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Томорқа", array('/tomorqa/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Чорва хайвонлари", array('/hayvon/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Хизмат кўрсатиш объектлари", array('/ximzat_korsatish/update', 'fid'=>$data->id));  ?>
		<?php echo  CHtml::link("Кўчанинг холати", array('/kocha_xolati/update', 'fid'=>$data->id));  ?>
	</div>
	<br />

    <?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kadastr_seriyasi_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->kadastr_seriyasi_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kadastr_buyruq_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->kadastr_buyruq_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ijara')); ?>:</b>
	<?php echo CHtml::encode($data->ijara); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('azo_soni')); ?>:</b>
	<?php echo CHtml::encode($data->azo_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jon_soni')); ?>:</b>
	<?php echo CHtml::encode($data->jon_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('voya_yetmagan_soni')); ?>:</b>
	<?php echo CHtml::encode($data->voya_yetmagan_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('taklif')); ?>:</b>
	<?php echo CHtml::encode($data->taklif); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('muammosi')); ?>:</b>
	<?php echo CHtml::encode($data->muammosi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ishchi_guruh_fio_id')); ?>:</b>
	<?php echo CHtml::encode($data->ishchi_guruh_fio_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tel_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->tel_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anketa_sana')); ?>:</b>
	<?php echo CHtml::encode($data->anketa_sana); ?>
	<br />
 <b><?php echo CHtml::encode($data->getAttributeLabel('uy_raqami_position')); ?>:</b>
	<?php echo CHtml::encode($data->uy_raqami_position); ?>
	<br />

	*/ ?>

