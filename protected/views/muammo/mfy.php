﻿<?php
/* @var $this Fuqaro_UyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Fuqaro  Uys'),
);
$this->menu=array(
#	array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Хонадон қўшиш'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Хонадон қидириш'))), 'url'=>array('admin')),
);
?>
<h1><?php echo Yii::t('strings','{mfy}да истиқомат қилувчи фуқаролардан келган мурожаатларни ташкилотларга бириктириш', array('{mfy}'=>$mfy))?></h1>
<div class="view">
	<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'sortableAttributes'=>array('id','uy_raqami','anketa_sana'),
	'itemsTagName'=>'table',
    'itemsCssClass'=>'table table-bordered table-striped',
    'itemsHeader'=>"
	<tr>
	<th>№</th>
	<th>Мурожаат қилган фуқаронинг ФИШ</th>
	<th>Туман,МФЙ,кўчанинг номи ва уй рақами</th>


	<th>Муаммолар</th>
	<th>Ташкилот бириктириш</th>




	</tr>
	",
)); ?>
</div>




