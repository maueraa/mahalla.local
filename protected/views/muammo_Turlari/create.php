<?php
/* @var $this Muammo_TurlariController */
/* @var $model Muammo_Turlari */

$this->breadcrumbs=array(
	Yii::t('strings','Муаммо турлари')=>array('index'),
	Yii::t('strings','Киритиш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Муаммо турлари рўйхати'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Админ'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Муаммо турларини киритиш')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>