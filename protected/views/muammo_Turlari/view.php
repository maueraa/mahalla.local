<?php
/* @var $this Muammo_TurlariController */
/* @var $model Muammo_Turlari */

$this->breadcrumbs=array(
	Yii::t('strings','Muammo  Turlaris')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Muammo_Turlari'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Muammo_Turlari'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Muammo_Turlari'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Muammo_Turlari'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Muammo_Turlari'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Muammo_Turlari') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'muammo_turlari',
		'user_id',
		'user_date',
/**
		array(
			'label'=>$model->getAttributeLabel('user_date'),
			'type'=>'raw',
			'value'=>$model->user_d->name,
		),
/**/
	),
)); ?>
