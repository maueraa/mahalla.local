﻿<?php
/* @var $this Fuqaro_UyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Моддий ёрдамга мухтож фуқаролар'),
);
$this->menu=array(
#	array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Хонадон қўшиш'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Хонадон қидириш'))), 'url'=>array('admin')),
);
?>
<h1><?php #echo Yii::t('strings','{tuman}даги истиқомат қилувчи фуқаролардан келган мурожаатларни ташкилотларга бириктириш', array('{tuman}'=>$tuman))?></h1>
<div class="view">
	<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'sortableAttributes'=>array('id','uy_raqami'),
	'itemsTagName'=>'table',
    'itemsHeader'=>"
	<tr>
	<th>№</th>
	<th>Мурожаат қилган фуқаронинг ФИШ</th>
	<th>Туман,МФЙ,кўчанинг номи ва уй рақами</th>


	<th>Муаммолар</th>
	<th>Ёрдам кўрсатиш</th>




	</tr>
	",
)); ?>
</div>




