<?php
/* @var $this Muammo_XolatiController */
/* @var $model Muammo_Xolati */

$this->breadcrumbs=array(
	Yii::t('strings','Муаммо холатлари')=>array('index'),
	Yii::t('strings','Киритиш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Muammo_Xolati'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Muammo_Xolati'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Муаммо холатлари')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>