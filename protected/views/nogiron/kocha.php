<?php
/* @var $this Fuqaro_kasallarController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Кўчалар'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Fuqaro'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Fuqaro'))), 'url'=>array('admin')),
);

?>
<h1><?php echo Yii::t('strings','{kocha}да истиқомат қилувчи <br>Ногирон фуқаролар<br> хақида маълумот', array('{kocha}'=>$kocha))?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'itemsTagName'=>'table',
    'itemsHeader'=>"
	<tr>
	<th>ID</th>
	<th>Фамалия исм шариф</th>
	<th>Жинси</th>
	<th>Туғилган санаси ва пасспорт ёки метирка рақами </th>
	<th>Вилоят, туман, МФЙ, кўча ва уй рақами</th>
	<th>Кассалик холати ва диагнози</th>
	<th>Иш жойи,лавозими ва телефон рақами</th>

	</tr>",
)); ?>
