<?php
/* @var $this FuqaroController */
/* @var $model Fuqaro */

$this->breadcrumbs=array(
	Yii::t('strings','Фуқаро')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Фуқароларни умумий рўйхати'))), 'url'=>array('index')),
	#array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Fuqaro'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ўзгартириш'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Ўчириш'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Фуқароларни қидириш'))), 'url'=>array('admin')),
);
$fuqaro_m = Fuqaro_Uy::model()->find("id=$model->fuqaro_uy_id");

?>
<h1 align="center" ID="kirish"><?php echo (CHtml::encode($model->fio)); ?> хақида умумий маълумот</h1>

<br />
<div class="view">
	<table width='640'  border='0'>
		<tr>
			<td width='145' height='23' align='left' valign='top'>Исми шаърифи:</td>
			<td width='161' valign='top' colspan="2"><?php if(isset($model->fio)) echo Chtml::encode($model->fio)?></td>
			<td width='170' rowspan='3' valign='top' align='right'><IMG src='../images/<?php echo CHtml::encode($model->rasm); ?>' height='120' width='100' ></td>
		</tr>
		<tr>
			<td width='145' height='27' align='left' valign='top'>Ту&#1171;илган санаси:</td>
			<td width='125' valign='top' align='right'><?php if(isset($model->tugilgan_sanasi)) echo Chtml::encode($model->tugilgan_sanasi)?></td>
			<td width='159' valign='top' align='right'>&nbsp;</td>
		</tr>
		<tr>
			<td width='145' align='right' valign='top'>
				<p align="left">Ту&#1171;илган жойи:</td>
			<td width='125' valign='top' align='right'>
				<?php if(isset($model->tugilganJoyViloyat->viloyat_nomi)) echo Chtml::encode($model->tugilganJoyViloyat->viloyat_nomi)?>
				<?php if(isset($model->tugilganJoyTuman->tuman_nomi)) echo Chtml::encode($model->tugilganJoyTuman->tuman_nomi)?>
			</td>
			<td width='159' valign='top' align='right'>&nbsp;</td>
		</tr>
		<tr>
			<td height='28' align='right' valign='top'>
				<p align="left"><span lang="uz-uz-cyrl">Паспорт ёки метирка ра&#1179;ами</span></td>
			<td height='28'  colspan='2' align='right' valign='top'>
				<?php
				if(isset($data->passport_raqami))
				{
					echo CHtml::encode($data->passport_raqami);
					echo'<br>';
					if(isset($data->passport_kim_tomonidan_id)) {echo CHtml::encode($data->passportKimTomonidan->tuman_nomi);}
				}
				elseif(isset($data->metirka_raqami))
				{
					echo CHtml::encode($data->metirka_raqami);
					if(isset($data->metirka_kim_tomonidan))echo CHtml::encode($data->metirka_kim_tomonidan);
				}
				?>
			</td>
			<td height='28' align='right' valign='top'>
				&nbsp;</td>
		</tr>
		<tr>
			<td height='28' align='right' valign='top'>
				<p align="left">Яшаш манзили</td>
			<td height='28'  colspan='2' align='right' valign='top'>
				<?php if (isset($fuqaro_m->ftuman->tuman_nomi))echo $fuqaro_m->ftuman->tuman_nomi; ?>
				<?php if (isset($fuqaro_m->mfy->mfy_nomi))	echo $fuqaro_m->mfy->mfy_nomi;	?>
				<?php if (isset($fuqaro_m->kochalar->tbl_kocha_nomi))echo $fuqaro_m->kochalar->tbl_kocha_nomi; ?>
				<?php if (isset($fuqaro_m->uy_raqami))echo CHtml::encode($fuqaro_m->uy_raqami); ?>
			</td>
			<td height='28' align='right' valign='top'>
				&nbsp;</td>
		</tr>
		<tr>
			<td width='145' height='24' align='left' valign='top'>Маълумоти:</td>
			<td valign='top'><?php if (isset($model->malumoti->malumot_turi))echo $model->malumoti->malumot_turi; ?></td>
			<td valign='top'>Битирган жойи, мутахассислиги ва ў&#1179;ишни тамомлаган йили:</td>
			<td valign='top'>
				<?php if(isset($model->bitirgan_joy_id))echo $model->bitirgan_joy_id; ?>
				<?php if(isset($model->bitirgan_mutaxasisligi))echo $model->bitirgan_mutaxasisligi; ?>
				<?php if(isset($model->bitirgan_yili))echo $model->bitirgan_yili; ?>
			</td>
		</tr>
		<tr>
			<td width='145' height='24' align='left' valign='top'>&nbsp;</td>
			<td valign='top' colspan="2">&nbsp;</td>
			<td valign='top'>&nbsp;</td>
		</tr>
		<tr>
			<td width='145' height='24' align='left' valign='top'>Иш жойи ва лавозими:</td>
			<td valign='top' colspan="2">
				<?php if(isset($model->ish_joy_nomi))echo $model->ish_joy_nomi; ?>
				<?php if(isset($model->lavozimi))echo $model->lavozimi; ?>
			</td>
			<td valign='top'>&nbsp;</td>
		</tr>
		<tr>
			<td width='145' height='24' align='left' valign='top'>Даромад тури:</td>
			<td valign='top'><?php if(isset($model->daromads->nomi))echo $model->daromads->nomi; ?></td>
			<td valign='top'>Даромад ми&#1179;дори:</td>
			<td valign='top'><?php if(isset($model->daromad_miqdori))echo $model->daromad_miqdori; ?></td>
		</tr>
		<tr>
			<td width='145' height='24' align='left' valign='top'>ИИБ хисобида
				турадими:</td>
			<td valign='top'>
				<?php if(isset($model->iibXisobidaTuradimi->iib_xisobi))echo $model->iibXisobidaTuradimi->iib_xisobi; ?>
				<br><?php if(isset($model->deoAzoligi->deo_xolati))echo $model->deoAzoligi->deo_xolati; ?>
			</td>
			<td valign='top'>Судланганлиги:</td>
			<td valign='top'><?php if(isset($model->sudlanganligi))echo $model->sudlanganligi; ?></td>
		</tr>
		<tr>
			<td width='145' height='24' align='left' valign='top'>&nbsp;</td>
			<td valign='top' colspan="2">&nbsp;</td>
			<td valign='top'>&nbsp;</td>
		</tr>
		<tr>
			<td width='145' height='24' align='left' valign='top'>Узо&#1179; муддатга кетган ва келган вакти:</td>
			<td valign='top'>Узо&#1179; муддатга кетган давлати:</td>
			<td valign='top'><span lang="uz-uz-cyrl">Узо&#1179; муддатдан &#1179;айтгандан
			сўнг сухбатдан ўтганлиги:</span></td>
			<td valign='top'><span lang="uz-uz-cyrl">Узо&#1179; муддатга борган
			давлатда шу&#1171;улланган мехнат фаолияти:</span></td>
		</tr>
		<tr>
			<td width='145' height='24' align='left' valign='top'>
				<?php if(isset($model->uzoq_muddatga_ketgan_vaqti))echo CHtml::encode($model->uzoq_muddatga_ketgan_vaqti); ?>
				<?php if(isset($model->uzoq_muddatga_kelgan_vaqti))echo CHtml::encode($model->uzoq_muddatga_kelgan_vaqti); ?>
			</td>
			<td valign='top'><?php if(isset($model->uzoq_muddatga_ketgan_davlati))echo CHtml::encode($model->uzoq_muddatga_ketgan_davlati); ?></td>
			<td valign='top'><?php if(isset($model->uzoq_muddat_suhbat_otgani))echo CHtml::encode($model->uzoq_muddat_suhbat_otgani); ?></td>
			<td valign='top'><?php if(isset($model->uzoq_muddat_ish))echo CHtml::encode($model->uzoq_muddat_ish); ?></td>
		</tr>
		<tr>
			<td width='145' height='24' align='left' valign='top'>Никох тур:</td>
			<td valign='top'>
				<?php $niko=array('1'=>'Никохланмаган','2'=>'қонуний', '3'=>'шаръий');if(isset($model->nikox_turi))echo CHtml::encode($niko[$model->nikox_turi]); ?>
				<?php if(isset($model->nikox_guvohnom_raqami))echo CHtml::encode($model->nikox_guvohnom_raqami); ?>
			</td>
			<td valign='top'>Алимент(бор/йў&#1179; ва &#1179;арздорлиги):</td>
			<td valign='top'><?php if(isset($model->aliment->nomi))echo CHtml::encode($model->aliment->nomi); ?></td>
		</tr>
		<tr>
			<td width='145' height='24' align='left' valign='top'>Со&#1171;лиги:</td>
			<td valign='top' colspan="2">
				<?php if(isset($model->kassalik->kassalik_nomi))echo CHtml::encode($model->kassalik->kassalik_nomi); ?>
				<?php
				$kk=array('0'=>'','1'=>'Даволанишга мухтож', '2'=>'Оғир касал');
				if(isset($model->kassalik_xolati))echo CHtml::encode($kk[$model->kassalik_xolati]); ?>
			</td>
			<td valign='top'>&nbsp;</td>
		</tr>
		<tr>
			<td width='145' height='24' align='left' valign='top'>
				<span lang="uz-uz-cyrl">Анкета ўзгарган санаси:</span></td>
			<td valign='top' colspan="2"><?php if(isset($model->anketa_ozgarish))echo CHtml::encode($model->anketa_ozgarish); ?></td>
			<td valign='top'>&nbsp;</td>
		</tr>
	</table>

</div>


