<?php
/* @var $this NotinchController */
/* @var $data Notinch */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('oila_xolati')); ?>:</b>
	<?php echo CHtml::encode($data->oila_xolati); ?>
	<br />


</div>