<?php
/* @var $this NotinchController */
/* @var $model Notinch */

$this->breadcrumbs=array(
	Yii::t('strings','Notinches')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Notinch'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Notinch'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Notinch')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>