<?php
/* @var $this NotinchController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Notinches'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Notinch'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Notinch'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Notinches')?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
