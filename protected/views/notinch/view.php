<?php
/* @var $this NotinchController */
/* @var $model Notinch */

$this->breadcrumbs=array(
	Yii::t('strings','Notinches')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Notinch'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Notinch'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Notinch'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Notinch'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Notinch'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Notinch') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'oila_xolati',
/**
		array(
			'label'=>$model->getAttributeLabel('oila_xolati'),
			'type'=>'raw',
			'value'=>$model->oila_xol->name,
		),
/**/
	),
)); ?>
