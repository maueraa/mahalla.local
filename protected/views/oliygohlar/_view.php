<?php
/* @var $this OliygohlarController */
/* @var $data Oliygohlar */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('oligoh_nomi')); ?>:</b>
	<?php echo CHtml::encode($data->oligoh_nomi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('manzili')); ?>:</b>
	<?php echo CHtml::encode($data->manzili); ?>
	<br />


</div>