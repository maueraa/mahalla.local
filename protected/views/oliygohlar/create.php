<?php
/* @var $this OliygohlarController */
/* @var $model Oliygohlar */

$this->breadcrumbs=array(
	Yii::t('strings','Oliygohlars')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Oliygohlar'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Oliygohlar'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Oliygohlar')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>