<?php
/* @var $this OliygohlarController */
/* @var $model Oliygohlar */

$this->breadcrumbs=array(
	Yii::t('strings','Oliygohlars')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Oliygohlar'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Oliygohlar'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Oliygohlar'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Oliygohlar'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Oliygohlar'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Oliygohlar') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'oligoh_nomi',
		'manzili',
/**
		array(
			'label'=>$model->getAttributeLabel('manzili'),
			'type'=>'raw',
			'value'=>$model->manz->name,
		),
/**/
	),
)); ?>
