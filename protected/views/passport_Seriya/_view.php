<?php
/* @var $this Passport_SeriyaController */
/* @var $data Passport_Seriya */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('harfi')); ?>:</b>
	<?php echo CHtml::encode($data->harfi); ?>
	<br />


</div>