<?php
/* @var $this Passport_SeriyaController */
/* @var $model Passport_Seriya */

$this->breadcrumbs=array(
	Yii::t('strings','Passport  Seriyas')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Passport_Seriya'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Passport_Seriya'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Passport_Seriya')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>