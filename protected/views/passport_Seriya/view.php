<?php
/* @var $this Passport_SeriyaController */
/* @var $model Passport_Seriya */

$this->breadcrumbs=array(
	Yii::t('strings','Passport  Seriyas')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Passport_Seriya'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Passport_Seriya'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Passport_Seriya'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Passport_Seriya'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Passport_Seriya'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Passport_Seriya') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'harfi',
/**
		array(
			'label'=>$model->getAttributeLabel('harfi'),
			'type'=>'raw',
			'value'=>$model->ha->name,
		),
/**/
	),
)); ?>
