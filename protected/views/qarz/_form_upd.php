﻿<?php
/* @var $this QarzController */
/* @var $model Qarz */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'qarz-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?>
	<br>
<br>
	Агар маълумотлар етарли бўлмаса киритилаётган майдонга 0 ва бошқа белгиларни киритманг
	</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'umumiy_qarz'); ?>
		<?php echo $form->textField($model,'umumiy_qarz',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'umumiy_qarz'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'eletr_qarz'); ?>
		<?php echo $form->textField($model,'eletr_qarz',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'eletr_qarz'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'suv_qarz'); ?>
		<?php echo $form->textField($model,'suv_qarz',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'suv_qarz'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gaz_qarz'); ?>
		<?php echo $form->textField($model,'gaz_qarz',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'gaz_qarz'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'soliq_yer_qarz'); ?>
		<?php echo $form->textField($model,'soliq_yer_qarz',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'soliq_yer_qarz'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'soliq_mulk_qarz'); ?>
		<?php echo $form->textField($model,'soliq_mulk_qarz',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'soliq_mulk_qarz'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'schr_qarz'); ?>
		<?php echo $form->textField($model,'schr_qarz',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'schr_qarz'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'chiqindi_qarz'); ?>
		<?php echo $form->textField($model,'chiqindi_qarz',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'chiqindi_qarz'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'qarz_boshqalar'); ?>
		<?php echo $form->textField($model,'aliment_qarz',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'aliment_qarz'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'qarz_boshqalar'); ?>
		<?php echo $form->textField($model,'qarz_boshqalar',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'qarz_boshqalar'); ?>
	</div>

	<div class="row">
		<?php  $form->labelEx($model,'tbl_fuqaro_uy_id'); ?>
		<?php echo $form->hiddenField($model,'tbl_fuqaro_uy_id',array('value'=>$fid)); ?>
		<?php echo $form->error($model,'tbl_fuqaro_uy_id'); ?>
	</div>

	<div class="row">
		<?php  $form->labelEx($model,'tbl_fuqaro_id'); ?>
		<?php echo $form->hiddenField($model,'tbl_fuqaro_id'); ?>
		<?php echo $form->error($model,'tbl_fuqaro_id'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'anketa_ozgarish').date("d.m.Y"); ?>
        <?php echo $form->hiddenField($model,'anketa_ozgarish',array('value'=>date("Y-m-d"))); ?>
        <?php echo $form->error($model,'anketa_ozgarish'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id').Yii::app()->user->name; ?>
		<?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_date').date("d.m.Y"); ?>
		<?php echo $form->hiddenField($model,'user_date',array('value'=>date("Y-m-d"))); ?>
		<?php echo $form->error($model,'user_date'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array("class"=>"btn btn-primary")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->