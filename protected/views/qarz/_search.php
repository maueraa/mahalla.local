<?php
/* @var $this QarzController */
/* @var $model Qarz */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'umumiy_qarz'); ?>
		<?php echo $form->textField($model,'umumiy_qarz',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'eletr_qarz'); ?>
		<?php echo $form->textField($model,'eletr_qarz',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gaz_qarz'); ?>
		<?php echo $form->textField($model,'gaz_qarz',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'suv_qarz'); ?>
		<?php echo $form->textField($model,'suv_qarz',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'soliq_yer_qarz'); ?>
		<?php echo $form->textField($model,'soliq_yer_qarz',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'soliq_mulk_qarz'); ?>
		<?php echo $form->textField($model,'soliq_mulk_qarz',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'schr_qarz'); ?>
		<?php echo $form->textField($model,'schr_qarz',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'chiqindi_qarz'); ?>
		<?php echo $form->textField($model,'chiqindi_qarz',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'aliment_qarz'); ?>
		<?php echo $form->textField($model,'aliment_qarz',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qarz_boshqalar'); ?>
		<?php echo $form->textField($model,'qarz_boshqalar',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tbl_fuqaro_uy_id'); ?>
		<?php echo $form->textField($model,'tbl_fuqaro_uy_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tbl_fuqaro_id'); ?>
		<?php echo $form->textField($model,'tbl_fuqaro_id'); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model,'anketa_ozgarish'); ?>
        <?php echo $form->textField($model,'anketa_ozgarish'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->