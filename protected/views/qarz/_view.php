<?php
/* @var $this QarzController */
/* @var $data Qarz */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('umumiy_qarz')); ?>:</b>
	<?php echo CHtml::encode($data->umumiy_qarz); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('eletr_qarz')); ?>:</b>
	<?php echo CHtml::encode($data->eletr_qarz); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gaz_qarz')); ?>:</b>
	<?php echo CHtml::encode($data->gaz_qarz); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('suv_qarz')); ?>:</b>
	<?php echo CHtml::encode($data->suv_qarz); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('soliq_yer_qarz')); ?>:</b>
	<?php echo CHtml::encode($data->soliq_yer_qarz); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('soliq_mulk_qarz')); ?>:</b>
	<?php echo CHtml::encode($data->soliq_mulk_qarz); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('schr_qarz')); ?>:</b>
	<?php echo CHtml::encode($data->schr_qarz); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('chiqindi_qarz')); ?>:</b>
	<?php echo CHtml::encode($data->chiqindi_qarz); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('aliment_qarz')); ?>:</b>
	<?php echo CHtml::encode($data->aliment_qarz); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qarz_boshqalar')); ?>:</b>
	<?php echo CHtml::encode($data->qarz_boshqalar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tbl_fuqaro_uy_id')); ?>:</b>
	<?php echo CHtml::encode($data->tbl_fuqaro_uy_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tbl_fuqaro_id')); ?>:</b>
	<?php echo CHtml::encode($data->tbl_fuqaro_id); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('anketa_ozgarish')); ?>:</b>
	<?php echo CHtml::encode($data->anketa_ozgarish); ?>
	<br />

	*/ ?>

</div>