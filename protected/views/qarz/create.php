﻿<?php
/* @var $this QarzController */
/* @var $model Qarz */

$this->breadcrumbs=array(
	Yii::t('strings','Qarzs')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Qarz'))), 'url'=>array('index')),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Qarz'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Солиқ ва коммунал хизматларидан бўлган қарздорлик тўғрисида маълумот')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'fid'=>$fid)); ?>