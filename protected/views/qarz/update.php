<?php
/* @var $this QarzController */
/* @var $model Qarz */

$this->breadcrumbs=array(
	Yii::t('strings','Қарздорлик')=>array('index'),
	Yii::t('strings',$model->id)=>array('view','id'=>$model->id),
	Yii::t('strings','Ўзгартириш'),
);

$this->menu=array(
	#array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Qarz'))), 'url'=>array('index')),
	#array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Qarz'))), 'url'=>array('create')),
	#array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Qarz'))), 'url'=>array('admin')),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон маълумотлари'))), 'url'=>array("/fuqaro_Uy/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон аъзолари'))), 'url'=>array("/fuqarolar?fuq=$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Маънавий мухит'))), 'url'=>array("/manaviy/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Даромади'))), 'url'=>array("/daromad/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Қарздоллик хақидаги маълумотларни кўриш'))), 'url'=>array('view', 'id'=>$model->tbl_fuqaro_uy_id)),
#    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Қарздоллик'))), 'url'=>array('/qarz/update', 'fid'=>$fid)),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт қатнови'))), 'url'=>array("/transport_qatnovi/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт воситалари'))), 'url'=>array("/xonadon_transport/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ижтимоий мухит'))), 'url'=>array("/ijtimoiy_muhit/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Шарт-шароити'))), 'url'=>array("/shaoriti/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Томорқа'))), 'url'=>array("/tomorqa/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Чорва хайвонлари'))), 'url'=>array("/hayvon/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хизмат кўрсатиш объектлари'))), 'url'=>array("/ximzat_korsatish/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчанинг холати'))), 'url'=>array("/kocha_xolati/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Барча маълумотларни кўриш'))), 'url'=>array('/jamlama/index', 'id'=>$model->tbl_fuqaro_uy_id)),

);
?>

<h1><?php echo Yii::t('strings',' {table} "{record}" (id={id})', array(
	'{table}'=>Yii::t('strings','Солиқ ва коммунал хизматларидан бўлган қарздорлик тўғрисида маълумотkтарни тахрирлаш'),
	'{record}'=>$model->id,
	'{id}'=>$model->id));?></h1>
<?php $this->renderPartial('_form_upd', array('model'=>$model,'fid'=>$fid)); ?>