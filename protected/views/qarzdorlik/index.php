﻿<?php
/* @var $this Fuqaro_UyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Хонадонлар'),
);
$this->menu=array(
	#array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Хонадон қўшиш'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Хонадон қидириш'))), 'url'=>array('admin')),
);

?>
<h1><?php echo Yii::t('strings','{tuman}даги <br>Қарздор хонадонлар<br> хақида маълумот', array('{tuman}'=>$tuman))?></h1>
<div class="view">
	<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'sortableAttributes'=>array('id','uy_raqami','anketa_sana'),
	'itemsTagName'=>'table',
    'itemsCssClass'=>'table table-bordered table-striped',
    'itemsHeader'=>"
	<thead><tr>
	<th>№</th>
	<th>Хонадон бошлиғи</th>
	<th>МФЙ номи</th>
	<th>Кўчанинг номи</th>
	<th>Уй рақами</th>
	<th>Кадастр мавжудлиги</th>
	<th>Хонадонда яшовчи фуқаролар</th>
	</tr></thead>
	",
)); ?>
</div>




