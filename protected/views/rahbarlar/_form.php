<?php
/* @var $this RahbarlarController */
/* @var $model Rahbarlar */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'rahbarlar-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'r_fio'); ?>
		<?php echo $form->textField($model,'r_fio',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'r_fio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ish_joy_nomi'); ?>
		<?php echo $form->textField($model,'ish_joy_nomi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ish_joy_nomi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lavozimi'); ?>
		<?php echo $form->textField($model,'lavozimi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'lavozimi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'viloyat_id'); ?>
        <?php echo $form->dropDownList($model,'viloyat_id',$model->getfviloyat(),array('id'=>'id_province','promt'=>Yii::t("strings",'Вилоятни танланг'))); ?>
		<?php echo $form->error($model,'viloyat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tuman_id'); ?>
        <?php echo $form->dropDownList($model,'tuman_id',array(),array('id'=>'id_tuman','promt'=>Yii::t("strings",'Туманни танланг'))); ?>
		<?php echo $form->error($model,'tuman_id'); ?>
	</div>
    <?php
    ECascadeDropDown::master('id_province')->setDependent('id_tuman',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadtuman');
    ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->