<?php
/* @var $this RahbarlarController */
/* @var $model Rahbarlar */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_fio'); ?>
		<?php echo $form->textField($model,'r_fio',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ish_joy_nomi'); ?>
		<?php echo $form->textField($model,'ish_joy_nomi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lavozimi'); ?>
		<?php echo $form->textField($model,'lavozimi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'viloyat_id'); ?>
		<?php echo $form->textField($model,'viloyat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tuman_id'); ?>
		<?php echo $form->textField($model,'tuman_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->