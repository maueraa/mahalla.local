<?php
/* @var $this RahbarlarController */
/* @var $data Rahbarlar */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('sektor')); ?>:</b>
	<b><?php echo CHtml::encode($data->sektor); ?></b>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_fio')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->r_fio),array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ish_joy_nomi')); ?>:</b>
	<?php echo CHtml::encode($data->ish_joy_nomi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lavozimi')); ?>:</b>
	<?php echo CHtml::encode($data->lavozimi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('viloyat_id')); ?>:</b>
	<?php echo CHtml::encode($data->viloyat->viloyat_nomi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tuman_id')); ?>:</b>
	<?php echo CHtml::encode($data->tuman->tuman_nomi); ?>
	<br />


</div>