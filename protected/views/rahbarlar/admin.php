<?php
/* @var $this RahbarlarController */
/* @var $model Rahbarlar */

$this->breadcrumbs=array(
	Yii::t('strings','Рахбарлар')=>array("index?val_mfy=".Yii::app()->user->tuman),
	Yii::t('strings','Тахрирлаш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label} рўйхати', array('{label}'=>Yii::t('strings','Рахбарлар'))), 'url'=>array("index?val_mfy=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label}ни киритиш', array('{label}'=>Yii::t('strings','Рахбарлар'))), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('fuqaro--uy-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1 style="text-align: center; text-transform: uppercase;"><?php echo Yii::t('strings','{label}ни тахрирлаш', array('{label}'=>Yii::t('strings','Рахбарлар'))) ?></h1>


<?php echo CHtml::link(Yii::t('zii','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'rahbarlar-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'r_fio',
		'ish_joy_nomi',
		'lavozimi',
		'viloyat.viloyat_nomi',
		'tuman.tuman_nomi',
		//'tuman.name',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
