<?php
/* @var $this RahbarlarController */
/* @var $model Rahbarlar */

$this->breadcrumbs=array(
	Yii::t('strings','Сектор рахбарлари')=>array("index?val_mfy=".Yii::app()->user->tuman),
	Yii::t('strings','Киритиш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label} рўйхати', array('{label}'=>Yii::t('strings','Рахбарлар'))), 'url'=>array("index?val_mfy=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label} тахрирлаш', array('{label}'=>Yii::t('strings','Рахбарлар'))), 'url'=>array('admin')),
);
?>

<h1 style="text-align: center; text-transform: uppercase;"><?php echo Yii::t('strings',' сектор {label}и киритиш', array('{label}'=>Yii::t('strings','Рахбарлар')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>