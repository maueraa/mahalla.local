<?php
/* @var $this RahbarlarController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Сектор рахбарлари'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label} киритиш', array('{label}'=>Yii::t('strings','Рахбарлар'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label} тахрирлаш', array('{label}'=>Yii::t('strings','Рахбарлар'))), 'url'=>array('admin')),
);
?>

<h1 style="text-align: center; text-transform: uppercase;"><?php echo Yii::app()->user->tuman; echo Yii::t('strings','СЕТКОР Рахбарлар')?></h1>

<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
