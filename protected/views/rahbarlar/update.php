<?php
/* @var $this RahbarlarController */
/* @var $model Rahbarlar */

$this->breadcrumbs=array(
	Yii::t('strings','Сектор рахбарлари')=>array("index?val_mfy=".Yii::app()->user->tuman),
	Yii::t('strings',$model->id)=>array('view','id'=>$model->id),
	Yii::t('strings','Ўзгартириш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label} рўйхати', array('{label}'=>Yii::t('strings','Рахбарлар'))), 'url'=>array("index?val_mfy=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label}ни киритиш', array('{label}'=>Yii::t('strings','Рахбарлар'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label} кўриш', array('{label}'=>Yii::t('strings','Рахбарлар'))), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','{label}ни тахрирлаш', array('{label}'=>Yii::t('strings','Рахбарлар'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Update {table} "{record}" (id={id})', array(
	'{table}'=>Yii::t('strings','Рахбарлар'),
	'{record}'=>$model->r_fio,
	'{id}'=>$model->id));?></h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>