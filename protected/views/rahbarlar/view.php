<?php
/* @var $this RahbarlarController */
/* @var $model Rahbarlar */

$this->breadcrumbs=array(
	Yii::t('strings','Рахбарлар')=>array("index?val_mfy=".Yii::app()->user->tuman),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label} рўйхати', array('{label}'=>Yii::t('strings','Рахбарлар'))), 'url'=>array("index?val_mfy=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label}ни киритиш', array('{label}'=>Yii::t('strings','Рахбарлар'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}ни ўзгартириш', array('{label}'=>Yii::t('strings','Рахбар'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','{label}ни ўчириш', array('{label}'=>Yii::t('strings','Рахбар'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','{label} тахрирлаш', array('{label}'=>Yii::t('strings','Рахбарлар'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Рахбарлар') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'r_fio',
		'ish_joy_nomi',
		'lavozimi',
		'viloyat_id',
		'tuman_id',
/**
		array(
			'label'=>$model->getAttributeLabel('tuman_id'),
			'type'=>'raw',
			'value'=>$model->tuman->name,
		),
/**/
	),
)); ?>
