<?php
/* @var $this SektorController */
/* @var $model Sektor */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'sektor-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'sektor_raqami'); ?>
		<?php echo $form->textField($model,'sektor_raqami'); ?>
		<?php echo $form->error($model,'sektor_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sektor_viloyat_id'); ?>
        <?php echo $form->dropDownList($model,'sektor_viloyat_id',$model->getfviloyat(),array('id'=>'id_province','promt'=>Yii::t("strings",'Вилоятни танланг'))); ?>
        <?php echo $form->error($model,'sektor_viloyat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sektor_tuman_id'); ?>
        <?php echo $form->dropDownList($model,'sektor_tuman_id',array(),array('id'=>'id_tuman','promt'=>Yii::t("strings",'Туманни танланг'))); ?>
		<?php echo $form->error($model,'sektor_tuman_id'); ?>
	</div>
    <?php
    ECascadeDropDown::master('id_province')->setDependent('id_tuman',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadtuman');
    ?>
    <div class="row">
        <?php echo $form->labelEx($model,'sektor_ish_joyi'); ?>
        <?php echo $form->dropdownlist($model,'sektor_ish_joyi',array(),array('id'=>'igf_id','prompt'=>Yii::t("strings",'Ташкилотни танланг'))); ?>
        <?php echo $form->error($model,'sektor_ish_joyi'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'sektor_rahbari'); ?>
		<?php echo $form->dropDownList($model,'sektor_rahbari',array(),array('id'=>'id_fio','prompt'=>Yii::t("strings",'Ташкилотни танланг'))); ?>
		<?php echo $form->error($model,'sektor_rahbari'); ?>
	</div>
    <?php
    ECascadeDropDown::master('id_tuman')->setDependent('igf_id',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_uy/loadigfs');
    ?>
    <?php
    ECascadeDropDown::master('igf_id')->setDependent('id_fio',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_uy/loadigffios');
    ?>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array('class'=>'btn bnt-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->