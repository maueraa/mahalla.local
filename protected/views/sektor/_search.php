<?php
/* @var $this SektorController */
/* @var $model Sektor */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sektor_raqami'); ?>
		<?php echo $form->textField($model,'sektor_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sektor_viloyat_id'); ?>
		<?php echo $form->textField($model,'sektor_viloyat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sektor_tuman_id'); ?>
		<?php echo $form->textField($model,'sektor_tuman_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sektor_mfy_id'); ?>
		<?php echo $form->textField($model,'sektor_mfy_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sektor_rahbari'); ?>
		<?php echo $form->textField($model,'sektor_rahbari'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sektor_ish_joyi'); ?>
		<?php echo $form->textField($model,'sektor_ish_joyi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->