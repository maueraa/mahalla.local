<?php
/* @var $this SektorController */
/* @var $data Sektor */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sektor_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->sektor_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sektor_viloyat_id')); ?>:</b>
	<?php if(isset($data->sektor_viloyat_id)) echo CHtml::encode($data->sektorViloyat->viloyat_nomi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sektor_tuman_id')); ?>:</b>
	<?php if (isset($data->sektor_tuman_id))echo CHtml::encode($data->sektorTuman->tuman_nomi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sektor_mfy_id')); ?>:</b>
	<?php if(isset($data->sektor_mfy_id))echo CHtml::encode($data->sektorMfy->mfy_nomi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sektor_rahbari')); ?>:</b>
	<?php if(isset($data->sektor_rahbari))echo CHtml::encode($data->sektorRahbari->r_fio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sektor_ish_joyi')); ?>:</b>
	<?php if(isset($data->sektor_rahbari))echo CHtml::encode($data->sektorRahbari->ish_joy_nomi); ?>
	<?php if(isset($data->sektor_rahbari))echo CHtml::encode($data->sektorRahbari->lavozimi); ?>
	<br />


</div>