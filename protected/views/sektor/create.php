<?php
/* @var $this SektorController */
/* @var $model Sektor */

$this->breadcrumbs=array(
	Yii::t('strings','Sektors')=>array("index?tuman_id=".Yii::app()->user->tuman),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label} рўйхати', array('{label}'=>Yii::t('strings','Сектор'))), 'url'=>array("index?tuman_id=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label} тахрирлаш', array('{label}'=>Yii::t('strings','Сектор'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{label} хосил қилиш', array('{label}'=>Yii::t('strings','Сектор')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>