<?php
/* @var $this SektorController */
/* @var $model Sektor */

$this->breadcrumbs=array(
	Yii::t('strings','Секторлар')=>array("index?tuman_id=".Yii::app()->user->tuman),
	Yii::t('strings','Секторга МФЙ бириктириш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label} рўйхати', array('{label}'=>Yii::t('strings','Сектор'))), 'url'=>array("index?tuman_id=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label} тахрирлаш', array('{label}'=>Yii::t('strings','Сектор'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{label}га МФЙ бириктириш ', array('{label}'=>Yii::t('strings','Сектор')));?></h1>

<?php $this->renderPartial('_form_add_mfy', array('model'=>$model)); ?>