<?php
/* @var $this SektorController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Сектор'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label} хосил қилиш', array('{label}'=>Yii::t('strings','Сектор'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}ни тахрирлаш', array('{label}'=>Yii::t('strings','Сектор'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Сектор')?></h1>

<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'itemsTagName'=>'table',
    'itemsHeader'=>"
	<tr>
	<th>№</th>
	<th>Сектор</th>
	<th>Бириктирилган МФЙ</th>
	<th>Хонадон сони</th>
	<th>Ахоли сони</th>
	</tr>
	",

)); ?>
