<?php
/* @var $this SektorController */
/* @var $model Sektor */

$this->breadcrumbs=array(
	Yii::t('strings','Сектор')=>array('index'),
	Yii::t('strings',$model->id)=>array('view','id'=>$model->id),
	Yii::t('strings','Ўзгартириш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}лар рўйхати', array('{label}'=>Yii::t('strings','Сектор'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','{label} киритиш', array('{label}'=>Yii::t('strings','Сектор'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}ни кўриш', array('{label}'=>Yii::t('strings','Сектор'))), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','{label}ларни тахрирлаш', array('{label}'=>Yii::t('strings','Сектор'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Update {table} "{record}" (id={id})', array(
	'{table}'=>Yii::t('strings','Секторлар'),
	'{record}'=>$model->id,
	'{id}'=>$model->id));?></h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>