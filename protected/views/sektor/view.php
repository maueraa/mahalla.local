<?php
/* @var $this SektorController */
/* @var $model Sektor */

$this->breadcrumbs=array(
	Yii::t('strings','Sektors')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Sektor'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Sektor'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Sektor'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Sektor'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Sektor'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Sektor') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'sektor_raqami',
		'sektor_viloyat_id',
		'sektor_tuman_id',
		'sektor_mfy_id',
		'sektor_rahbari',
		'sektor_ish_joyi',
/**
		array(
			'label'=>$model->getAttributeLabel('sektor_ish_joyi'),
			'type'=>'raw',
			'value'=>$model->sektor_ish_j->name,
		),
/**/
	),
)); ?>
