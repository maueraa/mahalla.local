<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sektor_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->sektor_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('viloyat_id')); ?>:</b>
	<?php echo CHtml::encode($data->viloyat_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tuman_id')); ?>:</b>
	<?php echo CHtml::encode($data->tuman_id); ?>
	<br />


</div>