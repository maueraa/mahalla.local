<?php
$this->breadcrumbs=array(
	'Sektorlars'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Sektorlar','url'=>array('index')),
	array('label'=>'Manage Sektorlar','url'=>array('admin')),
);
?>

<h1>Create Sektorlar</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>