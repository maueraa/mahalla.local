<?php
$this->breadcrumbs=array(
	'Sektorlars',
);

$this->menu=array(
	array('label'=>'Create Sektorlar','url'=>array('create')),
	array('label'=>'Manage Sektorlar','url'=>array('admin')),
);
?>

<h1>Sektorlars</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
