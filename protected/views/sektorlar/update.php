<?php
$this->breadcrumbs=array(
	'Sektorlars'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Sektorlar','url'=>array('index')),
	array('label'=>'Create Sektorlar','url'=>array('create')),
	array('label'=>'View Sektorlar','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Sektorlar','url'=>array('admin')),
);
?>

<h1>Update Sektorlar <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>