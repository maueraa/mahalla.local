<?php
$this->breadcrumbs=array(
	'Sektorlars'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Sektorlar','url'=>array('index')),
	array('label'=>'Create Sektorlar','url'=>array('create')),
	array('label'=>'Update Sektorlar','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Sektorlar','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Sektorlar','url'=>array('admin')),
);
?>

<h1>View Sektorlar #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'sektor_raqami',
		'viloyat_id',
		'tuman_id',
	),
)); ?>
