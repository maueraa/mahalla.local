<?php
/* @var $this ShaharController */
/* @var $model Shahar */

$this->breadcrumbs=array(
	Yii::t('strings','Shahars')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Shahar'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Shahar'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Shahar')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>