﻿<?php
/* @var $this ShaoritiController */
/* @var $model Shaoriti */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'shaoriti-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'vosita_soni'); ?>
		<?php echo $form->textField($model,'vosita_soni'); ?>
		<?php echo $form->error($model,'vosita_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'televizor'); ?>
		<?php echo $form->textField($model,'televizor'); ?>
		<?php echo $form->error($model,'televizor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kir_yuvish_vositasi'); ?>
		<?php echo $form->textField($model,'kir_yuvish_vositasi'); ?>
		<?php echo $form->error($model,'kir_yuvish_vositasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sovutgich'); ?>
		<?php echo $form->textField($model,'sovutgich'); ?>
		<?php echo $form->error($model,'sovutgich'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DVD'); ?>
		<?php echo $form->textField($model,'DVD'); ?>
		<?php echo $form->error($model,'DVD'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Konditsioner'); ?>
		<?php echo $form->textField($model,'Konditsioner'); ?>
		<?php echo $form->error($model,'Konditsioner'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kompyuter'); ?>
		<?php echo $form->textField($model,'kompyuter'); ?>
		<?php echo $form->error($model,'kompyuter'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'internet'); ?>
		<?php echo $form->textField($model,'internet',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'internet'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dazmol'); ?>
		<?php echo $form->textField($model,'dazmol'); ?>
		<?php echo $form->error($model,'dazmol'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vosita_boshqalar'); ?>
		<?php echo $form->textField($model,'vosita_boshqalar',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'vosita_boshqalar'); ?>
	</div>

	<div class="row">
		<?php  $form->labelEx($model,'tbl_fuqaro_uy_id'); ?>
		<?php echo $form->hiddenField($model,'tbl_fuqaro_uy_id',array('value'=>$fid)); ?>
		<?php echo $form->error($model,'tbl_fuqaro_uy_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id').Yii::app()->user->name; ?>
		<?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_date').date("d.m.Y"); ?>
		<?php echo $form->hiddenField($model,'user_date',array('value'=>date("Y-m-d"))); ?>
		<?php echo $form->error($model,'user_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array("class"=>"btn btn-primary")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->