<?php
/* @var $this ShaoritiController */
/* @var $model Shaoriti */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vosita_soni'); ?>
		<?php echo $form->textField($model,'vosita_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'televizor'); ?>
		<?php echo $form->textField($model,'televizor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kir_yuvish_vositasi'); ?>
		<?php echo $form->textField($model,'kir_yuvish_vositasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sovutgich'); ?>
		<?php echo $form->textField($model,'sovutgich'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DVD'); ?>
		<?php echo $form->textField($model,'DVD'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Konditsioner'); ?>
		<?php echo $form->textField($model,'Konditsioner'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kompyuter'); ?>
		<?php echo $form->textField($model,'kompyuter'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'internet'); ?>
		<?php echo $form->textField($model,'internet',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dazmol'); ?>
		<?php echo $form->textField($model,'dazmol'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vosita_boshqalar'); ?>
		<?php echo $form->textField($model,'vosita_boshqalar',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tbl_fuqaro_uy_id'); ?>
		<?php echo $form->textField($model,'tbl_fuqaro_uy_id'); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model,'anketa_ozgarish'); ?>
        <?php echo $form->textField($model,'anketa_ozgarish'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->