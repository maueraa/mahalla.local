<?php
/* @var $this ShaoritiController */
/* @var $data Shaoriti */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vosita_soni')); ?>:</b>
	<?php echo CHtml::encode($data->vosita_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('televizor')); ?>:</b>
	<?php echo CHtml::encode($data->televizor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kir_yuvish_vositasi')); ?>:</b>
	<?php echo CHtml::encode($data->kir_yuvish_vositasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sovutgich')); ?>:</b>
	<?php echo CHtml::encode($data->sovutgich); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DVD')); ?>:</b>
	<?php echo CHtml::encode($data->DVD); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Konditsioner')); ?>:</b>
	<?php echo CHtml::encode($data->Konditsioner); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kompyuter')); ?>:</b>
	<?php echo CHtml::encode($data->kompyuter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('internet')); ?>:</b>
	<?php echo CHtml::encode($data->internet); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dazmol')); ?>:</b>
	<?php echo CHtml::encode($data->dazmol); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vosita_boshqalar')); ?>:</b>
	<?php echo CHtml::encode($data->vosita_boshqalar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tbl_fuqaro_uy_id')); ?>:</b>
	<?php echo CHtml::encode($data->tbl_fuqaro_uy_id); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('anketa_ozgarish')); ?>:</b>
	<?php echo CHtml::encode($data->anketa_ozgarish); ?>
	<br />

	*/ ?>

</div>