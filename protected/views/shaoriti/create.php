﻿<?php
/* @var $this ShaoritiController */
/* @var $model Shaoriti */

$this->breadcrumbs=array(
	Yii::t('strings','Shaoritis')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Shaoriti'))), 'url'=>array('index')),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Shaoriti'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадондаги шарт - шароитлар тўғрисида маълумотларни киритиш ойнаси ')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'fid'=>$fid)); ?>