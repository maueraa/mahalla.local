<?php
/* @var $this ShaoritiController */
/* @var $model Shaoriti */

$this->breadcrumbs=array(
	Yii::t('strings','Хонадонинг шарт-шароити')=>array('index'),
	$model->id,
);

$this->menu=array(
#	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Shaoriti'))), 'url'=>array('index')),
#	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Shaoriti'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Shaoriti'))), 'url'=>array('update', 'id'=>$model->id)),
#	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Shaoriti'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Shaoriti'))), 'url'=>array('admin')),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон маълумотлари'))), 'url'=>array("/fuqaro_Uy/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон аъзолари'))), 'url'=>array("/fuqarolar?fuq=$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Маънавий мухит'))), 'url'=>array("/manaviy/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Даромади'))), 'url'=>array("/daromad/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Қарздоллик'))), 'url'=>array("/qarz/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт қатнови'))), 'url'=>array("/transport_qatnovi/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ижтимоий мухит'))), 'url'=>array("/ijtimoiy_muhit/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Шарт-шароит хақидаги маълумотларни ўзгартириш'))), 'url'=>array('update', 'fid'=>$model->tbl_fuqaro_uy_id)),
    #array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Шарт-шароити'))), 'url'=>array("/shaoriti/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Томорқа'))), 'url'=>array("/tomorqa/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Чорва хайвонлари'))), 'url'=>array("/hayvon/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хизмат кўрсатиш объектлари'))), 'url'=>array("/ximzat_korsatish/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчанинг холати'))), 'url'=>array("/kocha_xolati/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Барча маълумотларни кўриш'))), 'url'=>array('/jamlama/index', 'id'=>$model->tbl_fuqaro_uy_id)),
);
?>

<h1><?php echo Yii::t('strings','Хонадондаги шарт - шароитлар тўғрисида маълумотларни янгиланган холати ойнаси') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'vosita_soni',
		'televizor',
		'kir_yuvish_vositasi',
		'sovutgich',
		'DVD',
		'Konditsioner',
		'kompyuter',
		'internet',
		'dazmol',
		'vosita_boshqalar',
		'tbl_fuqaro_uy_id',
        'anketa_ozgarish',
		'user_id',
		'user_date',
/**
		array(
			'label'=>$model->getAttributeLabel('tbl_fuqaro_uy_id'),
			'type'=>'raw',
			'value'=>$model->tbl_fuqaro_uy->name,
		),
/**/
	),
)); ?>
