<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::t('zii',Yii::app()->name) . ' - '.Yii::t("zii",'Error');
$this->breadcrumbs=array(
	Yii::t("zii",'Error'),
);
?>

<h2><?php echo Yii::t("zii",'Error {code}',array("{code}"=>$code)); ?></h2>

<div class="error">
<?php echo CHtml::encode($message); ?>
</div>