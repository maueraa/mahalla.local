<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1><i><?php echo Yii::t('zii','Welcome to {appname}!', array('{appname}'=>"<i>".Yii::t('zii',CHtml::encode(Yii::app()->name))."</i>"));?></h1>

<h2>
    Хўжаобод тумани тўртта комплекс ижтимоий-иқтисодий ривожлантириш бўйича секторлар раҳбарлари хисоботи
</h2>
<!--  хонадонлар-->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
	'collapsed'=>false,
	'legend'=>Yii::t('strings','Хонадонлар рўйхати'),
	'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/xonadonlar/index')?>" method="get">Туман кесимида:
	<?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman15','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
	<input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/xonadonlar/sektor')?>" method="get">Сектор кесимида:
	<?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor15','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
	<input type="submit" value="Кўриш">
</form>
<?php
ECascadeDropDown::master('id_tuman15')->setDependent('id_sektor15',
	array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/xonadonlar/mfy')?>" method="get">МФЙ кесимида:
	<?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy15','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
	<?php
	ECascadeDropDown::master('id_sektor15')->setDependent('id_mfy15',
		array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
	?>
	<input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/xonadonlar/kocha')?>" method="get">Кўчалар кесимида:
	<?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha15','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
	<?php
	ECascadeDropDown::master('id_mfy15')->setDependent('id_kocha15',
		array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
	?>
	<input type="submit" value="Кўриш">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!--  фуқаролар-->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
	'collapsed'=>false,
	'legend'=>Yii::t('strings','Фуқаролар рўйхати'),
	'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/fruyxat/index')?>" method="get">Туман кесимида:
	<?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman16','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
	<input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/fruyxat/sektor')?>" method="get">Сектор кесимида:
	<?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor16','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
	<input type="submit" value="Кўриш">
</form>
<?php
ECascadeDropDown::master('id_tuman16')->setDependent('id_sektor16',
	array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/fruyxat/mfy')?>" method="get">МФЙ кесимида:
	<?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy16','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
	<?php
	ECascadeDropDown::master('id_sektor16')->setDependent('id_mfy16',
		array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
	?>
	<input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/fruyxat/kocha')?>" method="get">Кўчалар кесимида:
	<?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha16','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
	<?php
	ECascadeDropDown::master('id_mfy16')->setDependent('id_kocha16',
		array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
	?>
	<input type="submit" value="Кўриш">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->


<!-- оғир касаллар-->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Оғир касаллар'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
    <form action="<?=Yii::app()->createUrl('/fuqaro_kasallar/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
    </form>
<form action="<?=Yii::app()->createUrl('/fuqaro_kasallar/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<?php
    ECascadeDropDown::master('id_tuman')->setDependent('id_sektor',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/fuqaro_kasallar/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor')->setDependent('id_mfy',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/fuqaro_kasallar/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy')->setDependent('id_kocha',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>

    <input type="submit" value="Кўриш">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->
<!-- ногиронлар-->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Ногиронлар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/nogiron/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman2','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/nogiron/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor2','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<?php
ECascadeDropDown::master('id_tuman2')->setDependent('id_sektor2',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/nogiron/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy2','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor2')->setDependent('id_mfy2',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/nogiron/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha2','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy2')->setDependent('id_kocha2',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>

    <input type="submit" value="Кўриш">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- Вояга етмаганлар-->

<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Вояга етмаганлар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/yosh/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman4','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/yosh/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor4','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<?php
ECascadeDropDown::master('id_tuman4')->setDependent('id_sektor4',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/yosh/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy4','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor4')->setDependent('id_mfy4',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/yosh/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha4','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy4')->setDependent('id_kocha4',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>

    <input type="submit" value="Кўриш">
</form>
 </p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- Узоқ муддатга кетганлар-->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Узоқ муддатга кетганлар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/uzoq_muddat/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman3','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/uzoq_muddat/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor3','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<?php
ECascadeDropDown::master('id_tuman3')->setDependent('id_sektor3',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/uzoq_muddat/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy3','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor3')->setDependent('id_mfy3',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/uzoq_muddat/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha3','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy3')->setDependent('id_kocha3',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>

    <input type="submit" value="Кўриш">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- ДЭО азолари  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','ДЭО аъзолари рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/deo/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman5','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/deo/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor5','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<?php
ECascadeDropDown::master('id_tuman5')->setDependent('id_sektor5',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/deo/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy5','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor5')->setDependent('id_mfy5',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/deo/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha5','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy5')->setDependent('id_kocha5',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш">
</form>

</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->
<!-- Муаммолилар -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Муаммоларни кўриш'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/Muammo/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman6','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/Muammo/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor6','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<?php
ECascadeDropDown::master('id_tuman6')->setDependent('id_sektor6',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/Muammo/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy6','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor6')->setDependent('id_mfy6',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/Muammo/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha6','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy6')->setDependent('id_kocha6',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш">
</form>

</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- ДЭО фарзандлари азолари  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','ДЭО фарзандлари рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/deo_farzandlari/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman7','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/deo_farzandlari/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor7','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<?php
ECascadeDropDown::master('id_tuman7')->setDependent('id_sektor7',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/deo_farzandlari/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy7','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor7')->setDependent('id_mfy7',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/deo_farzandlari/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha7','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy7')->setDependent('id_kocha7',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш">
</form>

</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- Уюшмаган ёшлар рўйхати  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Уюшмаган ёшлар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/uyushmagan_yoshlar/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman8','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/uyushmagan_yoshlar/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor8','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<?php
ECascadeDropDown::master('id_tuman8')->setDependent('id_sektor8',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/uyushmagan_yoshlar/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy8','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor8')->setDependent('id_mfy8',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/uyushmagan_yoshlar/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha8','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy8')->setDependent('id_kocha8',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш">
</form>

</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->


<!-- Жиноят содир этган аёллар  рўйхати  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Жиноят содир этган аёллар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/jinoyatchi_ayollar/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman9','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/jinoyatchi_ayollar/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor9','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<?php
ECascadeDropDown::master('id_tuman9')->setDependent('id_sektor9',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/jinoyatchi_ayollar/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy9','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor9')->setDependent('id_mfy9',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/jinoyatchi_ayollar/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha9','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy9')->setDependent('id_kocha9',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш">
</form>

</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- Ёлғиз пенсионерлар рўйхати  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Ёлғиз пенсионерлар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/yolgiz_pensioner/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman10','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/yolgiz_pensioner/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor10','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<?php
ECascadeDropDown::master('id_tuman10')->setDependent('id_sektor10',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/yolgiz_pensioner/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy10','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor10')->setDependent('id_mfy10',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/yolgiz_pensioner/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha10','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy10')->setDependent('id_kocha10',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- нотинч оилалар рўйхати  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Нотинч оилалар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/notinch_oilalar/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman12','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/notinch_oilalar/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor12','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<?php
ECascadeDropDown::master('id_tuman12')->setDependent('id_sektor12',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/notinch_oilalar/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy12','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor12')->setDependent('id_mfy12',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/notinch_oilalar/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha12','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy12')->setDependent('id_kocha12',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- кадастр йўқ оилалар рўйхати  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Кадастр йўқ оилалар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/kadastr/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman13','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/kadastr/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor13','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<?php
ECascadeDropDown::master('id_tuman13')->setDependent('id_sektor13',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/kadastr/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy13','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor13')->setDependent('id_mfy13',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/kadastr/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha13','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy13')->setDependent('id_kocha13',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- Газ,сув ва электр энергиясидан карздор хонадонлар рўйхати  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Газ,сув ва электр энергиясидан карздор хонадонлар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/qarzdorlik/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman14','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/qarzdorlik/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor14','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш">
</form>
<?php
ECascadeDropDown::master('id_tuman14')->setDependent('id_sektor14',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/qarzdorlik/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy14','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor14')->setDependent('id_mfy14',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш">
</form>
<form action="<?=Yii::app()->createUrl('/qarzdorlik/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha14','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy14')->setDependent('id_kocha14',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

