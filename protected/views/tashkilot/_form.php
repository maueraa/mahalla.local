<?php
/* @var $this TashkilotController */
/* @var $model Tashkilot */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tashkilot-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'tviloyat_id'); ?>
		<?php echo $form->dropdownlist($model,'tviloyat_id',$model->getfviloyat(),array('id'=>'id_province','prompt'=>Yii::t("strings",'Вилоятни танланг'))); ?>
		<?php echo $form->error($model,'tviloyat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ttuman_id'); ?>
		<?php echo $form->dropDownList($model,'ttuman_id',array(),array('id'=>'id_tuman','prompt'=>Yii::t("strings",'Туманни танланг'))); ?>
		<?php echo $form->error($model,'ttuman_id'); ?>
	</div>
    <?php
    ECascadeDropDown::master('id_province')->setDependent('id_tuman',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadtuman');
    ?>
    <div class="row">
		<?php echo $form->labelEx($model,'tmfy_id'); ?>
		<?php echo $form->dropDownList($model,'tmfy_id',array(),array('id'=>'id_mfy','prompt'=>Yii::t("strings",'МФЙ танланг'))); ?>
		<?php echo $form->error($model,'tmfy_id'); ?>
	</div>
    <?php
    ECascadeDropDown::master('id_tuman')->setDependent('id_mfy',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadmfy');
    ?>
    <div class="row">
		<?php echo $form->labelEx($model,'tkocha_id'); ?>
		<?php echo $form->dropDownList($model,'tkocha_id',array(),array('id'=>'id_kocha','prompt'=>Yii::t("strings",'Кўчани танланг'))); ?>
		<?php echo $form->error($model,'tkocha_id'); ?>
	</div>
    <?php
    ECascadeDropDown::master('id_mfy')->setDependent('id_kocha',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadkocha');
    ?>
	<div class="row">
		<?php echo $form->labelEx($model,'tnomi'); ?>
		<?php echo $form->textField($model,'tnomi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'tnomi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'trahbari'); ?>
		<?php echo $form->textField($model,'trahbari',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'trahbari'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ttelefon_raqami'); ?>
		<?php echo $form->textField($model,'ttelefon_raqami'); ?>
		<?php echo $form->error($model,'ttelefon_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id').Yii::app()->user->name; ?>
		<?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_date').date("d.m.Y"); ?>
		<?php echo $form->hiddenField($model,'user_date',array('value'=>date("Y-m-d"))); ?>
		<?php echo $form->error($model,'user_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->