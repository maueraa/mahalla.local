<?php
/* @var $this TashkilotController */
/* @var $model Tashkilot */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tviloyat_id'); ?>
		<?php echo $form->textField($model,'tviloyat_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ttuman_id'); ?>
		<?php echo $form->textField($model,'ttuman_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tmfy_id'); ?>
		<?php echo $form->textField($model,'tmfy_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tkocha_id'); ?>
		<?php echo $form->textField($model,'tkocha_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tnomi'); ?>
		<?php echo $form->textField($model,'tnomi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trahbari'); ?>
		<?php echo $form->textField($model,'trahbari',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ttelefon_raqami'); ?>
		<?php echo $form->textField($model,'ttelefon_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_date'); ?>
		<?php echo $form->textField($model,'user_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->