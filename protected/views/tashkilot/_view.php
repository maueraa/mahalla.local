<?php
/* @var $this TashkilotController */
/* @var $data Tashkilot */
?>

<tr class="view">
    <td>
        <?php echo CHtml::encode($data->id); ?>
        <br />
    </td>
    <td>
        <?php echo CHtml::link(CHtml::encode($data->tnomi), array('view', 'id'=>$data->id)); ?>
    </td>
    <td><?php echo CHtml::encode($data->trahbari); ?></td>
    <td>
        <?php if(isset($data->tviloyat->viloyat_nomi))echo CHtml::encode($data->tviloyat->viloyat_nomi); ?>
        <?php if(isset($data->ttuman->tuman_nomi))echo CHtml::encode($data->ttuman->tuman_nomi); ?>
        <?php if(isset($data->tmfy_id))echo CHtml::encode($data->tmfy->mfy_nomi); ?>
        <?php if(isset($data->tkocha_id))echo CHtml::encode($data->tkocha->tbl_kocha_nomi); ?>
        <br />
        <b><?php echo CHtml::encode($data->getAttributeLabel('ttelefon_raqami')); ?>:</b>
        <?php echo CHtml::encode($data->ttelefon_raqami); ?>
        <br />
        <b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
        <?php echo CHtml::encode($data->email); ?>
    </td>
    <td>Мурожаатлар сони</td>
    <td>Хал этилган мурожаатлар сони</td>
    <td>Хал этилаётган мурожаатлар сони</td>
    <td>Хал этилмаган мурожаатлар сони</td>

	<?php /*
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_date')); ?>:</b>
	<?php echo CHtml::encode($data->user_date); ?>
	<br />

	*/ ?>

</tr>