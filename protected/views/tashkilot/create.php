<?php
/* @var $this TashkilotController */
/* @var $model Tashkilot */

$this->breadcrumbs=array(
	Yii::t('strings','Ташкилот')=>array('index'),
	Yii::t('strings','Қўшиш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Tashkilot'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Tashkilot'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Ташкилотлар')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>