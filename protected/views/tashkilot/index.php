<?php
/* @var $this TashkilotController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Ташкилот'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Tashkilot'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Tashkilot'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Ташкилотлар')?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'itemsTagName'=>'table',
    'itemsHeader'=>"
	<tr>
	<th>№</th>
	<th>Ташкилот номи</th>
	<th>Ташкилот рахбари</th>
	<th>Манзили, телефон рақами, электрон почта манзили</th>
	<th>Мурожаатлар сони</th>
	<th>Хал этилган мурожаатлар сони</th>
	<th>Хал этилаётган мурожаатлар сони</th>
	<th>Хал этилмаган мурожаатлар сони</th>

	</tr>
	",
)); ?>
