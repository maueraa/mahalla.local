<?php
/* @var $this TashkilotController */
/* @var $model Tashkilot */

$this->breadcrumbs=array(
	Yii::t('strings','Ташкилот')=>array('index'),
	Yii::t('strings',$model->id)=>array('view','id'=>$model->id),
	Yii::t('strings','Ўзгартириш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Tashkilot'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Tashkilot'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','View {label}', array('{label}'=>Yii::t('strings','Tashkilot'))), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Tashkilot'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings',' {table} "{record}" (id={id})', array(
	'{table}'=>Yii::t('strings','Ташкилот маълумотларини янгилаш'),
	'{record}'=>$model->id,
	'{id}'=>$model->id));?></h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>