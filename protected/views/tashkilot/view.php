<?php
/* @var $this TashkilotController */
/* @var $model Tashkilot */

$this->breadcrumbs=array(
	Yii::t('strings','Tashkilots')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Tashkilot'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Tashkilot'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Tashkilot'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Tashkilot'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Tashkilot'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Tashkilot') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'tviloyat_id',
		'ttuman_id',
		'tmfy_id',
		'tkocha_id',
		'tnomi',
		'trahbari',
		'ttelefon_raqami',
		'email',
		'user_id',
		'user_date',
/**
		array(
			'label'=>$model->getAttributeLabel('user_date'),
			'type'=>'raw',
			'value'=>$model->user_d->name,
		),
/**/
	),
)); ?>
