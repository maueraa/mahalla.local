<?php
/* @var $this Fuqaro_MuammolarController */
/* @var $data Fuqaro_Muammolar */

?>


<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('viloyat_id')); ?>:</b>
    <?php echo CHtml::encode($data->viloyat->viloyat_nomi); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('tuman_id')); ?>:</b>
    <?php echo CHtml::encode($data->tuman->tuman_nomi); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('mfy_id')); ?>:</b>
    <?php echo CHtml::encode($data->mfy->mfy_nomi); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('kocha_id')); ?>:</b>
    <?php echo CHtml::encode($data->kocha->tbl_kocha_nomi); ?>
    <br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fuqaro_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->fuqaro->fio), array('Fuqaro_muammolar/yechim', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('muammo_mazmuni')); ?>:</b>
	<?php echo CHtml::encode($data->muammo_mazmuni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('muammo_xolati_id')); ?>:</b>
	<?php if(isset($data->muammoXolati->xulosa)) echo CHtml::encode($data->muammoXolati->xulosa); ?>
	<br />
    <b><?php echo Yii::t('strings','Уй рақами'); ?>:</b>
    <?php echo CHtml::encode($data->fuqaroUy->uy_raqami); ?>
    <br />

    <?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('xulosa')); ?>:</b>
	<?php echo CHtml::encode($data->xulosa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fuqaro_uy_id')); ?>:</b>
	<?php echo CHtml::encode($data->fuqaro_uy_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fuqaro_id')); ?>:</b>
	<?php echo CHtml::encode($data->fuqaro_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tashkilot_id')); ?>:</b>
	<?php echo CHtml::encode($data->tashkilot_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qayd_sana')); ?>:</b>
	<?php echo CHtml::encode($data->qayd_sana); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('muddat_sana')); ?>:</b>
	<?php echo CHtml::encode($data->muddat_sana); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('korilgan_sana')); ?>:</b>
	<?php echo CHtml::encode($data->korilgan_sana); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xal_etilgan_sana')); ?>:</b>
	<?php echo CHtml::encode($data->xal_etilgan_sana); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	*/ ?>

</div>