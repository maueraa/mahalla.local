<?php
/* @var $this Fuqaro_MuammolarController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Муаммолар'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Fuqaro_Muammolar'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Fuqaro_Muammolar'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Ташкилотга тегишли бўлган муаммолар')?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
