﻿<?php
/* @var $this TomorqaController */
/* @var $model Tomorqa */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'tomorqa-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		
		<?php echo $form->hiddenField($model,'tbl_fuqaro_uy_id',array('value'=>$fid)); ?>
		<?php echo $form->error($model,'tbl_fuqaro_uy_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'umumiy_yer_maydoni'); ?>
		<?php echo $form->textField($model,'umumiy_yer_maydoni',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'umumiy_yer_maydoni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yer_ekin_maydoni'); ?>
		<?php echo $form->textField($model,'yer_ekin_maydoni',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yer_ekin_maydoni'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'yer_foy_dalanish_xolati'); ?>
		<?php echo $form->textField($model,'yer_foy_dalanish_xolati',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yer_foy_dalanish_xolati'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'issiq_xona_mavjud_id'); ?>
		<?php echo $form->dropDownList($model,'issiq_xona_mavjud_id',$model->getissiqxona_turi(),array('prompt'=>Yii::t("strings",'Иссиқхона турини кўрсатинг'))); ?>
		<?php echo $form->error($model,'issiq_xona_mavjud_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'issiq_xona_ekin_nomi_soni'); ?>
		<?php echo $form->textField($model,'issiq_xona_ekin_nomi_soni'); ?>
		<?php echo $form->error($model,'issiq_xona_ekin_nomi_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yer_ekin_xolati'); ?>
		<?php echo $form->dropdownlist($model,'yer_ekin_xolati',array('1'=>'экинлар экилган, бегона ўтлардан тозаланган(яхши)','2'=>'экинлар экилган, бегона ўтлардан тозаланмаган(ўртача)','3'=>'экинлар экилмаган(қониқарсиз)','4'=>'экинлар экилмаган ва тозаланмаган(ёмон)', '5'=>'Томорқа мавжуд эмас'),array('prompt'=>Yii::t("strings",'Томорқадан фойдаланиш холати'))); ?>
		<?php echo $form->error($model,'yer_ekin_xolati'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ekilgan_ekin_maydoni'); ?>
		<?php echo $form->textField($model,'ekilgan_ekin_maydoni',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ekilgan_ekin_maydoni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kartoshka'); ?>
		<?php echo $form->textField($model,'kartoshka'); ?>
		<?php echo $form->error($model,'kartoshka'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'piyoz'); ?>
		<?php echo $form->textField($model,'piyoz'); ?>
		<?php echo $form->error($model,'piyoz'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sarimsoq'); ?>
		<?php echo $form->textField($model,'sarimsoq'); ?>
		<?php echo $form->error($model,'sarimsoq'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sabzi'); ?>
		<?php echo $form->textField($model,'sabzi'); ?>
		<?php echo $form->error($model,'sabzi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pomidor'); ?>
		<?php echo $form->textField($model,'pomidor'); ?>
		<?php echo $form->error($model,'pomidor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bodiring'); ?>
		<?php echo $form->textField($model,'bodiring'); ?>
		<?php echo $form->error($model,'bodiring'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'balgariskiy'); ?>
		<?php echo $form->textField($model,'balgariskiy'); ?>
		<?php echo $form->error($model,'balgariskiy'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'baqlajon'); ?>
		<?php echo $form->textField($model,'baqlajon'); ?>
		<?php echo $form->error($model,'baqlajon'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'loviya'); ?>
		<?php echo $form->textField($model,'loviya'); ?>
		<?php echo $form->error($model,'loviya'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mosh'); ?>
		<?php echo $form->textField($model,'mosh'); ?>
		<?php echo $form->error($model,'mosh'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'makka'); ?>
		<?php echo $form->textField($model,'makka'); ?>
		<?php echo $form->error($model,'makka'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'kokat'); ?>
        <?php echo $form->textField($model,'kokat',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'kokat'); ?>
    </div>

    <div class="row">
		<?php echo $form->labelEx($model,'beda'); ?>
		<?php echo $form->textField($model,'beda'); ?>
		<?php echo $form->error($model,'beda'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tamaki'); ?>
		<?php echo $form->textField($model,'tamaki'); ?>
		<?php echo $form->error($model,'tamaki'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ekin_boshqalar'); ?>
		<?php echo $form->textField($model,'ekin_boshqalar',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'ekin_boshqalar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ekilgan_meva_soni'); ?>
		<?php echo $form->textField($model,'ekilgan_meva_soni'); ?>
		<?php echo $form->error($model,'ekilgan_meva_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yongoq_soni'); ?>
		<?php echo $form->textField($model,'yongoq_soni'); ?>
		<?php echo $form->error($model,'yongoq_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yongoq_yoshi'); ?>
		<?php echo $form->textField($model,'yongoq_yoshi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yongoq_yoshi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jiyda_soni'); ?>
		<?php echo $form->textField($model,'jiyda_soni',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'jiyda_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jiyda_yoshi'); ?>
		<?php echo $form->textField($model,'jiyda_yoshi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'jiyda_yoshi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'uzum_soni'); ?>
		<?php echo $form->textField($model,'uzum_soni',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'uzum_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'uzum_yoshi'); ?>
		<?php echo $form->textField($model,'uzum_yoshi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'uzum_yoshi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'shaftoli_soni'); ?>
		<?php echo $form->textField($model,'shaftoli_soni',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'shaftoli_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'shaftoli_yoshi'); ?>
		<?php echo $form->textField($model,'shaftoli_yoshi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'shaftoli_yoshi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'olma_soni'); ?>
		<?php echo $form->textField($model,'olma_soni',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'olma_soni'); ?>
	</div>
<div class="row">
	<?php echo $form->labelEx($model,'olm_yoshi'); ?>
	<?php echo $form->textField($model,'olm_yoshi',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'olm_yoshi'); ?>
</div>

	<div class="row">
		<?php echo $form->labelEx($model,'anor_soni'); ?>
		<?php echo $form->textField($model,'anor_soni',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'anor_soni'); ?>
	</div>

<div class="row">
	<?php echo $form->labelEx($model,'anor_yoshi'); ?>
	<?php echo $form->textField($model,'anor_yoshi',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'anor_yoshi'); ?>
</div>
	<div class="row">
		<?php echo $form->labelEx($model,'nok_soni'); ?>
		<?php echo $form->textField($model,'nok_soni',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nok_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nok_yoshi'); ?>
		<?php echo $form->textField($model,'nok_yoshi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nok_yoshi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'orik_soni'); ?>
		<?php echo $form->textField($model,'orik_soni',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'orik_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'orik_yoshi'); ?>
		<?php echo $form->textField($model,'orik_yoshi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'orik_yoshi'); ?>
	</div>
<div class="row">
	<?php echo $form->labelEx($model,'anjir_soni'); ?>
	<?php echo $form->textField($model,'anjir_soni',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'anjir_soni'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'anjiir_yoshi'); ?>
	<?php echo $form->textField($model,'anjiir_yoshi',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'anjiir_yoshi'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'bexi_soni'); ?>
	<?php echo $form->textField($model,'bexi_soni',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'bexi_soni'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'bexi_yoshi'); ?>
	<?php echo $form->textField($model,'bexi_yoshi',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'bexi_yoshi'); ?>
</div>
<div class="row">
<?php echo $form->labelEx($model,'gilos_soni'); ?>
	<?php echo $form->textField($model,'gilos_soni',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'gilos_soni'); ?>
</div>
<div class="row">
	<?php echo $form->labelEx($model,'gilos_yoshi'); ?>
	<?php echo $form->textField($model,'gilos_yoshi',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'gilos_yoshi'); ?>
</div>
<div class="row">
	<?php echo $form->labelEx($model,'sliva_soni'); ?>
	<?php echo $form->textField($model,'sliva_soni',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'sliva_soni'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'sliva_yoshi'); ?>
	<?php echo $form->textField($model,'sliva_yoshi',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'sliva_yoshi'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'humro_soni'); ?>
	<?php echo $form->textField($model,'humro_soni',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'humro_soni'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'hurmo_yoshi'); ?>
	<?php echo $form->textField($model,'hurmo_yoshi',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'hurmo_yoshi'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'bodom_soni'); ?>
	<?php echo $form->textField($model,'bodom_soni',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'bodom_soni'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'bodom_yoshi'); ?>
	<?php echo $form->textField($model,'bodom_yoshi',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'bodom_yoshi'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'terak_soni'); ?>
	<?php echo $form->textField($model,'terak_soni',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'terak_soni'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'terak_yoshi'); ?>
	<?php echo $form->textField($model,'terak_yoshi',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'terak_yoshi'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'tol_soni'); ?>
	<?php echo $form->textField($model,'tol_soni',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'tol_soni'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'tol_yoshi'); ?>
	<?php echo $form->textField($model,'tol_yoshi',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'tol_yoshi'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'boshqa_ekinlar'); ?>
	<?php echo $form->textField($model,'boshqa_ekinlar',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'boshqa_ekinlar'); ?>
</div>
<div class="row">
	<?php echo $form->labelEx($model,'user_id').Yii::app()->user->name; ?>
	<?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
	<?php echo $form->error($model,'user_id'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'user_date').date("d.m.Y"); ?>
	<?php echo $form->hiddenField($model,'user_date',array('value'=>date("Y-m-d"))); ?>
	<?php echo $form->error($model,'user_date'); ?>
</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array("class"=>"btn btn-primary")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->