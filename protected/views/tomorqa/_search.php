<?php
/* @var $this TomorqaController */
/* @var $model Tomorqa */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tbl_fuqaro_uy_id'); ?>
		<?php echo $form->textField($model,'tbl_fuqaro_uy_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'umumiy_yer_maydoni'); ?>
		<?php echo $form->textField($model,'umumiy_yer_maydoni',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yer_ekin_maydoni'); ?>
		<?php echo $form->textField($model,'yer_ekin_maydoni',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yer_ekin_xolati'); ?>
		<?php echo $form->textField($model,'yer_ekin_xolati',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'issiq_xona_mavjud_id'); ?>
		<?php echo $form->textField($model,'issiq_xona_mavjud_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'issiq_xona_ekin_nomi_soni'); ?>
		<?php echo $form->textField($model,'issiq_xona_ekin_nomi_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yer_foy_dalanish_xolati'); ?>
		<?php echo $form->textField($model,'yer_foy_dalanish_xolati',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ekilgan_ekin_maydoni'); ?>
		<?php echo $form->textField($model,'ekilgan_ekin_maydoni',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kartoshka'); ?>
		<?php echo $form->textField($model,'kartoshka'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'piyoz'); ?>
		<?php echo $form->textField($model,'piyoz'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sarimsoq'); ?>
		<?php echo $form->textField($model,'sarimsoq'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sabzi'); ?>
		<?php echo $form->textField($model,'sabzi'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pomidor'); ?>
		<?php echo $form->textField($model,'pomidor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bodiring'); ?>
		<?php echo $form->textField($model,'bodiring'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'balgariskiy'); ?>
		<?php echo $form->textField($model,'balgariskiy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'baqlajon'); ?>
		<?php echo $form->textField($model,'baqlajon'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'loviya'); ?>
		<?php echo $form->textField($model,'loviya'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mosh'); ?>
		<?php echo $form->textField($model,'mosh'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'makka'); ?>
		<?php echo $form->textField($model,'makka'); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model,'kokat'); ?>
        <?php echo $form->textField($model,'kokat',array('size'=>60,'maxlength'=>255)); ?>
    </div>

	<div class="row">
		<?php echo $form->label($model,'beda'); ?>
		<?php echo $form->textField($model,'beda'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tamaki'); ?>
		<?php echo $form->textField($model,'tamaki'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ekin_boshqalar'); ?>
		<?php echo $form->textField($model,'ekin_boshqalar',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ekilgan_meva_soni'); ?>
		<?php echo $form->textField($model,'ekilgan_meva_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yongoq_soni'); ?>
		<?php echo $form->textField($model,'yongoq_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yongoq_yoshi'); ?>
		<?php echo $form->textField($model,'yongoq_yoshi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jiyda_soni'); ?>
		<?php echo $form->textField($model,'jiyda_soni',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jiyda_yoshi'); ?>
		<?php echo $form->textField($model,'jiyda_yoshi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uzum_soni'); ?>
		<?php echo $form->textField($model,'uzum_soni',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uzum_yoshi'); ?>
		<?php echo $form->textField($model,'uzum_yoshi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'shaftoli_soni'); ?>
		<?php echo $form->textField($model,'shaftoli_soni',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'shaftoli_yoshi'); ?>
		<?php echo $form->textField($model,'shaftoli_yoshi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'olma_soni'); ?>
		<?php echo $form->textField($model,'olma_soni',array('size'=>60,'maxlength'=>255)); ?>
	</div>
<div class="row">
	<?php echo $form->label($model,'olm_yoshi'); ?>
	<?php echo $form->textField($model,'olm_yoshi',array('size'=>60,'maxlength'=>255)); ?>
</div>
	<div class="row">
		<?php echo $form->label($model,'anor_soni'); ?>
		<?php echo $form->textField($model,'anor_soni',array('size'=>60,'maxlength'=>255)); ?>
	</div>
<div class="row">
	<?php echo $form->label($model,'anor_yoshi'); ?>
	<?php echo $form->textField($model,'anor_yoshi',array('size'=>60,'maxlength'=>255)); ?>
</div>
	<div class="row">
		<?php echo $form->label($model,'nok_soni'); ?>
		<?php echo $form->textField($model,'nok_soni',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nok_yoshi'); ?>
		<?php echo $form->textField($model,'nok_yoshi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'orik_soni'); ?>
		<?php echo $form->textField($model,'orik_soni',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'orik_yoshi'); ?>
		<?php echo $form->textField($model,'orik_yoshi',array('size'=>60,'maxlength'=>255)); ?>
	</div>
    <div class="row">
        <?php echo $form->label($model,'anjir_soni'); ?>
        <?php echo $form->textField($model,'anjir_soni',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'anjiir_yoshi'); ?>
        <?php echo $form->textField($model,'anjiir_yoshi',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'bexi_soni'); ?>
        <?php echo $form->textField($model,'bexi_soni',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'bexi_yoshi'); ?>
        <?php echo $form->textField($model,'bexi_yoshi',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'sliva_soni'); ?>
        <?php echo $form->textField($model,'sliva_soni',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'gilos_soni'); ?>
        <?php echo $form->textField($model,'gilos_soni',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'gilos_yoshi'); ?>
        <?php echo $form->textField($model,'gilos_yoshi',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'sliva_yoshi'); ?>
        <?php echo $form->textField($model,'sliva_yoshi',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'humro_soni'); ?>
        <?php echo $form->textField($model,'humro_soni',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'hurmo_yoshi'); ?>
        <?php echo $form->textField($model,'hurmo_yoshi',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'bodom_soni'); ?>
        <?php echo $form->textField($model,'bodom_soni',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'bodom_yoshi'); ?>
        <?php echo $form->textField($model,'bodom_yoshi',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'terak_soni'); ?>
        <?php echo $form->textField($model,'terak_soni',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'terak_yoshi'); ?>
        <?php echo $form->textField($model,'terak_yoshi',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'tol_soni'); ?>
        <?php echo $form->textField($model,'tol_soni',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'tol_yoshi'); ?>
        <?php echo $form->textField($model,'tol_yoshi',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'boshqa_ekinlar'); ?>
        <?php echo $form->textField($model,'boshqa_ekinlar',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'anketa_ozgarish'); ?>
        <?php echo $form->textField($model,'anketa_ozgarish'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->