<?php
/* @var $this TomorqaController */
/* @var $data Tomorqa */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tbl_fuqaro_uy_id')); ?>:</b>
	<?php echo CHtml::encode($data->tbl_fuqaro_uy_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('umumiy_yer_maydoni')); ?>:</b>
	<?php echo CHtml::encode($data->umumiy_yer_maydoni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yer_ekin_maydoni')); ?>:</b>
	<?php echo CHtml::encode($data->yer_ekin_maydoni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yer_ekin_xolati')); ?>:</b>
	<?php echo CHtml::encode($data->yer_ekin_xolati); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('issiq_xona_mavjud_id')); ?>:</b>
	<?php echo CHtml::encode($data->issiq_xona_mavjud_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('issiq_xona_ekin_nomi_soni')); ?>:</b>
	<?php echo CHtml::encode($data->issiq_xona_ekin_nomi_soni); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('yer_foy_dalanish_xolati')); ?>:</b>
	<?php echo CHtml::encode($data->yer_foy_dalanish_xolati); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ekilgan_ekin_maydoni')); ?>:</b>
	<?php echo CHtml::encode($data->ekilgan_ekin_maydoni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kartoshka')); ?>:</b>
	<?php echo CHtml::encode($data->kartoshka); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('piyoz')); ?>:</b>
	<?php echo CHtml::encode($data->piyoz); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sarimsoq')); ?>:</b>
	<?php echo CHtml::encode($data->sarimsoq); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sabzi')); ?>:</b>
	<?php echo CHtml::encode($data->sabzi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pomidor')); ?>:</b>
	<?php echo CHtml::encode($data->pomidor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bodiring')); ?>:</b>
	<?php echo CHtml::encode($data->bodiring); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('balgariskiy')); ?>:</b>
	<?php echo CHtml::encode($data->balgariskiy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('baqlajon')); ?>:</b>
	<?php echo CHtml::encode($data->baqlajon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loviya')); ?>:</b>
	<?php echo CHtml::encode($data->loviya); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mosh')); ?>:</b>
	<?php echo CHtml::encode($data->mosh); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('makka')); ?>:</b>
	<?php echo CHtml::encode($data->makka); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('beda')); ?>:</b>
	<?php echo CHtml::encode($data->beda); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tamaki')); ?>:</b>
	<?php echo CHtml::encode($data->tamaki); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ekin_boshqalar')); ?>:</b>
	<?php echo CHtml::encode($data->ekin_boshqalar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ekilgan_meva_soni')); ?>:</b>
	<?php echo CHtml::encode($data->ekilgan_meva_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yongoq_soni')); ?>:</b>
	<?php echo CHtml::encode($data->yongoq_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yongoq_yoshi')); ?>:</b>
	<?php echo CHtml::encode($data->yongoq_yoshi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jiyda_soni')); ?>:</b>
	<?php echo CHtml::encode($data->jiyda_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jiyda_yoshi')); ?>:</b>
	<?php echo CHtml::encode($data->jiyda_yoshi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uzum_soni')); ?>:</b>
	<?php echo CHtml::encode($data->uzum_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uzum_yoshi')); ?>:</b>
	<?php echo CHtml::encode($data->uzum_yoshi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shaftoli_soni')); ?>:</b>
	<?php echo CHtml::encode($data->shaftoli_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shaftoli_yoshi')); ?>:</b>
	<?php echo CHtml::encode($data->shaftoli_yoshi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('olma_soni')); ?>:</b>
	<?php echo CHtml::encode($data->olma_soni); ?>
	<br />
 <b><?php echo CHtml::encode($data->getAttributeLabel('olm_yoshi')); ?>:</b>
	<?php echo CHtml::encode($data->olm_yoshi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anor_soni')); ?>:</b>
	<?php echo CHtml::encode($data->anor_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nok_soni')); ?>:</b>
	<?php echo CHtml::encode($data->nok_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nok_yoshi')); ?>:</b>
	<?php echo CHtml::encode($data->nok_yoshi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orik_soni')); ?>:</b>
	<?php echo CHtml::encode($data->orik_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orik_yoshi')); ?>:</b>
	<?php echo CHtml::encode($data->orik_yoshi); ?>
	<br />
<b><?php echo CHtml::encode($data->getAttributeLabel('anjir_soni')); ?>:</b>
	<?php echo CHtml::encode($data->anjir_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anjiir_yoshi')); ?>:</b>
	<?php echo CHtml::encode($data->anjiir_yoshi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bexi_soni')); ?>:</b>
	<?php echo CHtml::encode($data->bexi_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bexi_yoshi')); ?>:</b>
	<?php echo CHtml::encode($data->bexi_yoshi); ?>
	<br />
<b><?php echo CHtml::encode($data->getAttributeLabel('gilos_soni')); ?>:</b>
	<?php echo CHtml::encode($data->gilos_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gilos_yoshi')); ?>:</b>
	<?php echo CHtml::encode($data->gilos_yoshi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sliva_soni')); ?>:</b>
	<?php echo CHtml::encode($data->sliva_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sliva_yoshi')); ?>:</b>
	<?php echo CHtml::encode($data->sliva_yoshi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('humro_soni')); ?>:</b>
	<?php echo CHtml::encode($data->humro_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hurmo_yoshi')); ?>:</b>
	<?php echo CHtml::encode($data->hurmo_yoshi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bodom_soni')); ?>:</b>
	<?php echo CHtml::encode($data->bodom_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bodom_yoshi')); ?>:</b>
	<?php echo CHtml::encode($data->bodom_yoshi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('terak_soni')); ?>:</b>
	<?php echo CHtml::encode($data->terak_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('terak_yoshi')); ?>:</b>
	<?php echo CHtml::encode($data->terak_yoshi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tol_soni')); ?>:</b>
	<?php echo CHtml::encode($data->tol_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tol_yoshi')); ?>:</b>
	<?php echo CHtml::encode($data->tol_yoshi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('boshqa_ekinlar')); ?>:</b>
	<?php echo CHtml::encode($data->boshqa_ekinlar); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('anketa_ozgarish')); ?>:</b>
	<?php echo CHtml::encode($data->anketa_ozgarish); ?>
	<br />
	*/ ?>

</div>