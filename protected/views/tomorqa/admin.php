<?php
/* @var $this TomorqaController */
/* @var $model Tomorqa */

$this->breadcrumbs=array(
	Yii::t('strings','Tomorqas')=>array('index'),
	Yii::t('strings','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Tomorqa'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Tomorqa'))), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tomorqa-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Tomorqa'))) ?></h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link(Yii::t('zii','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tomorqa-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'tbl_fuqaro_uy_id',
		'umumiy_yer_maydoni',
		'yer_ekin_maydoni',
		'yer_ekin_xolati',
		'issiq_xona_mavjud_id',
		/*
		'issiq_xona_ekin_nomi_soni',
		'yer_foy_dalanish_xolati',
		'ekilgan_ekin_maydoni',
		'kartoshka',
		'piyoz',
		'sarimsoq',
		'sabzi',
		'pomidor',
		'bodiring',
		'balgariskiy',
		'baqlajon',
		'loviya',
		'mosh',
		'makka',
		'kokat',
		'beda',
		'tamaki',
		'ekin_boshqalar',
		'ekilgan_meva_soni',
		'yongoq_soni',
		'yongoq_yoshi',
		'jiyda_soni',
		'jiyda_yoshi',
		'uzum_soni',
		'uzum_yoshi',
		'shaftoli_soni',
		'shaftoli_yoshi',
		'olma_soni',
		'olm_yoshi',
		'anor_soni',
		'anor_yoshi',
		'nok_soni',
		'nok_yoshi',
		'orik_soni',
		'orik_yoshi',
		'anjir_soni',
		'anjiir_yoshi',
		'bexi_soni',
		'bexi_yoshi',
		'gilos_soni',
		'gilos_yoshi',
		'sliva_soni',
		'sliva_yoshi',
		'humro_soni',
		'hurmo_yoshi',
		'bodom_soni',
		'bodom_yoshi',
		'terak_soni',
		'terak_yoshi',
		'tol_soni',
		'tol_yoshi',
		'boshqa_ekinlar',
		'anketa_ozgarish',
		//'orik_yo.name',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
