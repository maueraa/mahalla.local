﻿<?php
/* @var $this TomorqaController */
/* @var $model Tomorqa */

$this->breadcrumbs=array(
	Yii::t('strings','Tomorqas')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Tomorqa'))), 'url'=>array('index')),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Tomorqa'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Хонадонни томорқадан фойдаланиш хақидаги маълумотларини киритиш ойнаси')?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'fid'=>$fid)); ?>