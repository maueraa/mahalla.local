<?php
/* @var $this TomorqaController */
/* @var $model Tomorqa */

$this->breadcrumbs=array(
	Yii::t('strings','Томорқа')=>array('index'),
	$model->id,
);

$this->menu=array(
#	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Tomorqa'))), 'url'=>array('index')),
#	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Tomorqa'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Tomorqa'))), 'url'=>array('update', 'id'=>$model->id)),
#	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Tomorqa'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Tomorqa'))), 'url'=>array('admin')),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон маълумотлари'))), 'url'=>array("/fuqaro_Uy/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон аъзолари'))), 'url'=>array("/fuqarolar?fuq=$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Маънавий мухит'))), 'url'=>array("/manaviy/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Даромади'))), 'url'=>array("/daromad/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Қарздоллик'))), 'url'=>array("/qarz/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт қатнови'))), 'url'=>array("/transport_qatnovi/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ижтимоий мухит'))), 'url'=>array("/ijtimoiy_muhit/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Шарт-шароити'))), 'url'=>array("/shaoriti/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Томорқа хақидаги маълумотларни ўзгартириш'))), 'url'=>array('update', 'fid'=>$model->tbl_fuqaro_uy_id)),
    #array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Томорқа'))), 'url'=>array("/tomorqa/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Чорва хайвонлари'))), 'url'=>array("/hayvon/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хизмат кўрсатиш объектлари'))), 'url'=>array("/ximzat_korsatish/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчанинг холати'))), 'url'=>array("/kocha_xolati/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Барча маълумотларни кўриш'))), 'url'=>array('/jamlama/index', 'id'=>$model->tbl_fuqaro_uy_id)),

);
?>

<h1><?php echo Yii::t('strings','Хонадонни томорқадан фойдаланиш хақидаги маълумотларини янгиланган холати') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'tbl_fuqaro_uy_id',
		'umumiy_yer_maydoni',
		'yer_ekin_maydoni',
		'yer_ekin_xolati',
		'issiq_xona_mavjud_id',
		'issiq_xona_ekin_nomi_soni',
		'yer_foy_dalanish_xolati',
		'ekilgan_ekin_maydoni',
		'kartoshka',
		'piyoz',
		'sarimsoq',
		'sabzi',
		'pomidor',
		'bodiring',
		'balgariskiy',
		'baqlajon',
		'loviya',
		'mosh',
		'makka',
        'kokat',
		'beda',
		'tamaki',
		'ekin_boshqalar',
		'ekilgan_meva_soni',
		'yongoq_soni',
		'yongoq_yoshi',
		'jiyda_soni',
		'jiyda_yoshi',
		'uzum_soni',
		'uzum_yoshi',
		'shaftoli_soni',
		'shaftoli_yoshi',
		'olma_soni',
		'olm_yoshi',
		'anor_soni',
		'anor_yoshi',
		'nok_soni',
		'nok_yoshi',
		'orik_soni',
		'orik_yoshi',
		'anjir_soni',
		'anjiir_yoshi',
		'bexi_soni',
		'bexi_yoshi',
		'gilos_soni',
		'gilos_yoshi',
		'sliva_soni',
		'sliva_yoshi',
		'humro_soni',
		'hurmo_yoshi',
		'bodom_soni',
		'bodom_yoshi',
		'terak_soni',
		'terak_yoshi',
		'tol_soni',
		'tol_yoshi',
		'boshqa_ekinlar',
        'anketa_ozgarish',
		'user_id',
		'user_date',

/** *
 * boshqa_ekin.name',

		array(
			'label'=>$model->getAttributeLabel('orik_yoshi'),
			'type'=>'raw',
			'value'=>$model->orik_yo->name,
		),
/**/
	),
)); ?>
