﻿<?php
/* @var $this Transport_QatnoviController */
/* @var $model Transport_Qatnovi */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'transport--qatnovi-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadonga_yaqin_tran_qatnov_masofa'); ?>
		<?php echo $form->textField($model,'xonadonga_yaqin_tran_qatnov_masofa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'xonadonga_yaqin_tran_qatnov_masofa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadondan_mahalla_markaz_masofa'); ?>
		<?php echo $form->textField($model,'xonadondan_mahalla_markaz_masofa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'xonadondan_mahalla_markaz_masofa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadon_mahalla_trans_soni'); ?>
		<?php echo $form->textField($model,'xonadon_mahalla_trans_soni'); ?>
		<?php echo $form->error($model,'xonadon_mahalla_trans_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadon_tuman_masofa'); ?>
		<?php echo $form->textField($model,'xonadon_tuman_masofa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'xonadon_tuman_masofa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadon_tuman_trans_soni'); ?>
		<?php echo $form->textField($model,'xonadon_tuman_trans_soni'); ?>
		<?php echo $form->error($model,'xonadon_tuman_trans_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadon_viloyat_masofa'); ?>
		<?php echo $form->textField($model,'xonadon_viloyat_masofa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'xonadon_viloyat_masofa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadon_viloyat_trans_soni'); ?>
		<?php echo $form->textField($model,'xonadon_viloyat_trans_soni'); ?>
		<?php echo $form->error($model,'xonadon_viloyat_trans_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yol_kira_mahalla'); ?>
		<?php echo $form->textField($model,'yol_kira_mahalla',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yol_kira_mahalla'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yol_kira_tuman'); ?>
		<?php echo $form->textField($model,'yol_kira_tuman',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yol_kira_tuman'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yol_kira_viloyat'); ?>
		<?php echo $form->textField($model,'yol_kira_viloyat',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yol_kira_viloyat'); ?>
	</div>

	<div class="row">
		
		<?php echo $form->hiddenField($model,'fuqaro_uy_id',array('value'=>$fid)); ?>
		<?php echo $form->error($model,'fuqaro_uy_id'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'anketa_ozgarish').":".date("d.m.Y"); ?>
        <?php echo $form->hiddenField($model,'anketa_ozgarish',array('value'=>date("Y-m-d"))); ?>
        <?php echo $form->error($model,'anketa_ozgarish'); ?>
    </div>
	<div class="row">
		<?php echo $form->labelEx($model,'user_id').Yii::app()->user->name; ?>
		<?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_date').date("d.m.Y"); ?>
		<?php echo $form->hiddenField($model,'user_date',array('value'=>date("Y-m-d"))); ?>
		<?php echo $form->error($model,'user_date'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array("class"=>"btn btn-primary")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->