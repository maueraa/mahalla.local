<?php
/* @var $this Transport_QatnoviController */
/* @var $model Transport_Qatnovi */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadonga_yaqin_tran_qatnov_masofa'); ?>
		<?php echo $form->textField($model,'xonadonga_yaqin_tran_qatnov_masofa',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadondan_mahalla_markaz_masofa'); ?>
		<?php echo $form->textField($model,'xonadondan_mahalla_markaz_masofa',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadon_mahalla_trans_soni'); ?>
		<?php echo $form->textField($model,'xonadon_mahalla_trans_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadon_tuman_masofa'); ?>
		<?php echo $form->textField($model,'xonadon_tuman_masofa',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadon_tuman_trans_soni'); ?>
		<?php echo $form->textField($model,'xonadon_tuman_trans_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadon_viloyat_masofa'); ?>
		<?php echo $form->textField($model,'xonadon_viloyat_masofa',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadon_viloyat_trans_soni'); ?>
		<?php echo $form->textField($model,'xonadon_viloyat_trans_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yol_kira_mahalla'); ?>
		<?php echo $form->textField($model,'yol_kira_mahalla',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yol_kira_tuman'); ?>
		<?php echo $form->textField($model,'yol_kira_tuman',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yol_kira_viloyat'); ?>
		<?php echo $form->textField($model,'yol_kira_viloyat',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fuqaro_uy_id'); ?>
		<?php echo $form->textField($model,'fuqaro_uy_id'); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model,'anketa_ozgarish'); ?>
        <?php echo $form->textField($model,'anketa_ozgarish'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->