<?php
/* @var $this Transport_QatnoviController */
/* @var $data Transport_Qatnovi */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadonga_yaqin_tran_qatnov_masofa')); ?>:</b>
	<?php echo CHtml::encode($data->xonadonga_yaqin_tran_qatnov_masofa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadondan_mahalla_markaz_masofa')); ?>:</b>
	<?php echo CHtml::encode($data->xonadondan_mahalla_markaz_masofa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadon_mahalla_trans_soni')); ?>:</b>
	<?php echo CHtml::encode($data->xonadon_mahalla_trans_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadon_tuman_masofa')); ?>:</b>
	<?php echo CHtml::encode($data->xonadon_tuman_masofa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadon_tuman_trans_soni')); ?>:</b>
	<?php echo CHtml::encode($data->xonadon_tuman_trans_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadon_viloyat_masofa')); ?>:</b>
	<?php echo CHtml::encode($data->xonadon_viloyat_masofa); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadon_viloyat_trans_soni')); ?>:</b>
	<?php echo CHtml::encode($data->xonadon_viloyat_trans_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yol_kira_mahalla')); ?>:</b>
	<?php echo CHtml::encode($data->yol_kira_mahalla); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yol_kira_tuman')); ?>:</b>
	<?php echo CHtml::encode($data->yol_kira_tuman); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yol_kira_viloyat')); ?>:</b>
	<?php echo CHtml::encode($data->yol_kira_viloyat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fuqaro_uy_id')); ?>:</b>
	<?php echo CHtml::encode($data->fuqaro_uy_id); ?>
	<br />

    <div class="row">
		<?php echo $form->label($model,'anketa_ozgarish'); ?>
		<?php echo $form->textField($model,'anketa_ozgarish'); ?>
	</div>

	*/ ?>

</div>