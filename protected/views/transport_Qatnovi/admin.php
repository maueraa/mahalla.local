<?php
/* @var $this Transport_QatnoviController */
/* @var $model Transport_Qatnovi */

$this->breadcrumbs=array(
	Yii::t('strings','Transport  Qatnovis')=>array('index'),
	Yii::t('strings','Manage'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Transport_Qatnovi'))), 'url'=>array('index')),
#	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Transport_Qatnovi'))), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#transport--qatnovi-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Transport_Qatnovi'))) ?></h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link(Yii::t('zii','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'transport--qatnovi-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'xonadonga_yaqin_tran_qatnov_masofa',
		'xonadondan_mahalla_markaz_masofa',
		'xonadon_mahalla_trans_soni',
		'xonadon_tuman_masofa',
		'xonadon_tuman_trans_soni',
		/*
		'xonadon_viloyat_masofa',
		'xonadon_viloyat_trans_soni',
		'yol_kira_mahalla',
		'yol_kira_tuman',
		'yol_kira_viloyat',
		'fuqaro_uy_id',
		'anketa_ozgarish',
		//'fuqaro_uy.name',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
