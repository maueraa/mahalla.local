<?php
/* @var $this TumanController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Туман'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Tuman'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Tuman'))), 'url'=>array('admin')),
);



?>

<h1><?php echo Yii::t('strings','{vil}даги туманлар', array('{vil}'=>$vil))?></h1>
<?php

$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'viewData'=>array('cp'=>$dataProvider),
	'itemsTagName'=>'table',
	'itemsHeader'=>"
	<tr>
	<th style='width: 20px;'>№</th>
	<th style='width: 625px;'>Туман</th>

	</tr>
	",
)); ?>
