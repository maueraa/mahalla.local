<?php
/* @var $this TumanqController */
/* @var $model Tumanq */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tumanq-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'tuman_nomi'); ?>
		<?php echo $form->textField($model,'tuman_nomi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'tuman_nomi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'viloyat_id'); ?>
		<?php echo $form->textField($model,'viloyat_id'); ?>
		<?php echo $form->error($model,'viloyat_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->