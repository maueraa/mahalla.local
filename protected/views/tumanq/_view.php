<?php
/* @var $this TumanqController */
/* @var $data Tumanq */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tuman_nomi')); ?>:</b>
	<?php echo CHtml::encode($data->tuman_nomi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('viloyat_id')); ?>:</b>
	<?php echo CHtml::encode($data->viloyat_id); ?>
	<br />


</div>