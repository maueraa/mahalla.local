<?php
/* @var $this TumanqController */
/* @var $model Tumanq */

$this->breadcrumbs=array(
	Yii::t('strings','Tumanqs')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Tumanq'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Tumanq'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Tumanq')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>