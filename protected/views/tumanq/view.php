<?php
/* @var $this TumanqController */
/* @var $model Tumanq */

$this->breadcrumbs=array(
	Yii::t('strings','Tumanqs')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Tumanq'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Tumanq'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Tumanq'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Tumanq'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Tumanq'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Tumanq') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'tuman_nomi',
		'viloyat_id',
/**
		array(
			'label'=>$model->getAttributeLabel('viloyat_id'),
			'type'=>'raw',
			'value'=>$model->viloyat->name,
		),
/**/
	),
)); ?>
