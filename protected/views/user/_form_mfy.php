<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
$roles=array(0=>'admin',1=>'boshqaruvchi',2=>'kotiba',3=>'inspektor',4=>'tashkilot',5=>'sektor')?>

<div class="form">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'user-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

    <p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>

    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo Yii::t('strings','Вилоят') ?><br/>
        <?php echo CHtml::DropDownList('m8643','',CHtml::listData(Viloyatlar::model()->findAll(),'id','viloyat_nomi'),array('id'=>'id_vil2','prompt'=>Yii::t("strings",'Вилоятни танланг')))  ?>
    </div>
    <?php
    ECascadeDropDown::master('id_vil2')->setDependent('id_tuman2',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadtuman');
    ?>
    <div class="row">
        <?php echo Yii::t('strings','Туман') ?><br/>
        <?php echo CHtml::DropDownList('m3492','',CHtml::listData(Tuman::model()->findAll(),'id','tuman_nomi'),array('id'=>'id_tuman2','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'mfy_id'); ?>
        <?php echo $form->dropDownList($model,'mfy_id',array(),array('id'=>'id_mfy','prompt'=>Yii::t("strings","МФЙни танланг"))); ?>
        <?php echo $form->error($model,'mfy_id'); ?>
</div>
    <?php
    ECascadeDropDown::master('id_tuman2')->setDependent('id_mfy',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_Uy/loadmfy');
    ?>

    <div class="row">
        <?php echo $form->labelEx($model,'username'); ?>
        <?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($model,'username'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'password'); ?>
        <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($model,'password'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'role'); ?>
        <?php echo $form->dropDownList($model,'role',$roles); ?>
        <?php echo $form->error($model,'role'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'user_id').Yii::app()->user->username; ?>
        <?php #echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
        <?php echo $form->error($model,'user_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'user_date').date("d.m.Y"); ?>
        <?php echo $form->hiddenField($model,'user_date',array('value'=>date("Y-m-d H:i:s"))); ?>
        <?php echo $form->error($model,'user_date'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array('confirm'=>'Сиз хақиқаттан хам фойдаланувчи яратмоқчимисиз','class'=>'btn btn-primary')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->