<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'role'); ?>
		<?php echo $form->textField($model,'role'); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model,'user_id'); ?>
        <?php echo $form->textField($model,'user_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'user_date'); ?>
        <?php echo $form->textField($model,'user_date'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'tasgkilot_id'); ?>
        <?php echo $form->textField($model,'tasgkilot_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'mfy_id'); ?>
        <?php echo $form->textField($model,'mfy_id'); ?>
    </div>

    <div class="row buttons">
        <div class="form-actions">
          <?php  $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Search',
        )); ?>
	</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->