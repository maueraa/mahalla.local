<?php
/* @var $this UserController */
/* @var $data User */
$roles=array(0=>'admin',1=>'boshqaruvchi',2=>'kotiba',3=>'inspektor',4=>'tashkilot',5=>'sektor')
?>

<tr class="view">

<td>
	<?php echo CHtml::link(CHtml::encode($data->id)); ?>
</td>

<td>
	<?php  echo CHtml::link(CHtml::encode($data->username), array('view', 'id'=>$data->id)); ?>
</td>

<td>
    <?php if(isset($data->viloyat)) echo CHtml::encode($data->viloyatlars->viloyat_nomi); ?>,
    <?php if(isset($data->tuman))echo CHtml::encode($data->tumen->tuman_nomi); ?>

</td>
<td>
    <?php echo CHtml::encode($data->user_date); ?>
</td>

<td>
	<?php echo CHtml::encode($roles[$data->role]); ?>
</td>

<td>
    Фойдаланувчи охирги кирган санаси
</td>

<td>
    <?php echo CHtml::encode($data->email); ?>
</td>

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tasgkilot_id')); ?>:</b>
	<?php echo CHtml::encode($data->tasgkilot_id); ?>
	<br />
*/?>

</tr>