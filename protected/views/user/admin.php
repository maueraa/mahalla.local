<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	Yii::t('strings','Фойдаланувчилар')=>array("index?tuman_id=".Yii::app()->user->tuman),
	Yii::t('strings','Қидириш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label} рўйхати', array('{label}'=>Yii::t('strings','Фойдаланувчи'))), 'url'=>array("index?tuman_id=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label} киритиш', array('{label}'=>Yii::t('strings','Фойдаланувчи'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','МФЙга {label} киритиш', array('{label}'=>Yii::t('strings','Фойдаланувчи'))), 'url'=>array('create_mfy')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('fuqaro--uy-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('strings','{label} рўйхати', array('{label}'=>Yii::t('strings','Фойдаланувчилар'))) ?></h1>

<?php echo CHtml::link(Yii::t('zii','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php $roles=array(3=>'oddiy',2=>'kotiba',4=>'tashkilot');?>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'username',

		'email',
		'role',
        'user.user_id',
        'user_date',
        'tasgkilot_id',
		//'r.name',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
