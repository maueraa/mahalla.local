<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	Yii::t('strings','Фойдаланувчилар')=>array("index?tuman_id=".Yii::app()->user->tuman),
	Yii::t('strings','Қўшиш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label} рўйхати', array('{label}'=>Yii::t('strings','Фойдаланувчилар'))), 'url'=>array("index?tuman_id=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label} қидириш', array('{label}'=>Yii::t('strings','Фойдаланувчи'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{label} қўшиш', array('{label}'=>Yii::t('strings','User')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>