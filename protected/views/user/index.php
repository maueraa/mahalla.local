<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Фойдаланувчилар'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','{label} қўшиш', array('{label}'=>Yii::t('strings','Фойдаланувчи'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','МФЙга {label} қўшиш', array('{label}'=>Yii::t('strings','фодаланувчи'))), 'url'=>array('create_mfy')),
	array('label'=>Yii::t('strings','{label} қидириш', array('{label}'=>Yii::t('strings','Фодаланувчи'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Фойдаланувчилар')?></h1>

<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'itemsTagName'=>'table',
    'itemsCssClass'=>'table table-bordered table-striped',
    'itemsHeader'=>"
	<thead><tr>
	<th>№</th>
	<th>Фойдаланувчи логини</th>
	<th>Туман,МФЙ номи</th>
	<th>Фойдаланувчи рўйхатга олинган сана</th>
	<th>Фойдаланувчи даражаси</th>
	<th>Охирги кирган санаси</th>
	<th>Телефон рақами</th>
	</tr></thead>
	",
)); ?>
