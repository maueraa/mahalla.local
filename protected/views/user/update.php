<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	Yii::t('strings','Фойдаланувчи')=>array("index?tuman_id=".Yii::app()->user->tuman),
	Yii::t('strings',$model->id)=>array('view','id'=>$model->id),
	Yii::t('strings','Ўзгартириш'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Фойдаланувчилар'))), 'url'=>array("index?tuman_id=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Фойдаланувчи киритиш'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','МФЙга фойдаланувчи киритиш'))), 'url'=>array('create_mfy')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Фойдаланувчини кўриш'))), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Фойдаланувчини қидириш'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Update {table} "{record}" (id={id})', array(
	'{table}'=>Yii::t('strings','User'),
	'{record}'=>$model->id,
	'{id}'=>$model->id));?></h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>