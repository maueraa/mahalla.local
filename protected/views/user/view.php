<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	Yii::t('strings','Фойдаланувчилар')=>array("index?tuman_id=".Yii::app()->user->tuman),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Фойдаланувчилар'))), 'url'=>array("index?tuman_id=".Yii::app()->user->tuman)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Фойдаланувчи қўшиш'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Фойдаланувчини ўзгартириш'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Фойдаланучини ўчириш'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Фойдаланувчини қидириш'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','User') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',

		'email',
		'role',
/**
		array(
			'label'=>$model->getAttributeLabel('role'),
			'type'=>'raw',
			'value'=>$model->r->name,
		),
/**/
	),
)); ?>
