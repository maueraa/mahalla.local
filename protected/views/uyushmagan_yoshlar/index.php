<?php
/* @var $this Fuqaro_kasallarController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Fuqaros'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Fuqaro'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Fuqaro'))), 'url'=>array('admin')),
);

?>
<h1><?php echo Yii::t('strings','{tuman}даги истиқомат қилувчи <br>Уюшмган ёшлар<br> хақида маълумот', array('{tuman}'=>$tuman))?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'itemsTagName'=>'table',
    'itemsCssClass'=>'table table-bordered table-striped',
    'itemsHeader'=>"<thead>
	<tr>
	<th>ID</th>
	<th>Фамалия исм шариф</th>
	<th>Туғилган санаси ва пасспорт рақами </th>
	<th>Вилоят, туман, МФЙ, кўча ва уй рақами</th>
	<th>Маълумоти, битирган жойи ва мутахассислиги</th>
	<th>Иш жойи,лавозими ва телефон рақами</th>

	</tr></thead>",
)); ?>
