<?php
/* @var $this Fuqaro_kasallarController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Кўча'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Fuqaro'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Fuqaro'))), 'url'=>array('admin')),
);

?>
<h1><?php echo Yii::t('strings','{kocha}дан <br>Узоқ муддатга чикиб-кетган ва қайтган фуқаролар<br> хақида маълумот', array('{kocha}'=>$kocha))?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'itemsTagName'=>'table',
    'itemsCssClass'=>'table table-bordered table-striped',
    'itemsHeader'=>"
	<thead>
	<tr>
	<th>ID</th>
	<th>Фамалия исм шариф</th>
	<th>Туғилган санаси ва пасспорт ёки метирка рақами </th>
	<th>Вилоят, туман, МФЙ, кўча ва уй рақами</th>
	<th>Узоқ муддатга чиқиб кетган - келган вакти ва давлати</th>
	<th>Узоқ муддатга борган давлатидаги мехнат фаолияти хақида маълумот</th>
	<th>Узоқ муддатга бориб келганданг сўнг сухбат ўтганлиги</th>
	<th>Хозирги даврдаги мехнат фаолияти: иш жойи,лавозими ва телефон рақами</th>
	</tr>
	</thead>
	",
)); ?>
