<?php
/* @var $this ViloyatlarController */
/* @var $model Viloyatlar */

$this->breadcrumbs=array(
	Yii::t('strings','Viloyatlars')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Viloyatlar'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Viloyatlar'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Viloyatlar')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>