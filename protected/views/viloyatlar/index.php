<?php
/* @var $this ViloyatlarController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Вилоятлар'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Viloyatlar'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Viloyatlar'))), 'url'=>array('admin')),
);

?>

<h1><?php echo Yii::t('strings','Вилоятлар')?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'itemsTagName'=>'table',
	'viewData'=>array('cp'=>$dataProvider),
    'itemsHeader'=>"
	<tr>
	<th style='width: 20px;'>№</th>
	<th style='width: 625px;'>Вилоят</th>

	</tr>
	",
)); ?>
