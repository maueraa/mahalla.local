<?php
/* @var $this ViloyatlarController */
/* @var $model Viloyatlar */

$this->breadcrumbs=array(
	Yii::t('strings','Viloyatlars')=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Viloyatlar'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Viloyatlar'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Viloyatlar'))), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Viloyatlar'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Viloyatlar'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Viloyatlar') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'viloyat_nomi',
/**
		array(
			'label'=>$model->getAttributeLabel('viloyat_nomi'),
			'type'=>'raw',
			'value'=>$model->viloyat_n->name,
		),
/**/
	),
)); ?>
