﻿<?php
/* @var $this Ximzat_KorsatishController */
/* @var $model Ximzat_Korsatish */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'ximzat--korsatish-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>
		Vасофа метрда киритинг (масалан:100), метр ёки м сўзини ёзиш шарт эмас
	<br>
	<br>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'xondonga_yaqin_maktab_id'); ?>
		<?php echo $form->dropDownList($model,'xondonga_yaqin_maktab_id',$model->getmaktab(),array('prompt'=>'Мактабни танланг')); ?>
		<?php echo $form->error($model,'xondonga_yaqin_maktab_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xondonga_yaqin_maktab_masofa'); ?>
		<?php echo $form->textField($model,'xondonga_yaqin_maktab_masofa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'xondonga_yaqin_maktab_masofa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadonga_yaqin_bogcha_id'); ?>
		<?php echo $form->dropDownList($model,'xonadonga_yaqin_bogcha_id',$model->getbogcha(),array('prompt'=>'Боғчани танланг')); ?>
		<?php echo $form->error($model,'xonadonga_yaqin_bogcha_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadonga_yaqin_bogcha_masofa'); ?>
		<?php echo $form->textField($model,'xonadonga_yaqin_bogcha_masofa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'xonadonga_yaqin_bogcha_masofa'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'xonadonga_yaqin_kollej_nomi'); ?>
        <?php echo $form->textField($model,'xonadonga_yaqin_kollej_nomi',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'xonadonga_yaqin_kollej_nomi'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'xonadonga_yaqin_kollej_masofa'); ?>
        <?php echo $form->textField($model,'xonadonga_yaqin_kollej_masofa',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'xonadonga_yaqin_kollej_masofa'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadonga_yaqin_savdo_obyekt_masofa'); ?>
		<?php echo $form->textField($model,'xonadonga_yaqin_savdo_obyekt_masofa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'xonadonga_yaqin_savdo_obyekt_masofa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadonga_yaqin_xizmat_korsatish_masofa'); ?>
		<?php echo $form->textField($model,'xonadonga_yaqin_xizmat_korsatish_masofa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'xonadonga_yaqin_xizmat_korsatish_masofa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadonga_yaqin_bozorcha_masofa'); ?>
		<?php echo $form->textField($model,'xonadonga_yaqin_bozorcha_masofa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'xonadonga_yaqin_bozorcha_masofa'); ?>
	</div>
    <div class="row">
        <?php echo $form->labelEx($model,'xonadonga_yaqin_xamom_masofa'); ?>
        <?php echo $form->textField($model,'xonadonga_yaqin_xamom_masofa',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'xonadonga_yaqin_xamom_masofa'); ?>
    </div>
	<div class="row">
		<?php echo $form->labelEx($model,'xonadonga_yaqin_sport_majmuasi_masofa'); ?>
		<?php echo $form->textField($model,'xonadonga_yaqin_sport_majmuasi_masofa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'xonadonga_yaqin_sport_majmuasi_masofa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadonga_yaqin_togarak_obyekt_masofa'); ?>
		<?php echo $form->textField($model,'xonadonga_yaqin_togarak_obyekt_masofa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'xonadonga_yaqin_togarak_obyekt_masofa'); ?>
	</div>

<?php
/*	<div class="row">
		<?php echo $form->labelEx($model,'xonadonga_yaqin_bogacha_masofa'); ?>
		<?php echo $form->textField($model,'xonadonga_yaqin_bogacha_masofa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'xonadonga_yaqin_bogacha_masofa'); ?>
	</div>
*/?>

	<div class="row">
		<?php echo $form->labelEx($model,'istiroxat_bog'); ?>
		<?php echo $form->textField($model,'istiroxat_bog',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'istiroxat_bog'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'istiroxat_bog_masofa'); ?>
		<?php echo $form->textField($model,'istiroxat_bog_masofa',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'istiroxat_bog_masofa'); ?>
	</div>
	<div class="row">
		
		<?php echo $form->hiddenField($model,'tbl_fuqaro_uy_id',array('value'=>$fid)); ?>
		<?php echo $form->error($model,'tbl_fuqaro_uy_id'); ?>	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'anketa_ozgarish').":".date("d.m.Y"); ?>
        <?php echo $form->hiddenField($model,'anketa_ozgarish',array('value'=>date("Y-m-d"))); ?>
        <?php echo $form->error($model,'anketa_ozgarish'); ?>
    </div>
	<div class="row">
		<?php echo $form->labelEx($model,'user_id').Yii::app()->user->name; ?>
		<?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_date').date("d.m.Y"); ?>
		<?php echo $form->hiddenField($model,'user_date',array('value'=>date("Y-m-d"))); ?>
		<?php echo $form->error($model,'user_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array("class"=>"btn btn-primary")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->