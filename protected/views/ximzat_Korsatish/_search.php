<?php
/* @var $this Ximzat_KorsatishController */
/* @var $model Ximzat_Korsatish */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xondonga_yaqin_maktab_id'); ?>
		<?php echo $form->textField($model,'xondonga_yaqin_maktab_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xondonga_yaqin_maktab_masofa'); ?>
		<?php echo $form->textField($model,'xondonga_yaqin_maktab_masofa',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadonga_yaqin_bogcha_id'); ?>
		<?php echo $form->textField($model,'xonadonga_yaqin_bogcha_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadonga_yaqin_bogcha_masofa'); ?>
		<?php echo $form->textField($model,'xonadonga_yaqin_bogcha_masofa',array('size'=>60,'maxlength'=>255)); ?>
	</div>
    <div class="row">
        <?php echo $form->label($model,'xonadonga_yaqin_kollej_masofa'); ?>
        <?php echo $form->textField($model,'xonadonga_yaqin_kollej_masofa',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'xonadonga_yaqin_kollej_nomi'); ?>
        <?php echo $form->textField($model,'xonadonga_yaqin_kollej_nomi',array('size'=>60,'maxlength'=>255)); ?>
    </div>
	<div class="row">
		<?php echo $form->label($model,'xonadonga_yaqin_savdo_obyekt_masofa'); ?>
		<?php echo $form->textField($model,'xonadonga_yaqin_savdo_obyekt_masofa',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadonga_yaqin_xizmat_korsatish_masofa'); ?>
		<?php echo $form->textField($model,'xonadonga_yaqin_xizmat_korsatish_masofa',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadonga_yaqin_bozorcha_masofa'); ?>
		<?php echo $form->textField($model,'xonadonga_yaqin_bozorcha_masofa',array('size'=>60,'maxlength'=>255)); ?>
	</div>
    <div class="row">
        <?php echo $form->label($model,'xonadonga_yaqin_xamom_masofa'); ?>
        <?php echo $form->textField($model,'xonadonga_yaqin_xamom_masofa',array('size'=>60,'maxlength'=>255)); ?>
    </div>
	<div class="row">
		<?php echo $form->label($model,'xonadonga_yaqin_sport_majmuasi_masofa'); ?>
		<?php echo $form->textField($model,'xonadonga_yaqin_sport_majmuasi_masofa',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadonga_yaqin_togarak_obyekt_masofa'); ?>
		<?php echo $form->textField($model,'xonadonga_yaqin_togarak_obyekt_masofa',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadonga_yaqin_bogacha_masofa'); ?>
		<?php echo $form->textField($model,'xonadonga_yaqin_bogacha_masofa',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tbl_fuqaro_uy_id'); ?>
		<?php echo $form->textField($model,'tbl_fuqaro_uy_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model,'istiroxat_bog'); ?>
		<?php echo $form->textField($model,'istiroxat_bog',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'istiroxat_bog_masofa'); ?>
		<?php echo $form->textField($model,'istiroxat_bog_masofa',array('size'=>60,'maxlength'=>255)); ?>
	</div>

    <div class="row">
        <?php echo $form->label($model,'anketa_ozgarish'); ?>
        <?php echo $form->textField($model,'anketa_ozgarish'); ?>
    </div>
	<div class="row">
		<?php echo $form->label($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_date'); ?>
		<?php echo $form->textField($model,'user_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->