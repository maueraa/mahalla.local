<?php
/* @var $this Ximzat_KorsatishController */
/* @var $data Ximzat_Korsatish */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xondonga_yaqin_maktab_id')); ?>:</b>
	<?php echo CHtml::encode($data->xondonga_yaqin_maktab_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xondonga_yaqin_maktab_masofa')); ?>:</b>
	<?php echo CHtml::encode($data->xondonga_yaqin_maktab_masofa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadonga_yaqin_bogcha_id')); ?>:</b>
	<?php echo CHtml::encode($data->xonadonga_yaqin_bogcha_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadonga_yaqin_bogcha_masofa')); ?>:</b>
	<?php echo CHtml::encode($data->xonadonga_yaqin_bogcha_masofa); ?>
	<br />
    <b><?php echo CHtml::encode($data->getAttributeLabel('xonadonga_yaqin_kollej_masofa')); ?>:</b>
    <?php echo CHtml::encode($data->xonadonga_yaqin_kollej_masofa); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('xonadonga_yaqin_kollej_nomi')); ?>:</b>
    <?php echo CHtml::encode($data->xonadonga_yaqin_kollej_nomi); ?>
    <br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadonga_yaqin_savdo_obyekt_masofa')); ?>:</b>
	<?php echo CHtml::encode($data->xonadonga_yaqin_savdo_obyekt_masofa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadonga_yaqin_xizmat_korsatish_masofa')); ?>:</b>
	<?php echo CHtml::encode($data->xonadonga_yaqin_xizmat_korsatish_masofa); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadonga_yaqin_bozorcha_masofa')); ?>:</b>
	<?php echo CHtml::encode($data->xonadonga_yaqin_bozorcha_masofa); ?>
	<br /><b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_date')); ?>:</b>
	<?php echo CHtml::encode($data->user_date); ?>
	<br />
 <b><?php echo CHtml::encode($data->getAttributeLabel('xonadonga_yaqin_xamom_masofa')); ?>:</b>
	<?php echo CHtml::encode($data->xonadonga_yaqin_xamom_masofa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadonga_yaqin_sport_majmuasi_masofa')); ?>:</b>
	<?php echo CHtml::encode($data->xonadonga_yaqin_sport_majmuasi_masofa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadonga_yaqin_togarak_obyekt_masofa')); ?>:</b>
	<?php echo CHtml::encode($data->xonadonga_yaqin_togarak_obyekt_masofa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadonga_yaqin_bogacha_masofa')); ?>:</b>
	<?php echo CHtml::encode($data->xonadonga_yaqin_bogacha_masofa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tbl_fuqaro_uy_id')); ?>:</b>
	<?php echo CHtml::encode($data->tbl_fuqaro_uy_id); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('anketa_ozgarish')); ?>:</b>
	<?php echo $form->textField($model,'anketa_ozgarish');CHtml::encode($data->anketa_ozgarish); ?>
	<br />

 */ ?>

</div>