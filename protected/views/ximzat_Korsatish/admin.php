<?php
/* @var $this Ximzat_KorsatishController */
/* @var $model Ximzat_Korsatish */

$this->breadcrumbs=array(
	Yii::t('strings','Ximzat  Korsatishes')=>array('index'),
	Yii::t('strings','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Ximzat_Korsatish'))), 'url'=>array('index')),
	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Ximzat_Korsatish'))), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#ximzat--korsatish-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Ximzat_Korsatish'))) ?></h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link(Yii::t('zii','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ximzat--korsatish-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'xondonga_yaqin_maktab_id',
		'xondonga_yaqin_maktab_masofa',
		'xonadonga_yaqin_bogcha_id',
		'xonadonga_yaqin_bogcha_masofa',
		'xonadonga_yaqin_savdo_obyekt_masofa',
		/*
		 * 'xonadonga_yaqin_kollej_masofa',
		/*
		'xonadonga_yaqin_kollej_nomi',
		'xonadonga_yaqin_savdo_obyekt_masofa',
		'xonadonga_yaqin_xizmat_korsatish_masofa',
		'xonadonga_yaqin_bozorcha_masofa',
		'xonadonga_yaqin_sport_majmuasi_masofa',
		'xonadonga_yaqin_togarak_obyekt_masofa',
		'xonadonga_yaqin_xamom_masofa',
		'xonadonga_yaqin_bogacha_masofa',
		'tbl_fuqaro_uy_id',
		'istiroxat_bog',
		'istiroxat_bog_masofa',
		'anketa_ozgarish',
		'user_id',
		'user_date',
		//'tbl_fuqaro_uy.name',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
