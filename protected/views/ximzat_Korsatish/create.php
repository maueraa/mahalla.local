﻿<?php
/* @var $this Ximzat_KorsatishController */
/* @var $model Ximzat_Korsatish */

$this->breadcrumbs=array(
	Yii::t('strings','Ximzat  Korsatishes')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
	#array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Ximzat_Korsatish'))), 'url'=>array('index')),
	#array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Ximzat_Korsatish'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Хонадонга яқин ижтимоий ва маиший хизмат кўрсатиш объектларини масофаси хақида маълумотларини киритиш ойнаси')?> </h1>

<?php $this->renderPartial('_form', array('model'=>$model,'fid'=>$fid)); ?>