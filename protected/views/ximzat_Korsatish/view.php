<?php
/* @var $this Ximzat_KorsatishController */
/* @var $model Ximzat_Korsatish */

$this->breadcrumbs=array(
	Yii::t('strings','Хизмат кўрсатиш')=>array('index'),
	$model->id,
);

$this->menu=array(
	#array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Ximzat_Korsatish'))), 'url'=>array('index')),
	#array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Ximzat_Korsatish'))), 'url'=>array('create')),
	#array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Ximzat_Korsatish'))), 'url'=>array('update', 'id'=>$model->id)),
	#array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Ximzat_Korsatish'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	#array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Ximzat_Korsatish'))), 'url'=>array('admin')),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон маълумотлари'))), 'url'=>array("/fuqaro_Uy/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон аъзолари'))), 'url'=>array("/fuqarolar?fuq=$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Маънавий мухит'))), 'url'=>array("/manaviy/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Даромади'))), 'url'=>array("/daromad/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Қарздоллик'))), 'url'=>array("/qarz/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт қатнови'))), 'url'=>array("/transport_qatnovi/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ижтимоий мухит'))), 'url'=>array("/ijtimoiy_muhit/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Шарт-шароити'))), 'url'=>array("/shaoriti/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Томорқа'))), 'url'=>array("/tomorqa/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Чорва хайвонлари'))), 'url'=>array("/hayvon/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хизмат кўрсатиш объектлари хақидаги маълумотларни ўзгартириш'))), 'url'=>array('update', 'fid'=>$model->tbl_fuqaro_uy_id)),
    #array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хизмат кўрсатиш объектлари'))), 'url'=>array("/ximzat_korsatish/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчанинг холати'))), 'url'=>array("/kocha_xolati/$model->tbl_fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Барча маълумотларни кўриш'))), 'url'=>array('/jamlama/index', 'id'=>$model->tbl_fuqaro_uy_id)),
);
?>

<h1><?php echo Yii::t('strings','Хонадонга яқин ижтимоий ва маиший хизмат кўрсатиш объектларини масофаси хақида маълумотларини янгиланган холатдаги кўриниши ') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'xondonga_yaqin_maktab_id',
		'xondonga_yaqin_maktab_masofa',
		'xonadonga_yaqin_bogcha_id',
		'xonadonga_yaqin_bogcha_masofa',
        'xonadonga_yaqin_kollej_masofa',
        'xonadonga_yaqin_kollej_nomi',
		'xonadonga_yaqin_savdo_obyekt_masofa',
		'xonadonga_yaqin_xizmat_korsatish_masofa',
		'xonadonga_yaqin_bozorcha_masofa',
        'xonadonga_yaqin_xamom_masofa',
		'xonadonga_yaqin_sport_majmuasi_masofa',
		'xonadonga_yaqin_togarak_obyekt_masofa',
		'xonadonga_yaqin_bogacha_masofa',
		'istiroxat_bog',
		'istiroxat_bog_masofa',
		'anketa_ozgarish',
		'user_id',
		'user_date',
		'tbl_fuqaro_uy_id',
/**
		array(
			'label'=>$model->getAttributeLabel('tbl_fuqaro_uy_id'),
			'type'=>'raw',
			'value'=>$model->tbl_fuqaro_uy->name,
		),
/**/
	),
)); ?>
