﻿<?php
/* @var $this Xonadon_TransportController */
/* @var $model Xonadon_Transport */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'xonadon--transport-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><?php echo Yii::t('zii','Fields with {require} are required', array('{require}'=>'<span class="required">*</span>')) ?></p>
	Агар хонадонда транспорт воситаси мавжуд бўлмаса қолган майдонларни тўлдириш шарт эмас.
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'xonadon_transport_soni'); ?>
		<?php echo $form->textField($model,'xonadon_transport_soni'); ?>
		<?php echo $form->error($model,'xonadon_transport_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yengil_avto_rusumi'); ?>
		<?php echo $form->textArea($model,'yengil_avto_rusumi',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'yengil_avto_rusumi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yengil_avto_raqami'); ?>
		<?php echo $form->textField($model,'yengil_avto_raqami',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yengil_avto_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yengil_avto_yili'); ?>
		<?php echo $form->textField($model,'yengil_avto_yili',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yengil_avto_yili'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yengil_avto_tex_pas'); ?>
		<?php echo $form->textField($model,'yengil_avto_tex_pas',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yengil_avto_tex_pas'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'yengi_avto_egasi'); ?>
        <?php echo $form->textField($model,'yengi_avto_egasi',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'yengi_avto_egasi'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'yengil_prvara_raqami'); ?>
        <?php echo $form->textField($model,'yengil_prvara_raqami',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'yengil_prvara_raqami'); ?>
    </div>

    <div class="row">
		<?php echo $form->labelEx($model,'yengil_sugurta_mavjudligi'); ?>
		<?php echo $form->textField($model,'yengil_sugurta_mavjudligi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yengil_sugurta_mavjudligi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yengil_ishonchnoma'); ?>
		<?php echo $form->textField($model,'yengil_ishonchnoma',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yengil_ishonchnoma'); ?>
	</div>

    <div class="row">
		<?php echo $form->labelEx($model,'yuk_avto_rusumi'); ?>
		<?php echo $form->textField($model,'yuk_avto_rusumi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yuk_avto_rusumi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yuk_avto_raqami'); ?>
		<?php echo $form->textField($model,'yuk_avto_raqami',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yuk_avto_raqami'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yuk_avto_yili'); ?>
		<?php echo $form->textField($model,'yuk_avto_yili',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yuk_avto_yili'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yuk_avto_tex_pas'); ?>
		<?php echo $form->textField($model,'yuk_avto_tex_pas',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yuk_avto_tex_pas'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yuk_avto_egasi'); ?>
		<?php echo $form->textField($model,'yuk_avto_egasi',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yuk_avto_egasi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yuk_avto_prva'); ?>
		<?php echo $form->textField($model,'yuk_avto_prva',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yuk_avto_prva'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yuk_avto_sugurta'); ?>
		<?php echo $form->textField($model,'yuk_avto_sugurta',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yuk_avto_sugurta'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yuk_avto_ishonchnoma'); ?>
		<?php echo $form->textField($model,'yuk_avto_ishonchnoma',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'yuk_avto_ishonchnoma'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'trans_boshqalar'); ?>
		<?php echo $form->textField($model,'trans_boshqalar',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'trans_boshqalar'); ?>
	</div>

	<div class="row">
		<?php  $form->labelEx($model,'fuqaro_uy_id'); ?>
		<?php echo $form->hiddenField($model,'fuqaro_uy_id',array('value'=>$fid)); ?>
		<?php echo $form->error($model,'fuqaro_uy_id'); ?>
	</div>

	<div class="row">
		<?php  $form->labelEx($model,'fuqaro_id'); ?>
		<?php echo $form->hiddenField($model,'fuqaro_id'); ?>
		<?php echo $form->error($model,'fuqaro_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id').Yii::app()->user->name; ?>
		<?php echo $form->hiddenField($model,'user_id',array('value'=>Yii::app()->user->id)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_date').date("d.m.Y"); ?>
		<?php echo $form->hiddenField($model,'user_date',array('value'=>date("Y-m-d"))); ?>
		<?php echo $form->error($model,'user_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('zii','Create') : Yii::t('zii','Save'),array("class"=>"btn btn-primary")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->