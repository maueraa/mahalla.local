<?php
/* @var $this Xonadon_TransportController */
/* @var $model Xonadon_Transport */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'xonadon_transport_soni'); ?>
		<?php echo $form->textField($model,'xonadon_transport_soni'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yengil_avto_rusumi'); ?>
		<?php echo $form->textArea($model,'yengil_avto_rusumi',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yengil_avto_raqami'); ?>
		<?php echo $form->textField($model,'yengil_avto_raqami',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yengil_avto_yili'); ?>
		<?php echo $form->textField($model,'yengil_avto_yili',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yengil_avto_tex_pas'); ?>
		<?php echo $form->textField($model,'yengil_avto_tex_pas',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'yengi_avto_egasi'); ?>
        <?php echo $form->textField($model,'yengi_avto_egasi',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
		<?php echo $form->label($model,'yengil_prvara_raqami'); ?>
		<?php echo $form->textField($model,'yengil_prvara_raqami',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
		<?php echo $form->label($model,'yengil_sugurta_mavjudligi'); ?>
		<?php echo $form->textField($model,'yengil_sugurta_mavjudligi',array('size'=>60,'maxlength'=>255)); ?>
    </div>

    <div class="row">
		<?php echo $form->label($model,'yengil_ishonchnoma'); ?>
		<?php echo $form->textField($model,'yengil_ishonchnoma',array('size'=>60,'maxlength'=>255)); ?>
    </div>


	<div class="row">
		<?php echo $form->label($model,'yuk_avto_rusumi'); ?>
		<?php echo $form->textField($model,'yuk_avto_rusumi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yuk_avto_raqami'); ?>
		<?php echo $form->textField($model,'yuk_avto_raqami',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yuk_avto_yili'); ?>
		<?php echo $form->textField($model,'yuk_avto_yili',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yuk_avto_tex_pas'); ?>
		<?php echo $form->textField($model,'yuk_avto_tex_pas',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yuk_avto_egasi'); ?>
		<?php echo $form->textField($model,'yuk_avto_egasi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yuk_avto_prva'); ?>
		<?php echo $form->textField($model,'yuk_avto_prva',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yuk_avto_sugurta'); ?>
		<?php echo $form->textField($model,'yuk_avto_sugurta',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yuk_avto_ishonchnoma'); ?>
		<?php echo $form->textField($model,'yuk_avto_ishonchnoma',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trans_boshqalar'); ?>
		<?php echo $form->textField($model,'trans_boshqalar',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fuqaro_uy_id'); ?>
		<?php echo $form->textField($model,'fuqaro_uy_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fuqaro_id'); ?>
		<?php echo $form->textField($model,'fuqaro_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->