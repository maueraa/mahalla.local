<?php
/* @var $this Xonadon_TransportController */
/* @var $data Xonadon_Transport */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('xonadon_transport_soni')); ?>:</b>
	<?php echo CHtml::encode($data->xonadon_transport_soni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yengil_avto_rusumi')); ?>:</b>
	<?php echo CHtml::encode($data->yengil_avto_rusumi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yengil_avto_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->yengil_avto_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yengil_avto_yili')); ?>:</b>
	<?php echo CHtml::encode($data->yengil_avto_yili); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yengil_avto_tex_pas')); ?>:</b>
	<?php echo CHtml::encode($data->yengil_avto_tex_pas); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('yengi_avto_egasi')); ?>:</b>
    <?php echo CHtml::encode($data->yengi_avto_egasi); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('yengil_prvara_raqami')); ?>:</b>
    <?php echo CHtml::encode($data->yengil_prvara_raqami); ?>
    <br />

    <?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('yengil_sugurta_mavjudligi')); ?>:</b>
	<?php echo CHtml::encode($data->yengil_sugurta_mavjudligi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yengil_ishonchnoma')); ?>:</b>
	<?php echo CHtml::encode($data->yengil_ishonchnoma); ?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('yuk_avto_rusumi')); ?>:</b>
	<?php echo CHtml::encode($data->yuk_avto_rusumi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yuk_avto_raqami')); ?>:</b>
	<?php echo CHtml::encode($data->yuk_avto_raqami); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yuk_avto_yili')); ?>:</b>
	<?php echo CHtml::encode($data->yuk_avto_yili); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yuk_avto_tex_pas')); ?>:</b>
	<?php echo CHtml::encode($data->yuk_avto_tex_pas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yuk_avto_egasi')); ?>:</b>
	<?php echo CHtml::encode($data->yuk_avto_egasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yuk_avto_prva')); ?>:</b>
	<?php echo CHtml::encode($data->yuk_avto_prva); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yuk_avto_sugurta')); ?>:</b>
	<?php echo CHtml::encode($data->yuk_avto_sugurta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yuk_avto_ishonchnoma')); ?>:</b>
	<?php echo CHtml::encode($data->yuk_avto_ishonchnoma); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trans_boshqalar')); ?>:</b>
	<?php echo CHtml::encode($data->trans_boshqalar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fuqaro_uy_id')); ?>:</b>
	<?php echo CHtml::encode($data->fuqaro_uy_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fuqaro_id')); ?>:</b>
	<?php echo CHtml::encode($data->fuqaro_id); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('anketa_ozgarish')); ?>:</b>
	<?php echo CHtml::encode($data->anketa_ozgarish); ?>
	<br />

	*/ ?>

</div>