<?php
/* @var $this Xonadon_TransportController */
/* @var $model Xonadon_Transport */

$this->breadcrumbs=array(
	Yii::t('strings','Xonadon  Transports')=>array('index'),
	Yii::t('strings','Manage'),
);

$this->menu=array(
	#array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Xonadon_Transport'))), 'url'=>array('index')),
	#array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Xonadon_Transport'))), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#xonadon--transport-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Xonadon_Transport'))) ?></h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link(Yii::t('zii','Advanced Search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'xonadon--transport-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'xonadon_transport_soni',
		'yengil_avto_rusumi',
		'yengil_avto_raqami',
		'yengil_avto_yili',
		'yengil_avto_tex_pas',
		/*
		'yengil_prvara_raqami',
		'yengil_sugurta_mavjudligi',
		'yengil_ishonchnoma',
		'yengi_avto_egasi',
		'yuk_avto_rusumi',
		'yuk_avto_raqami',
		'yuk_avto_yili',
		'yuk_avto_tex_pas',
		'yuk_avto_egasi',
		'yuk_avto_prva',
		'yuk_avto_sugurta',
		'yuk_avto_ishonchnoma',
		'trans_boshqalar',
		'fuqaro_uy_id',
		'fuqaro_id',
		//'fuqaro.name',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
