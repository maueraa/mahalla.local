﻿<?php
/* @var $this Xonadon_TransportController */
/* @var $model Xonadon_Transport */

$this->breadcrumbs=array(
	Yii::t('strings','Xonadon  Transports')=>array('index'),
	Yii::t('strings','Create'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Xonadon_Transport'))), 'url'=>array('index')),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Xonadon_Transport'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадондаги транспорт воситалари хақидаги маълумотларни киритиш ойнаси')));?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'fid'=>$fid)); ?>