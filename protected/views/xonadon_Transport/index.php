<?php
/* @var $this Xonadon_TransportController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Xonadon  Transports'),
);

$this->menu=array(
	#array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Xonadon_Transport'))), 'url'=>array('create')),
	#array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Xonadon_Transport'))), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('strings','Xonadon  Transports')?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
