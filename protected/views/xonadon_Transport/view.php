<?php
/* @var $this Xonadon_TransportController */
/* @var $model Xonadon_Transport */

$this->breadcrumbs=array(
	Yii::t('strings','Хонадондаги транспорт воситалар')=>array('index'),
	$model->id,
);

$this->menu=array(
	#array('label'=>Yii::t('strings','List {label}', array('{label}'=>Yii::t('strings','Xonadon_Transport'))), 'url'=>array('index')),
	#array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Xonadon_Transport'))), 'url'=>array('create')),
	#array('label'=>Yii::t('strings','Update {label}', array('{label}'=>Yii::t('strings','Xonadon_Transport'))), 'url'=>array('update', 'id'=>$model->id)),
	#array('label'=>Yii::t('strings','Delete {label}', array('{label}'=>Yii::t('strings','Xonadon_Transport'))), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('strings','Are you sure you want to delete this item?'))),
	#array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Xonadon_Transport'))), 'url'=>array('admin')),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон маълумотлари'))), 'url'=>array("/fuqaro_Uy/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хонадон аъзолари'))), 'url'=>array("/fuqarolar?fuq=$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Маънавий мухит'))), 'url'=>array("/manaviy/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Даромади'))), 'url'=>array("/daromad/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Қарздоллик'))), 'url'=>array("/qarz/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт воситалар маълумотларни ўзгартириш'))), 'url'=>array('update', 'fid'=>$model->fuqaro_uy_id)),
    #array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Транспорт воситалари'))), 'url'=>array("/xonadon_transport/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Ижтимоий мухит'))), 'url'=>array("/ijtimoiy_muhit/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Шарт-шароити'))), 'url'=>array("/shaoriti/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Томорқа'))), 'url'=>array("/tomorqa/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Чорва хайвонлари'))), 'url'=>array("/hayvon/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Хизмат кўрсатиш объектлари'))), 'url'=>array("/ximzat_korsatish/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Кўчанинг холати'))), 'url'=>array("/kocha_xolati/$model->fuqaro_uy_id")),
    array('label'=>Yii::t('strings','{label}', array('{label}'=>Yii::t('strings','Барча маълумотларни кўриш'))), 'url'=>array('/jamlama/index', 'id'=>$model->fuqaro_uy_id)),

);
?>

<h1><?php echo Yii::t('strings','Хонаднодаги транспорт воситалари хақидаги маълумотларни янгиланган холатдаги маълумотлар') .' - '. Yii::t('strings',$model->id); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'xonadon_transport_soni',
		'yengil_avto_rusumi',
		'yengil_avto_raqami',
		'yengil_avto_yili',
		'yengil_avto_tex_pas',
		'yengil_prvara_raqami',
		'yengil_sugurta_mavjudligi',
		'yengil_ishonchnoma',
		'yengi_avto_egasi',
		'yuk_avto_rusumi',
		'yuk_avto_raqami',
		'yuk_avto_yili',
		'yuk_avto_tex_pas',
		'yuk_avto_egasi',
		'yuk_avto_prva',
		'yuk_avto_sugurta',
		'yuk_avto_ishonchnoma',
		'trans_boshqalar',
		'fuqaro_uy_id',
		'fuqaro_id',
        'anketa_ozgarish',
		'user_id',
		'user_date',
/**
		array(
			'label'=>$model->getAttributeLabel('fuqaro_id'),
			'type'=>'raw',
			'value'=>$model->fuqaro->name,
		),
/**/
	),
)); ?>
