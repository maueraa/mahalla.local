﻿<?php
/* @var $this Fuqaro_UyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','Хонадонлар рўйхати'),
);
$this->menu=array(
	array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Хонадон қўшиш'))), 'url'=>array('create')),
	array('label'=>Yii::t('strings',' {label}', array('{label}'=>Yii::t('strings','Хонадон қидириш'))), 'url'=>array('admin')),
);
?>
<h1>
	<h1><?php echo Yii::t('strings','{kocha}даги  хақида маълумот', array('{kocha}'=>$kocha))?></h1>

</h1>
<div class="view">
	<?php $this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'itemsTagName'=>'table',
    'itemsCssClass'=>'table table-bordered table-striped',
    'itemsHeader'=>"
	<tr>
	<th>№</th>
	<th>Хонадон бошлиғи</th>
	<th>МФЙ, кўчанинг номи, уй рақами</th>
	<th>Кадастр мавжудлиги</th>
	<th>Хонадонда яшовчи фуқаролар сони</th>
	<th>Хонадонда хақида кисқача маълумот</th>
	<th>Янги фуқаро қўшиш</th>
	</tr>
	",
)); ?>
</div>



