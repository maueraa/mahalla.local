<?php
/* @var $this FuqarolarController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	Yii::t('strings','МФЙ'),
);

$this->menu=array(
#	array('label'=>Yii::t('strings','Create {label}', array('{label}'=>Yii::t('strings','Fuqarolar'))), 'url'=>array('create')),
#	array('label'=>Yii::t('strings','Manage {label}', array('{label}'=>Yii::t('strings','Fuqarolar'))), 'url'=>array('admin')),
);
?>
<h1><?php echo Yii::t('strings','{mfy}да истиқомат қилувчи <br>Ёлғиз пенсионер<br> хақида маълумот', array('{mfy}'=>$mfy))?></h1>


<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    'itemsTagName'=>'table',
    'itemsCssClass'=>'table table-bordered table-striped',
    'itemsHeader'=>"
	<thead><tr>
	<th>ID</th>
	<th>Фамилия исми шарифи</th>
	<th>Жинси</th>
	<th>Туғилган санаси ва пасспорт рақами</th>
	<th>Вилоят,туман, МФЙ, кўча номи ва уй рақами </th>
	<th>Маълумоти ва иш жой номи,</th>
	<th> Соғлиги хақида маълумот</th>
	</tr></thead>
	",
)); ?>
