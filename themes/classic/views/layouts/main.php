<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery.mobile.structure-1.4.5.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery.mobile.structure-1.4.5.css" />
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/bootstrap-modal.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/bootstrap-tab.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/jquery.mobile-1.4.5.min.js"></script>
    <link rel="stylesheet" type="text/css"  href="<?php echo Yii::app()->theme->baseUrl;?>/css/bootstrap-theme.css"/>
    <link rel="stylesheet" type="text/css"  href="<?php echo Yii::app()->theme->baseUrl;?>/css/bootstrap.icon-large.css"/>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<?php Yii::app()->bootstrap->register(); ?>
</head>
<body>
<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'fluid' => true,
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'type' => 'navbar',
            'items'=>array(
            array('label'=>Yii::t('strings','Бош сахифа'), 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest),
            array('label'=>Yii::t('strings','МФЙлар'), 'url'=>array('/mfy/index','val_mfy'=>2),'visible'=>isset(Yii::app()->user->role)? Yii::app()->user->role=='5':''),
            array('label'=>Yii::t('strings','Созлаш'), 'url'=>array('/site/tuner'),'visible'=>isset(Yii::app()->user->role)? Yii::app()->user->role=='0':''),
            array('label'=>Yii::t('strings','Хисобот'), 'url'=>array('/site/index2'),'visible'=>isset(Yii::app()->user->role)? Yii::app()->user->role=='0':''),
                array('label'=>Yii::t('strings','МФЙ хонадонлар'), 'url'=>array('/fuqaro_Uy/index','mfy_id'=>isset(Yii::app()->user->mfy_id)? Yii::app()->user->mfy_id:0),'visible'=>!Yii::app()->user->isGuest),
                array('label'=>Yii::t('strings','МФЙ хисобот'), 'url'=>array('#'),
                'items'=>array(
                    array('label'=>Yii::t('strings','МФЙ паспорти'),'url'=>array('jamlama/lala')),
                    array('label'=>Yii::t('strings','Кам таъминланган оилалар'),'url'=>array('jamlama/lala2')),
                    #array('label'=>Yii::t('strings','Ногиронлар'),'url'=>array('jamlama/lala3')),
                    array('label'=>Yii::t('strings','Оғир касаллар'),'url'=>array('fuqaro_kasallar/mfy','mfy'=>isset(Yii::app()->user->mfy_id)? Yii::app()->user->mfy_id:0),'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('strings','Ногиронлар'),'url'=>array('nogiron/mfy','mfy'=>isset(Yii::app()->user->mfy_id)? Yii::app()->user->mfy_id:0),'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('strings','Вояга етмаган ёшлар'),'url'=>array('yosh/mfy','mfy'=>isset(Yii::app()->user->mfy_id)? Yii::app()->user->mfy_id:0),'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('strings','Узоқ муддатга кетган фуқаролар'),'url'=>array('uzoq_muddat/mfy','mfy'=>isset(Yii::app()->user->mfy_id)? Yii::app()->user->mfy_id:0),'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('strings','Узоқ муддатдан келган фуқаролар'),'url'=>array('uzoq_muddat/mfy','mfy'=>isset(Yii::app()->user->mfy_id)? Yii::app()->user->mfy_id:0),'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('strings','ДЭО аъзолари'),'url'=>array('deo/mfy','mfy'=>isset(Yii::app()->user->mfy_id)? Yii::app()->user->mfy_id:0),'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('strings','Уюшмаган ёшлар'),'url'=>array('uyushmagan_yoshlar/mfy','mfy'=>isset(Yii::app()->user->mfy_id)? Yii::app()->user->mfy_id:0),'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('strings','Жиноят содир этган аёллар'),'url'=>array('jinoyatchi_ayollar/mfy','mfy'=>isset(Yii::app()->user->mfy_id)? Yii::app()->user->mfy_id:0),'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('strings','Ёлғиз пенсионерлар'),'url'=>array('yolgiz_pensioner/mfy','mfy'=>isset(Yii::app()->user->mfy_id)? Yii::app()->user->mfy_id:0),'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('strings','Нотинч оилалар'),'url'=>array('notinch_oilalar/mfy','mfy'=>isset(Yii::app()->user->mfy_id)? Yii::app()->user->mfy_id:0),'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('strings','Кадастр хужжати йўқ хонадонлар'),'url'=>array('kadastr/mfy','mfy'=>isset(Yii::app()->user->mfy_id)? Yii::app()->user->mfy_id:0),'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>Yii::t('strings',"Электр,сув ва табиий газдан қарздор хонадонлар"),'url'=>array('qarzdorlik/mfy','mfy'=>isset(Yii::app()->user->mfy_id)? Yii::app()->user->mfy_id:0),'visible'=>!Yii::app()->user->isGuest),
                ),
                    'visible'=>isset(Yii::app()->user->role)? Yii::app()->user->role=='2':''),
                array('label'=>Yii::t('strings','Фуқаролар'), 'url'=>array('/fuqaro'),'visible'=>isset(Yii::app()->user->role)? Yii::app()->user->role=='2':''),
                array('label'=>Yii::t('strings','Хонадон киритиш'), 'url'=>array('/fuqaro_uy/create'),'visible'=>isset(Yii::app()->user->role)? Yii::app()->user->role=='2':''),
                array('label'=>Yii::t('strings','Муаммо'),
                    'items'=>array(
                        array('label'=>Yii::t('strings','Муаммони киритиш'),'url'=>array('/fuqaro_Muammolar/create_kiritish'),'visible'=>isset(Yii::app()->user->role)? Yii::app()->user->role=='2':''),
                        array('label'=>Yii::t('strings','Киритилган муаммолар'),'url'=>array('/fuqaro_Muammolar/index','mfy_id'=>isset(Yii::app()->user->mfy_id)? Yii::app()->user->mfy_id:0),'visible'=>isset(Yii::app()->user->role)? Yii::app()->user->role=='2':''),
                        array('label'=>Yii::t('strings','Хал этилган муаммолар'),'url'=>array('/fuqaro_Muammolar/index','mfy_idy'=>isset(Yii::app()->user->mfy_id)? Yii::app()->user->mfy_id:0),'visible'=>isset(Yii::app()->user->role)? Yii::app()->user->role=='2':''),
                        array('label'=>Yii::t('strings','Хал этилмаган муаммолар'),'url'=>array('/fuqaro_Muammolar/index','mfy_idx'=>isset(Yii::app()->user->mfy_id)? Yii::app()->user->mfy_id:0),'visible'=>isset(Yii::app()->user->role)? Yii::app()->user->role=='2':''),
                        array('label'=>Yii::t('strings','Хисобот'),'url'=>array('/fuqaro_Muammolar/xisobot'),'visible'=>isset(Yii::app()->user->role)? Yii::app()->user->role=='2':'')
                    ),
                    'url'=>'#','visible'=>isset(Yii::app()->user->role)? Yii::app()->user->role=='2':''),
                array('label'=>Yii::t('strings','Ташкилот'), 'url'=>array("/tashkilot_users/index", 'tashkilot_id'=>isset(Yii::app()->user->tashkilot_id)? Yii::app()->user->tashkilot_id:0),'visible'=>isset(Yii::app()->user->role)? Yii::app()->user->role=='4':''),
                array('label'=>Yii::t('strings','Муаммони хал этиш'), 'url'=>array("/Muammo/yechim" ),'visible'=>isset(Yii::app()->user->role)? Yii::app()->user->role=='0':''),
                array('label'=>Yii::t('strings','Моддий ёрдам'), 'url'=>array("/muammo_turlari_yordam/index?tuman=29"),'visible'=>isset(Yii::app()->user->role)? Yii::app()->user->role=='4':''),
                #array('label'=>Yii::t('strings','About'), 'url'=>array('/site/page', 'view'=>'about')),
                #array('label'=>Yii::t('strings','Contact'), 'url'=>array('/site/contact')),
                array('label'=>Yii::t('strings','Login'), 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                #array('label'=>Yii::t('strings','Mobile'), 'url'=>((substr($_SERVER['HTTP_HOST'],0,4)==='www.')?("http://www.m.".substr($_SERVER['HTTP_HOST'],4)):("http://m.$_SERVER[HTTP_HOST]"))),
                array('label'=>Yii::t('strings','Logout').' ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
            ),
        ),
    ),
));
?>
<div class="container" id="page">
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>
	<?php echo $content; ?>
	<div class="clear">

	</div>
	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->
</div><!-- page -->
</body>
</html>
