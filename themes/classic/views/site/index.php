<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
    <?php
        if(isset(Yii::app()->user->role) and Yii::app()->user->role==0)
        {
            echo"admin uchun";
    ?>
            <h1><i><?php echo Yii::t('zii','Welcome to {appname}!', array('{appname}'=>"<i>".Yii::t('zii',CHtml::encode(Yii::app()->name))."</i>"));?></h1>

<h2>
    Хўжаобод тумани тўртта комплекс ижтимоий-иқтисодий ривожлантириш бўйича секторлар раҳбарлари хисоботи
</h2>
<!--  хонадонлар-->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
            'collapsed'=>false,
            'legend'=>Yii::t('strings','Хонадонлар рўйхати'),
            'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
        )); ?>

<form action="<?=Yii::app()->createUrl('/xonadonlar/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman15','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/xonadonlar/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor15','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman15')->setDependent('id_sektor15',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/xonadonlar/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy15','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor15')->setDependent('id_mfy15',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/xonadonlar/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha15','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy15')->setDependent('id_kocha15',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>

<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!--  фуқаролар-->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
            'collapsed'=>false,
            'legend'=>Yii::t('strings','Фуқаролар рўйхати'),
            'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
        )); ?>
<p>
<form action="<?=Yii::app()->createUrl('/fruyxat/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman16','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/fruyxat/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor16','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman16')->setDependent('id_sektor16',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/fruyxat/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy16','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor16')->setDependent('id_mfy16',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/fruyxat/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha16','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy16')->setDependent('id_kocha16',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- оғир касаллар-->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
            'collapsed'=>false,
            'legend'=>Yii::t('strings','Оғир касаллар'),
            'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
        )); ?>
<p>
    <form action="<?=Yii::app()->createUrl('/fuqaro_kasallar/index')?>" method="get">Туман кесимида:
        <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
        <input type="submit" value="Кўриш" class="btn btn-default">
    </form>
<form action="<?=Yii::app()->createUrl('/fuqaro_kasallar/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
    ECascadeDropDown::master('id_tuman')->setDependent('id_sektor',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/fuqaro_kasallar/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor')->setDependent('id_mfy',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/fuqaro_kasallar/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy')->setDependent('id_kocha',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->
<!-- ногиронлар-->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
            'collapsed'=>false,
            'legend'=>Yii::t('strings','Ногиронлар рўйхати'),
            'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
        )); ?>
<p>
<form action="<?=Yii::app()->createUrl('/nogiron/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman2','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/nogiron/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor2','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman2')->setDependent('id_sektor2',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/nogiron/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy2','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor2')->setDependent('id_mfy2',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/nogiron/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha2','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy2')->setDependent('id_kocha2',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- Вояга етмаганлар-->

<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
            'collapsed'=>false,
            'legend'=>Yii::t('strings','Вояга етмаганлар рўйхати'),
            'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
        )); ?>
<p>
<form action="<?=Yii::app()->createUrl('/yosh/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman4','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/yosh/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor4','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman4')->setDependent('id_sektor4',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/yosh/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy4','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor4')->setDependent('id_mfy4',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/yosh/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha4','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy4')->setDependent('id_kocha4',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
 </p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- Узоқ муддатга кетганлар-->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
            'collapsed'=>false,
            'legend'=>Yii::t('strings','Узоқ муддатга кетганлар рўйхати'),
            'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
        )); ?>
<p>
<form action="<?=Yii::app()->createUrl('/uzoq_muddat/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman3','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/uzoq_muddat/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor3','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman3')->setDependent('id_sektor3',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/uzoq_muddat/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy3','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor3')->setDependent('id_mfy3',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/uzoq_muddat/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha3','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy3')->setDependent('id_kocha3',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- ДЭО азолари  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
            'collapsed'=>false,
            'legend'=>Yii::t('strings','ДЭО аъзолари рўйхати'),
            'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
        )); ?>
<p>
<form action="<?=Yii::app()->createUrl('/deo/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman5','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/deo/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor5','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman5')->setDependent('id_sektor5',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/deo/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy5','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor5')->setDependent('id_mfy5',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/deo/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha5','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy5')->setDependent('id_kocha5',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>

</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->
<!-- Муаммолилар -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
            'collapsed'=>false,
            'legend'=>Yii::t('strings','Муаммоларни кўриш'),
            'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
        )); ?>
<p>
<form action="<?=Yii::app()->createUrl('/Muammo/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman6','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/Muammo/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor6','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman6')->setDependent('id_sektor6',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/Muammo/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy6','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor6')->setDependent('id_mfy6',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/Muammo/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha6','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy6')->setDependent('id_kocha6',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>

</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- ДЭО фарзандлари азолари  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
            'collapsed'=>false,
            'legend'=>Yii::t('strings','ДЭО фарзандлари рўйхати'),
            'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
        )); ?>
<p>
<form action="<?=Yii::app()->createUrl('/deo_farzandlari/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman7','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/deo_farzandlari/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor7','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman7')->setDependent('id_sektor7',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/deo_farzandlari/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy7','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor7')->setDependent('id_mfy7',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/deo_farzandlari/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha7','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy7')->setDependent('id_kocha7',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>

</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- Уюшмаган ёшлар рўйхати  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
            'collapsed'=>false,
            'legend'=>Yii::t('strings','Уюшмаган ёшлар рўйхати'),
            'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
        )); ?>
<p>
<form action="<?=Yii::app()->createUrl('/uyushmagan_yoshlar/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman8','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/uyushmagan_yoshlar/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor8','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman8')->setDependent('id_sektor8',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/uyushmagan_yoshlar/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy8','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor8')->setDependent('id_mfy8',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/uyushmagan_yoshlar/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha8','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy8')->setDependent('id_kocha8',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>

</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->


<!-- Жиноят содир этган аёллар  рўйхати  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
            'collapsed'=>false,
            'legend'=>Yii::t('strings','Жиноят содир этган аёллар рўйхати'),
            'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
        )); ?>
<p>
<form action="<?=Yii::app()->createUrl('/jinoyatchi_ayollar/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman9','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/jinoyatchi_ayollar/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor9','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman9')->setDependent('id_sektor9',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/jinoyatchi_ayollar/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy9','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor9')->setDependent('id_mfy9',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/jinoyatchi_ayollar/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha9','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy9')->setDependent('id_kocha9',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>

</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- Ёлғиз пенсионерлар рўйхати  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
            'collapsed'=>false,
            'legend'=>Yii::t('strings','Ёлғиз пенсионерлар рўйхати'),
            'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
        )); ?>
<p>
<form action="<?=Yii::app()->createUrl('/yolgiz_pensioner/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman10','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/yolgiz_pensioner/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor10','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman10')->setDependent('id_sektor10',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/yolgiz_pensioner/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy10','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor10')->setDependent('id_mfy10',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/yolgiz_pensioner/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha10','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy10')->setDependent('id_kocha10',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- нотинч оилалар рўйхати  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
            'collapsed'=>false,
            'legend'=>Yii::t('strings','Нотинч оилалар рўйхати'),
            'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
        )); ?>
<p>
<form action="<?=Yii::app()->createUrl('/notinch_oilalar/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman12','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/notinch_oilalar/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor12','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman12')->setDependent('id_sektor12',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/notinch_oilalar/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy12','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor12')->setDependent('id_mfy12',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/notinch_oilalar/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha12','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy12')->setDependent('id_kocha12',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- кадастр йўқ оилалар рўйхати  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
            'collapsed'=>false,
            'legend'=>Yii::t('strings','Кадастр йўқ оилалар рўйхати'),
            'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
        )); ?>
<p>
<form action="<?=Yii::app()->createUrl('/kadastr/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman13','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/kadastr/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor13','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman13')->setDependent('id_sektor13',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/kadastr/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy13','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor13')->setDependent('id_mfy13',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/kadastr/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha13','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy13')->setDependent('id_kocha13',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- Газ,сув ва электр энергиясидан карздор хонадонлар рўйхати  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
            'collapsed'=>false,
            'legend'=>Yii::t('strings','Газ,сув ва электр энергиясидан карздор хонадонлар рўйхати'),
            'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
        )); ?>
<p>
<form action="<?=Yii::app()->createUrl('/qarzdorlik/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman14','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/qarzdorlik/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor14','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman14')->setDependent('id_sektor14',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/qarzdorlik/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy14','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor14')->setDependent('id_mfy14',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/qarzdorlik/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha14','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy14')->setDependent('id_kocha14',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->
<?php
        }
        elseif(isset(Yii::app()->user->role) and Yii::app()->user->role==2)
        {
           #kotiba uchun
            echo"kotiba uchun";
        ?>
            <h1><b><?php echo Yii::t('zii','Welcome to {appname}!', array('{appname}'=>"<b>".Yii::t('zii',CHtml::encode(Yii::app()->name))."</b>"));?></b></h1>
        <blockquote id="blockquotei">
            <a href="/fuqaro_Uy/index?mfy_id=<?php echo Yii::app()->user->mfy_id;?>">
                <button class="btn btn-info btn-small">
                    <i class="icon-list-alt icon-white"></i>
                    Киритилган аризалар
                </button>
            </a>
            <small>
                МФЙ томонидан киритилган аризалар.
            </small>
        </blockquote>
<ul class="thumbnails">
                <?php
                $mfy_r=Yii::app()->user->mfy_id;
                $ariza_k = fuqaro_Muammolar::model()->findAll("mfy_id = $mfy_r and xal_etilgan_sana is null order by qayd_sana asc limit 6");
                foreach($ariza_k as $valu)
                {
                 ?>
    <li class='span4'>
        <a class="thumbnail" href="/fuqaro_Uy/index?mfy_id=<?php echo Yii::app()->user->mfy_id;?>">
         <div class="panel panel-primary">
            <div class="panel-heading">
                Фуқаро <?php echo($valu->fuqaro->fio); ?>нинг аризаси
            </div>
            <div class="panel-body">
                    <b>Манзил:</b>
                <i>
     <?php echo($valu->tuman->tuman_nomi);echo', ';
       echo($valu->mfy->mfy_nomi);echo', ';
       echo($valu->kocha->tbl_kocha_nomi);echo'.<br>';
      ?>
                </i>
<hr>
                          <b>Аризанинг мазмуни: </b>
                <?php
                echo($valu->muammo_mazmuni);echo'<br>';
                            ?>
   <hr>
                   <b>Ариза тушган сана: </b><i><?php echo($valu->qayd_sana);?></i>
                        <br><b>Фойдаланувчи: </b><i><?php echo($valu->user->username);?></i>

            </div>
         </div>
        </a>
    </li>
            <?php }   ?>
</ul>
        <blockquote id="blockquote">
            <a href="/fuqaro_Muammolar/index?mfy_idy=<?php echo Yii::app()->user->mfy_id;?>">
                <button class="btn btn-success btn-small">
                    <i class="icon-list-alt icon-white"></i>
                    Киритилган аризаларга келган жавоблар
                </button>
            </a>
            <small>
                МФЙ томонидан киритилган аризаларга келган жавоблар.
            </small>
        </blockquote>
<ul class="thumbnails">
            <?php
            $ariza_y = fuqaro_Muammolar::model()->findAll("mfy_id = $mfy_r and xal_etilgan_sana is not null");
            foreach($ariza_y as $valuy)
            {
                ?>
    <li class='span6'>
        <a class="thumbnail">
        <div class="panel panel-success">
            <div class="panel-heading">
                <?php echo($valuy->fuqaro->fio); ?>
            </div>
                <div class="panel-body">
                    <table border="1" width="100%" height="68">
                        <tr>
                            <td height="68" width="309">
                                Манзил:
                                <?php echo($valuy->tuman->tuman_nomi);echo' ';
                                echo($valuy->mfy->mfy_nomi);echo' ';
                                echo($valuy->kocha->tbl_kocha_nomi);echo'<br>';
                                ?>
                            </td>
                            <td height="68" width="309">
                                <?php if(isset($valuy->tashkilot->tnomi)){echo($valuy->tashkilot->tnomi);} ?>
                            </td>
                        </tr>
                        <tr>
                            <td height="68" width="309">
                                Аризанинг мазмуни:<?php
                                echo($valu->muammo_mazmuni);echo'<br>';
                                ?>

                            </td>
                            <td height="68" width="309">
                               Жавоб хати: <?php  echo($valuy->xulosa)?>
                            </td>
                        </tr>
                        <tr>
                            <td height="68" width="309">
                                Ариза тушган сана:<?php echo($valu->qayd_sana); ?>
                                Фойдаланувчи:<?php echo($valu->user->username);?>
                            </td>
                            <td height="68" width="309">
Ариза кўрилган сана:<?php echo ($valuy->korilgan_sana); echo'<bR>'; ?>
Аризанинг муддати:<?php echo ($valuy->muddat_sana);echo'<bR>';?>
Ариза хал этилган сана:<?php echo ($valuy->xal_etilgan_sana);echo'<bR>';?>
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
            </a>
    </li>
                <?php }   ?>
</ul>
        <blockquote id="blockquoted">
            <a href="/fuqaro_Muammolar/index?mfy_idy=<?php echo Yii::app()->user->mfy_id;?>">
                <button class="btn btn-danger btn-small">
                    <i class="icon-list-alt icon-white"></i>
                    Бажарилиш муддати 3 кун қолган аризалар.
                </button>
            </a>
            <small>
                Бажарилиш муддати 3 кун қолган аризалар
            </small>
        </blockquote>
<ul class="thumbnails">
    <?php

    #$ariza_3 = fuqaro_Muammolar::model()->findAll("mfy_id = $mfy_r and muddat_sana > '".date('Y-m-d')."'and muddat_sana <='".date('Y-m-d,time()+4*24*3600')."' ");
    $ariza_3 = fuqaro_Muammolar::model()->findAll("mfy_id = $mfy_r and muddat_sana < '".date('Y-m-d')."'");
    foreach($ariza_3 as $valu3)
    {
        ?>
    <li class='span3'>
        <a class="thumbnail">
            <div class="panel panel-danger">
                <div class="panel-heading"><?php echo $valu3->fuqaro->fio ?></div>
                <div class="panel-body">Манзил:<?php echo $valu3->tuman->tuman_nomi ?>
                    <?php echo $valu3->mfy->mfy_nomi ?><?php echo $valu3->kocha->tbl_kocha_nomi ?>
                    <br>Муаммо мазмуни:<?php echo $valu3->muammo_mazmuni; ?>
                    <br>Муддати сана:<?php echo $valu3->muddat_sana; ?>
                </div>
            </div>
            </a>
    </li>
                <?php }   ?>
   </ul>
        <blockquote id="blockquotew">
            <a href="/fuqaro_Muammolar/index?mfy_idy=<?php echo Yii::app()->user->mfy_id;?>">
                <button class="btn btn-warning btn-small">
                    <i class="icon-list-alt icon-white"></i>
                    Муддати тугаган аризалар
                </button>
            </a>
            <small>
                Муддати тугаган аризалар
            </small>
        </blockquote>
<ul class="thumbnails">
    <?php
    $ariza_t = fuqaro_Muammolar::model()->findAll("mfy_id = $mfy_r and muddat_sana < '".date('Y-m-d')."'");
    foreach($ariza_t as $valut)
    {
        ?>
    <li class='span4'>
        <a class="thumbnail">
            <div class="panel panel-warning">
                <div class="panel-heading"><?php echo $valut->fuqaro->fio; ?></div>
                <div class="panel-body">
<?php echo $valut->tuman->tuman_nomi; ?>
<?php echo $valut->mfy->mfy_nomi; ?>
<?php echo $valut->kocha->tbl_kocha_nomi; ?>
<?php echo "<br>".$valut->muammo_mazmuni; ?>
<?php echo "<br>".$valut->qayd_sana; ?>
<?php echo "<br>".$valut->muddat_sana; ?>
<?php echo "<br>".$valut->korilgan_sana; ?>
<?php echo "<br>".$valut->xal_etuvchi_fio; ?>
                </div>
            </div>
            </a>
    </li>
   <?php }   ?>
</ul>
        <blockquote id="blockquotep">
            <a href="/fuqaro_Muammolar/index?mfy_idy=<?php echo Yii::app()->user->mfy_id;?>">
                <button class="btn btn-primary btn-small">
                    <i class="icon-list-alt icon-white"></i>
                    Маълумотлари ўзгартирилган хонадонлар
                </button>
            </a>
            <small>
                Маълумотлари ўзгартирилган хонадонлар
            </small>
        </blockquote>
<ul class="thumbnails">
    <li class='span3'>
        <a class="thumbnail">
            <div class="panel panel-info">
                <div class="panel-heading">Маълумотлари ўзгартирилган хонадонлар</div>
                <div class="panel-body"> … </div>
            </div>
            </a>
    </li>
</ul>
        <blockquote id="blockquotep">
            <a href="/fuqaro_Muammolar/index?mfy_idy=<?php echo Yii::app()->user->mfy_id;?>">
                <button class="btn btn-primary btn-small">
                    <i class="icon-list-alt icon-white"></i>
                    Маълумотлари ўзгартирилган фуқаролар
                </button>
            </a>
            <small>
                <?php echo CHtml::Link('<i class="icon-info-sign"></i>', null, array(
                'rel' => 'popover',
                'data-trigger' => 'hover',
                'data-title' => 'Your title',
                'data-content' => 'Your content',
            ))?>
                <?php Yii::app()->clientScript->registerScript("", "$('.ipopover').popover();", CClientScript::POS_READY) ?>
                <?php echo CHtml::Link('<i class="icon-info-sign"></i>', null, array(
                'class' => 'ipopover',
                'data-trigger' => 'hover',
                'data-title' => 'Your title',
                'data-content' => 'Your content',
            ))?>
            </small>
        </blockquote>
<ul class="thumbnails">
    <li class='span3'>
        <a class="thumbnail">
            <div class="panel panel-info">
                <div class="panel-heading">Маълумотлари ўзгартирилган фуқаролар</div>
                <div class="panel-body">

                    dfd

                </div>
            </div>
            </a>
    </li>
</ul>
        <blockquote id="blockquotew">
            <a href="/fuqaro_Muammolar/index?mfy_idy=<?php echo Yii::app()->user->mfy_id;?>">
                <button class="btn btn-warning btn-small">
                    <i class="icon-list-alt icon-white"></i>
                    Вояга етмаган ёшлар
                </button>
            </a>
            <small>
                Вояга етмаган ёшлар
            </small>
        </blockquote>
<ul class="thumbnails">
    <?php
    $fmfy=Fuqaro::model()->findAll("fuqaro_uy_id in
        (
            select id from tbl_fuqaro_uy where mfy_id =$mfy_r
        )
        and tugilgan_sanasi>'".(date("Y")-18)."-".date("m")."-".date("d")."'and (passport_raqami='' or passport_raqami is null)LIMIT 8 ");
    foreach($fmfy as $valuf)
    {
        $fuqaro_m = Fuqaro_Uy::model()->find("id=$valuf->fuqaro_uy_id");
        ?>
    <li class='span3'>
        <a class="thumbnail" href="fuqaro/<?php echo $valuf->id ?>">
            <div class="panel panel-warning">
            <div class="panel-heading">Ф.И.О.:<?php echo($valuf->fio); ?></div>
            <div class="panel-body">

                <b>Манзил:</b><i><?php echo $fuqaro_m->ftuman->tuman_nomi; ?>,
                <?php echo $fuqaro_m->mfy->mfy_nomi; ?>,
                <?php echo $fuqaro_m->kochalar->tbl_kocha_nomi; ?>,
                уй рақами №<?php echo $fuqaro_m->uy_raqami; ?>
            </i>
            <br><b>Метирка рақами:</b><i><?php echo $valuf->metirka_raqami; ?>;</i>
                <br><b>Метирка ким томонидан берилган:</b><i>Хўжаобод туман ФХДЁ <?php #echo $valuf->metirka_kim_tomonidan; ?>;</i>
                <br><b>Маълумоти:</b><i> <?php #echo $valuf->malumoti->malumot_turi; ?>;</i>
            </div>
        </div>
        </a>
    </li>
        <?php } ?>
</ul>
        <?php
        }
        elseif(isset(Yii::app()->user->role) and Yii::app()->user->role==3)
        {
            #inspektor uchun
            echo"inspektor uchun";?>
        <h1><i><?php echo Yii::t('zii','Welcome to {appname}!', array('{appname}'=>"<i>".Yii::t('zii',CHtml::encode(Yii::app()->name))."</i>"));?></i></h1>

<h2>
    Хўжаобод тумани тўртта комплекс ижтимоий-иқтисодий ривожлантириш бўйича секторлар раҳбарлари хисоботи
</h2>
<!--  хонадонлар-->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Хонадонлар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>

<form action="<?=Yii::app()->createUrl('/xonadonlar/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman15','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/xonadonlar/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor15','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman15')->setDependent('id_sektor15',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/xonadonlar/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy15','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor15')->setDependent('id_mfy15',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/xonadonlar/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha15','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy15')->setDependent('id_kocha15',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>

<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!--  фуқаролар-->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Фуқаролар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/fruyxat/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman16','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/fruyxat/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor16','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman16')->setDependent('id_sektor16',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/fruyxat/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy16','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor16')->setDependent('id_mfy16',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/fruyxat/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha16','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy16')->setDependent('id_kocha16',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- оғир касаллар-->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Оғир касаллар'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/fuqaro_kasallar/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/fuqaro_kasallar/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman')->setDependent('id_sektor',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/fuqaro_kasallar/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor')->setDependent('id_mfy',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/fuqaro_kasallar/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy')->setDependent('id_kocha',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->
<!-- ногиронлар-->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Ногиронлар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/nogiron/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman2','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/nogiron/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor2','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman2')->setDependent('id_sektor2',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/nogiron/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy2','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor2')->setDependent('id_mfy2',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/nogiron/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha2','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy2')->setDependent('id_kocha2',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- Вояга етмаганлар-->

<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Вояга етмаганлар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/yosh/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman4','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/yosh/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor4','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman4')->setDependent('id_sektor4',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/yosh/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy4','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor4')->setDependent('id_mfy4',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/yosh/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha4','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy4')->setDependent('id_kocha4',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- Узоқ муддатга кетганлар-->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Узоқ муддатга кетганлар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/uzoq_muddat/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman3','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/uzoq_muddat/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor3','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman3')->setDependent('id_sektor3',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/uzoq_muddat/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy3','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor3')->setDependent('id_mfy3',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/uzoq_muddat/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha3','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy3')->setDependent('id_kocha3',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- ДЭО азолари  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','ДЭО аъзолари рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/deo/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman5','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/deo/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor5','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman5')->setDependent('id_sektor5',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/deo/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy5','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor5')->setDependent('id_mfy5',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/deo/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha5','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy5')->setDependent('id_kocha5',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>

</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

            <!-- ДЭО фарзандлари азолари  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','ДЭО фарзандлари рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/deo_farzandlari/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman7','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/deo_farzandlari/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor7','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman7')->setDependent('id_sektor7',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/deo_farzandlari/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy7','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor7')->setDependent('id_mfy7',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/deo_farzandlari/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha7','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy7')->setDependent('id_kocha7',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>

</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- Уюшмаган ёшлар рўйхати  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Уюшмаган ёшлар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/uyushmagan_yoshlar/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman8','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/uyushmagan_yoshlar/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor8','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman8')->setDependent('id_sektor8',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/uyushmagan_yoshlar/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy8','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor8')->setDependent('id_mfy8',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/uyushmagan_yoshlar/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha8','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy8')->setDependent('id_kocha8',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>

</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->


<!-- Жиноят содир этган аёллар  рўйхати  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Жиноят содир этган аёллар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/jinoyatchi_ayollar/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman9','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/jinoyatchi_ayollar/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor9','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman9')->setDependent('id_sektor9',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/jinoyatchi_ayollar/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy9','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor9')->setDependent('id_mfy9',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>

    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/jinoyatchi_ayollar/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha9','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy9')->setDependent('id_kocha9',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>

</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- Ёлғиз пенсионерлар рўйхати  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Ёлғиз пенсионерлар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/yolgiz_pensioner/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman10','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/yolgiz_pensioner/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor10','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman10')->setDependent('id_sektor10',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/yolgiz_pensioner/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy10','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor10')->setDependent('id_mfy10',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/yolgiz_pensioner/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha10','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy10')->setDependent('id_kocha10',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<!-- нотинч оилалар рўйхати  -->
<?php $this->beginWidget('ext.coolfieldset.JCollapsibleFieldset', array(
    'collapsed'=>false,
    'legend'=>Yii::t('strings','Нотинч оилалар рўйхати'),
    'legendHtmlOptions'=>array('title'=>Yii::t('strings','Сичқончани чап тугмасини босинг'))
)); ?>
<p>
<form action="<?=Yii::app()->createUrl('/notinch_oilalar/index')?>" method="get">Туман кесимида:
    <?php echo CHtml::DropDownList('tuman','',CHtml::listData(Tuman::model()->findAll('viloyat_id=2'),'id','tuman_nomi'),array('id'=>'id_tuman12','prompt'=>Yii::t("strings",'Туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/notinch_oilalar/sektor')?>" method="get">Сектор кесимида:
    <?php echo CHtml::DropDownList('sektor','2',array(),array('id'=>'id_sektor12','prompt'=>Yii::t("strings",'Аввал туманни танланг')))  ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<?php
ECascadeDropDown::master('id_tuman12')->setDependent('id_sektor12',
    array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadsektor');
?>
<form action="<?=Yii::app()->createUrl('/notinch_oilalar/mfy')?>" method="get">МФЙ кесимида:
    <?php echo CHtml::DropDownList('mfy','0',array(),array('id'=>'id_mfy12','prompt'=>Yii::t("strings",'МФЙ танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_sektor12')->setDependent('id_mfy12',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadmfy');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
<form action="<?=Yii::app()->createUrl('/notinch_oilalar/kocha')?>" method="get">Кўчалар кесимида:
    <?php echo CHtml::DropDownList('kocha','0',array(),array('id'=>'id_kocha12','promt'=>Yii::t("strings",'Кўчани танланг')))  ?>
    <?php
    ECascadeDropDown::master('id_mfy12')->setDependent('id_kocha12',
        array('dependentLoadingLabel'=>'yuklanyapti'),'fuqaro_kasallar/loadkocha');
    ?>
    <input type="submit" value="Кўриш" class="btn btn-default">
</form>
</p>
<?php $this->endWidget('ext.coolfieldset.JCollapsibleFieldset'); ?><!-- collabsible fieldset -->

<?php
        }
        elseif(isset(Yii::app()->user->role) and Yii::app()->user->role==4)
        {
            echo"tashkilot uchun";
        }
       ?>

