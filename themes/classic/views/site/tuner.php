<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
$this->breadcrumbs=array(
    Yii::t('strings','Созлаш'),
);
?>
<blockquote id="blockquotede">
    <a href="/mfy/index?val_mfy=<?php echo Yii::app()->user->tuman;?>">
        <button class="btn btn-default btn-small">
            <i class="icon-list-alt icon-black"></i>
            МФЙ қўшиш
        </button>
    </a>
    <small>
<?php $m=Yii::app()->user->viloyat;
    foreach ($vil as $v)
    {
        echo $v->viloyat_nomi[$m];
    }
$mahalla = Mfy::model()->findAll('tuman_id ='.Yii::app()->user->tuman .' order by user_date desc limit 10');
        ?>жойлашган махалла фуқаро йиғинларини умумий рўйхати.
        </small>
</blockquote>
<table class="table table-striped table-bordered table-condensed">
    <thead></thead>
    <tbody>
    <tr>
        <td>№</td>
        <td>МФЙ номи</td>
        <td>Туман номи</td>
        <td>Кўшилган сана</td>
    </tr>
    <?php $m0=1; foreach ($mahalla as $m )
    {
    ?>
    <tr>
        <td><?php echo $m0 ?></td>
        <td><?php if(isset($m->mfy_nomi)) echo CHtml::link(CHtml::encode($m->mfy_nomi),array("kochalar/index?val=$m->id"))?></td>
        <td><?php if(isset($m->tuman->tuman_nomi)) echo $m->tuman->tuman_nomi ?></td>
        <td><?php if(isset($m->user_date)) echo $m->user_date ?></td>
    </tr>
    <?php $m0=$m0+1;} ?>
    </tbody>
</table>
<blockquote id="blockquotede">
    <a href="/kochalar/kocha?val_mfy=<?php echo Yii::app()->user->tuman;?>">
        <button class="btn btn-default btn-small">
            <i class="icon-road icon-black"></i>
            Кўчалар қўшиш
        </button>
    </a>
    <small>
        <?php $m=Yii::app()->user->viloyat;
        foreach ($vil as $v)
        {
            echo $v->viloyat_nomi[$m];
        }
        $kocha = Kochalar::model()->findAll("tuman_id =".Yii::app()->user->tuman." order by user_date desc limit 10")
        ?>жойлашган махалла фуқаро йиғинларига кўчалар қўшиш.
    </small>
</blockquote>

<table class="table table-striped table-bordered table-condensed">
    <thead></thead>
    <tbody>
    <tr>
        <td>№</td>
        <td>Кўча номи</td>
        <td>МФЙ номи</td>
        <td>Кўшилган сана</td>
    </tr>
   <?php $k0=1; foreach($kocha as $k)
    {
   ?>
    <tr>
        <td><?php echo $k0; ?></td>
        <td><?php if(isset($k->tbl_kocha_nomi)) echo ($k->tbl_kocha_nomi) ?></td>
        <td><?php if(isset($k->tbl_mfy_id)) echo $k->tblMfy->mfy_nomi ?></td>
        <td><?php if(isset($k->user_date)) echo $k->user_date ?></td>
    </tr>
    <?php $k0=$k0+1;}  ?>
    </tbody>
</table>

<blockquote id="blockquotede">
    <a href="/mfyxodimlar/index?tuman_id=<?php echo Yii::app()->user->tuman;?>">
        <button class="btn btn-default btn-small">
            <i class="icon-user icon-black"></i>
            МФЙ ходимлари
        </button>
    </a>
    <small>
        <?php $m=Yii::app()->user->viloyat;
        foreach ($vil as $v)
        {
            echo $v->viloyat_nomi[$m];
        }
        $mxodimlar = MfyXodimlar::model()->findAll("tuman_id =".Yii::app()->user->tuman." order by user_date desc limit 10")
        ?> Махалла фуқаролар йиғинлари ходимларини киритиш,тахрирлаш,ўчириш.
    </small>
</blockquote>

<table class="table table-striped table-bordered table-condensed">
    <thead></thead>
    <tbody>
    <tr>
        <td>№</td>
        <td>МФЙ номи</td>
        <td>МФЙ котиби/котибасининг Ф.И.О</td>
        <td>Телефон рақами</td>
        <td>Охирги марта кирган санаси</td>
    </tr>
    <?php $xodim0=1; foreach($mxodimlar as $xodim)
    {
    ?>
    <tr>
        <td><?php echo $xodim0 ?></td>
        <td><?php if(isset($xodim->mfy_id)) echo $xodim->mfy->mfy_nomi ?></td>
        <td><?php if(isset($xodim->mfy_rais)) echo $xodim->mfy_rais ?></td>
        <td><?php if(isset($xodim->mfy_rais_tel)) echo $xodim->mfy_rais_tel?></td>
        <td><?php if(isset($xodim->user->user_date)) echo $xodim->user->user_date?></td>
    </tr>
    <?php $xodim0=$xodim0+1;} ?>
    </tbody>
</table>

<blockquote id="blockquotede">
    <a href="/user/index?tuman_id=<?php echo Yii::app()->user->tuman;?>">
        <button class="btn btn-default btn-small">
            <i class="icon-user icon-black"></i>
            МФЙ фойдаланувчилари
        </button>
    </a>
    <small>
        <?php $m=Yii::app()->user->viloyat;
        foreach ($vil as $v)
        {
            echo $v->viloyat_nomi[$m];
        }
       $muser=User::model()->findAll("tuman=".Yii::app()->user->tuman." and role =2 order by user_date desc limit 10");

        ?> Махалла фуқаролар йиғинлари ходимлари учун ЛОГИН ва ПАРОЛ бериш.
    </small>
</blockquote>

<table class="table table-striped table-bordered table-condensed">
    <thead></thead>
    <tbody>
    <tr>
        <td>№</td>
        <td>МФЙ номи</td>
        <td>МФЙ котиби/котибасининг Ф.И.О</td>
        <td>Логин</td>
        <td>Охирги марта кирган санаси</td>
    </tr>
    <?php $muser0=1;foreach($muser as $user){ ?>
    <tr>
        <td><?php echo $muser0; ?></td>
        <td><?php if(isset($user->mfy_id)) echo $user->mfy->mfy_nomi?></td>
        <td><?php if(isset($user->mfy_id)) $mfio = MfyXodimlar::model()->find("mfy_id=".$user->mfy_id); if(isset($mfio->mfy_rais)) echo $mfio->mfy_rais; ?></td>
        <td><?php if(isset($user->username)) echo $user->username;?></td>
        <td>сана</td>
    </tr>
        <?php $muser0=$muser0+1;}?>
    </tbody>
</table>
<blockquote id="blockquotede">
    <a href="/rahbarlar/index?val_mfy=<?php echo Yii::app()->user->tuman;?>">
        <button class="btn btn-default btn-small">
            <i class="icon-user icon-black"></i>
            Сеткор рахбарлари
        </button>
    </a>
    <small>
        <?php $m=Yii::app()->user->viloyat;
        foreach ($vil as $v)
        {
            echo $v->viloyat_nomi[$m];
        }
        $rahbar = Rahbarlar::model()->findAll("tuman_id =".Yii::app()->user->tuman);

        ?> Сектор рахбарларини киритиш/тахрирлаш/ўчириш.
    </small>
</blockquote>
<table class="table table-striped table-bordered table-condensed">
    <thead></thead>
    <tbody>
    <tr>
        <td>№</td>
        <td>Ташкилот номи</td>
        <td>Ташкилот рахбари Ф.И.О</td>
        <td>Масъул сектор рақами</td>
        <td>Телефон рақами</td>
    </tr>
    <?php $r0=1;foreach($rahbar as $boshliq){ ?>
    <tr>
        <td><?php echo $r0; ?></td>
        <td><?php if(isset($boshliq->ish_joy_nomi)) echo $boshliq->ish_joy_nomi;?></td>
        <td><?php if(isset($boshliq->r_fio))echo $boshliq->r_fio;?></td>
        <td><?php if(isset($boshliq->sektor)) echo $boshliq->sektor?> - сектор</td>
        <td>тел рақам</td>
    </tr>
    <?php $r0=$r0+1; }?>
    </tbody>
</table>

<blockquote id="blockquotede">
    <a href="/sektor/sector?tuman_id=<?php echo Yii::app()->user->tuman;?>">
        <button class="btn btn-default btn-small">
            <i class="icon-th-large icon-black"></i>
            Сектор хосил килиш
        </button>
    </a>
    <small>
        <?php $m=Yii::app()->user->viloyat;
        foreach ($vil as $v)
        {
            echo $v->viloyat_nomi[$m];
        }
        $sektorlar = Sektor::model()->findAll("sektor_tuman_id =".Yii::app()->user->tuman." group by sektor_raqami");
        ?>Секторлар хосил қилиш ва уларга маъсулларни бириктириш.
    </small>
</blockquote>

<table class="table table-striped table-bordered table-condensed">
    <thead></thead>
    <tbody>
    <tr>
        <td>№</td>
        <td>Сектор рақами</td>
        <td>Маъсул шахс Ф.И.О.</td>
        <td>Бириктирилган шахснинг иш жойи</td>
        <td>Телефон рақами</td>
    </tr>
    <?php $s0=1; foreach($sektorlar as $sektor){ ?>
    <tr>
        <td><?php echo $s0;?></td>
        <td><?php if(isset($sektor->sektor_raqami)) echo $sektor->sektor_raqami;?> - сектор</td>
        <td><?php if(isset($sektor->sektor_rahbari)) $rio= Rahbarlar::model()->find("id =".$sektor->sektor_rahbari); if(isset($rio->r_fio)) echo $rio->r_fio;?></td>
        <td><?php if(isset($sektor->sektor_rahbari)) $rio= Rahbarlar::model()->find("id =".$sektor->sektor_rahbari); if(isset($rio->ish_joy_nomi)) echo $rio->r_fio;?></td>
        <td>+99874-ххх-чч-сс</td>
    </tr>
      <?php $s0=$s0+1;}?>
    </tbody>
</table>
<blockquote id="blockquotede">
    <a href="/mfy/index?val_mfy=<?php echo Yii::app()->user->tuman;?>">
        <button class="btn btn-default btn-small">
            <i class="icon-user icon-black"></i>
            Сектор фойдаланувчилари
        </button>
    </a>
    <small>
        <?php $m=Yii::app()->user->viloyat;
        foreach ($vil as $v)
        {
            echo $v->viloyat_nomi[$m];
        }
        $suser = User::model()->findAll("tuman =".Yii::app()->user->tuman." and role =5");
        ?> Сектор рахбар ходимлари учун ЛОГИН ва ПАРОЛ бериш.
    </small>
</blockquote>

<table class="table table-striped table-bordered table-condensed">
    <thead></thead>
    <tbody>
    <tr>
        <td>№</td>
        <td>Сектор рақами</td>
        <td>Ташкилот номи</td>
        <td>Ташкилот рахбари Ф.И.О</td>
        <td>Логин</td>
        <td>Охирги марта кирган санаси</td>
    </tr>
   <?php $su0=1; foreach($suser as $users){?>
    <tr>
        <td><?php echo $su0;?></td>
        <td>cx</td>
        <td>номи</td>
        <td>ФИО</td>
        <td><?php if(isset($users->username)) echo $users->username; ?></td>
        <td>сана</td>
    </tr>
    <?php $su0=1+$su0;}?>
    </tbody>
</table>

<blockquote id="blockquotede">
    <a href="/sektor/index_mfy?val_mfy=<?php echo Yii::app()->user->tuman;?>">
        <button class="btn btn-default btn-small">
            <i class="icon-th icon-black"></i>
            Секторга МФЙ бириктириш
        </button>
    </a>
    <small>
        <?php $m=Yii::app()->user->viloyat;
        foreach ($vil as $v)
        {
            echo $v->viloyat_nomi[$m];
        }
        $sektor_mfy =Sektor::model()->findAll("sektor_tuman_id=".Yii::app()->user->tuman)
        ?> Хосил қилинган Секторларга махалла фуқаролар йиғинларини бириктириш.
    </small>
</blockquote>
<table class="table table-striped table-bordered table-condensed">
    <thead></thead>
    <tbody>
    <tr>
        <td>№</td>
        <td>Сектор рақами</td>
        <td>Сектор рахбари Ф.И.О.</td>
        <td>Сектор рахбарининг иш жойи</td>
        <td>МФЙ номи</td>
        <td>МФЙ раис Ф.И.О.</td>
        <td>Телефон рақами</td>
    </tr>
   <?php $smfy=1; foreach($sektor_mfy as $mfys){?>
    <tr>
        <td><?php echo $smfy;?></td>
        <td><?php if(isset($mfys->sektor_raqami)) echo $mfys->sektor_raqami;?> - сектор</td>
        <td><?php if(isset($mfys->sektor_rahbari)) $srio = Rahbarlar::model()->find("id =".$mfys->sektor_rahbari); echo $srio->r_fio;?></td>
        <td><?php if(isset($mfys->sektor_rahbari)) $srio = Rahbarlar::model()->find("id =".$mfys->sektor_rahbari); echo $srio->ish_joy_nomi;?></td>
        <td><?php if(isset($mfys->sektor_mfy_id)) $s_mfy = Mfy::model()->find("id =".$mfys->sektor_mfy_id); echo $s_mfy->mfy_nomi;?></td>
        <td><?php if(isset($mfys->sektor_mfy_id)) $s_mfy = MfyXodimlar::model()->find("mfy_id =".$mfys->sektor_mfy_id); if(isset($s_mfy->mfy_rais)) echo $s_mfy->mfy_rais;?></td>
        <td>+99874-ххх-чч-сс</td>
    </tr>
    <?php $smfy=$smfy+1;} ?>
    </tbody>
</table>

<blockquote id="blockquotede">
    <a href="/ishchi_guruh_fio/index?guruh=<?php echo Yii::app()->user->tuman;?>">
        <button class="btn btn-default btn-small">
            <i class="icon-user icon-black"></i>
            Ташкилотлар
        </button>
    </a>
    <small>
        <?php $m=Yii::app()->user->viloyat;
        foreach ($vil as $v)
        {
            echo $v->viloyat_nomi[$m];
        }
        $ishchi = Ishchi_Guruh_Fio::model()->findAll("tuman_id =".Yii::app()->user->tuman);
        ?>даги ташкилотларни киритиш/тахрирлаш/ўчириш.
    </small>
</blockquote>
<table class="table table-striped table-bordered table-condensed">
    <thead></thead>
    <tbody>
    <tr>
        <td>№</td>
        <td>Сектор рақами</td>
        <td>Ташкилот номи</td>
        <td>Ташкилот рахбари Ф.И.О</td>
        <td>Логин</td>
        <td>Телефон рақами</td>
        <td>Охирги марта кирган санаси</td>
    </tr>
    <?php $igf0=1; foreach($ishchi as $guruh_fio){?>
    <tr>
        <td><?php echo $igf0;?></td>
        <td>1 - сектор</td>
        <td><?php if(isset($guruh_fio->tashkilot_nomi)) echo $guruh_fio->tashkilot_nomi;?></td>
        <td><?php if(isset($guruh_fio->ishchi_guruh_fio)) echo $guruh_fio->ishchi_guruh_fio;?></td>
        <td><?php if(isset($guruh_fio->id)) $user_s = User::model()->find("tasgkilot_id=".$guruh_fio->id." and role = 4"); if(isset($user_s->username)) echo $user_s->username;?></td>
        <td>тел рақами</td>
        <td>сана</td>
    </tr>
     <?php $igf0=$igf0+1;}?>
    </tbody>
</table>
<blockquote id="blockquotede">
    <a href="/user/tashkilot?tashkilot=<?php echo Yii::app()->user->tuman;?>">
        <button class="btn btn-default btn-small">
            <i class="icon-user icon-black"></i>
            Ташкилот фойдаланувчилари
        </button>
    </a>
    <small>
        <?php $m=Yii::app()->user->viloyat;
        foreach ($vil as $v)
        {
            echo $v->viloyat_nomi[$m];
        }
        $usert = User::model()->findAll("tuman =".Yii::app()->user->tuman." and role = 4")
        ?> Ташкилот рахбар ходимлари учун ЛОГИН ва ПАРОЛ бериш.
    </small>
</blockquote>
<table class="table table-striped table-bordered table-condensed">
    <thead></thead>
    <tbody>
    <tr>
        <td>№</td>
        <td>Сектор рақами</td>
        <td>Ташкилот номи</td>
        <td>Ташкилот рахбари Ф.И.О</td>
        <td>Логин</td>
        <td>Охирги марта кирган санаси</td>
    </tr>
    <?php $usert0=1; foreach($usert as $t_user){?>
    <tr>
        <td><?php echo $usert0; ?></td>
        <td><?php if(isset($t_user->tasgkilot_id)) echo 'sektor raqami';?></td>
        <td><?php if(isset($t_user->tasgkilot_id)) $tash = Ishchi_Guruh_Fio::model()->find("id=".$t_user->tasgkilot_id); if(isset($tash->ishchi_guruh_fio)) echo $tash->ishchi_guruh_fio;?></td>
        <td><?php if(isset($t_user->tasgkilot_id)) $tash = Ishchi_Guruh_Fio::model()->find("id=".$t_user->tasgkilot_id); if(isset($tash->tashkilot_nomi)) echo $tash->tashkilot_nomi;?></td>
        <td><?php if(isset($t_user->tasgkilot_id)) echo $t_user->username;?></td>
        <td>сана</td>
    </tr>
        <?php $usert0=1+$usert0; }?>
    </tbody>
</table>
